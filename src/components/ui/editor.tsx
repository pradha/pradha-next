import EditorJS, { OutputData } from "@editorjs/editorjs";
import DragDrop from 'editorjs-drag-drop';
import Header from '@editorjs/header'
import List from '@editorjs/list'
import Paragraph from '@editorjs/paragraph'
import Embed from '@editorjs/embed'
import Quote from '@editorjs/quote'
import Table from '@editorjs/table'
import CodeTool from '@editorjs/code'
import Underline from '@editorjs/underline'
import InlineCode from '@editorjs/inline-code'
import Warning from '@editorjs/warning'
import Delimiter from '@editorjs/delimiter'
import ImageTool from '@editorjs/image'
import ColorPlugin from 'editorjs-text-color-plugin'
import { useEffect, useRef } from 'react';

type Props = {
    data?: OutputData;
    onChange(val: OutputData): void;
    holder: string;
};
export const Editor = ({ data, onChange, holder }: Props) => {
    const ref = useRef<EditorJS>();
    useEffect(() => {
        if (!ref.current) {
            const editor = new EditorJS({
                holder: holder,
                placeholder: 'Let`s write an awesome story!',
                minHeight:0,
                tools: {
                    header: {
                        class: Header,
                        inlineToolbar: true,
                        config: {
                            placeholder: 'Enter a header',
                            levels: [2, 3, 4, 5],
                            defaultLevel: 2
                        }
                    },
                    paragraph: {
                        class: Paragraph,
                        inlineToolbar: true
                    },
                    Underline,
                    InlineCode,
                    list: {
                        class: List,
                        inlineToolbar: true
                    },
                    embed: {
                        class: Embed,
                        config: {
                            services: {
                                youtube: true,
                                facebook: true,
                                instagram: true,
                                tiktok: true
                            }
                        }
                    },
                    quote: {
                        class: Quote,
                        shortcut: 'CMD+SHIFT+Q',
                        config: {
                            quotePlaceHolder: "Enter a quote",
                            captionPlaceholder: "Quote's author"
                        }
                    },
                    table: {
                        class: Table,
                        config: {
                            rows: 2,
                            cols: 3
                        }
                    },
                    CodeTool,
                    warning: {
                        class: Warning,
                        inlineToolbar: true,
                        shortcut: 'CMD+SHIFT+W',
                        config: {
                            titlePlaceholder: 'Title',
                            messagePlaceholder: 'Message',
                        },
                    },
                    Delimiter,
                    image: {
                        class: ImageTool,
                        config: {
                            field: "file",
                            endpoints: {
                                accept: 'image/*',
                                byFile: '/api/content/image', // Your backend file uploader endpoint
                                // byUrl: 'http://localhost:8008/fetchUrl', // Your endpoint that provides uploading by Url
                            },
                            additionalRequestData : {
                                app: process.env.NEXT_PUBLIC_APP_NAME
                            }
                        }
                    },
                    Color: {
                        class: ColorPlugin, // if load from CDN, please try: window.ColorPlugin
                        config: {
                            colorCollections: ['#EC7878', '#9C27B0', '#673AB7', '#3F51B5', '#0070FF', '#03A9F4', '#00BCD4', '#4CAF50', '#8BC34A', '#CDDC39', '#FFF'],
                            defaultColor: '#FF1300',
                            type: 'text',
                            customPicker: true // add a button to allow selecting any colour  
                        }
                    },
                    Marker: {
                        class: ColorPlugin, 
                        config: {
                            defaultColor: '#FFBF00',
                            type: 'marker',
                            icon: `<svg fill="#000000" height="200px" width="200px" version="1.1" id="Icons" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" xml:space="preserve"><g id="SVGRepo_bgCarrier" stroke-width="0"></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"> <g> <path d="M17.6,6L6.9,16.7c-0.2,0.2-0.3,0.4-0.3,0.6L6,23.9c0,0.3,0.1,0.6,0.3,0.8C6.5,24.9,6.7,25,7,25c0,0,0.1,0,0.1,0l6.6-0.6 c0.2,0,0.5-0.1,0.6-0.3L25,13.4L17.6,6z"></path> <path d="M26.4,12l1.4-1.4c1.2-1.2,1.1-3.1-0.1-4.3l-3-3c-0.6-0.6-1.3-0.9-2.2-0.9c-0.8,0-1.6,0.3-2.2,0.9L19,4.6L26.4,12z"></path> </g> <g> <path d="M28,29H4c-0.6,0-1-0.4-1-1s0.4-1,1-1h24c0.6,0,1,0.4,1,1S28.6,29,28,29z"></path> </g> </g></svg>`
                        }
                    },

                },
                data,
                async onChange(api, event) {
                    const data = await api.saver.save();
                    onChange(data);
                },
                onReady: () => { new DragDrop(editor).default; console.log('Editor.js is ready to work!') },
                logLevel: "ERROR" as EditorJS.LogLevels.ERROR,
            })
            ref.current = editor;
        }
        return () => {
            if (ref.current && ref.current.destroy) {
                ref.current.destroy();
            }
        };
    }, [])

    return <div id={holder} className="bg-slate-50 rounded-lg dark:bg-gray-600 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"></div>
}

export default Editor;