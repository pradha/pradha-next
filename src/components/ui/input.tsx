import React from "react"
import CreatableSelect from 'react-select/creatable';


export const MultiSelect = (props: any) =>
    <CreatableSelect
        name="tags"
        instanceId={"tags"}
        noOptionsMessage={() => null}
        components={{ IndicatorSeparator: null }}
        isSearchable
        isClearable
        isMulti
        defaultValue={props.defaultValue}
        options={props.options}
        classNames={{
            control: (state) =>
              state.isFocused ? 'border-sky-500' : 'border-grey-300',
          }}
    />