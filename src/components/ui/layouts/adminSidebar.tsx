'use client'
import Link from "next/link"
import { UserIcon, MagnifyingGlassIcon, Bars3Icon, BellIcon, ArrowRightOnRectangleIcon, UserGroupIcon, ChartPieIcon, AdjustmentsHorizontalIcon, TagIcon, NewspaperIcon, ChatBubbleLeftRightIcon, KeyIcon, PhotoIcon, WrenchScrewdriverIcon, BanknotesIcon, HashtagIcon, CreditCardIcon, ListBulletIcon, ClipboardDocumentIcon, ClipboardDocumentListIcon, ChartBarIcon, BookOpenIcon, UsersIcon, IdentificationIcon, SquaresPlusIcon, HomeModernIcon, GiftIcon, BuildingOffice2Icon, SpeakerWaveIcon, MegaphoneIcon, PuzzlePieceIcon, TicketIcon, PresentationChartBarIcon, ComputerDesktopIcon, BriefcaseIcon, ShoppingCartIcon } from "@heroicons/react/24/outline"
import Image from "next/image"
import { useSession, signOut } from "next-auth/react"
import { redirect } from 'next/navigation';
import { Fragment, useEffect, useState } from 'react'
import { Dialog, Transition, Menu, Popover } from '@headlessui/react'
import { XMarkIcon } from '@heroicons/react/24/outline'

export default function AdminSidebar() {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })
    const [currentUrl, setCurrentUrl] = useState<string | null>(null);

    const [open, setOpen] = useState(false)
    useEffect(() => {
        setCurrentUrl(window.location.href);
    }, []);
    return (

        <div className="sticky top-0 flex flex-col max-h-screen min-h-screen ">
            <div className="p-4 text-2xl font-bold text-slate-600 dark:text-slate-300"><Link href={"/"} title="Go to Homepage">{process.env.NEXT_PUBLIC_APP_NAME}</Link></div>
            <div className="flex-1 py-4 overflow-hidden hover:overflow-y-auto active:overflow-y-auto bg-scroll hover:transition-all transition-all ease-in-out delay-150 duration-300">
                <ul className="py-2 mb-2">
                    <li className="aside-title">Main</li>
                    <li>
                        <Link href={"/dashboard"} title="dashboard" className={(currentUrl && currentUrl?.indexOf('/dashboard') >= 0 ? "aside-menu-active" : "aside-menu")}>
                            <ChartPieIcon width={20} height={20} />
                            <span>Dashboard</span>
                        </Link>
                    </li>
                </ul>
                {session?.user.group.roles.includes("61e3de6ecc4029a9d11ab000") &&
                    <ul className="py-2 mb-2">
                        <li className="aside-title">COMPANY</li>
                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11ab001") &&
                            <li>
                                <Link href={"/company/region"} title="Company Regions" className={(currentUrl && currentUrl?.indexOf('/company/region') >= 0 ? "aside-menu-active" : "aside-menu")}>
                                    <SquaresPlusIcon width={20} height={20} />
                                    <span>Regions</span>
                                </Link>
                            </li>
                        }
                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11ab005") &&
                            <li>
                                <Link href={"/company/customer"} title="Company Customers" className={(currentUrl && currentUrl?.indexOf('/company/customer') >= 0 ? "aside-menu-active" : "aside-menu")}>
                                    <IdentificationIcon width={20} height={20} />
                                    <span>Customers</span>
                                </Link>
                            </li>
                        }
                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11ab010") &&
                            <li>
                                <Link href={"/company/warehouse"} title="Company Warehouses" className={(currentUrl && currentUrl?.indexOf('/company/warehouse') >= 0 ? "aside-menu-active" : "aside-menu")}>
                                    <HomeModernIcon width={20} height={20} />
                                    <span>Warehouses</span>
                                </Link>
                            </li>
                        }
                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11ab015") &&
                            <li>
                                <Link href={"/company/product"} title="Company Products" className={(currentUrl && currentUrl?.indexOf('/company/product') >= 0 ? "aside-menu-active" : "aside-menu")}>
                                    <GiftIcon width={20} height={20} />
                                    <span>Products</span>
                                </Link>
                            </li>
                        }
                    </ul>
                }
                {session?.user.group.roles.includes("61e3de6ecc4029a9d1111000") &&
                    <ul className="py-2 mb-2">
                        <li className="aside-title">HEALT CARE</li>
                        {session?.user.group.roles.includes("61e3de6ecc4029a9d1111001") &&
                            <li>
                                <Link href={"/health/service"} title="Health Service" className={(currentUrl && currentUrl?.indexOf('/health/service') >= 0 ? "aside-menu-active" : "aside-menu")}>
                                    <BuildingOffice2Icon width={20} height={20} />
                                    <span>Services</span>
                                </Link>
                            </li>
                        }
                        {session?.user.group.roles.includes("61e3de6ecc4029a9d1111005") &&
                            <li>
                                <Link href={"/health/officer"} title="Health Officers" className={(currentUrl && currentUrl?.indexOf('/health/officer') >= 0 ? "aside-menu-active" : "aside-menu")}>
                                    <UsersIcon width={20} height={20} />
                                    <span>Officers</span>
                                </Link>
                            </li>
                        }
                        {session?.user.group.roles.includes("61e3de6ecc4029a9d1111005") &&
                            <li>
                                <Link href={"/health/mr"} title="Medical Records" className={(currentUrl && currentUrl?.indexOf('/health/officer') >= 0 ? "aside-menu-active" : "aside-menu")}>
                                    <BriefcaseIcon width={20} height={20} />
                                    <span>Medical Records</span>
                                </Link>
                            </li>
                        }
                    </ul>
                }
                {session?.user.group.roles.includes("61e3de6ecc4029a9d11a8000") &&
                    <ul className="py-2 mb-2">
                        <li className="aside-title">QUEUE</li>
                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a8013") &&
                            <li>
                                <Link href={"/queue/call"} title="Call Queue" className={(currentUrl && currentUrl?.indexOf('/queue/call') >= 0 ? "aside-menu-active" : "aside-menu")}>
                                    <MegaphoneIcon width={20} height={20} />
                                    <span>Call</span>
                                </Link>
                            </li>
                        }
                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a8005") &&
                            <li>
                                <Link href={"/queue/counter"} title="Queue Counter" className={(currentUrl && currentUrl?.indexOf('/queue/counter') >= 0 ? "aside-menu-active" : "aside-menu")}>
                                    <UsersIcon width={20} height={20} />
                                    <span>Counters</span>
                                </Link>
                            </li>
                        }
                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a8001") &&
                            <li>
                                <Link href={"/queue/service"} title="Queue Service" className={(currentUrl && currentUrl?.indexOf('/queue/service') >= 0 ? "aside-menu-active" : "aside-menu")}>
                                    <PuzzlePieceIcon width={20} height={20} />
                                    <span>Services</span>
                                </Link>
                            </li>
                        }
                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a8010") &&
                            <li>
                                <Link href={"/queue/ticket"} title="Queue Ticket" className={(currentUrl && currentUrl?.indexOf('/queue/ticket') >= 0 ? "aside-menu-active" : "aside-menu")}>
                                    <TicketIcon width={20} height={20} />
                                    <span>Ticket</span>
                                </Link>
                            </li>
                        }
                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a8015") &&
                            <li>
                                <Link href={"/queue/report"} title="Queue Ticket" className={(currentUrl && currentUrl?.indexOf('/queue/report') >= 0 ? "aside-menu-active" : "aside-menu")}>
                                    <PresentationChartBarIcon width={20} height={20} />
                                    <span>Report</span>
                                </Link>
                            </li>
                        }
                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a8017") &&
                            <li>
                                <Link href={"/queue/display"} title="Queue Display" className={(currentUrl && currentUrl?.indexOf('/queue/display') >= 0 ? "aside-menu-active" : "aside-menu")}>
                                    <ComputerDesktopIcon width={20} height={20} />
                                    <span>Display</span>
                                </Link>
                            </li>
                        }
                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a8020") &&
                            <li>
                                <Link href={"/queue/setting"} title="Queue Setting" className={(currentUrl && currentUrl?.indexOf('/queue/setting') >= 0 ? "aside-menu-active" : "aside-menu")}>
                                    <WrenchScrewdriverIcon width={20} height={20} />
                                    <span>Settings</span>
                                </Link>
                            </li>
                        }
                    </ul>
                }
                {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5000") &&
                    <ul className="py-2 mb-2">
                        <li className="aside-title">ACCOUNTING</li>
                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5001") &&
                            <li>
                                <Link href={"/accounting/coa"} title="Chart of Account" className={(currentUrl && currentUrl?.indexOf('/accounting/coa') >= 0 ? "aside-menu-active" : "aside-menu")}>
                                    <HashtagIcon width={20} height={20} />
                                    <span>Chart of Account (CoA)</span>
                                </Link>
                            </li>
                        }
                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5005") &&
                            <li>
                                <Link href={"/accounting/account"} title="Accounting Account" className={(currentUrl && currentUrl?.indexOf('/accounting/account') >= 0 ? "aside-menu-active" : "aside-menu")}>
                                    <BanknotesIcon width={20} height={20} />
                                    <span>Accounts</span>
                                </Link>
                            </li>
                        }
                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5015") &&
                            <li>
                                <Link href={"/accounting/transaction"} title="Transaction" className={(currentUrl && currentUrl?.indexOf('/accounting/transaction') >= 0 ? "aside-menu-active" : "aside-menu")}>
                                    <CreditCardIcon width={20} height={20} />
                                    <span>Transactions</span>
                                </Link>
                            </li>
                        }
                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5030") &&
                            <li>
                                <Link href={"/accounting/ledger"} title="Ledger Account" className={(currentUrl && currentUrl?.indexOf('/accounting/ledger') >= 0 ? "aside-menu-active" : "aside-menu")}>
                                    <ListBulletIcon width={20} height={20} />
                                    <span>Ledger Account</span>
                                </Link>
                            </li>
                        }
                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5031") &&
                            <li>
                                <Link href={"/accounting/general"} title="General Ledger" className={(currentUrl && currentUrl?.indexOf('/accounting/general') >= 0 ? "aside-menu-active" : "aside-menu")}>
                                    <BookOpenIcon width={20} height={20} />
                                    <span>General Ledger</span>
                                </Link>
                            </li>
                        }
                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5032") &&
                            <li>
                                <Link href={"/accounting/quantity"} title="Quantity Ledger" className={(currentUrl && currentUrl?.indexOf('/accounting/quantity') >= 0 ? "aside-menu-active" : "aside-menu")}>
                                    <ShoppingCartIcon width={20} height={20} />
                                    <span>Quantity Ledger</span>
                                </Link>
                            </li>
                        }
                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5033") &&
                            <li>
                                <Link href={"/accounting/balancesheet"} title="Balancesheet" className={(currentUrl && currentUrl?.indexOf('/accounting/balancesheet') >= 0 ? "aside-menu-active" : "aside-menu")}>
                                    <ChartPieIcon width={20} height={20} />
                                    <span>Balancesheet</span>
                                </Link>
                            </li>
                        }
                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5034") &&
                            <li>
                                <Link href={"/accounting/incomestatement"} title="Income Statement" className={(currentUrl && currentUrl?.indexOf('/accounting/incomestatement') >= 0 ? "aside-menu-active" : "aside-menu")}>
                                    <ChartBarIcon width={20} height={20} />
                                    <span>Income Statement</span>
                                </Link>
                            </li>
                        }
                    </ul>

                }

                {session?.user.group.roles.includes("61e3de6ecc4029a9d11a3000") &&
                    <ul className="py-2 mb-2">
                        <li className="aside-title">CONTENTS</li>
                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a3101") &&
                            <li>
                                <Link href={"/content/category"} title="Category of Content" className={(currentUrl && currentUrl?.indexOf('/content/category') >= 0 ? "aside-menu-active" : "aside-menu")}>
                                    <TagIcon width={20} height={20} />
                                    <span>Categories</span>
                                </Link>
                            </li>
                        }
                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a3201") &&
                            <li>
                                <Link href={"/content/post"} title="Post Contents" className={(currentUrl && currentUrl?.indexOf('/content/post') >= 0 ? "aside-menu-active" : "aside-menu")}>
                                    <NewspaperIcon width={20} height={20} />
                                    <span>Posts</span>
                                </Link>
                            </li>
                        }
                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a3401") &&
                            <li>
                                <Link href={"/content/media"} title="Post Media" className={(currentUrl && currentUrl?.indexOf('/content/media') >= 0 ? "aside-menu-active" : "aside-menu")}>
                                    <PhotoIcon width={20} height={20} />
                                    <span>Files & Media</span>
                                </Link>
                            </li>
                        }
                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a3501") &&
                            <li>
                                <Link href={"/content/comment"} title="Post Comment" className={(currentUrl && currentUrl?.indexOf('/content/comment') >= 0 ? "aside-menu-active" : "aside-menu")}>
                                    <ChatBubbleLeftRightIcon width={20} height={20} />
                                    <span>Comments</span>
                                </Link>
                            </li>
                        }
                    </ul>
                }
                {session?.user.group.roles.includes("61e3de6ecc4029a9d11a2000") &&
                    <ul className="py-2 mb-2">
                        <li className="aside-title">System</li>
                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a2001") &&
                            <li>
                                <Link href={"/system/setting"} title="System Settings" className={(currentUrl && currentUrl?.indexOf('/system/setting') >= 0 ? "aside-menu-active" : "aside-menu")}>
                                    <WrenchScrewdriverIcon width={20} height={20} />
                                    <span>Settings</span>
                                </Link>
                            </li>
                        }
                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a2005") &&
                            <li>
                                <Link href={"/system/group"} title="Groups of accounts" className={(currentUrl && currentUrl?.indexOf('/system/group') >= 0 ? "aside-menu-active" : "aside-menu")}>
                                    <AdjustmentsHorizontalIcon width={20} height={20} />
                                    <span>Account Group</span>
                                </Link>
                            </li>
                        }
                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a2010") &&
                            <li>
                                <Link href={"/system/user"} title="User Accounts" className={(currentUrl && currentUrl?.indexOf('/system/user') >= 0 ? "aside-menu-active" : "aside-menu")}>
                                    <UserGroupIcon width={20} height={20} />
                                    <span>User Accounts</span>
                                </Link>
                            </li>
                        }
                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a2020") &&
                            <li>
                                <Link href={"/system/billing"} title="System Billing Payment" className={(currentUrl && currentUrl?.indexOf('/system/billing') >= 0 ? "aside-menu-active" : "aside-menu")}>
                                    <CreditCardIcon width={20} height={20} />
                                    <span>System Billing</span>
                                </Link>
                            </li>
                        }
                    </ul>
                }
            </div>
            <div className="p-4 border-t border-slate-200 dark:border-slate-700">
                <Menu>
                    <Transition
                        as={Fragment}
                        enter="transition ease-out duration-100"
                        enterFrom="transform opacity-0 scale-95"
                        enterTo="transform opacity-100 scale-100"
                        leave="transition ease-in duration-75"
                        leaveFrom="transform opacity-100 scale-100"
                        leaveTo="transform opacity-0 scale-95"
                    >
                        <Menu.Items className="absolute z-10 w-56 origin-bottom-left bg-white divide-y divide-gray-100 rounded-md shadow-lg bottom-16 ring-1 ring-black ring-opacity-5 focus:outline-none">
                            <ul className="py-1">
                                {session?.user.group.roles.includes("61e3de6ecc4029a9d11a1002") &&
                                    <Menu.Item as={"li"}>
                                        {({ active }) => (
                                            <Link href={"/account/profile"} title="My Profile" className="flex items-center px-4 py-2 space-x-2 text-sm hover:bg-slate-50">
                                                <UserIcon width={16} height={16} />
                                                <span>My Profile</span>
                                            </Link>
                                        )}
                                    </Menu.Item>
                                }
                                <Menu.Item as={"li"}>
                                    {({ active }) => (
                                        <Link href={"/account/profile"} title="Sign Out" className="flex items-center px-4 py-2 space-x-2 text-sm hover:bg-slate-50">
                                            <ArrowRightOnRectangleIcon width={16} height={16} />
                                            <span>Sign Out</span>
                                        </Link>
                                    )}
                                </Menu.Item>
                            </ul>
                        </Menu.Items>
                    </Transition>
                    <Menu.Button as={"div"} className="flex flex-row items-center w-full cursor-pointer">
                        <div className="relative flex flex-col">
                            <div className="flex flex-col justify-center flex-shrink-0 w-10 h-10 bg-opacity-50 rounded-full bg-slate-200 dark:bg-slate-600">
                                <img src={session?.user.photo ? process.env.NEXT_PUBLIC_STATIC_FILES + session.user.photo : "/img/person.jpg"}
                                    className="object-cover w-12 h-12 rounded-full shadow hover:shadow-xl"
                                    alt={session?.user.name} />
                                <span className="absolute top-0 right-0 flex w-3 h-3">
                                    <span className="absolute inline-flex w-full h-full bg-green-400 rounded-full opacity-75 animate-ping"></span>
                                    <span className="relative inline-flex w-3 h-3 bg-green-500 rounded-full"></span>
                                </span>
                            </div>
                        </div>

                        <div className="self-center ml-4 overflow-x-hidden">
                            <div className="w-full text-base font-semibold leading-5 tracking-tight truncate text-slate-700 dark:text-slate-400">{session?.user.name ? session?.user.name : "please wait..."}</div>
                            <div className="text-sm  text-slate-500">{session?.user?.group.name}</div>
                        </div>
                    </Menu.Button>

                </Menu>

            </div>
        </div>
    )
}
