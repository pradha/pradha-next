'use client'
import { BriefcaseIcon, CheckIcon, ExclamationTriangleIcon, EyeSlashIcon, HomeIcon, PhoneIcon, QuestionMarkCircleIcon } from "@heroicons/react/24/outline"
import Link from "next/link"
import { usePathname } from "next/navigation"

export const Pages = () => {
    const pathName = usePathname()
    return <div className="bg-white shadow-lg rounded-xl group mb-4 dark:bg-slate-700 dark:text-slate-300">
        <ul className="py-2 mb-2">
            <li className="aside-title py-2 border-b border-slate-100 dark:border-slate-600">Pages</li>
            <li>
                <Link href={"/"} title="Home Page" className="aside-menu">
                    <HomeIcon width={20} height={20} />
                    <span>Home Page</span>
                </Link>
            </li>
            <li>
                <Link href={"/about"} title="About" className={"aside-menu " + (pathName == "/about" ? "font-semibold bg-slate-300 dark:text-slate-500" : "")}>
                    <QuestionMarkCircleIcon width={20} height={20} />
                    <span>About Us</span>
                </Link>
            </li>
            <li>
                <Link href={"/portofolio"} title="Portofolio" className={"aside-menu " + (pathName == "/portofolio" ? "font-semibold bg-slate-300 dark:text-slate-500" : "")}>
                    <BriefcaseIcon width={20} height={20} />
                    <span>Portofolio</span>
                </Link>
            </li>
            <li>
                <Link href={"/contact"} title="Contact" className={"aside-menu " + (pathName == "/contact" ? "font-semibold bg-slate-300 dark:text-slate-500" : "")}>
                    <PhoneIcon width={20} height={20} />
                    <span>Contact</span>
                </Link>
            </li>
        </ul>
    </div>
}

export const OtherPages = () => {
    const pathName = usePathname()
    return <div className="bg-white shadow-lg rounded-xl group mb-4 dark:bg-slate-700">
        <ul className="py-2 mb-2">
            <li className="aside-title py-2 border-b border-slate-100 dark:border-slate-600 dark:text-slate-300">Other Pages</li>
            <li>
                <Link href={"/disclaimer"} title="Disclaimer" className={"aside-menu " + (pathName == "/disclaimer" ? "font-semibold bg-slate-300 dark:text-slate-500" : "")}>
                    <ExclamationTriangleIcon width={20} height={20} />
                    <span>Disclaimer</span>
                </Link>
            </li>
            <li>
                <Link href={"/terms-conditions"} title="Terms & Condition" className={"aside-menu " + (pathName == "/terms-conditions" ? "font-semibold bg-slate-300 dark:text-slate-500" : "")}>
                    <CheckIcon width={20} height={20} />
                    <span>Terms & Condition</span>
                </Link>
            </li>
            <li>
                <Link href={"/privacy-policy"} title="Privacy Policy" className={"aside-menu " + (pathName == "/privacy-policy" ? "font-semibold bg-slate-300 dark:text-slate-500" : "")}>
                    <EyeSlashIcon width={20} height={20} />
                    <span>Privacy Policy</span>
                </Link>
            </li>
        </ul>
    </div>
}

export const OtherCategories = (props: any) => {
    const pathName = usePathname()
    return <div className="bg-white shadow-lg rounded-xl group mb-4 dark:bg-slate-700">
        <ul className="py-2 mb-2">
        <li className="aside-title py-2 border-b border-slate-100 dark:border-slate-600 dark:text-slate-300">Other Categories</li>
            {props.categories?.map((cat: any, i: number) => <li key={"cat" + i}>
                <Link href={"/category/" + cat.slug} title={cat.name} className={"aside-menu " + (pathName == "/category/"+cat.slug ? "font-semibold bg-slate-300 dark:text-slate-500" : "")}>
                    <span>{cat.name}</span>
                </Link>
            </li>)}
        </ul>
    </div>
}