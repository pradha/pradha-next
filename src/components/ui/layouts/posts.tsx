'use client'
import { Truncate } from "@/lib/text"
import { FullShort } from "@/lib/time"
import Link from "next/link"
import Image from "next/image"


export const PostList = (props: any) => {
    if (props?.posts)
        return <>
            {props?.posts.map((post: any, i: number) => <Link title={post?.title} key={post._id} href={"/read/" + post.slug} className="flex lg:flex-row flex-col bg-white shadow-lg rounded-lg overflow-hidden mb-4 dark:bg-slate-600 transition-all transform ease-in-out hover:shadow-2xl">
                <div className="relative lg:w-1/3 w-full bg-cover lg:h-48 h-36" >
                    <Image src={process.env.NEXT_PUBLIC_STATIC_FILES + post.thumbnail} alt={post.thumbnail_caption} fill className="object-cover lg:w-96 w-full h-40 lg:h-52" sizes="(max-width: 768px) 100vw,
              (max-width: 1200px) 50vw,
              33vw" blurDataURL="data:image/webp;base64,UklGRvwDAABXRUJQVlA4WAoAAAAgAAAAwQAAwQAASUNDUMgBAAAAAAHIAAAAAAQwAABtbnRyUkdCIFhZWiAAAAAAAAAAAAAAAABhY3NwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAA9tYAAQAAAADTLQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAlkZXNjAAAA8AAAACRyWFlaAAABFAAAABRnWFlaAAABKAAAABRiWFlaAAABPAAAABR3dHB0AAABUAAAABRyVFJDAAABZAAAAChnVFJDAAABZAAAAChiVFJDAAABZAAAAChjcHJ0AAABjAAAADxtbHVjAAAAAAAAAAEAAAAMZW5VUwAAAAgAAAAcAHMAUgBHAEJYWVogAAAAAAAAb6IAADj1AAADkFhZWiAAAAAAAABimQAAt4UAABjaWFlaIAAAAAAAACSgAAAPhAAAts9YWVogAAAAAAAA9tYAAQAAAADTLXBhcmEAAAAAAAQAAAACZmYAAPKnAAANWQAAE9AAAApbAAAAAAAAAABtbHVjAAAAAAAAAAEAAAAMZW5VUwAAACAAAAAcAEcAbwBvAGcAbABlACAASQBuAGMALgAgADIAMAAxADZWUDggDgIAALAUAJ0BKsIAwgA+7XCwVK0mJCMjU8mxoB2JZ27gCA2P7UuDUbB9eMfoBcDe/GvCX4kS3JrR3qvu2othCTck3cluZuNnY3y0tE22kAmbB73x82OQSh4UkhKC+QKH47sehZdaeqxb/WWZavQji1p+3sXO5DOEESU4biG50oYJrqHMShSuJaRjQZ3Z/jCZXxB7H73RAq3E3btzzznJLOx2dfJHE12dnLWX5VAzePBuAAD+7RoxlXq9Mh73LRXj2GcwTcJrX1SZbsNzyMQpOHkE3jX3tMWspBdfzLgUnOBdhHc97O9PxD63gCUyTYQQYFSly343ihzwQ6xbwDMDF9ExSjVJd8PrKvyEWv0ymJYO6HNpiInsKib0cGpX5dmeoafkqvmoLNl1MjhztGd2SArxEon17Gced9ixPyRvxp2R7NNY8n2ViYn3X3TD8zXXCeuR+GwCkQDavPgVxhICYYeFiYLmlfYH5tMgBjjTmDR4RLiKwUTU5ku5/JIh+z7w5uue0zBtmcyLTNpt9kv4pt4HNccE9VWI/IpPi7/yxrga84cO7N57Zaicm+vpHrewzE42g0kyaC4NixL+b64U3s20IALku160Tsq+n9/jdQXUq4pvbM1IlixSb76prSwkU0uakKB2UZ5prX7jEPHRlbshuBvLVaPEaoHTuv0z09ZE984+C3IHYU7XWjflaErgAAA="
                        placeholder="blur" loading="lazy" />
                </div>
                <div className="lg:w-2/3 w-full p-4 mt-2 lg:mt-0">
                    <h3 title={post?.title} className="text-gray-900 font-bold text-sm lg:text-lg dark:text-slate-200">{Truncate(post?.title, 50)}</h3>
                    <p className="my-2 hidden lg:block text-gray-600 text-sm dark:text-slate-300">{Truncate(post.description, 160)}</p>
                    <div className="text-xs text-slate-600 dark:text-slate-400">{post.published?.at ? FullShort(post.published?.at) : FullShort(post.created?.at)} <span className="hidden lg:inline-block">&#8594; by {post.created?.by?.name}</span></div>
                </div>
            </Link>
            )}
        </>
    else return <></>
}