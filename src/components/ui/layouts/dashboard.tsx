'use client'
import Link from "next/link"
import { UserIcon, MagnifyingGlassIcon, Bars3Icon, BellIcon, ArrowRightOnRectangleIcon, UserGroupIcon, ChartPieIcon, AdjustmentsHorizontalIcon, TagIcon, NewspaperIcon, ChatBubbleLeftRightIcon, KeyIcon, PhotoIcon, WrenchScrewdriverIcon, BanknotesIcon, HashtagIcon, CreditCardIcon, ListBulletIcon, ClipboardDocumentIcon, ClipboardDocumentListIcon, ChartBarIcon, BookOpenIcon, UsersIcon, IdentificationIcon, SquaresPlusIcon, HomeModernIcon, GiftIcon, BuildingOffice2Icon, SpeakerWaveIcon, MegaphoneIcon, PuzzlePieceIcon, TicketIcon, PresentationChartBarIcon, ComputerDesktopIcon, BriefcaseIcon } from "@heroicons/react/24/outline"
import Image from "next/image"
import { useSession, signOut } from "next-auth/react"
import { redirect } from 'next/navigation';
import { Fragment, useEffect, useState } from 'react'
import { Dialog, Transition, Menu, Popover } from '@headlessui/react'
import { XMarkIcon } from '@heroicons/react/24/outline'
import AdminSidebar from "./adminSidebar"

export default function DashboardLayout({ children }: { children: React.ReactNode }) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })
    const [currentUrl, setCurrentUrl] = useState<string | null>(null);

    const [open, setOpen] = useState(false)
    useEffect(() => {
        setCurrentUrl(window.location.href);
    }, []);
    return (
        <div>
            <Transition.Root show={open} as={Fragment}>
                <Dialog as="div" className="relative z-10" onClose={setOpen}>
                    <Transition.Child
                        as={Fragment}
                        enter="ease-in-out duration-500"
                        enterFrom="opacity-0"
                        enterTo="opacity-100"
                        leave="ease-in-out duration-500"
                        leaveFrom="opacity-100"
                        leaveTo="opacity-0"
                    >
                        <div className="fixed inset-0 transition-opacity bg-gray-500 bg-opacity-75" />
                    </Transition.Child>

                    <div className="fixed inset-0 overflow-hidden">
                        <div className="absolute inset-0 overflow-hidden">
                            <div className="fixed inset-y-0 left-0 flex max-w-full pr-10 pointer-events-none">
                                <Transition.Child
                                    as={Fragment}
                                    enter="transform transition ease-in-out duration-500 sm:duration-700"
                                    enterFrom="-translate-x-full"
                                    enterTo="translate-x-0"
                                    leave="transform transition ease-in-out duration-500 sm:duration-700"
                                    leaveFrom="translate-x-0"
                                    leaveTo="-translate-x-full"
                                >
                                    <Dialog.Panel className="relative w-screen max-w-xs pointer-events-auto">
                                        <Transition.Child
                                            as={Fragment}
                                            enter="ease-in-out duration-500"
                                            enterFrom="opacity-0"
                                            enterTo="opacity-100"
                                            leave="ease-in-out duration-500"
                                            leaveFrom="opacity-100"
                                            leaveTo="opacity-0"
                                        >
                                            <div className="absolute top-0 right-0 flex pt-4 pr-2 -mr-10 sm:-mr-10 sm:pl-4">
                                                <button
                                                    type="button"
                                                    className="text-gray-300 rounded-md hover:text-white focus:outline-none focus:ring-2 focus:ring-white"
                                                    onClick={() => setOpen(false)}
                                                >
                                                    <span className="sr-only">Close panel</span>
                                                    <XMarkIcon className="w-6 h-6" aria-hidden="true" />
                                                </button>
                                            </div>
                                        </Transition.Child>
                                        <div className="flex flex-col h-full overflow-y-auto bg-white shadow-xl">
                                            <div className="relative flex-1">
                                                {/* Replace with your content */}
                                                <div className="absolute inset-0">
                                                    <aside className="relative w-full max-w-xs shadow-lg bg-gradient-to-br from-slate-50 to-slate-100 dark:from-slate-700 dark:to-slate-800">
                                                        <AdminSidebar />
                                                    </aside>
                                                </div>

                                                {/* /End replace */}
                                            </div>
                                        </div>
                                    </Dialog.Panel>
                                </Transition.Child>
                            </div>
                        </div>
                    </div>
                </Dialog>
            </Transition.Root>

            <section className="flex flex-row items-stretch min-h-screen bg-slate-50 dark:bg-slate-800 text-slate-900 dark:text-slate-400">
                <aside className="relative hidden w-[18rem] max-w-xs shadow-lg xl:block bg-gradient-to-br from-slate-50 to-slate-100 dark:from-slate-700 dark:to-slate-800">
                    <AdminSidebar />
                </aside>
                <div className="w-full h-full">
                    <div className="flex items-center justify-between p-2 space-x-2 shadow-lg bg-gradient-to-br from-slate-100 to-slate-200 dark:from-slate-700 dark:to-slate-800">
                        <button onClick={() => setOpen(true)} type="button" className="p-2 border border-gray-300 rounded-md lg:hidden"><Bars3Icon width={20} height={20} /></button>
                        <div className="relative hidden w-64 text-gray-600 lg:block">
                            <input className="bg-gray-50 border border-gray-300  text-gray-900 text-sm rounded-lg focus:ring-sky-500 focus:border-sky-500 focus:outline-none block w-full p-2  dark:bg-gray-600 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500;" type="search" name="search" placeholder="Search" />
                            <button type="submit" className="absolute top-0 right-0 mt-2 mr-3 dark:text-slate-500">
                                <MagnifyingGlassIcon width={20} height={20} />
                            </button>
                        </div>
                        <div className="flex items-center">
                            <Popover className="relative mx-4">
                                <Popover.Button as="div" className={"mx-2"}>
                                    <BellIcon width={20} height={20} />
                                    <span className="absolute top-0 right-1 flex w-2 h-2">
                                        <span className="absolute inline-flex w-full h-full bg-green-400 rounded-full opacity-50 animate-ping"></span>
                                        <span className="relative inline-flex w-2 h-2 bg-green-500 rounded-full"></span>
                                    </span>
                                </Popover.Button>

                                <Transition
                                    as={Fragment}
                                    enter="transition ease-out duration-200"
                                    enterFrom="opacity-0 translate-y-1"
                                    enterTo="opacity-100 translate-y-0"
                                    leave="transition ease-in duration-150"
                                    leaveFrom="opacity-100 translate-y-0"
                                    leaveTo="opacity-0 translate-y-1"
                                >
                                    <Popover.Panel className="absolute z-10 w-screen max-w-xs p-4 mt-3 text-sm transform bg-white border rounded-md shadow-lg -right-0 lg:left-1/2 lg:-translate-x-1/2 border-gray-50 dark:border-gray-500 lg:max-w-sm dark:text-slate-100 dark:bg-slate-600">
                                        <div className="text-center"> no information right now</div>
                                    </Popover.Panel>
                                </Transition>
                            </Popover>

                            <Menu as="div" className="relative inline-block ml-4 mr-2 text-left lg:shadow-md rounded-xl">
                                <Menu.Button as={"div"} className="flex items-center text-sm rounded-lg cursor-pointer group">
                                    <div className="relative w-8 h-8 -ml-5 overflow-hidden  transition-all ease-in-out rounded-full  ring ring-sky-300 group-hover:ring-sky-500 dark:bg-gray-600">
                                        <Image src={session?.user.photo ? process.env.NEXT_PUBLIC_STATIC_FILES + session.user.photo : "/img/person.jpg"} width={150} height={150} loading="lazy" className="object-cover w-8 h-8 rounded-full shadow hover:shadow-xl" alt={session?.user.name ? session?.user.name : "please wait..."} />
                                    </div>
                                    <div className="hidden py-2 pl-6 pr-3 -ml-4 text-gray-600 lg:block bg-gradient-to-br from-slate-100 to-slate-200 rounded-r-xl">{session?.user.name ? session?.user.name : "please wait..."}</div>
                                </Menu.Button>
                                <Transition
                                    as={Fragment}
                                    enter="transition ease-out duration-100"
                                    enterFrom="transform opacity-0 scale-95"
                                    enterTo="transform opacity-100 scale-100"
                                    leave="transition ease-in duration-75"
                                    leaveFrom="transform opacity-100 scale-100"
                                    leaveTo="transform opacity-0 scale-95"
                                >
                                    <Menu.Items className="absolute z-10 w-48 mt-2 origin-top-right bg-white divide-y divide-gray-100 rounded-md shadow-lg right-1 ring-1 ring-black ring-opacity-5 focus:outline-none dark:bg-slate-600">
                                        <ul className="py-1 text-sm dark:text-slate-100">
                                            {session?.user.group.roles.includes("61e3de6ecc4029a9d11a1002") &&
                                                <Menu.Item as={"li"}>
                                                    {({ active }) => (
                                                        <Link href={"/account/profile"} title="My Profile" className="flex items-center px-4 py-2 space-x-2 hover:bg-slate-50 dark:hover:bg-slate-500 ">
                                                            <UserIcon width={16} height={16} />
                                                            <span>My Profile</span>
                                                        </Link>
                                                    )}
                                                </Menu.Item>
                                            }
                                            {session?.user.group.roles.includes("61e3de6ecc4029a9d11a1007") &&
                                                <Menu.Item as={"li"}>
                                                    {({ active }) => (
                                                        <Link href={"/account/billing"} title="My Billing" className="flex items-center px-4 py-2 space-x-2 hover:bg-slate-50 dark:hover:bg-slate-500">
                                                            <CreditCardIcon width={16} height={16} />
                                                            <span>Billing</span>
                                                        </Link>
                                                    )}
                                                </Menu.Item>
                                            }
                                            {session?.user.group.roles.includes("61e3de6ecc4029a9d11a1006") &&
                                                <Menu.Item as={"li"}>
                                                    {({ active }) => (
                                                        <Link href={"/account/password"} title="Change Password" className="flex items-center px-4 py-2 space-x-2 hover:bg-slate-50 dark:hover:bg-slate-500">
                                                            <KeyIcon width={16} height={16} />
                                                            <span>Change Password</span>
                                                        </Link>
                                                    )}
                                                </Menu.Item>
                                            }
                                            <hr className="border-b border-slate-50 dark:border-slate-500" />
                                            <Menu.Item as={"li"}>
                                                {({ active }) => (
                                                    <button type="button" onClick={() => signOut({ callbackUrl: '/' })} title="My Profile" className="flex items-center w-full px-4 py-2 space-x-2 hover:bg-slate-50 dark:hover:bg-slate-500">
                                                        <ArrowRightOnRectangleIcon width={16} height={16} />
                                                        <span>Sign Out</span>
                                                    </button>
                                                )}
                                            </Menu.Item>
                                        </ul>
                                    </Menu.Items>
                                </Transition>
                            </Menu>
                        </div>
                    </div>
                    <div id="content" className="p-4 lg:p-6 text-slate-900 dark:text-slate-400">
                        {children}
                    </div>
                </div>

            </section>
        </div>
    )
}
