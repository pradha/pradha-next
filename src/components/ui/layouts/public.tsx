'use client'
import { Dialog, Menu, Transition } from "@headlessui/react";
import { ArrowLeftOnRectangleIcon, ArrowRightOnRectangleIcon, Bars3Icon, BookOpenIcon, BriefcaseIcon, ChartPieIcon, CheckIcon, EyeSlashIcon, HomeIcon, KeyIcon, MagnifyingGlassIcon, PhoneIcon, PresentationChartLineIcon, QuestionMarkCircleIcon, UserIcon, XMarkIcon } from "@heroicons/react/24/outline";
import { useSession, signOut } from "next-auth/react";
import Link from "next/link";
import { FormEvent, Fragment, useState } from "react";
import Image from "next/image";
import Footer from "../footer";

export default function PublicLayout({ children }: { children: React.ReactNode }) {
    const { data: session, status } = useSession({
        required: false
    })
    const [open, setOpen] = useState(false)

    const search = async(e: FormEvent) => {
        e.preventDefault()
        const target = e.target as HTMLFormElement
        if (target.search.value.length > 0){
            window.location.href = "/search/"+target.search.value.replaceAll(" ","+").toLowerCase()
        } else {
            alert("Please provide search keywords")
        }
    }
    return (
        <main className="grid grid-cols-1 gap-4 content-between min-h-screen dark:bg-slate-800">
            <Transition.Root show={open} as={Fragment}>
                <Dialog as="div" className="relative z-10" onClose={setOpen}>
                    <Transition.Child
                        as={Fragment}
                        enter="ease-in-out duration-500"
                        enterFrom="opacity-0"
                        enterTo="opacity-100"
                        leave="ease-in-out duration-500"
                        leaveFrom="opacity-100"
                        leaveTo="opacity-0"
                    >
                        <div className="fixed inset-0 transition-opacity bg-gray-500 bg-opacity-75" />
                    </Transition.Child>

                    <div className="fixed inset-0 overflow-hidden">
                        <div className="absolute inset-0 overflow-hidden">
                            <div className="fixed inset-y-0 left-0 flex max-w-full pr-10 pointer-events-none">
                                <Transition.Child
                                    as={Fragment}
                                    enter="transform transition ease-in-out duration-500 sm:duration-700"
                                    enterFrom="-translate-x-full"
                                    enterTo="translate-x-0"
                                    leave="transform transition ease-in-out duration-500 sm:duration-700"
                                    leaveFrom="translate-x-0"
                                    leaveTo="-translate-x-full"
                                >
                                    <Dialog.Panel className="relative w-screen max-w-xs pointer-events-auto">
                                        <Transition.Child
                                            as={Fragment}
                                            enter="ease-in-out duration-500"
                                            enterFrom="opacity-0"
                                            enterTo="opacity-100"
                                            leave="ease-in-out duration-500"
                                            leaveFrom="opacity-100"
                                            leaveTo="opacity-0"
                                        >
                                            <div className="absolute top-0 right-0 flex pt-4 pr-2 -mr-10 sm:-mr-10 sm:pl-4">
                                                <button
                                                    type="button"
                                                    className="text-gray-300 rounded-md hover:text-white focus:outline-none focus:ring-2 focus:ring-white"
                                                    onClick={() => setOpen(false)}
                                                >
                                                    <span className="sr-only">Close panel</span>
                                                    <XMarkIcon className="w-6 h-6" aria-hidden="true" />
                                                </button>
                                            </div>
                                        </Transition.Child>
                                        <div className="flex flex-col h-full py-6 overflow-y-auto bg-white shadow-xl">
                                            <div className="px-4 sm:px-6">
                                                <Dialog.Title className="text-lg font-medium px-2 text-gray-900">Main Menu</Dialog.Title>
                                            </div>
                                            <div className="relative flex-1 px-2">
                                                <div className="flex-1 py-4 overflow-y-auto">
                                                    <ul className="py-2 mb-2">
                                                        <li className="aside-title">Pages</li>
                                                        <li>
                                                            <Link href={"/"} title="Home Page" className="aside-menu">
                                                                <HomeIcon width={20} height={20} />
                                                                <span>Home Page</span>
                                                            </Link>
                                                        </li>
                                                        <li>
                                                            <Link href={"/about"} title="About" className="aside-menu">
                                                                <QuestionMarkCircleIcon width={20} height={20} />
                                                                <span>About Us</span>
                                                            </Link>
                                                        </li>
                                                        <li>
                                                            <Link href={"/portofolio"} title="Portofolio" className="aside-menu">
                                                                <BriefcaseIcon width={20} height={20} />
                                                                <span>Portofolio</span>
                                                            </Link>
                                                        </li>
                                                        <li>
                                                            <Link href={"/contact"} title="Contact" className="aside-menu">
                                                                <PhoneIcon width={20} height={20} />
                                                                <span>Contact</span>
                                                            </Link>
                                                        </li>
                                                        {!session &&
                                                            <li>
                                                                <Link href={"/account/signin"} title="Sign In" className="aside-menu">
                                                                    <ArrowLeftOnRectangleIcon width={20} height={20} />
                                                                    <span>Sign In</span>
                                                                </Link>
                                                            </li>
                                                        }
                                                        {session &&
                                                            <li>
                                                                <Link href={"/dashboard"} title="Contact" className="aside-menu">
                                                                    <PresentationChartLineIcon width={20} height={20} />
                                                                    <span>Dashboard</span>
                                                                </Link>
                                                            </li>
                                                        }
                                                        {session &&
                                                            <li>
                                                                <button type="button" onClick={() => signOut({ callbackUrl: '/' })} title="Sign Out" className="aside-menu">
                                                                    <ArrowRightOnRectangleIcon width={20} height={20} />
                                                                    <span>Sign Out</span>
                                                                </button>
                                                            </li>
                                                        }
                                                    </ul>

                                                    <ul className="py-2 mb-2">
                                                        <li className="aside-title">Other Pages</li>
                                                        <li>
                                                            <Link href={"/quran"} title="Terms & Conditions" className="aside-menu">
                                                                <BookOpenIcon width={20} height={20} />
                                                                <span>Online Quran</span>
                                                            </Link>
                                                        </li>
                                                        <li>
                                                            <Link href={"/terms-conditions"} title="Terms & Conditions" className="aside-menu">
                                                                <CheckIcon width={20} height={20} />
                                                                <span>Terms & Conditions</span>
                                                            </Link>
                                                        </li>
                                                        <li>
                                                            <Link href={"/privacy-policy"} title="Privacy Policy" className="aside-menu">
                                                                <EyeSlashIcon width={20} height={20} />
                                                                <span>Privacy Policy</span>
                                                            </Link>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </Dialog.Panel>
                                </Transition.Child>
                            </div>
                        </div>
                    </div>
                </Dialog>
            </Transition.Root>
            <div className="fixed z-10 top-0 w-full shadow-lg bg-gradient-to-br from-slate-100 to-slate-200 dark:from-slate-700 dark:to-slate-800">
                <div className="container flex items-center justify-between p-2 space-x-4 mx-auto max-w-6xl">
                    <button id="sidebar" aria-label="sidebar-menu" onClick={() => setOpen(true)} type="button" className="p-2 border border-gray-300 rounded-md lg:hidden dark:text-slate-400 dark:border-slate-600"><Bars3Icon width={20} height={20} /></button>
                    <div className="font-semibold text-xl text-sky-600 flex-1 lg:flex-none text-center lg:text-left mx-4 dark:text-slate-300">
                        <Link href={"/"} title="Home Page" className="pr-8 lg:pr-0 flex justify-center space-x-2 items-center">
                            <Image src={"/img/logo.webp"} alt="logo" width={28} height={28} priority />
                            <div>{process.env.NEXT_PUBLIC_APP_NAME}</div>
                        </Link>
                    </div>

                    <form onSubmit={search} className="relative flex-1 hidden max-w-lg text-gray-600 dark:text-slate-300 lg:block mx-4">
                        <input className="bg-gray-50 border border-gray-300  text-gray-900 text-sm rounded-full focus:ring-sky-500 focus:border-sky-500 focus:outline-none block w-full py-2 px-4  dark:bg-gray-600 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500;" type="search" name="search" id="search" placeholder="Search" />
                        <button id="btn-search" aria-label="btn-search" type="submit" className="absolute top-0 right-0 mt-2 mr-3 dark:text-slate-500">
                            <MagnifyingGlassIcon width={20} height={20} />
                        </button>
                    </form>

                    <div className="flex space-x-4 dark:text-slate-300">
                        <ul className="lg:flex items-center relative hidden space-x-4 text-sm mx-4">
                            <li><Link href="/about" className="border-b-transparent hover:border-b-2 hover:border-b-sky-600 p-1 transition-all ease-out" title="">About Us</Link></li>
                            <li><Link href="/portofolio" className="border-b-transparent hover:border-b-2 hover:border-b-sky-600 p-1 transition-all ease-out" title="">Portofolio</Link></li>
                            <li><Link href="/contact" className="border-b-transparent hover:border-b-2 hover:border-b-sky-600 p-1 transition-all ease-out" title="">Contact</Link></li>
                            <li><Link href="/quran" className="border-b-transparent hover:border-b-2 hover:border-b-sky-600 p-1 transition-all ease-out" title="">Online Quran</Link></li>
                        </ul>
                        {!session &&
                            <ul className="lg:flex hidden items-center relative space-x-4 text-sm">
                                <li className="">
                                    <Link className="flex border-b-transparent border-b-2 hover:border-b-sky-600 p-1 transition-all ease-out" href="/account/signin" title="">
                                        <ArrowLeftOnRectangleIcon width={18} className="mr-2" />
                                        <span className="hidden lg:block">Sign In</span>
                                    </Link>
                                </li>
                            </ul>
                        }

                        {session &&
                            <Menu as="div" className="relative inline-block ml-4 mr-2 text-left lg:shadow-md rounded-xl">
                                <Menu.Button as={"div"} className="flex items-center text-sm rounded-lg cursor-pointer group">
                                    <div className="relative w-8 h-8 -ml-5 overflow-hidden transition-all ease-in-out rounded-full  ring ring-sky-300 group-hover:ring-sky-500 dark:bg-gray-600">
                                    <Image src={session?.user.photo ? process.env.NEXT_PUBLIC_STATIC_FILES + session.user.photo : "/img/person.jpg"} width={150} height={150} loading="lazy" className="object-cover w-8 h-8 rounded-full shadow hover:shadow-xl" alt={session?.user.name ? session?.user.name : "please wait..."} />

                                    </div>
                                    <div className="hidden py-2 pl-6 pr-3 -ml-4 text-gray-600 lg:block bg-gradient-to-br from-slate-100 to-slate-200 rounded-r-xl">{session?.user.name ? session?.user.name : "please wait..."}</div>
                                </Menu.Button>
                                <Transition
                                    as={Fragment}
                                    enter="transition ease-out duration-100"
                                    enterFrom="transform opacity-0 scale-95"
                                    enterTo="transform opacity-100 scale-100"
                                    leave="transition ease-in duration-75"
                                    leaveFrom="transform opacity-100 scale-100"
                                    leaveTo="transform opacity-0 scale-95"
                                >
                                    <Menu.Items className="absolute z-10 w-48 mt-2 origin-top-right bg-white divide-y divide-gray-100 rounded-md shadow-lg right-1 ring-1 ring-black ring-opacity-5 focus:outline-none dark:bg-slate-600">
                                        <ul className="py-1 text-sm dark:text-slate-100">
                                        <Menu.Item as={"li"}>
                                                {({ active }) => (
                                                    <Link href={"/dashboard"} title="My Profile" className="flex items-center px-4 py-2 space-x-2 hover:bg-slate-50 dark:hover:bg-slate-500 ">
                                                        <ChartPieIcon width={16} height={16} />
                                                        <span>Dashboard</span>
                                                    </Link>
                                                )}
                                            </Menu.Item>
                                            <hr className="border-b border-slate-50 dark:border-slate-500" />
                                            <Menu.Item as={"li"}>
                                                {({ active }) => (
                                                    <Link href={"/account/profile"} title="My Profile" className="flex items-center px-4 py-2 space-x-2 hover:bg-slate-50 dark:hover:bg-slate-500 ">
                                                        <UserIcon width={16} height={16} />
                                                        <span>My Profile</span>
                                                    </Link>
                                                )}
                                            </Menu.Item>
                                            <Menu.Item as={"li"}>
                                                {({ active }) => (
                                                    <Link href={"/account/password"} title="My Profile" className="flex items-center px-4 py-2 space-x-2 hover:bg-slate-50 dark:hover:bg-slate-500">
                                                        <KeyIcon width={16} height={16} />
                                                        <span>Change Password</span>
                                                    </Link>
                                                )}
                                            </Menu.Item>
                                            <hr className="border-b border-slate-50 dark:border-slate-500" />
                                            <Menu.Item as={"li"}>
                                                {({ active }) => (
                                                    <button type="button" onClick={() => signOut({ callbackUrl: '/' })} title="My Profile" className="flex items-center w-full px-4 py-2 space-x-2 hover:bg-slate-50 dark:hover:bg-slate-500">
                                                        <ArrowRightOnRectangleIcon width={16} height={16} />
                                                        <span>Sign Out</span>
                                                    </button>
                                                )}
                                            </Menu.Item>
                                        </ul>
                                    </Menu.Items>
                                </Transition>
                            </Menu>
                        }
                    </div>
                </div>
            </div>
            <div className="w-full">
                <section className="container p-4 space-x-2 mx-auto max-w-6xl mt-14 text-slate-600 dark:text-slate-300">
                    {children}
                </section>
            </div>
            <Footer className=" text-slate-500" />
        </main>
    )
}