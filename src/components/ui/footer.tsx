'use client'
import Link from "next/link";
import Copyright from "./copyright";
import { Subscribe } from "./subscribe";

export default function Footer(props: any) {
    return (
        <footer className={"mt-4 bg-gradient-to-br from-slate-100 to-slate-200 dark:from-slate-700 dark:to-slate-800 w-full " + props.className}>
            <div className="container mx-auto max-w-6xl lg:py-14 py-8">
                <div className="flex flex-wrap text-left lg:text-left">
                    <Subscribe />
                    <div className="w-full lg:w-6/12 px-4">
                        <div className="flex flex-wrap items-top mb-6">
                            <div className="w-full lg:w-4/12 px-4 ml-auto">
                                <span className="block uppercase text-blueGray-500 text-sm font-semibold mb-2">Main Pages</span>
                                <ul className="list-unstyled flex flex-col space-y-2 mb-4">
                                    <li>
                                        <Link className="text-sm" href="/">Home</Link>
                                    </li>
                                    <li>
                                        <Link className="text-sm" href="/about">About Us</Link>
                                    </li>
                                    <li>
                                        <Link className="text-sm" href="/portofolio">Portofolio</Link>
                                    </li>
                                    <li>
                                        <Link className="text-sm" href="/contact">Contact</Link>
                                    </li>
                                </ul>
                            </div>
                            <div className="w-full lg:w-4/12 px-4">
                                <span className="block uppercase text-blueGray-500 text-sm font-semibold mb-2">Other Resources</span>
                                <ul className="list-unstyled flex flex-col space-y-2 mb-4">
                                    <li>
                                        <Link className="text-sm" href="/quran">Online Quran</Link>
                                    </li>
                                    <li>
                                        <Link className="text-sm" href="/disclaimer">Disclaimer</Link>
                                    </li>
                                    <li>
                                        <Link className="text-sm" href="/terms-conditions">Terms & Conditions</Link>
                                    </li>
                                    <li>
                                        <Link className="text-sm" href="/privacy-policy">Privacy Policy</Link>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <Copyright className="text-center py-8 mx-auto max-w-6xl px-4" />
        </footer>
    )
}