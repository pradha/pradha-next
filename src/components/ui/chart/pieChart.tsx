'use client'

import { Chart, registerables } from "chart.js";
Chart.register(...registerables);
import { useEffect, useState } from "react";

export const PieChart = (props: any) => {
    let [labels, setLabels] = useState(props.labels)
    let [datasets, setDatasets] = useState(props.datasets)

    const chart = (id: string) => {
        const ctx = document.getElementById(id) as HTMLCanvasElement;
        return new Chart(ctx, {
            type: 'pie',
            data: {
                labels: labels,
                datasets: datasets
            },
            options: {
                responsive: true,
                plugins: {
                    legend: {
                        position: 'left'
                    }
                }
            }
        });
    }
    let crt: any
    useEffect(() => {
        crt = chart(props.id)
        return () => {
            crt.destroy()
        }
    })


    if (labels != props.labels) {
        setLabels(props.labels)
        setDatasets(props.datasets)
    }
    //if (crt) crt.update()

    return (
        <canvas id={props.id} className="w-full" style={{height:'10vh'}}></canvas>

    )
}

export default PieChart;