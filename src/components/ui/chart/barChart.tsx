'use client'

import { Chart, registerables } from "chart.js";
Chart.register(...registerables);
import { useEffect, useState } from "react";

export const BarChart = (props: any) => {
    let [labels, setLabels] = useState(props.labels)
    let [datasets, setDatasets] = useState(props.datasets)

    const chart = (id: string) => {
        const ctx = document.getElementById(id) as HTMLCanvasElement;
        return new Chart(ctx, {
            type: 'bar',
            data: {
                labels: labels,
                datasets: datasets
            },
            options: {
                plugins: {
                    legend: {
                        display: props?.legend ? props.legend : false
                    }
                }
            }
        });
    }
    let crt: any
    useEffect(() => {
        crt = chart(props.id)
        return () => {
            crt.destroy()
        }
    })

    if (labels != props.labels) {
        setLabels(props.labels)
        setDatasets(props.datasets)
    }

    return (
        <canvas id={props.id} className="w-full"></canvas>

    )
}

export default BarChart;