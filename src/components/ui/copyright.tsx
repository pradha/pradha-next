export default function Copyright(props:any){
    return <div className={"text-sm text-slate-500 dark:text-slate-300 "+props.className }>
        Copyright &copy; 2011 - {new Date().getFullYear()} {process.env.NEXT_PUBLIC_COPYRIGHT} <br />All Rights Reserved
    </div>
}