'use client'
import { usePathname } from "next/navigation"
import path from "path"
import { FormEvent, useState } from "react"
import { Success, Warning } from "./alert"
import { Spinner } from "./loader"

export const Subscribe = () => {
    const pathName = usePathname()

    let errors = {
        email: ""
    }

    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState({ errors: errors })
    const [success, setSuccess] = useState({ title: "", description: "" })

    const submit = async (e: FormEvent) => {
        e.preventDefault()
        setWarning({ errors: errors })
        setSuccess({ title: "", description: "" })
        setSpinner(true)

        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement
        const submit = await fetch(path.posix.join("/api", "subscribe"), {
            method: 'POST',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: target.email.value
            })
        });
        //await new Promise(resolve => setTimeout(resolve, 3000));
        const res = await submit.json()
        if (submit.ok) {
            target.email.value = ""

            setSuccess({ title: "Success", description: "Please check your email to confirm your subscription" })
        } else {
            errors = await res.errors
            setWarning({ errors: errors })
            errors = {
                email: ""
            }
        }
        setSpinner(false)
    }
    return <>
        <div className="w-full lg:w-6/12 px-8 lg:px-4">
            <div className="text-2xl fonat-semibold text-slate-700 dark:text-slate-300">Let's keep in touch!</div>
            <div className=" mt-0 mb-2 text-slate-600 dark:text-slate-300">
                Please provide your email address to get any updates through your mail box
            </div>
            <div className="relative my-4 lg:mb-0">
                {spinner && <Spinner className="rounded-tl-3xl rounded-br-3xl" />}
                <Success data={success} hide={() => setSuccess({ title: "", description: "" })} />
                <Warning className="" errors={Object.values(warning.errors).filter(a => a != "")} hide={() => setWarning({ errors: errors })} />
                <form onSubmit={submit} className="flex space-x-2">
                    
                    <input type={"email"} id="email" name="email" required className="input" placeholder="your@mail.com" />
                    <button type="submit" className="btn btn-primary">Subscribe</button>
                </form>
            </div>
        </div>
    </>
}