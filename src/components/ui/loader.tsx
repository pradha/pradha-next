export const Spinner = (props: any) => {
    return (
        <div className={"absolute ease-in-out transition-all dark:opacity-60 opacity-25 delay-150 z-10 w-full h-full bg-slate-700  top-0 left-0 right-0 bottom-0 "+props.className}>
            <svg className="spinner" viewBox="0 0 50 50">
                <circle className="path" cx="25" cy="25" r="20" fill="none" strokeWidth="5"></circle>
            </svg>
        </div>
    )
}