'use client'
import { ExclamationTriangleIcon, XMarkIcon, CheckIcon } from "@heroicons/react/24/outline"
export const Warning = (props: any) => {
    return (
        <>
            {
                props.errors.length > 0 &&
                <div className={"flex w-full mb-4 rounded-lg shadow-lg " + (props?.className ? props.className : "")}>
                    <div className="flex items-center px-6 py-4 bg-yellow-600 rounded-l-lg">
                        <ExclamationTriangleIcon width={25} height={25} className="text-white" />
                    </div>
                    <div className="flex items-center w-full px-4 py-6 bg-white border border-gray-200 rounded-r-lg border-l-transparent">
                        <div className="flex-1 text-sm">
                            {props.errors.length == 1 && props.errors[0]}
                            {props.errors.length > 1 &&
                                <><span className="font-semibold">Please check this error<sup>(s)</sup>:</span>
                                    <ul className="ml-4 font-light list-disc">
                                        {props.errors.map((v: string) => <li key={Math.random()}>{v}</li>)}
                                    </ul>
                                </>
                            }
                        </div>
                        <button type="button" onClick={props.hide}><XMarkIcon className="" width={18} height={18} /> </button>
                    </div>
                </div>
            }
        </>
    )
}

export const Success = (props: any) => {
    return (
        <>
            {
                props?.data.title.length >= 1 &&
                <div className={"flex w-full mb-4 rounded-lg shadow-lg " + (props?.className ? props.className : "")}>
                    <div className="flex items-center px-6 py-4 bg-green-600 rounded-l-lg">
                        <CheckIcon width={25} height={25} className="text-white" />
                    </div>
                    <div className="flex items-center w-full px-4 py-6 bg-white border border-gray-200 rounded-r-lg border-l-transparent">
                        <div className="flex-1 text-sm">
                            <h3 className="mb-1 text-base font-semibold">{props.data.title}</h3>
                            <p>{props.data.description}</p>
                        </div>
                        <button type="button" onClick={props.hide}><XMarkIcon width={18} height={18} /> </button>
                    </div>
                </div>
            }
        </>
    )
}