import { ChevronLeftIcon, ChevronRightIcon } from "@heroicons/react/24/outline";
import { Fragment } from "react";

export const Pagination = (props: any) => {
    return <div className={"p-4 text-center "}>
        <div className={"flex flex-col space-x-2 items-center " + (props.count! <= 0 ? "hidden" : "")}>
            <div className="flex mb-4 text-xs text-gray-700">
                {props.page != 1 &&
                    <button key={Math.random()} onClick={() => props.click(props.page - 1)} className="btn btn-round dark:text-white">
                        <ChevronLeftIcon width={16} className="text-slate-700 dark:text-slate-300" />
                    </button>
                }
                <div className="flex h-8 space-x-1 font-medium rounded-full ">
                    {[...Array(props.total)].map((e, i) => {
                        if (props.page >= 11 && i <= 2)
                            return <Fragment>
                                <button key={Math.random()} onClick={() => props.click(i + 1)} className="items-center justify-center hidden w-8 leading-5 transition duration-150 ease-in bg-white border border-gray-200 rounded-full cursor-pointer md:flex">{i + 1}</button>
                                {i == 2 && <div className="items-center justify-center w-8 leading-5">...</div>}
                            </Fragment>

                        if (i > (props.page - 5) && i < (props.page + 3))
                            if (props.page == (i + 1)) {
                                return <button key={Math.random()} onClick={() => props.click(i + 1)} className="items-center justify-center hidden w-8 leading-5 text-white transition duration-150 ease-in rounded-full cursor-pointer md:flex bg-sky-400">{i + 1}</button>
                            } else {
                                return <button key={Math.random()} onClick={() => props.click(i + 1)} className="items-center justify-center hidden w-8 leading-5 transition duration-150 ease-in bg-white border border-gray-200 rounded-full cursor-pointer md:flex">{i + 1}</button>
                            }

                        if (i >= (props.total - 3))
                            return <Fragment>
                                {i == props.total - 3 && <div className="items-center justify-center w-8 leading-5">...</div>}
                                <button key={Math.random()} onClick={() => props.click(i + 1)} className="items-center justify-center hidden w-8 leading-5 transition duration-150 ease-in bg-white border border-gray-200 rounded-full cursor-pointer md:flex">{i + 1}</button>
                            </Fragment>
                    })}

                </div>
                {props.page < props.total &&
                    <button key={Math.random()} onClick={() => props.click(props.page + 1)} className="btn btn-round dark:text-white">
                        <ChevronRightIcon width={16} className="text-slate-700 dark:text-slate-300" />
                    </button>
                }
            </div>
        </div>
        {props.total >= 1 && <div className="text-sm text-gray-400">Page <span className="font-semibold">{props?.page}</span> From <span className="font-semibold">{props.total} </span></div>}
    </div>
}