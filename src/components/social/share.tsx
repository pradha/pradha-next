'use client'
import {
    EmailShareButton,
    EmailIcon,
    WhatsappShareButton,
    WhatsappIcon,
    FacebookShareButton,
    FacebookIcon,
    FacebookShareCount,
    FacebookMessengerShareButton,
    FacebookMessengerIcon,
    TwitterShareButton,
    TwitterIcon,
    LineShareButton,
    LineIcon,
    LinkedinShareButton,
    LinkedinIcon,
    PinterestShareButton,
    PinterestIcon,
    WeiboShareButton,
    WeiboIcon,
    RedditShareButton,
    RedditIcon,
    TelegramShareButton,
    TelegramIcon,
    LivejournalShareButton,
    LivejournalIcon,
} from 'next-share';

export const SocialShare = (props: any) => {
    return <div className={"flex flex-wrap "+props.className}>
        <EmailShareButton url={props.url} subject={props.title} body={props.description} >
            <EmailIcon size={32} round />
        </EmailShareButton>
        <WhatsappShareButton url={props.url} title={props.title} separator=":: " >
            <WhatsappIcon size={32} round />
        </WhatsappShareButton>
        <FacebookShareButton url={props.url} quote={props.title} hashtag={props?.hashtag ? props.hashtag : ""} >
            <FacebookIcon size={32} round />
            <FacebookShareCount url={props.url} />
        </FacebookShareButton>
        <FacebookMessengerShareButton url={props.url} appId={''} >
            <FacebookMessengerIcon size={32} round />
        </FacebookMessengerShareButton>
        <TwitterShareButton url={props.url} title={props.title} >
            <TwitterIcon size={32} round />
        </TwitterShareButton>
        <LinkedinShareButton url={props.url} title={props.title} >
            <LinkedinIcon size={32} round />
        </LinkedinShareButton>
        <LineShareButton url={props.url} title={props.title} >
            <LineIcon size={32} round />
        </LineShareButton>
        <PinterestShareButton url={props.url} media={props.title} >
            <PinterestIcon size={32} round />
        </PinterestShareButton>
        <RedditShareButton url={props.url} title={props.title} >
            <RedditIcon size={32} round />
        </RedditShareButton>
        <TelegramShareButton url={props.url} title={props.title} >
            <TelegramIcon size={32} round />
        </TelegramShareButton>
        <WeiboShareButton url={props.url} title={props.title} >
            <WeiboIcon size={32} round />
        </WeiboShareButton>
        <LivejournalShareButton url={props.url} title={props.title} >
            <LivejournalIcon size={32} round />
        </LivejournalShareButton>
    </div>
}