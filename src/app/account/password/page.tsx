'use client'
import { useSession } from "next-auth/react"
import { redirect } from 'next/navigation';
import { FormEvent, useState } from 'react'
import Link from "next/link";
import { UserIcon, KeyIcon, ArrowPathIcon } from "@heroicons/react/24/outline";
import { Success, Warning } from "@/components/ui/alert";
import { Spinner } from "@/components/ui/loader";

export default function Page() {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })

    let errors: string[] = []
    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState(errors)
    const [success, setSuccess] = useState({ title: "", description: "" })

    const submit = async (e: FormEvent) => {
        e.preventDefault()
        setWarning(errors)
        setSpinner(true)
        setSuccess({ title: "", description: "" })

        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement
        const submit = await fetch("/api/account/password", {
            method: 'PUT',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'X-CSRF-Token': '',
            },
            body: JSON.stringify({
                oldpassword: target.oldpassword.value,
                password: target.password.value,
                repassword: target.repassword.value,
            })
        });
        target.oldpassword.value = "";
        target.password.value = "";
        target.repassword.value = "";

        //await new Promise(resolve => setTimeout(resolve, 3000));
        try {
            const res = await submit.json()
            if (submit.ok) {
                setSuccess({ title: "Success", description: "Password has been successfully changed" })
            } else {
                errors = await res.errors
                setWarning(Object.values(errors).filter(a => a != ""))
                errors = []
            }
        } catch (err: any) {
            setWarning(err.message)
        }
        setSpinner(false)
    }
    return (
        <main>
            <div className="flex flex-col justify-between mb-4 space-y-4 md:flex-row md:space-y-0">
                <div>
                    <h1 className="text-xl font-semibold lg:text-2xl">Change Password</h1>
                    <div className="text-sm text-slate-500">Change My Account Password</div>
                </div>
                <div className="flex items-center space-x-2">
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a1002") &&
                        <Link href={"/account/profile"} title="My Profile" className="btn-circle" aria-label="group-data">
                            <UserIcon width={20} height={20} />
                        </Link>
                    }
                </div>
            </div>

            <div className="relative w-full p-6 bg-white shadow-lg rounded-tl-3xl rounded-br-3xl dark:bg-slate-700">
                {spinner && <Spinner className="rounded-tl-3xl rounded-br-3xl" />}
                <Success data={success} className="" hide={() => setSuccess({ title: "", description: "" })} />
                <form onSubmit={submit} className="ng-untouched ng-pristine ng-valid">
                    <Warning className="" errors={warning} hide={() => setWarning(errors)} />
                    <div className='flex flex-col space-x-0 lg:flex-row lg:space-x-4 sm:mb-2 lg:mb-0'>
                        <div className="flex-1 mb-2">
                            <label className="label" htmlFor="password">Old Password <sup className="text-rose-500">*</sup></label>
                            <div className="relative mb-2">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <KeyIcon width={20} height={20} className="text-gray-400" />
                                </div>
                                <input type="password" id="oldpassword" name='oldpassword' className={"input-icon"} placeholder="Old Password" />
                            </div>
                        </div>
                        <div className="flex-1 mb-2">
                            <label className="label" htmlFor="password">New Password <sup className="text-rose-500">*</sup></label>
                            <div className="relative mb-2">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <KeyIcon width={20} height={20} className="text-gray-400" />
                                </div>
                                <input type="password" id="password" name='password' className={"input-icon"} placeholder="Password" />
                            </div>
                        </div>
                        <div className="flex-1 mb-2">
                            <label className="label" htmlFor="repassword">Repeat Password <sup className="text-rose-500">*</sup></label>
                            <div className="relative mb-2">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <KeyIcon width={20} height={20} className="text-gray-400" />
                                </div>
                                <input type="password" id="repassword" name='repassword' className={"input-icon"} placeholder="Repeat Password" />
                            </div>
                        </div>
                    </div>
                    <div className="">
                        <button type="submit" className="items-center btn btn-primary">
                            <ArrowPathIcon width={20} height={20} className="mr-2" />
                            Change Password
                        </button>
                    </div>
                </form>
            </div>
        </main>
    )
}