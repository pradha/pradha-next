
'use client'
import { useSession } from "next-auth/react"
import { redirect } from 'next/navigation';
import { FormEvent, useState } from 'react'
import Link from "next/link";
import { ClockIcon, KeyIcon, UserCircleIcon, UserIcon, PencilSquareIcon, IdentificationIcon, EnvelopeIcon, CalendarIcon, CalendarDaysIcon, MapPinIcon, ArrowPathIcon } from "@heroicons/react/24/outline";
import Image from "next/image"
import { FullShortDay, ISODate } from "@/lib/time";
import { Spinner } from "@/components/ui/loader";
import { Success, Warning } from "@/components/ui/alert";

export default function Page(props: any) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })

    let errors: string[] = []
    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState(errors)
    const [success, setSuccess] = useState({ title: "", description: "" })
    const [data, setData] = useState(props.params.data)
    const [photo, setPhoto] = useState(data?.photo ? data?.photo : "/img/person.jpg")


    const submit = async (e: FormEvent) => {
        e.preventDefault()
        setWarning(errors)
        setSpinner(true)
        setSuccess({ title: "", description: "" })

        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement
        const submit = await fetch("/api/account/edit", {
            method: 'PUT',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                name: (document.getElementById("name") as HTMLInputElement).value,
                username: target.username.value,
                email: target.email.value,
                gender: target.gender.value,
                biography: target.biography.value,
                date_of_birth: target.date_of_birth.value,
                location: target.location.value
            })
        });
        //await new Promise(resolve => setTimeout(resolve, 3000));
        const res = await submit.json()
        if (submit.ok) {
            setSuccess({ title: "Success", description: "Your Profile has been successfully updated" })
        } else {
            errors = await res.errors
            setWarning(Object.values(errors).filter(a => a != ""))
            errors = []
        }
        setSpinner(false)
    }
    return (
        <main>
            <div className="flex flex-col justify-between mb-4 space-y-4 md:flex-row md:space-y-0">
                <div>
                    <h1 className="text-xl font-semibold lg:text-2xl">Edit Profile</h1>
                    <div className="text-sm text-slate-500">My Account Profile</div>
                </div>
                <div className="flex items-center space-x-2">
                    <Link href={"/account/profile"} title="Edit Profile" className="btn-circle" aria-label="edit-profile">
                        <UserIcon width={20} height={20} />
                    </Link>
                    <Link href={"/account/password"} title="Change Password" className="btn-circle" aria-label="change-password">
                        <KeyIcon width={20} height={20} />
                    </Link>
                </div>
            </div>

            <div className="relative w-full p-6 bg-white shadow-lg rounded-tl-3xl rounded-br-3xl dark:bg-slate-700">
                {spinner && <Spinner className=" rounded-tl-3xl rounded-br-3xl" />}
                <Success data={success} hide={() => setSuccess({ title: "", description: "" })} />
                <form onSubmit={submit} className="ng-untouched ng-pristine ng-valid">
                    <Warning className="" errors={warning} hide={() => setWarning(errors)} />
                    <div className="mb-4">
                        <label className="label" htmlFor="name">Full Name <sup className="text-rose-500">*</sup></label>
                        <div className="relative mb-2">
                            <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                <IdentificationIcon className="w-5 h-5 text-gray-400" />
                            </div>
                            <input type="name" id="name" name='name' defaultValue={data?.name} className={"input-icon"} placeholder="Full Name" />
                        </div>
                    </div>
                    <div className='flex flex-col mb-2 space-x-0 lg:flex-row lg:space-x-4'>
                        <div className="w-full mb-2 lg:w-1/4">
                            <label className="label" htmlFor="usermail">Username <sup className="text-rose-500">*</sup></label>
                            <div className="relative mb-2">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <UserIcon className="w-5 h-5 text-gray-400" />
                                </div>
                                <input type="text" id="username" name='username' defaultValue={data?.username} className={"input-icon"} placeholder="Username" />
                            </div>
                        </div>
                        <div className="flex-1 mb-2">
                            <label className="label" htmlFor="email">Email <sup className="text-rose-500">*</sup></label>
                            <div className="relative mb-2">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <EnvelopeIcon className="w-5 h-5 text-gray-400" />
                                </div>
                                <input type="email" id="email" name='email' defaultValue={data?.email} className={"input-icon"} placeholder="Email" />
                            </div>
                        </div>
                        <div className="w-full mb-2 lg:w-1/4">
                            <label className="label" htmlFor="gender">Gender <sup className="text-rose-500">*</sup></label>
                            <div className="relative inline-block w-full text-gray-700">
                                <select name="gender" id="gender" defaultValue={data?.gender ? data.gender : "Unknown"} className="select">
                                    <option value="Unknown">Unknown</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div className="mb-4">
                        <label htmlFor="biography" className="label">Biography</label>
                        <textarea name="biography" id="biography" placeholder="User Biography" className={"input"} rows={3} defaultValue={data.biography}></textarea>
                    </div>
                    <div className='flex flex-col mb-2 space-x-0 space-y-2 lg:flex-row lg:space-x-4 lg:space-y-0'>
                        <div className="w-full mb-2 lg:w-1/4">
                            <label className="label" htmlFor="date_of_birth">Date of Birth</label>
                            <div className="relative mb-2">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <CalendarDaysIcon className="w-5 h-5 text-gray-400" />
                                </div>
                                <input type="date" id="date_of_birth" defaultValue={data?.date_of_birth ? ISODate(data?.date_of_birth) : ""} name='date_of_birth' className="input-icon" placeholder="Date of Birth" />
                            </div>
                        </div>
                        <div className="flex-1 mb-2">
                            <label className="label" htmlFor="location">Location</label>
                            <div className="relative mb-2">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <MapPinIcon className="w-5 h-5 text-gray-400" />
                                </div>
                                <input type="location" id="location" name='location' defaultValue={data?.location} className={"input-icon"} placeholder="Location (ex. Jakarta, Indonesia)" />
                            </div>
                        </div>
                    </div>
                    <div className="">
                        <button type="submit" className="btn btn-primary">
                            <ArrowPathIcon width={20} height={20} className="mr-2" />
                            Update Account
                        </button>
                    </div>
                </form>
            </div>
        </main>
    )

}