import { authOptions } from "@/lib/auth/options";
import DashboardLayout from "@/components/ui/layouts/dashboard"
import clientPromise from "@/lib/connections/mongo";
import { getServerSession } from "next-auth/next"
import { redirect } from "next/navigation";

export const revalidate = 0
export const dynamic = 'force-dynamic'
export default async function RootLayout(props: any) {
    const session = await getServerSession(authOptions);
    if (!session)
        redirect('/');

    const db = (await clientPromise).db()
    props.params.data = JSON.parse(JSON.stringify(await db.collection("v_system_users").findOne({ email: session?.user.email })));

    return (
        <DashboardLayout>{props.children}</DashboardLayout>
    )

}

const title = 'Edit Profile'
const description = 'Edit My Profile Account'
const keywords = ["account", "profile", "account profile", "my profile"]
const url = process.env.SITE_URL + '/account/edit'
export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: false,
        follow: false,
        nocache: false,
        googleBot: {
            index: false,
            follow: false
        },
    },
}