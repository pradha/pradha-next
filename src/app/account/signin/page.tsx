'use client'
import Link from "next/link"
import { UserIcon, KeyIcon, EyeIcon, EyeSlashIcon } from "@heroicons/react/24/outline"
import Image from "next/image"
import { Spinner } from "@/components/ui/loader"
import { FormEvent, useCallback, useState, MouseEvent } from "react"
import { signIn, useSession } from "next-auth/react"
import { redirect } from 'next/navigation';
import { Warning } from "@/components/ui/alert"
import Copyright from "@/components/ui/copyright"
import { useGoogleReCaptcha } from "react-google-recaptcha-v3"

export default function Page() {
    const { data: session, status } = useSession({ required: false })
    if (status !== "loading" && session)
        redirect('/dashboard');

    const { executeRecaptcha } = useGoogleReCaptcha();
    let errors = {
        recaptcha: "",
        login: ""
    }
    const [spinner, setSpinner] = useState(false)
    const [password, setPassword] = useState("password")
    const [warning, setWarning] = useState({ errors: errors })

    const handleBeforeSubmit = useCallback((e: FormEvent) => {
        e.preventDefault();
        setWarning({ errors: errors })
        setSpinner(true)
        if (!executeRecaptcha) {
            //console.log("Execute recaptcha not yet available");
            return;
        }
        executeRecaptcha("enquiryFormSubmit").then((gReCaptchaToken) => {
            //console.log(gReCaptchaToken, "response Google reCaptcha server");
            submit(gReCaptchaToken, e);
        });
    }, [executeRecaptcha]);
    const submit = async (gReCaptchaToken: string, e: FormEvent) => {
        e.preventDefault()
        setWarning({ errors: errors })
        setSpinner(true)

        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement
        let usermail = target.usermail.value
        let password = target.password.value
        let remember = target.remember.checked
        let gRecaptchaToken = gReCaptchaToken
        target.password.value = ""
        const sign = await signIn('credentials', { usermail, password, remember, gRecaptchaToken, callbackUrl: '/dashboard', redirect: false })
        if (sign?.error && sign?.error == "") window.location.replace('/dashboard') // redirect('/dashboard')
        else if (sign?.error && sign?.error != "") {
            setWarning({ errors: { recaptcha: "", login: sign.error } })
            errors = {
                recaptcha: "",
                login: ""
            }
            setSpinner(false)
        }
    }
    return (
        <section className='grid content-center min-h-screen bg-slate-300 dark:bg-slate-600'>
            <div className="container mx-auto transition ease-in-out delay-150">
                <div className='flex justify-center px-3 my-6 lg:px-6 lg:my-12'>
                    <div className='flex w-full shadow-lg xl:w-3/4 lg:w-11/12 rounded-xl'>
                        <div className='content-center hidden w-full h-auto bg-gray-100 bg-cover bg-gradient-to-tr from-cyan-200 to-sky-500 lg:grid lg:w-1/2 rounded-l-xl'>
                            <Link href={"/"} title="Home Page"><Image className="hidden mx-auto mt-4 lg:block" src="/img/logo.webp" width={200} height={200} alt="logo" priority /></Link>
                        </div>
                        <div className='relative w-full p-3 bg-white lg:w-1/2 lg:p-5 dark:bg-slate-700 rounded-xl lg:rounded-l-none'>
                            {spinner && <Spinner className="rounded-xl lg:rounded-l-none" />}

                            <Link href={"/"} title="Home Page"><Image className="block mx-auto mt-4 lg:hidden" src="/img/logo.webp" width={50} height={50} alt="logo" priority /></Link>
                            <h3 className="pt-4 text-2xl text-center dark:text-gray-200">Welcome Back!</h3>
                            <div className="text-center dark:text-gray-200">Plase sign in to access application</div>


                            <form onSubmit={handleBeforeSubmit} className="px-4 pt-2 pb-4 mt-4 mb-4 rounded lg:px-8 lg:pt-4 lg:pb-8">
                                <Warning className="" errors={Object.values(warning.errors).filter(a => a != "")} hide={() => setWarning({ errors: errors })} />
                                <div className="mb-2">
                                    <label className="label" htmlFor="usermail">Username <sup className="text-rose-500">*</sup></label>
                                    <div className="relative mb-4">
                                        <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none ">
                                            <UserIcon className="w-5 h-5 text-gray-400" />
                                        </div>
                                        <input type="text" id="usermail" name='usermail' placeholder="Username / Email" className="input-icon" autoFocus />
                                    </div>
                                </div>
                                <div className="mb-2">
                                    <label className="label" htmlFor="password">Password <sup className="text-rose-500">*</sup></label>
                                    <div className="relative mb-4">
                                        <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                            <KeyIcon className="w-5 h-5 text-gray-400" />
                                        </div>
                                        <input type={password} id="password" name='password' placeholder="Password" className="input-icon" />
                                        <button type="button" className="absolute inset-y-0 right-0 flex items-center pr-3 cursor-pointer" onClick={() => setPassword(password=="text" ? "password" : "text")}>
                                            {password=="password" ? <EyeSlashIcon className="w-5 h-5 text-gray-400" /> : <EyeIcon className="w-5 h-5 text-gray-400" />}
                                        </button>
                                    </div>
                                </div>
                                <div className="mb-2">
                                    <div className="flex items-center">
                                        <input type="checkbox" id="remember" name="remember" placeholder="remember me" className="checkbox" />
                                        <label htmlFor="remember" className="pl-2 text-xs font-medium text-gray-600 cursor-pointer dark:text-gray-400">Remember me</label>
                                    </div>
                                </div>
                                <div className="mb-6 text-center">
                                    <button className="w-full btn btn-primary" type="submit">
                                        Sign In
                                    </button>
                                </div>
                                <hr className="mb-6 border-t dark:border-gray-600" />
                                <div className="text-center dark:text-slate-300">
                                    Not registered yet? &nbsp;
                                    <Link className="text-sm align-baseline iniline-block text-sky-500 hover:text-sky-600" href="/account/signup">
                                        Create an Account
                                    </Link>
                                </div>
                                <Link className="block text-sm text-center align-baseline text-sky-500 hover:text-sky-600" href="/account/forgot">
                                    Forgot Password
                                </Link>

                            </form>
                        </div>
                    </div>
                </div>
                <Copyright className="text-center" />
            </div>
        </section>
    )
}
