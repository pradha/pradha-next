'use client'
import Link from "next/link"
import { UserIcon } from "@heroicons/react/24/outline"
import Image from "next/image"
import { useSession } from "next-auth/react"
import { redirect } from 'next/navigation';
import { FormEvent, useCallback, useState } from "react"
import { Success, Warning } from "@/components/ui/alert"
import { ExclamationTriangleIcon, XMarkIcon, CheckIcon } from "@heroicons/react/24/outline"
import Copyright from "@/components/ui/copyright"
import { useGoogleReCaptcha } from "react-google-recaptcha-v3"
import { Spinner } from "@/components/ui/loader"

export default function Page() {
    const { data: session, status } = useSession({ required: false })
    if (status !== "loading" && session)
        redirect('/dashboard');

    const { executeRecaptcha } = useGoogleReCaptcha();
    let errors = {
        recaptcha: "",
        usermail: ""
    }
    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState({ errors: errors })
    const [success, setSuccess] = useState({ title: "", description: "" })

    const handleBeforeSubmit = useCallback((e: FormEvent) => {
        e.preventDefault();
        setWarning({ errors: errors })
        setSpinner(true)
        if (!executeRecaptcha) {
            //console.log("Execute recaptcha not yet available");
            return;
        }
        executeRecaptcha("enquiryFormSubmit").then((gReCaptchaToken) => {
            //console.log(gReCaptchaToken, "response Google reCaptcha server");
            submit(gReCaptchaToken, e);
        });
    }, [executeRecaptcha]);
    const submit = async (gReCaptchaToken: string, e: FormEvent) => {
        e.preventDefault()
        setWarning({ errors: errors })
        setSpinner(true)

        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement
        const submit = await fetch("/api/account/forgot", {
            method: 'POST',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                gRecaptchaToken: gReCaptchaToken,
                usermail: target.usermail.value
            })
        });
        //await new Promise(resolve => setTimeout(resolve, 3000));
        const res = await submit.json()
        if (submit.ok) {
            setSuccess({ title: "Check Your Email", description: "If your account exist in our database, we have sent you an email for confirmation to reset your password, please check your email" })
        } else {
            errors = await res.errors
            setWarning({ errors: errors })
            errors = {
                recaptcha: "",
                usermail: ""
            }
        }

        setSpinner(false)
    }

    return (
        <section className='grid content-center min-h-screen bg-slate-300 dark:bg-slate-600'>
            <div className="container mx-auto transition ease-in-out delay-150">
                <div className='flex justify-center px-3 my-6 lg:px-6 lg:my-12'>
                    <div className='flex w-full shadow-lg xl:w-1/2 lg:w-9/12 rounded-xl'>
                        <div className='relative w-full p-2 bg-white lg:p-4 dark:bg-slate-700 rounded-xl'>
                            <Image className="block mx-auto mt-4 lg:hidden" src="/img/logo.webp" width={50} height={50} alt="logo" loading='lazy' />
                            <h3 className="pt-4 text-2xl text-center dark:text-gray-200">Forgot Password</h3>
                            <div className="text-center dark:text-gray-200">Please provide your username or email</div>
                            {spinner && <Spinner className="rounded-xl" />}
                            <Success data={success} className="mt-16" hide={() => setSuccess({ title: "", description: "" })} />
                            {success?.title?.length == 0 &&
                                <form onSubmit={handleBeforeSubmit} className="px-4 pt-2 pb-4 mt-4 mb-4 rounded lg:px-8 lg:pt-4 lg:pb-8">
                                    <Warning className="" errors={Object.values(warning.errors).filter(a => a != "")} hide={() => setWarning({ errors: errors })} />
                                    <div className="mb-4">
                                        <label className="label" htmlFor="usermail">Username / Email <sup className="text-rose-500">*</sup></label>
                                        <div className="relative mb-2">
                                            <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                                <UserIcon className="w-5 h-5 text-gray-400" />
                                            </div>
                                            <input type="text" id="usermail" name='usermail' className={"input-icon"} placeholder="Username or Email" required />
                                        </div>
                                    </div>
                                    <div className="mb-6 text-center">
                                        <button className="w-full btn btn-primary" type="submit">Send Me a Confirmation Link</button>
                                    </div>
                                    <hr className="mb-6 border-t dark:border-gray-600" />
                                    <div className="text-center dark:text-slate-300">
                                        Not registered yet? &nbsp;
                                        <Link className="text-sm align-baseline iniline-block text-sky-500 hover:text-sky-600" href="/account/signup">
                                            Create an Account
                                        </Link>
                                    </div>
                                    <Link className="block text-sm text-center align-baseline text-sky-500 hover:text-sky-600" href="/account/signin">
                                        Sign In
                                    </Link>
                                </form>
                            }
                        </div>
                    </div>
                </div>
                <Copyright className="text-center" />
            </div>
        </section>
    )
}