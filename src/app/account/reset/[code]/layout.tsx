import clientPromise from "@/lib/connections/mongo";
import { notFound, } from "next/navigation";

export default async function ResetLayout({ children, params }: { children: React.ReactNode; params: { code: string } }) {
    const db = (await clientPromise).db()
    let verify = await db.collection("v_system_users").findOne({ reset: params.code });
    if (!verify)
        notFound()

    return (
        <>
            {children}
        </>
    )
}

const title = 'Reset Password'
const description = 'Reset my Account Password'
const keywords = ["reset", "password", "reset password", "account password", "account"]
const url = process.env.SITE_URL + '/account/reset'
export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: false,
        follow: false,
        nocache: false,
        googleBot: {
            index: false,
            follow: false
        },
    },
}