'use client'
import Link from "next/link"
import { KeyIcon } from "@heroicons/react/24/outline"
import Image from "next/image"
import { redirect } from 'next/navigation';
import { FormEvent, useState } from "react";
import { useSession } from "next-auth/react";
import { Success, Warning } from "@/components/ui/alert";
import Copyright from "@/components/ui/copyright";
import { Spinner } from "@/components/ui/loader";

async function getAccount(code: string) {
    const res = await fetch(`${process.env.SITE_URL}/api/account/reset?code=${code}`, { method: 'POST', credentials: 'include', cache: 'no-store' });
    return res.json();
}

export default function Page({ params: { code } }: { params: any }) {
    const { data: session, status } = useSession({ required: false })
    if (status !== "loading" && session)
        redirect('/dashboard');

    let errors: string[] = []
    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState(errors)
    const [success, setSuccess] = useState({ title: "", description: "" })
    const submit = async (e: FormEvent) => {
        e.preventDefault()
        setWarning(errors)
        setSpinner(true)

        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement

        const submit = await fetch(`/api/account/reset?code=${code}`, {
            method: 'PUT',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                password: target.password.value,
                repassword: target.repassword.value,
            })
        });
        //await new Promise(resolve => setTimeout(resolve, 3000));
        const res = await submit.json()
        if (submit.ok) {
            setSuccess({ title: "Success", description: "Your account password has been successfully changed" })
        } else {
            errors = await res.errors
            setWarning(Object.values(errors).filter(a => a != ""))
            errors = []
        }
        target.password.value = ""
        target.repassword.value = ""
        setSpinner(false)
    }
    return (
        <section className='grid content-center min-h-screen bg-slate-300 dark:bg-slate-600'>
            <div className="container mx-auto transition ease-in-out delay-150">
                <div className='flex justify-center px-3 my-6 lg:px-6 lg:my-12'>
                    <div className='flex w-full shadow-lg xl:w-3/4 lg:w-11/12 rounded-xl'>
                        <div className='relative w-full p-2 bg-white lg:p-4 dark:bg-slate-700 rounded-xl'>
                            {spinner && <Spinner className="rounded-xl" />}
                            <Image className="block mx-auto mt-4 lg:hidden" src="/img/logo.webp" width={50} height={50} alt="logo" loading='lazy' />
                            <h3 className="pt-4 text-2xl text-center dark:text-gray-200">Reset Password</h3>
                            <div className="text-center dark:text-gray-200">Reset your account password</div>

                            <Success data={success} className="mt-16" hide={() => setSuccess({ title: "", description: "" })} />

                            {success?.title?.length == 0 &&
                                <form onSubmit={submit} className="px-4 pt-2 pb-4 mt-4 mb-4 rounded lg:px-8 lg:pt-4 lg:pb-8">
                                    <Warning className="" errors={warning} hide={() => setWarning(errors)} />
                                    <div className='flex flex-col space-x-0 lg:flex-row lg:space-x-4 sm:mb-2 lg:mb-0'>
                                        <div className="flex-1 mb-2">
                                            <label className="label" htmlFor="password">Password <sup className="text-rose-500">*</sup></label>
                                            <div className="relative mb-2">
                                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                                    <KeyIcon className="w-5 h-5 text-gray-400" />
                                                </div>
                                                <input type="password" id="password" name='password' className={"input-icon"} placeholder="Password" />
                                            </div>
                                        </div>
                                        <div className="flex-1 mb-2">
                                            <label className="label" htmlFor="repassword">Repeat Password <sup className="text-rose-500">*</sup></label>
                                            <div className="relative mb-2">
                                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                                    <KeyIcon className="w-5 h-5 text-gray-400" />
                                                </div>
                                                <input type="password" id="repassword" name='repassword' className={"input-icon"} placeholder="Repeat Password" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="mb-6 text-center">
                                        <button className="w-full btn btn-primary" type="submit"> Reset My Password </button>
                                    </div>
                                    <hr className="mb-6 border-t dark:border-gray-600" />

                                    <Link className="block text-sm text-center align-baseline text-sky-500 hover:text-sky-600" href="/account/signin">
                                        Sign In
                                    </Link>
                                </form>
                            }
                        </div>
                    </div>
                </div>
                <Copyright className="text-center" />
            </div>
        </section>
    )

}

function setWarning(errors: any) {
    throw new Error("Function not implemented.");
}
function setSpinner(arg0: boolean) {
    throw new Error("Function not implemented.");
}

