'use client'
import { useSession } from "next-auth/react"
import { redirect, usePathname } from 'next/navigation';
import { ChangeEvent, FormEvent, Fragment, useEffect, useState } from 'react'
import Link from "next/link";
import { UserIcon, KeyIcon, ArrowPathIcon, XMarkIcon, IdentificationIcon, CheckIcon, PencilIcon, NoSymbolIcon, PlusIcon, MagnifyingGlassIcon } from "@heroicons/react/24/outline";
import { Success, Warning } from "@/components/ui/alert";
import { Spinner } from "@/components/ui/loader";
import { DayDateFormat, FullShort } from "@/lib/time";
import path from "path";
import { Pagination } from "@/components/ui/pagination";
import { Dialog, Transition } from "@headlessui/react";
import { FormatMoney } from "@/lib/text";

export default function Page() {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })

    const pathName = usePathname()
    let fill: any
    let errors: string[] = []
    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState(errors)
    const [success, setSuccess] = useState({ title: "", description: "" })
    const [data, setData] = useState(fill)
    const [src, setSrc] = useState(false)
    const [createBilling, setCreateBilling] = useState(false)
    const [billing, setBilling] = useState({
        type: "",
        name: "",
        description: "",
        amount: 0
    })
    const [payment, setPayment] = useState({
        type: "",
        amount: 0
    })
    const [pageNum, setPageNum] = useState(1)
    const oneYear = new Date()
    oneYear.setFullYear(oneYear.getFullYear() + 1)
    const selectBilling = (bil: ChangeEvent<HTMLSelectElement>) => {
        if (bil.target.value == "Annual Membership")
            setBilling({
                type: "annual-membership",
                name: "Annual Membership",
                description: "Annual Membership User " + session?.user.id + " (" + session?.user.email + ") Until : " + DayDateFormat(oneYear.toString()),
                amount: 100000
            })
        else
            setBilling({
                type: "",
                name: "",
                description: "",
                amount: 0
            })
    }

    const selectPayment = (payment: ChangeEvent<HTMLSelectElement>) => {
        if (payment.target.value == "")
            setPayment({
                type: payment.target.value,
                amount: billing.amount * Math.floor(Math.random() * 999)
            })
        else
            setPayment({
                type: payment.target.value,
                amount: billing.amount + Math.floor(Math.random() * 999)
            })
    }
    const submit = async (e: FormEvent) => {
        e.preventDefault()
        setWarning(errors)
        setSpinner(true)

        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        const submit = await fetch(path.posix.join("/api/", pathName as string), {
            method: 'POST',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                type: billing.type,
                name: billing.name,
                description: billing.description,
                amount: billing.amount,
                payment_method: payment.type,
                payment_amount: payment.amount
            })
        });
        //await new Promise(resolve => setTimeout(resolve, 3000));
        const res = await submit.json()
        if (submit.ok) {
            setData(await getData(pageNum))

            setSuccess({ title: "Success", description: "New billing has been created" })
        } else {
            errors = await res.errors
            setWarning(Object.values(errors).filter(a => a != ""))
            errors = []
        }
        setCreateBilling(false)
        setBilling({
            type: "",
            name: "",
            description: "",
            amount: 0
        })
        setPayment({
            type: "",
            amount: 0
        })
        setSpinner(false)

    }
    const search = async (e: FormEvent) => {
        e.preventDefault()
        setSpinner(true);
        let target = e.target as HTMLFormElement

        if (target.search.value.length > 0) {
            const res = await fetch(path.posix.join('/api/', pathName ? pathName : "", '/search'), {
                body: JSON.stringify({
                    search: target.search.value
                }),
                headers: {
                    'Content-Type': 'application/json',
                },
                method: 'POST'
            })

            const result = await res.json()
            setData({ data: result.search, count: result.search.length })
            setSrc(true)
        }
        setSpinner(false);
    }

    const getData = async (page: number) => {
        setSpinner(true);
        setPageNum(page)
        const data = await fetch('/api/' + (pathName ? pathName : "") + "?page=" + (page ? page : pageNum), {
            headers: { 'Content-Type': 'application/json' },
            method: 'GET'
        })
        setSpinner(false);
        return data.json()
    }

    const getPage = async (page: number) => {
        setSpinner(true);
        let data = await getData(page)
        setData(data)
        setSpinner(false);
    }
    var loaded = false;
    useEffect(() => {
        if (!loaded) {
            (async () => {
                let data = await getData(pageNum)
                setData(data)

            })();
            loaded = true
            return
        }
    }, [])
    return (
        <main>
            <div className="flex flex-col justify-between mb-4 space-y-4 md:flex-row md:space-y-0">
                <div>
                    <h1 className="text-xl font-semibold lg:text-2xl">Billing</h1>
                    <div className="text-sm text-slate-500">Account Billing</div>
                </div>
                <div className="flex items-center space-x-2">
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a1008") &&
                        <Fragment>
                            <div title="Create Billing" className="btn-circle" aria-label="create-billing" onClick={() => {
                                setBilling({
                                    type: "",
                                    name: "",
                                    description: "",
                                    amount: 0
                                }), setPayment({
                                    type: "",
                                    amount: 0
                                }), setCreateBilling(true)
                            }}>
                                <PlusIcon width={20} height={20} />
                            </div>
                            <Transition appear show={createBilling} as={Fragment}>
                                <Dialog as="div" className="relative z-10" onClose={() => setCreateBilling(false)}>
                                    <Transition.Child
                                        as={Fragment}
                                        enter="ease-out duration-300"
                                        enterFrom="opacity-0"
                                        enterTo="opacity-100"
                                        leave="ease-in duration-200"
                                        leaveFrom="opacity-100"
                                        leaveTo="opacity-0"
                                    >
                                        <div className="fixed inset-0 bg-white dark:bg-black dark:bg-opacity-75 blur-lg bg-opacity-75" />
                                    </Transition.Child>

                                    <div className="fixed inset-0 overflow-y-auto">
                                        <div className="flex min-h-full items-center justify-center p-4 text-center">
                                            <Transition.Child
                                                as={Fragment}
                                                enter="ease-out duration-300"
                                                enterFrom="opacity-0 scale-95"
                                                enterTo="opacity-100 scale-100"
                                                leave="ease-in duration-200"
                                                leaveFrom="opacity-100 scale-100"
                                                leaveTo="opacity-0 scale-95"
                                            >
                                                <Dialog.Panel className="w-full max-w-xl border border-slate-100 dark:border-slate-600 transform overflow-hidden rounded-2xl bg-white dark:bg-slate-700 p-6 text-left align-middle shadow-xl transition-all">
                                                    <Dialog.Title
                                                        as="h3"
                                                        className="text-lg flex justify-between font-medium leading-6 text-gray-900 dark:text-slate-100"
                                                    >
                                                        Create Billing
                                                        <XMarkIcon title="close" width={16} className="cursor-pointer  text-slate-400" onClick={() => setCreateBilling(false)} />
                                                    </Dialog.Title>
                                                    {spinner && <Spinner className=" rounded-tl-3xl rounded-br-3xl" />}

                                                    <form onSubmit={submit} className="mt-4">

                                                        <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                                                            <div className="flex-1 mb-2">
                                                                <label className="label" htmlFor="group">Billing</label>
                                                                <div className="relative inline-block w-full text-gray-700">
                                                                    <select name="billing" id="billing" className="select" defaultValue={""} onChange={(e) => selectBilling(e)}>
                                                                        <option value="">- Select Billing -</option>
                                                                        <option value="Annual Membership">Annual Membership</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        {billing.type != "" &&
                                                            <Fragment>
                                                                <div className="mb-4">
                                                                    <label className="label mb-0" htmlFor="name">Name</label>
                                                                    <strong className="dark:text-slate-400 text-slate-800">{billing.name}</strong>
                                                                </div>
                                                                <div className="mb-4">
                                                                    <label className="label mb-0" htmlFor="description">Description</label>
                                                                    <strong className="dark:text-slate-400 text-slate-800">{billing.description}</strong>
                                                                </div>
                                                                <div className="mb-4">
                                                                    <label className="label mb-0" htmlFor="amount">Amount</label>
                                                                    <strong className="dark:text-slate-400 text-slate-800">{FormatMoney(billing.amount)}</strong>
                                                                </div>
                                                                <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                                                                    <div className="flex-1 mb-2">
                                                                        <label className="label" htmlFor="payment_method">Payment Method</label>
                                                                        <div className="relative inline-block w-full text-gray-700">
                                                                            <select name="payment_method" id="payment_method" className="select" defaultValue={""} onChange={(e) => selectPayment(e)}>
                                                                                <option value="">- Select Billing -</option>
                                                                                <option value="BCA">(manual) Bank BCA 299-093-2959 a.n Aditya Y Pradhana</option>
                                                                                <option value="Mandiri">(manual) Bank Mandiri 134-000-690-049-1 a.n Aditya Y Pradhana</option>
                                                                                <option value="Jago">(manual) Bank Jago 5075-4305-7144 a.n Aditya Y Pradhana</option>
                                                                                <option value="BSI">(manual) Bank BSI 1016-8534-25 a.n Aditya Y Pradhana</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                {payment.type != "" &&
                                                                    <div className="mb-4">
                                                                        <label className="label mb-0" htmlFor="amount">Transfer Amount + Confirm Fee</label>
                                                                        <strong className="dark:text-slate-400 text-slate-800">{FormatMoney(payment.amount)}</strong>
                                                                    </div>
                                                                }
                                                            </Fragment>
                                                        }
                                                        <div className='flex flex-row space-x-4 mb-2'>
                                                            {payment.type != "" &&
                                                                <button className="btn btn-primary flex items-center space-x-2">
                                                                    <CheckIcon width={20} height={20} className="mr-2" />
                                                                    Confirm
                                                                </button>
                                                            }
                                                            <button
                                                                type="button"
                                                                className="inline-flex justify-center rounded-md border border-transparent bg-blue-100 px-4 py-2 text-sm font-medium text-blue-900 hover:bg-blue-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-blue-500 focus-visible:ring-offset-2"
                                                                onClick={() => setCreateBilling(false)}
                                                            >
                                                                Close
                                                            </button>
                                                        </div>
                                                    </form>
                                                </Dialog.Panel>
                                            </Transition.Child>
                                        </div>
                                    </div>
                                </Dialog>
                            </Transition>
                        </Fragment>
                    }
                    <form onSubmit={search} className="flex items-center flex-1 bg-white rounded-full h-9">
                        <div className="w-full">
                            <input type="search" id="search" name="search" required className="w-full px-4 py-1 text-sm border-none rounded-full outline-1 focus:ring-0 focus:outline-sky-100" placeholder="Search..." x-model="search" />
                        </div>
                        <div className="-ml-8">
                            <button type="submit" className="flex items-center justify-center w-8 h-8 rounded-full text-slate-100 bg-sky-500" >
                                <MagnifyingGlassIcon width={18} height={18} />
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <Success data={success} hide={() => setSuccess({ title: "", description: "" })} />
            <Warning className="" errors={warning} hide={() => setWarning(errors)} />
            <div className="relative w-full text-gray-600 bg-white shadow-lg rounded-tl-3xl rounded-br-3xl dark:bg-slate-700 dark:text-slate-300 transform transition-all ease-in-out">
                {spinner && <Spinner className=" rounded-tl-3xl rounded-br-3xl" />}
                <div className="flex items-center px-6 py-3 space-x-2 text-sm tracking-wide">
                    <div>Total {src ? "Search result" : "Data"} :</div>
                    <div className="font-semibold">{data?.count ? data.count : "0"}</div>
                    <div>Item{data?.count > 1 ? "s" : ""}</div>
                    {src &&
                        <button className="p-1 mx-2 text-slate-100 rounded-md shadow-sm bg-gradient-to-br from-sky-300 to-sky-400 hover:from-sky-300 hover:to-sky-300" onClick={() => { setSrc(false); getPage(pageNum); (document.getElementById("search") as HTMLInputElement).value = "" }} type="button">
                            <XMarkIcon width={16} />
                        </button>}
                </div>
                <div className="relative overflow-x-auto">
                    <table className="min-w-full border-b border-gray-300 divide-y divide-gray-200 table-auto">
                        <thead className="thead">
                            <tr className="">
                                <th className="thead-column">#</th>
                                <th className="thead-column">Name</th>
                                <th className="thead-column">Description</th>
                                <th className="thead-column">Amount</th>
                                <th className="thead-column">Status</th>
                                <th className="thead-column">Created</th>
                                {session?.user.group.roles.includes("61e3de6ecc4029a9d11a1009") &&
                                    <th className="thead-column">Action</th>
                                }
                            </tr>
                        </thead>
                        <tbody>
                            {typeof data !== undefined && data?.data?.map((item: any, i: number) =>
                                <tr key={item._id} className="transition-all ease-in-out delay-200 border-b border-gray-200 divide-slate-200 bg-gradient-to-br from-slate-50 odd:to-gray-100 even:to-slate-200 hover:from-sky-50 hover:to-sky-100">

                                    <td className="tbody-column"><div className='truncate w-16 md:w-max ' title={item._id}>{item._id}</div></td>
                                    <td className="tbody-column">{item.name}</td>
                                    <td className="tbody-column">{item.description}</td>
                                    <td className="tbody-column">{FormatMoney(item.amount.$numberDecimal)}</td>

                                    <td className="tbody-column">{item.status == "Confirmed" ? <CheckIcon title={item.status} width={20} className="text-green-600" /> : <NoSymbolIcon title={item.status} width={20} className="text-red-600" />}</td>
                                    <td className="tbody-column w-52">
                                        <div className="flex flex-col text-xs font-light">
                                            <div>{FullShort(item.created.at)}</div>
                                            <div><span className="font-semibold">{item.created?.by?.name}</span></div>
                                        </div>
                                    </td>
                                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a1009") &&
                                        <td className="w-32 tbody-column">
                                            <div className="flex flex-row space-x-2">
                                                {session?.user.group.roles.includes("61e3de6ecc4029a9d11a1009") &&
                                                    <Link href={path.posix.join('/system/billing/detail', item._id)} title="Billing Detail" className="p-1 rounded-md shadow-sm bg-gradient-to-br from-sky-400 to-sky-500 hover:from-sky-400 hover:to-sky-400">
                                                        <IdentificationIcon className="w-3 h-3 text-white" />
                                                    </Link>
                                                }
                                            </div>
                                        </td>
                                    }
                                </tr>
                            )}
                            {(!data || data?.count <= 0) &&
                                <tr>
                                    <td className="p-6 text-center italic text-sm" colSpan={(session?.user.group.roles.includes("61e3de6ecc4029a9d11a5007") || session?.user.group.roles.includes("61e3de6ecc4029a9d11a5009")) ? 8 : 7}>no data at this time</td>
                                </tr>
                            }
                        </tbody>
                    </table>
                    {!src && <Pagination count={data?.count} page={data?.page} total={data?.total} click={getPage} />}
                </div>
            </div>
        </main>
    )
}