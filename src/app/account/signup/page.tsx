'use client'
import Link from "next/link"
import { UserIcon, EnvelopeIcon, KeyIcon, IdentificationIcon, EyeSlashIcon, EyeIcon } from "@heroicons/react/24/outline"
import Image from "next/image"
import { useSession } from "next-auth/react"
import { redirect } from 'next/navigation';
import { FormEvent, useCallback, useState } from "react"
import { Spinner } from "@/components/ui/loader"
import { Success, Warning } from "@/components/ui/alert"
import { ExclamationTriangleIcon, XMarkIcon, CheckIcon } from "@heroicons/react/24/outline"
import Copyright from "@/components/ui/copyright"
import { useGoogleReCaptcha } from "react-google-recaptcha-v3"

export default function Page() {
    const { data: session, status } = useSession({ required: false })
    if (status !== "loading" && session)
        redirect('/dashboard');

    const { executeRecaptcha } = useGoogleReCaptcha();
    let errors = {
        recaptcha: "",
        username: "",
        email: "",
        password: "",
        repassword: "",
    }
    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState({ errors: errors })
    const [success, setSuccess] = useState({ title: "", description: "" })
    const [password, setPassword] = useState("password")
    const [repassword, setRePassword] = useState("password")

    const handleBeforeSubmit = useCallback((e: FormEvent) => {
        e.preventDefault();
        setWarning({ errors: errors })
        setSpinner(true)
        if (!executeRecaptcha) {
            // console.log("Execute recaptcha not yet available");
            return;
        }
        executeRecaptcha("enquiryFormSubmit").then((gReCaptchaToken) => {
            //console.log(gReCaptchaToken, "response Google reCaptcha server");
            submit(gReCaptchaToken, e);
        });
    }, [executeRecaptcha]);

    const submit = async (gReCaptchaToken: string, e: FormEvent) => {
        e.preventDefault()
        setWarning({ errors: errors })
        setSpinner(true)

        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement

        const submit = await fetch("/api/account/register", {
            method: 'POST',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                gRecaptchaToken: gReCaptchaToken,
                username: target.username.value,
                email: target.email.value,
                name: (document.getElementById("name") as HTMLInputElement).value,
                password: target.password.value,
                repassword: target.repassword.value,
            })
        });
        //await new Promise(resolve => setTimeout(resolve, 3000));
        const res = await submit.json()
        if (submit.ok) {
            setSuccess({ title: "Thank You For Signing Up", description: "We have sent you an email for confirmation & activate your account, please check your email" })
        } else {
            errors = await res.errors
            setWarning({ errors: errors })
            errors = {
                recaptcha: "",
                username: "",
                email: "",
                password: "",
                repassword: "",
            }
        }
        target.password.value = ""
        target.repassword.value = ""
        setSpinner(false)
    }
    return (
        <section className='grid content-center min-h-screen bg-slate-300 dark:bg-slate-600'>
            <div className="container mx-auto transition ease-in-out delay-150">
                <div className='flex justify-center px-3 my-6 lg:px-6 lg:my-12'>
                    <div className='flex w-full shadow-lg xl:w-3/4 lg:w-11/12 rounded-xl'>
                        <div className='content-center hidden w-full h-auto bg-gray-100 bg-cover bg-gradient-to-tr from-cyan-200 to-sky-500 lg:grid lg:w-1/2 rounded-l-xl'>
                            <Link href={"/"} title="Home Page"><Image className="hidden mx-auto mt-4 lg:block" src="/img/logo.webp" width={200} height={200} alt="logo" priority /></Link>
                        </div>
                        <div className='relative w-full p-2 bg-white lg:w-1/2 lg:p-4 dark:bg-slate-700 rounded-xl lg:rounded-l-none'>
                            {spinner && <Spinner className="rounded-xl lg:rounded-l-none" />}
                            <Link href={"/"} title="Home Page"><Image className="block mx-auto mt-4 lg:hidden" src="/img/logo.webp" width={50} height={50} alt="logo" priority /></Link>
                            <h3 className="pt-4 text-2xl text-center dark:text-gray-200">Sign Up</h3>
                            <div className="text-center dark:text-gray-200">Get access to application</div>

                            <Success data={success} className="mt-16" hide={() => setSuccess({ title: "", description: "" })} />

                            {success?.title?.length == 0 &&
                                <form onSubmit={handleBeforeSubmit} className="px-4 pt-2 pb-4 mt-4 mb-4 rounded lg:px-8 lg:pt-4 lg:pb-8">
                                    <Warning className="" errors={Object.values(warning.errors).filter(a => a != "")} hide={() => setWarning({ errors: errors })} />
                                    <div className='flex flex-col space-x-0 lg:flex-row lg:space-x-4 sm:mb-2 lg:mb-0'>
                                        <div className="flex-1 mb-2">
                                            <label className="label" htmlFor="usermail">Username <sup className="text-rose-500">*</sup></label>
                                            <div className="relative mb-2">
                                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                                    <UserIcon className="w-5 h-5 text-gray-400" />
                                                </div>
                                                <input type="text" id="username" name='username' className={"input-icon"} placeholder="Username" />
                                            </div>
                                        </div>
                                        <div className="flex-1 mb-2">
                                            <label className="label" htmlFor="email">Email <sup className="text-rose-500">*</sup></label>
                                            <div className="relative mb-2">
                                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                                    <EnvelopeIcon className="w-5 h-5 text-gray-400" />
                                                </div>
                                                <input type="email" id="email" name='email' className={"input-icon"} placeholder="Email" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="mb-4">
                                        <label className="label" htmlFor="name">Full Name <sup className="text-rose-500">*</sup></label>
                                        <div className="relative mb-2">
                                            <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                                <IdentificationIcon className="w-5 h-5 text-gray-400" />
                                            </div>
                                            <input type="text" id="name" name='name' className={"input-icon"} placeholder="Full Name" />
                                        </div>
                                    </div>
                                    <div className='flex flex-col space-x-0 lg:flex-row lg:space-x-4 sm:mb-2 lg:mb-0'>
                                        <div className="mb-2 flex-1">
                                            <label className="label" htmlFor="password">Password <sup className="text-rose-500">*</sup></label>
                                            <div className="relative mb-2">
                                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                                    <KeyIcon className="w-5 h-5 text-gray-400" />
                                                </div>
                                                <input type={password} id="password" name='password' className={"input-icon"} placeholder="Password" />
                                                <button type="button" className="absolute inset-y-0 right-0 flex items-center pr-3 cursor-pointer" onClick={() => setPassword(password == "text" ? "password" : "text")}>
                                                    {password == "password" ? <EyeSlashIcon className="w-5 h-5 text-gray-400" /> : <EyeIcon className="w-5 h-5 text-gray-400" />}
                                                </button>
                                            </div>
                                        </div>
                                        <div className="mb-2 flex-1">
                                            <label className="label" htmlFor="repassword">Repeat Password <sup className="text-rose-500">*</sup></label>
                                            <div className="relative mb-2">
                                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                                    <KeyIcon className="w-5 h-5 text-gray-400" />
                                                </div>
                                                <input type={repassword} id="repassword" name='repassword' className={"input-icon"} placeholder="Repeat" />
                                                <button type="button" className="absolute inset-y-0 right-0 flex items-center pr-3 cursor-pointer" onClick={() => setRePassword(repassword=="text" ? "password" : "text")}>
                                            {repassword=="password" ? <EyeSlashIcon className="w-5 h-5 text-gray-400" /> : <EyeIcon className="w-5 h-5 text-gray-400" />}
                                        </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="mb-6 text-center">
                                        <button className="w-full btn btn-primary" type="submit">Sign Up</button>
                                    </div>
                                    <hr className="mb-6 border-t dark:border-gray-600" />
                                    <div className="text-center dark:text-slate-300">Already have account?&nbsp;
                                        <Link className="inline-block text-sm align-baseline text-sky-500 hover:text-sky-600" href="/account/signin">
                                            Sign in
                                        </Link>
                                    </div>
                                    <Link className="block text-sm text-center align-baseline text-sky-500 hover:text-sky-600" href="/account/forgot">
                                        Forgot Password
                                    </Link>
                                </form>
                            }
                        </div>
                    </div>
                </div>
                <Copyright className="text-center" />
            </div>
        </section>
    )
}