import clientPromise from "@/lib/connections/mongo";
import { notFound } from "next/navigation";

export default async function RootLayout({ children, params }: { children: React.ReactNode; params: { code: string } }) {
    const db = (await clientPromise).db()
    let verify = await db.collection("v_system_users").findOne({ activation: params.code });
    if (!verify)
        notFound()

    return (
        <>
            {children}
        </>
    )
}

const title = 'Verifying Account'
const description =  'Verifying & Activating your account'
const keywords = ["verify", "active", "Verifying", "activating", "account"]
const url = process.env.SITE_URL + '/account/verify'
export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: false,
        follow: false,
        nocache: false,
        googleBot: {
            index: false,
            follow: false
        },
    },
}