import Link from "next/link"
import Image from "next/image"
import { notFound } from "next/navigation";

export const revalidate = 0;
async function getAccount(code: string) {
    const res = await fetch(`${process.env.SITE_URL}/api/account/verify?code=${code}`, { cache: 'no-store' });
    return res.json();
}

export default async function Page({ params: { code } }: { params: any }) {
    const account = await getAccount(code)
    if (account.status != "OK") {
        notFound
    } else
        return (
            <section className='grid content-center min-h-screen bg-slate-300 dark:bg-slate-600'>
                <div className="container mx-auto transition ease-in-out delay-150">
                    <div className='flex justify-center px-3 my-6 lg:px-6 lg:my-12'>
                        <div className='flex w-full shadow-lg xl:w-1/2 lg:w-10/12 rounded-xl'>
                            <div className='relative w-full p-2 bg-white lg:p-4 dark:bg-slate-700 rounded-xl'>
                                <Image className="block mx-auto mt-4 lg:hidden" src="/img/logo.webp" width={50} height={50} alt="logo" loading='lazy' />
                                <h3 className="pt-4 mb-2 text-2xl text-center dark:text-gray-200">Verified</h3>
                                <div className="mb-8 text-center dark:text-gray-200">Congratulation, Your account has been verified & activated. Now, you can sign in and access your dashboard data</div>
                                <div className="mb-2 text-center"><Link href={"/account/signin"} title={"Sign In"} className="w-1/3 px-0 mx-auto btn btn-primary">Sign In</Link></div>
                                <div className="text-center"><Link href={"/"} title={"Home Page"} className="w-1/3 mx-auto link">Home</Link></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
}