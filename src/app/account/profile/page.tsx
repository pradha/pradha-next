
'use client'
import { useSession } from "next-auth/react"
import { redirect } from 'next/navigation';
import { FormEvent, useState } from 'react'
import Link from "next/link";
import { ClockIcon, KeyIcon, UserCircleIcon, UserIcon, PencilSquareIcon } from "@heroicons/react/24/outline";
import Image from "next/image"
import { DayDateFormat, FullShortDay } from "@/lib/time";
import { Spinner } from "@/components/ui/loader";
import { Success, Warning } from "@/components/ui/alert";
import QRCode from "react-qr-code";

export default function Page(props: any) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })

    let errors: string[] = []
    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState(errors)
    const [success, setSuccess] = useState({ title: "", description: "" })
    const [data, setData] = useState(props.params.data)
    const [photo, setPhoto] = useState(data?.photo ? process.env.NEXT_PUBLIC_STATIC_FILES+data.photo : "/img/person.jpg")

    if (session) {
        const downloadBadge = async(id:string) => {
            if (data?.valid_thru && new Date() <= new Date(data.valid_thru)){
                window.open(`/api/account/badge?id=${data._id}`)
            } else {
                alert("Inactive account")
            }
        }
        const changePhoto = async (e: FormEvent) => {
            e.preventDefault()
            setWarning(errors)
            setSpinner(true)
            setSuccess({ title: "", description: "" })

            if (document.activeElement instanceof HTMLElement) {
                document.activeElement.blur();
            }
            const body = new FormData()
            const file = e.target as HTMLInputElement
            if (file && file.files) {
                body.append("file", file.files[0])
                try {
                    const submit = await fetch("/api/account/photo", {
                        method: "POST",
                        body
                    });
                    const res = await submit.json()

                    if (submit.ok) {
                        setPhoto(res.file.url + "?" + Math.random())
                        setSuccess({ title: "Success", description: res.message })
                    } else {
                        setWarning([res.msg])
                    }
                } catch (err: any) {
                    errors = [await err.message]
                    setWarning(errors)
                }
                file.value = ""
            }
            setSpinner(false)
        }
        return (
            <main>
                <div className="flex flex-col justify-between mb-4 space-y-4 md:flex-row md:space-y-0">
                    <div>
                        <h1 className="text-xl font-semibold lg:text-2xl">Profile</h1>
                        <div className="text-sm text-slate-500">My Account Profile</div>
                    </div>
                    <div className="flex items-center space-x-2">
                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a1003") &&
                            <Link href={"/account/edit"} title="Edit Profile" className="btn-circle" aria-label="edit-profile">
                                <PencilSquareIcon width={20} height={20} />
                            </Link>
                        }
                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a1005") &&
                            <Link href={"/account/password"} title="Change Password" className="btn-circle" aria-label="change-password">
                                <KeyIcon width={20} height={20} />
                            </Link>
                        }
                    </div>
                </div>
                <Success data={success} hide={() => setSuccess({ title: "", description: "" })} />
                <Warning className="" errors={warning} hide={() => setWarning(errors)} />
                <div className="relative flex flex-col items-stretch mb-4 space-x-0 lg:flex-row lg:space-x-4 ">
                    <div className="relative w-full text-gray-700 lg:w-1/4 xl:w-1/6 dark:text-slate-300">

                        <div className="sticky top-4 ">

                            <div className="relative overflow-hidden py-6 bg-white shadow-lg rounded-tl-3xl rounded-br-3xl group h-72 lg:h-56">
                                {spinner && <Spinner className="rounded-tl-3xl rounded-br-3xl" />}
                                <Image
                                    className="object-cover object-center w-full h-64 rounded-tl-3xl rounded-br-3xl"
                                    src={photo}
                                    alt="Person"
                                    fill
                                    priority
                                    sizes="(max-width: 768px) 100vw,(max-width: 1200px) 50vw,33vw"
                                />
                                {session?.user.group.roles.includes("61e3de6ecc4029a9d11a1004") &&
                                    <>
                                        <button onClick={() => (document.getElementById("photo") as HTMLInputElement).click()} className="absolute hidden w-32 px-1 py-2 mx-auto text-sm text-center transform -translate-x-1/2 bg-gray-200 border border-gray-500 rounded-lg shadow group-hover:block bottom-10 left-1/2 right-1/2 dark:text-slate-600">Change Photo</button>
                                        <input onChange={changePhoto} type="file" id="photo" name="photo" className="hidden" accept="image/png, image/gif, image/jpeg, image/webp" />
                                    </>
                                }
                            </div>
                        </div>
                    </div>
                    <div className="flex-1 text-slate-700 lg:w-3/4 xl:w-5/6">
                        <div className="p-6 mt-4 mb-2 bg-white shadow-lg rounded-tl-3xl rounded-br-3xl lg:mt-0 dark:bg-slate-700 dark:text-slate-300 lg:mb-4">
                            <div className="flex items-center content-center py-2 mb-4 space-x-2 border-b border-slate-200 dark:border-slate-600">
                                <UserIcon width={24} height={24} />
                                <span className="text-lg text-semibold">About</span>
                            </div>
                            <div className="flex flex-col px-2 space-x-0 lg:flex-row lg:space-x-4">
                                <div className="flex-1">
                                    <div className="flex flex-col mb-2">
                                        <div className="text-sm">User ID</div>
                                        <div className="font-semibold">{data?._id}</div>
                                    </div>
                                    <div className="flex flex-col mb-2">
                                        <div className="text-sm">Full Name</div>
                                        <div key={Math.random()} className="font-semibold">{data?.name}</div>
                                    </div>
                                    <div className="flex flex-col mb-2">
                                        <div className="text-sm">E-Mail</div>
                                        <div className="font-semibold">{data?.email}</div>
                                    </div>
                                    <div className="flex flex-col mb-2">
                                        <div className="text-sm">Date of Birth</div>
                                        <div className="font-semibold">{data?.date_of_birth ? DayDateFormat(data?.date_of_birth) : "-"}</div>
                                    </div>
                                </div>
                                <div className="flex-1">
                                    <div className="flex flex-col mb-2">
                                        <div className="text-sm">Username</div>
                                        <div className="font-semibold">{data?.username}</div>
                                    </div>
                                    <div className="flex flex-col mb-2">
                                        <div className="text-sm">Group Level</div>
                                        <div className="font-semibold">{data?.group?.name}</div>
                                    </div>
                                    <div className="flex flex-col mb-2">
                                        <div className="text-sm">Registered</div>
                                        <div className="font-semibold">{FullShortDay(data?.created?.at)}</div>
                                    </div>
                                    <div className="flex flex-col mb-2">
                                        <div className="text-sm">Valid Thru</div>
                                        <div className="font-semibold">{data?.valid_thru ? FullShortDay(data?.valid_thru) : "N/A"}</div>
                                    </div>
                                </div>
                                <div onClick={() => downloadBadge(data._id)} className="aspect-[1.2/1.8] cursor-pointer p-4 leading-3 text-center dark:text-black" style={{
                                    backgroundImage: "url(/img/badge-bg.jpg)", backgroundSize: "cover", backgroundRepeat: "no-repeat", backgroundPosition: "center center"
                                }}>
                                    <img
                                        className="h-24 w-24 rounded-full ring-2 ring-white object-cover object-center mx-auto mb-2"
                                        src={photo}
                                        alt="Person"
                                        loading="lazy"
                                    />
                                    <div className="font-semibold text-lg p-0 m-0">{data?.name}</div>
                                    <div className="m-0 mb-4 p-0">Anggota SKMN</div>
                                    <div className="font-semibold text-xs">No ID:<br /><span className="underline font-bold">{data?._id?.toString()}</span></div>
                                    <div className="font-semibold text-xs">VALID THRU : {data?.valid_thru ? DayDateFormat(data.valid_thru) : "N/A"}</div>
                                    <QRCode size={75} className={"my-4 mx-auto"} value={data._id} />
                                </div>
                            </div>
                        </div>
                        <div className="p-6 mt-4 mb-2 bg-white shadow-lg rounded-tl-3xl rounded-br-3xl lg:mt-0 dark:bg-slate-700 dark:text-slate-300 lg:mb-4">
                            <div className="flex items-center content-center py-2 mb-4 space-x-2 border-b border-slate-200 dark:border-slate-600">
                                <UserCircleIcon width={24} height={24} />
                                <span className="text-lg text-semibold">Biography</span>
                            </div>
                            <p>{data?.biography}</p>
                        </div>
                        <div className="p-6 mt-4 mb-2 bg-white shadow-lg rounded-tl-3xl rounded-br-3xl lg:mt-0 dark:bg-slate-700 dark:text-slate-300 lg:mb-4">
                            <div className="flex items-center content-center py-2 mb-4 space-x-2 border-b border-slate-200 dark:border-slate-600">
                                <ClockIcon width={24} height={24} />
                                <span className="text-lg text-semibold">Status &amp; Creation Info</span>
                            </div>
                            <div className="flex flex-col px-2 space-x-0 lg:flex-row lg:space-x-4">
                                <div className="flex-1">
                                    <div className="flex flex-col mb-2">
                                        <div className="text-sm">Status</div>
                                        <div className="font-semibold">Verification : {data?.status?.verified ? "Verified" : "Unverified"}</div>
                                        <div className="font-semibold">Active : {data?.status?.active ? "Active" : "Inactive"}</div>
                                    </div>
                                </div>
                                <div className="flex-1">
                                    <div className="flex flex-col mb-2">
                                        <div className="text-sm">Created</div>
                                        {data?.created &&
                                            <>
                                                <div className="font-semibold">{FullShortDay(data?.created?.at)}</div>
                                                {data?.created?.by && <div className="font-semibold">{data?.created?.by}</div>}
                                            </>
                                        }
                                    </div>
                                    <div className="flex flex-col mb-2">
                                        <div className="text-sm">Updated</div>
                                        {data && data?.updated && data?.updated?.at ?
                                            <>
                                                <div className="font-semibold">{FullShortDay(data?.updated?.at)}</div>
                                                {data?.updated?.by && <div className="font-semibold">{data?.updated?.by?.name}</div>}
                                            </> : <div className="font-semibold">Never</div>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main >
        )
    }
}