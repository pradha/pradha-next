import DashboardLayout from "@/components/ui/layouts/dashboard"
import clientPromise from "@/lib/connections/mongo";
import { getServerSession } from "next-auth/next"
import { getSession } from "next-auth/react";
import { redirect } from "next/navigation";
import { use } from "react";
import { authOptions } from "@/lib/auth/options";

export const revalidate = 0
export const dynamic = 'force-dynamic'
export default async function RootLayout(props: any) {
    const session = await getServerSession();
    if (!session)
        redirect('/');

    const db = (await clientPromise).db()
    props.params.data = JSON.parse(JSON.stringify(await db.collection("v_system_users").findOne({ email: session?.user.email })));

    return (
        <DashboardLayout>{props.children}</DashboardLayout>
    )

}

const title = 'My Profile'
const description = 'My Account Profile'
const keywords = ["profile", "account profile", "my profile"]
const url = process.env.SITE_URL + '/account/profile'
export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: false,
        follow: false,
        nocache: false,
        googleBot: {
            index: false,
            follow: false
        },
    },
}