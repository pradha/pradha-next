import { getServerSession } from "next-auth"
import { authOptions } from "@/lib/auth/options";
import clientPromise from "@/lib/connections/mongo"
import { ObjectId } from "mongodb"
import { createTypeReferenceDirectiveResolutionCache } from "typescript"

export async function GET(request: Request) {
    const session = await getServerSession(authOptions)
    const db = (await clientPromise).db()
    const { searchParams } = new URL(request.url)
    if (searchParams.get('slug')) {
        const post = await db.collection("v_content_posts").findOne({ slug: searchParams.get('slug') })
        let relateSrc = []
        for (var tag of post?.tags) {
            relateSrc.push({ tags: new RegExp(tag, "i") })
            relateSrc.push({ title: new RegExp(tag, "i") })
            relateSrc.push({ description: new RegExp(tag, "i") })
        }

        const related = JSON.parse(JSON.stringify(await db.collection("v_content_posts").find({ $or: relateSrc, status: "Published", _id: { $ne: post?._id } }).sort('published.at', -1).limit(8).toArray()))
        related?.forEach((data: any) => {
            delete data.content
            delete data.status
        })
        return new Response(JSON.stringify({ status: "OK", related }), { status: 200, headers: { 'Content-Type': 'application/json' } })
    }
    return new Response(JSON.stringify({ status: "Invalid" }), { status: 406, headers: { 'Content-Type': 'application/json' } })


}