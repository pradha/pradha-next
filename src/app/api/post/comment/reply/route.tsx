import { getServerSession } from "next-auth"
import { authOptions } from "@/lib/auth/options";
import clientPromise from "@/lib/connections/mongo"
import { ObjectId } from "mongodb"

const collection = "content_comments"

export async function GET(request: Request) {

    const db = (await clientPromise).db()
    const { searchParams } = new URL(request.url)
    let data: any
    let comments = searchParams.get('comments')?.split(",").map(s => new ObjectId(s))
    
    data = await db.collection("v_" + collection).find({ 'reply': { $exists: true }, status: 'Published', 'reply._id': {$in: comments} }).sort('created.at', 1).toArray()
    return new Response(JSON.stringify({ status: "OK", data }), { status: 200, headers: { 'Content-Type': 'application/json' } })
}