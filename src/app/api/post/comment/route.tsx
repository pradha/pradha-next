import { getServerSession } from "next-auth"
import { authOptions } from "@/lib/auth/options";
import clientPromise from "@/lib/connections/mongo"
import { ObjectId } from "mongodb"
import { VContentComments } from "@/lib/connections/views/content";

const collection = "content_comments"

export async function GET(request: Request) {

    const db = (await clientPromise).db()
    const { searchParams } = new URL(request.url)
    if (searchParams.get('id') && ObjectId.isValid(searchParams.get('id') as string)) {
        const data = await db.collection("v_" + collection).findOne({ _id: new ObjectId(searchParams.get('id') as string) })
        return new Response(JSON.stringify({ status: "OK", data }), { status: 200, headers: { 'Content-Type': 'application/json' } })
    } else if (searchParams.get('number') != null) {
        let data: any
        if (searchParams.get('type') && searchParams.get('type') == "all")
            data = await db.collection("v_" + collection).findOne({ number: parseInt(searchParams.get('number') as string) })
        else
            data = await db.collection("v_" + collection).findOne({ number: parseInt(searchParams.get('number') as string), status: "Active" })
        if (data)
            return new Response(JSON.stringify({ status: "OK", data }), { status: 200, headers: { 'Content-Type': 'application/json' } })
        else
            return new Response(JSON.stringify({ status: "INVALID" }), { status: 406, headers: { 'Content-Type': 'application/json' } })
    } else {
        let pageNum = searchParams.get('page') ? parseInt(searchParams.get('page') as string) : 1
        let dataPerPage = process.env.DATA_PER_PAGE ? parseInt(process.env.DATA_PER_PAGE) : 15
        let data: any
        if (searchParams.get('content'))
            data = await db.collection("v_" + collection).find({ 'content': new ObjectId(searchParams.get('content') as string), 'status': "Published", 'reply': { $exists: false } }).sort('created.at', -1).skip(dataPerPage * (pageNum - 1)).limit(dataPerPage).toArray()
        else
            data = await db.collection("v_" + collection).find({ 'reply': { $exists: false }, 'status': "Published", }).sort('created.at', -1).skip(dataPerPage * (pageNum - 1)).limit(dataPerPage).toArray()
        const count = await db.collection("v_" + collection).countDocuments();
        data.forEach((v: any) => {
            if (v.created && v.created.by) {
                delete v.created.by.password;
                delete v.created.by.address;
                delete v.created.by.reset;
                delete v.created.by.status;
            }
            if (v.updated && v.updated.by) {
                delete v.updated.by.password;
                delete v?.updated?.by.address;
                delete v?.updated?.by.reset;
                delete v?.updated?.by.status;
            }
        });
        return new Response(JSON.stringify({ status: "OK", data, count, page: pageNum, dataPerPage, total: Math.ceil(count / dataPerPage) }), { status: 200, headers: { 'Content-Type': 'application/json' } })
    }

}

export async function POST(request: Request) {
    const session = await getServerSession(authOptions)
    let errors = {
        auth: "",
        content: "",
        comment: ""
    }
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()

        if (body.content.trim().length < 1 || body.comment.length < 10) {
            if (body.content.trim().length < 1) errors.content = "Oops...!Post error"
            if (body.comment.trim().length < 10) errors.comment = "Please fill your comment, 10 chars minimum"
            return new Response(JSON.stringify({ status: "Invalid", errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        } else {
            body.content = new ObjectId(body.content)
            body.reply = body?.reply ? new ObjectId(body.reply) : null
            body.status = process.env.DEFAULT_COMMENT_STATUS || 'Pending'
            body.created = { 'at': new Date(), 'by': new ObjectId(session.user.id) }
            const insert = await db.collection(collection).insertOne(body)
            VContentComments()
            return new Response(JSON.stringify({ status: "OK", id: insert.insertedId, insert }), { status: 201, headers: { 'Content-Type': 'application/json' } })
        }
    } else {
        errors.auth = "Please sign in to post your comment"
        return new Response(JSON.stringify({ message: 'Unauthorized', errors }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}