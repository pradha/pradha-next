import clientPromise from "@/lib/connections/mongo"
import { contactMail, reportContactMail, sendMail } from "@/lib/mail"
import url from 'url'

export async function POST(request: Request) {
    let errors = {
        recaptcha: "",
        name: "",
        email: "",
        phone: "",
        message: ""
    }
    let body = await request.json()
    const db = (await clientPromise).db()
    const reCaptcha = await fetch("https://www.google.com/recaptcha/api/siteverify", {
        method: "POST",
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
        },
        body: `secret=${process.env.RECAPTCHA_SECRET_KEY}&response=${body.gRecaptchaToken}`,
    })
    const verify = await reCaptcha.json()

    if (verify.score <= 0.5 || body?.name.trim() == "" || body?.email.trim() == "" || body?.message.trim() == "") {
        if (verify.score <= 0.5) errors.recaptcha = "ReCaptcha score bellow 0.5"
        if (body?.name.trim() == "") errors.name = "Your name is required"
        if (body?.email.trim() == "") errors.email = "Your email is required"
        if (body?.message.trim() == "") errors.message = "Message is required"
        return new Response(JSON.stringify({ status: "Invalid", errors, verify }), {status: 406, headers: {'Content-Type': 'application/json'} })
    } else {
        const q = url.parse(request.headers.get('referer') as string)
        body.created = { 'at': new Date() }
        const contact = await db.collection('system_calls').insertOne(body)

        // notification to sender
        let message = contactMail(body.name, body.email, body.phone, body.message, process.env.NEXT_PUBLIC_SITE_URL + "//" + q.host)
        sendMail(process.env.NAME as string, body.email, "Message Received", message.text, message.html)

        // notification to developer
        message = reportContactMail(body.name, body.email, body.phone, body.message, process.env.NEXT_PUBLIC_SITE_URL + "//" + q.host)
        sendMail("Contact Mail", process.env.MAIL_ADMIN as string, "Message Received", message.text, message.html)

        return new Response(JSON.stringify({ status: "OK", data: contact }), {status: 201, headers: {'Content-Type': 'application/json'} })
    }
}