import { ImageResponse } from "@vercel/og";
import clientPromise from "@/lib/connections/mongo";
import { notFound } from "next/navigation";

const revalidate = 0

/*const getSurah = async (surah:string|null, verse:string|null) => {
    const db = (await clientPromise).db()
    let verses: any
    if (surah && verse) {
        verses = await db.collection("quran").aggregate([
            { $unwind: '$verses' },
            {
                $addFields: {
                    verses: {
                        name: '$name',
                        surahNum: '$number',
                        preBismillah: '$preBismillah'
                    }
                }
            },
            { $match: { 'verses.surahNum': surah, 'verses.number.inSurah': verse } },
            {
                $group: {
                    _id: '$verses.meta.page',
                    verses: { $push: '$verses' }
                }
            }
        ]).toArray()
        return verses.verses
    } else if (surah) {
        verses = await db.collection("quran").aggregate([
            { $unwind: '$verses' },
            {
                $addFields: {
                    verses: {
                        name: '$name',
                        surahNum: '$number',
                        preBismillah: '$preBismillah'
                    }
                }
            },
            { $match: { 'verses.surahNum': surah } },
            {
                $group: {
                    _id: '$verses.meta.page',
                    verses: { $push: '$verses' }
                }
            }
        ]).toArray()
        return verses.verses
    } else {
        return null
    }
}*/

const unicodeToChar = (text: string) => {
    return text.replace(/\\u[\dA-F]{4}/gi,
        function (match) {
            return String.fromCharCode(parseInt(match.replace(/\\u/g, ''), 16));
        });
}
export async function GET(request: Request) {
    const { searchParams } = new URL(request.url);
    let data: any
    if (searchParams.get('s') && searchParams.get('v'))
        data = await fetch(`${process.env.SITE_URL}/api/quran?s=${searchParams.get('s')}&v=${searchParams.get('v')}`, { cache: 'force-cache' });
    else if (searchParams.get('s'))
        data = await fetch(`${process.env.SITE_URL}/api/quran?s=${searchParams.get('s')}`, { cache: 'force-cache' });
    else
        notFound()
    data = await data.json()
    data = data.verses
    return new ImageResponse(
        (
            <main
                style={{
                    width: '100%',
                    height: '100%',
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    textAlign: 'center',
                    justifyContent: 'center',
                    background: 'linear-gradient(#456fe8, #294861)',
                    color: 'white',
                    padding: '2em'
                }}>
                <div style={{ display: 'flex', position: 'absolute', top: '1.5em', left: '1.5em', justifyContent: 'center', alignItems: 'center' }}>
                    <img src={process.env.SITE_URL + "/img/favicon/apple-touch-icon.png"} width="50" height="50" style={{ top: '1px', left: '1px', margin: '0 10px 0 0' }} />
                        <div style={{display: 'flex', flexDirection: 'column', fontSize: '25px'}}>
                            {process.env.NEXT_PUBLIC_APP_NAME}
                            <span style={{fontSize: '12px'}}>{process.env.SITE_URL}/quran/{searchParams.get('s')}{searchParams.get('v') ? ":"+searchParams.get('v') : ""}</span>
                            </div>
                </div>
                <h1 style={{ fontSize: '25px', marginBottom: '0.5em' }}>{data[0].verses[0].name.transliteration.id} [{searchParams.get('s')}{searchParams.get('v') ? ":" + searchParams.get('v') : ""}]</h1>
                {searchParams.get('v') &&
                    data[0].verses.map((verse: any, i: number) => {

                        const arabic = unicodeToChar(verse.text.arab)
                        return <div key={Math.random()} style={{ display: 'flex', flexDirection: 'column', marginBottom: '0.8em', textAlign: 'center', alignItems: 'center' }}>
                            <span style={{ textAlign: 'center', fontWeight: 'bold' }} key={Math.random()}>({verse.number.inSurah}) {verse.text.transliteration.en} </span>
                            <em style={{ fontStyle: 'italic', textAlign: 'center', fontSize: '13px' }} key={Math.random()}> {verse.translation.id} </em>
                        </div>

                    })
                }
                {!searchParams.get('v') && <div key={Math.random()} style={{ display: 'flex', flexDirection: 'column', marginBottom: '1em', textAlign: 'center', alignItems: 'center' }}>

                    <em style={{ fontStyle: 'italic', textAlign: 'center', fontSize: '0.5em' }} key={Math.random()}> {data[0].verses[0].tafsirSurah.id} </em>
                </div>
                }



            </main>
        ),
        {
            width: 1200,
            height: 600
        }
    )
}