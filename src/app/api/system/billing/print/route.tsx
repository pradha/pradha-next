import { NextApiRequest, NextApiResponse } from 'next';
import { NextRequest } from 'next/server';
import puppeteer from 'puppeteer';

const collection = 'system_billing'

const saveAsPdf = async (url: string) => {
    const browser = await puppeteer.launch({ headless: true });
    const page = await browser.newPage();

    await page.goto(url, {
        waitUntil: 'networkidle0',
    });

    const result = await page.pdf({
        format: 'a4',
    });
    await browser.close();

    return result;
};

export async function GET(req: NextRequest, res: NextApiResponse) {
    const { searchParams } = new URL(req.url as string)
    const id = searchParams.get("id")

    const pdf = await saveAsPdf(process.env.NEXT_PUBLIC_SITE_URL + '/system/billing/print/' + id)
    const responseHeaders = new Headers();
     // responseHeaders.set('Content-Disposition', `attachment; filename="billing-${id}.pdf"`);
      responseHeaders.set('Content-Type', 'application/pdf');
    return new Response(pdf, {
        status: 200,
        statusText: "OK",
        headers: responseHeaders,
      });
}