import clientPromise from "@/lib/connections/mongo";
import { ObjectId, Decimal128 } from "mongodb";
import { getServerSession } from "next-auth";
import { authOptions } from "@/lib/auth/options";
import path from 'path';
import fs from "fs";
import { createFolder } from "@/lib/folder";
import { NextRequest } from "next/server";
import slugify from "slugify";

const collection = 'system_billing'
let errors = {
    id: "",
    type: "",
    name: "",
    description: "",
    amount: "",
    payment_method: "",
    payment_amount: "",
    pay_date: "",
    exist: ''
}
export async function GET(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        const { searchParams } = new URL(request.url)
        if (searchParams.get('id') && ObjectId.isValid(searchParams.get('id') as string)) {
            const data = await db.collection("v_" + collection).findOne({ _id: new ObjectId(searchParams.get('id') as string) })
            return new Response(JSON.stringify({ status: "OK", data }), { status: 200, headers: { 'Content-Type': 'application/json' } })
        } else if (searchParams.get('number') != null) {
            let data: any
            if (searchParams.get('type') && searchParams.get('type') == "all")
                data = await db.collection("v_" + collection).findOne({ number: parseInt(searchParams.get('number') as string) })
            else
                data = await db.collection("v_" + collection).findOne({ number: parseInt(searchParams.get('number') as string), status: "Active" })
            if (data)
                return new Response(JSON.stringify({ status: "OK", data }), { status: 200, headers: { 'Content-Type': 'application/json' } })
            else
                return new Response(JSON.stringify({ status: "INVALID" }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        } else {
            let pageNum = searchParams.get('page') ? parseInt(searchParams.get('page') as string) : 1
            let dataPerPage = process.env.DATA_PER_PAGE ? parseInt(process.env.DATA_PER_PAGE) : 15
            const data = await db.collection("v_" + collection).find().sort('created.at', -1).skip(dataPerPage * (pageNum - 1)).limit(dataPerPage).toArray()
            const count = await db.collection("v_" + collection).countDocuments();
            data.forEach((v: any) => {
                if (v.created && v.created.by) {
                    delete v.created.by.password;
                    delete v.created.by.address;
                    delete v.created.by.reset;
                    delete v.created.by.status;
                }
                if (v.updated && v.updated.by) {
                    delete v.updated.by.password;
                    delete v?.updated?.by.address;
                    delete v?.updated?.by.reset;
                    delete v?.updated?.by.status;
                }
            });
            return new Response(JSON.stringify({ status: "OK", data, count, page: pageNum, dataPerPage, total: Math.ceil(count / dataPerPage) }), { status: 200, headers: { 'Content-Type': 'application/json' } })
        }

    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}
export async function POST(req: NextRequest, res: Response) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        const folder = "./public/uploads/" + new Date().getFullYear() + "/" + (new Date().getMonth() + 1).toString().padStart(2, '0')
        createFolder(folder)

        const formData = await req.formData()
        const file = formData.get('file') as File
        let newName: string;
        let type: string | null;

        if (formData.get('payment_method') == "" || formData.get('payment_amount') == "" || formData.get('pay_date') == "") {
            if (formData.get('payment_method') == "") errors.payment_method = "Payment method is empty"
            if (formData.get('payment_amount') == "") errors.payment_amount = "Payment Amount is empty"
            if (formData.get('pay_date') == "") errors.pay_date = "Pay Date is empty"
            return new Response(JSON.stringify({ status: "Invalid", errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        }
        const ab = await file.arrayBuffer()
        const bf = Buffer.from(ab)

        let size = Buffer.byteLength(bf) / 1024
        if (size > (parseInt(process.env.MAX_IMAGE_UPLOAD_SIZE as string) || 200)) {
            return new Response(
                JSON.stringify({
                    errors: { file: 'File too large, Please select file less than ' + (parseFloat(process.env.MAX_IMAGE_UPLOAD_SIZE as string) || 200) + 'KB' }
                }),
                {
                    status: 406
                }
            )
        }

        const allowedExt = process.env.ALLOWED_IMAGE_FILE_EXT?.split(",") || ["jpg", "jpeg", "webp", "png", "gif"]
        if (!allowedExt.includes(path.parse(file.name).ext.replace(".", ""))) {
            return new Response(
                JSON.stringify({
                    errors: { file: 'Allowed file type is ' + allowedExt.join(", ") }
                }),
                {
                    status: 406
                }
            )
        }

        newName = file.name
        if (file?.name) {
            newName = slugify(path.parse(file?.name).name, { lower: true, strict: true }) + path.parse(file?.name).ext
            let i = 1;
            while (true) {
                if (fs.existsSync(path.resolve(folder, newName))) {
                    i++;
                    newName = slugify(path.parse(file?.name).name, { lower: true, strict: true }) + '-' + i + path.parse(file?.name).ext
                } else break
            }
        }
        await fs.promises.writeFile(path.join(folder, newName), bf, { encoding: 'binary' })

        let filePath = path.posix.join(folder, newName).slice(path.posix.join(folder, newName).indexOf("/", 1))
        await db.collection(collection).updateOne({ _id: new ObjectId(formData.get('id') as string) }, {
            $set: {
                payment_method: formData.get('payment_method'),
                payment_amount: new Decimal128((formData.get('payment_amount') as string).replaceAll(",", "")),
                proof_of_payment: filePath,
                status: "Pending Confirmation",
                pay_date: new Date(formData.get('pay_date') as string),
                updated: { 'at': new Date(), 'by': new ObjectId(session.user.id) }
            }
        })
        let data = await db.collection(collection).findOne({ _id: new ObjectId(formData.get('id') as string) })
        return new Response(JSON.stringify({ message: 'Your payment has been updated', success: 1, file: { url: filePath }, data }), { status: 201, headers: { 'Content-Type': 'application/json' } })
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}

export async function PUT(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()
        if (body.id.trim().length < 1) {
            if (body.id.trim().length < 1) errors.id = "invalid name"
            return new Response(JSON.stringify({ status: "Invalid", errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        } else {
            body.status = "Confirmed"
            body.confirmed = { 'at': new Date(), 'by': new ObjectId(session.user.id) }
            let id = body.id
            delete body.id
            const update = await db.collection(collection).updateOne({ _id: new ObjectId(id) }, { $set: body })

            let data = await db.collection(collection).findOne({ _id: new ObjectId(id) })
            if (data?.type == "annual-membership") {
                const oneYear = new Date()
                oneYear.setFullYear(oneYear.getFullYear() + 1)
                await db.collection('system_users').updateOne({ _id: new ObjectId(data?.created.by) }, { $set: { 'valid_thru': oneYear } })
            }

            return new Response(JSON.stringify({ status: "OK", update, data }), { status: 202, headers: { 'Content-Type': 'application/json' } })
        }
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}