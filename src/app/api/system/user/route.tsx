import clientPromise from "@/lib/connections/mongo"
import { VSystemUsers } from "@/lib/connections/views/system"
import { ObjectId } from "mongodb"
import { getServerSession } from "next-auth"
import { authOptions } from "@/lib/auth/options";
import url from 'url'
import { registrationMail, sendMail } from "@/lib/mail";
import { NextRequest } from "next/server";

const collection = 'system_users'
let errors = {
    group: "",
    name: "",
    username: "",
    email: "",
    gender: "",
}
let email: any
let username: any;

export async function GET(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        const { searchParams } = new URL(request.url)
        if (searchParams.get('id') && ObjectId.isValid(searchParams.get('id') as string)) {
            const data = await db.collection("v_" + collection).findOne({ _id: new ObjectId(searchParams.get('id') as string) })
            return new Response(JSON.stringify({ status: true, message: "OK", data }), { status: 200, headers: { 'Content-Type': 'application/json' } })
        } else {
            let pageNum = searchParams.get('page') ? parseInt(searchParams.get('page') as string) : 1
            let dataPerPage = process.env.DATA_PER_PAGE ? parseInt(process.env.DATA_PER_PAGE) : 15
            const data = await db.collection("v_" + collection).find().skip(dataPerPage * (pageNum - 1)).limit(dataPerPage).toArray()
            const count = await db.collection("v_" + collection).countDocuments();
            if (data)
                data.forEach((v) => {
                    if (v?.created?.by) {
                        delete v.created.by.password;
                        delete v.created.by.address;
                        delete v.created.by.reset;
                        delete v.created.by.status;
                    }
                    if (v?.updated && v?.updated.by) {
                        delete v?.updated.by.password;
                        delete v?.updated?.by.address;
                        delete v?.updated?.by.reset;
                        delete v?.updated?.by.status;
                    }
                });
            return new Response(JSON.stringify({ status: true, message: "OK", data, count, page: pageNum, dataPerPage, total: Math.ceil(count / dataPerPage) }), { status: 200, headers: { 'Content-Type': 'application/json' } })
        }
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}

export async function POST(request: NextRequest) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()
        email = await db.collection('system_users').findOne({ 'email': body.email })
        username = await db.collection('system_users').findOne({ 'username': body.username })
        if (email || username || body.username.indexOf(' ') >= 0 || body.username.length < 5 || body.name.trim().length < 1 || body.name.trim().length < 1 || !body?.group || body?.group == "") {
            if (!body?.group || body?.group == "") errors.group = "please select group"
            if (email) errors.email = "email is invalid"
            if (username) errors.username = "username is invalid"
            if (body.username.indexOf(' ') >= 0) errors.username = "username cannot contain spaces"
            if (body.username.length < 5) errors.username = "username minimum 5 characters"
            if (body.email.trim().length < 1) errors.email = "invalid email"
            if (body.name.trim().length < 1) errors.name = "invalid name"
            return new Response(JSON.stringify({ status: "Invalid", errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        } else {
            if (body.date_of_birth != "") {
                body.date_of_birth = new Date(body.date_of_birth)
            }
            body.group = new ObjectId(body.group)
            body.created = { 'at': new Date(), 'by': new ObjectId(session.user.id) }
            const insert = await db.collection(collection).insertOne(body)

            VSystemUsers()
            const q = url.parse(request.nextUrl.href)

            let message = registrationMail(body.name, process.env.NEXT_PUBLIC_SITE_URL + "//" + q.host + "/account/verify/" + body.activation)
            sendMail("Registration", body.email, "Account Registration", message.text, message.html)
            return new Response(JSON.stringify({ status: "OK", id: insert.insertedId, insert }), { status: 201, headers: { 'Content-Type': 'application/json' } })
        }
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}

export async function PUT(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()
        email = await db.collection(collection).findOne({ 'email': body.email, _id: { $ne: new ObjectId(body.id) } })
        username = await db.collection(collection).findOne({ 'username': body.username, _id: { $ne: new ObjectId(body.id) } })
        if (email || username || (!body?.group || body?.group == "") || body.username.indexOf(' ') >= 0 || body.username.length < 5 || body.name.trim().length < 1 || body.name.trim().length < 1) {
            if (!body?.group && body?.group == "") errors.group = "please select group"
            if (email) errors.email = "email is invalid"
            if (username) errors.username = "username is invalid"
            if (body.username.indexOf(' ') >= 0) errors.username = "username cannot contain spaces"
            if (body.username.length < 5) errors.username = "username minimum 5 characters"
            if (body.email.trim().length < 1) errors.email = "invalid email"
            if (body.name.trim().length < 1) errors.name = "invalid name"
            return new Response(JSON.stringify({ status: "Invalid", errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        } else {
            if (body.date_of_birth != "") {
                body.date_of_birth = new Date(body.date_of_birth)
            }
            body.group = new ObjectId(body.group)
            body.updated = { 'at': new Date(), 'by': new ObjectId(session.user.id) }
            let id = body.id
            delete body.id
            const update = await db.collection(collection).updateOne({ _id: new ObjectId(id) }, { $set: body })

            return new Response(JSON.stringify({ status: "OK", update }), { status: 202, headers: { 'Content-Type': 'application/json' } })
        }

    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}
