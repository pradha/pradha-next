import clientPromise from "@/lib/connections/mongo"
import { CheckDigit } from "@/lib/text"
import { ObjectId } from "mongodb"
import { getServerSession } from "next-auth"

export async function GET(request: Request) {
    const session = await getServerSession()
    if (session) {
        const db = (await clientPromise).db()

        let groups = JSON.parse(JSON.stringify(await db.collection('v_system_groups').aggregate([
            {
                $group: {
                    _id: '$name',
                    roles: {$first: { $size: '$roles' }},
                    count: { $sum: 1 },
                },

            }
        ]).toArray()))
        let users = JSON.parse(JSON.stringify(await db.collection('v_system_users').aggregate([
            {
                $unwind: '$group'
            },
            {
                $group: {
                    _id: '$group.name',
                    count: { $sum: 1 }
                }
            },

        ]).toArray()))


        return new Response(JSON.stringify({ statue: true, message: "OK", groups, users }), { status: 200, headers: { 'Content-Type': 'application/json' } })

    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}