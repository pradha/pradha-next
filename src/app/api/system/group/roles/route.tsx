import clientPromise from "@/lib/connections/mongo"
import { CheckDigit } from "@/lib/text"
import { ObjectId } from "mongodb"
import { getServerSession } from "next-auth"

export async function GET(request: Request) {
    const session = await getServerSession()
    if (session) {
        const db = (await clientPromise).db()
        const roles = await db.collection("system_roles").find().toArray()
        return new Response(JSON.stringify({ status: true, message: "OK", roles }), { status: 200, headers: { 'Content-Type': 'application/json' } })
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}