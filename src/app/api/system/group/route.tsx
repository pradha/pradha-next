import clientPromise from "@/lib/connections/mongo"
import { VSystemGroups } from "@/lib/connections/views/system"
import { ObjectId, PushOperator } from "mongodb"
import { getServerSession } from "next-auth"
import { authOptions } from "@/lib/auth/options";

const collection = 'system_groups'
let errors = {
    name: "",
    description: "",
    type: "",
    group: "",
    role: "",
}
export async function GET(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        const { searchParams } = new URL(request.url)
        if (searchParams.get('id') && ObjectId.isValid(searchParams.get('id') as string)) {
            const data = await db.collection("v_" + collection).findOne({ _id: new ObjectId(searchParams.get('id') as string) })
            return new Response(JSON.stringify({ status: true, message: "OK", data }), { status: 200, headers: { 'Content-Type': 'application/json' } })
        } else {
            let pageNum = searchParams.get('page') ? parseInt(searchParams.get('page') as string) : 1
            let dataPerPage = process.env.DATA_PER_PAGE ? parseInt(process.env.DATA_PER_PAGE) : 15
            const data = await db.collection("v_" + collection).find().skip(dataPerPage * (pageNum - 1)).limit(dataPerPage).toArray()
            const count = await db.collection("v_" + collection).countDocuments();
            if (data)
                data.forEach((v) => {
                    if (v?.updated?.by) {
                        delete v.created.by.password;
                        delete v.created.by.address;
                        delete v.created.by.reset;
                        delete v.created.by.status;
                    }
                    if (v.updated && v.updated.by) {
                        delete v.updated.by.password;
                        delete v?.updated?.by.address;
                        delete v?.updated?.by.reset;
                        delete v?.updated?.by.status;
                    }
                });
            return new Response(JSON.stringify({ status: true, message: "OK", data, count, page: pageNum, dataPerPage, total: Math.ceil(count / dataPerPage) }), { status: 200, headers: { 'Content-Type': 'application/json' } })
        }
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}

export async function POST(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()
        if (body.name.trim().length < 1 || body.description.trim().length < 1) {
            if (body.name.trim().length < 1) errors.name = "invalid name"
            if (body.description.trim().length < 1) errors.description = "invalid description"
            return new Response(JSON.stringify({ status: "Invalid", errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        } else {
            body.created = { 'at': new Date(), 'by': new ObjectId(session.user.id) }
            const insert = await db.collection(collection).insertOne(body)

            // Create Group View
            VSystemGroups()
            return new Response(JSON.stringify({ status: "OK", id: insert.insertedId, insert }), { status: 201, headers: { 'Content-Type': 'application/json' } })
        }
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}

export async function PUT(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()
        if (body.name.trim().length < 1 || body.description.trim().length < 1) {
            if (body.name.trim().length < 1) errors.name = "invalid name"
            if (body.description.trim().length < 1) errors.description = "invalid description"
            return new Response(JSON.stringify({ status: "Invalid", errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        } else {
            body.updated = { 'at': new Date(), 'by': new ObjectId(session.user.id) }
            let id = body.id
            delete body.id
            const update = await db.collection(collection).updateOne({ _id: new ObjectId(id) }, { $set: body })

            return new Response(JSON.stringify({ status: "OK", update }), { status: 202, headers: { 'Content-Type': 'application/json' } })
        }
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}

export async function OPTIONS(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()
        if (body.group.trim().length < 1 || body.role.trim().length < 1) {
            if (body.group.trim().length < 1) errors.group = "invalid group"
            if (body.role.trim().length < 1) errors.role = "invalid role"
            return new Response(JSON.stringify({ status: "Invalid", errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        } else {
            let update: any
            if (body.type) {
                update = await db.collection("system_groups").findOneAndUpdate({ _id: new ObjectId(body.group) }, { $push: { roles: new ObjectId(body.role) } } as unknown as PushOperator<Document>)
            } else {
                update = await db.collection("system_groups").findOneAndUpdate({ _id: new ObjectId(body.group) }, { $pull: { roles: new ObjectId(body.role) } } as unknown as PushOperator<Document>)
            }
            const data = await db.collection("system_groups").findOne({ _id: new ObjectId(body.group) })
            const roles = await db.collection("system_roles").find().toArray()
            return new Response(JSON.stringify({ status: "OK", update, data, roles }), { status: 202, headers: { 'Content-Type': 'application/json' } })
        }
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}