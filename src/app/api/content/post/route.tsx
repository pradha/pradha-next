import clientPromise from "@/lib/connections/mongo";
import { ObjectId, Decimal128 } from "mongodb";
import { getServerSession } from "next-auth";
import { authOptions } from "@/lib/auth/options";
import { VAccountingAccounts } from "@/lib/connections/views/accounting";
import { VContentCategories, VContentPosts } from "@/lib/connections/views/content";
import slugify from "slugify";

const collection = 'content_posts'
let errors = {
    title: "",
    content: "",
    tags: "",
    description: "",
    categories: "",
    thumbnail: "",
    thumbnail_caption: "",
    language: ""
}
export async function GET(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        const { searchParams } = new URL(request.url)
        if (searchParams.get('id') && ObjectId.isValid(searchParams.get('id') as string)) {
            const data = await db.collection("v_" + collection).findOne({ _id: new ObjectId(searchParams.get('id') as string) })
            return new Response(JSON.stringify({ status: "OK", data }), { status: 200, headers: { 'Content-Type': 'application/json' } })
        } else if (searchParams.get('number') != null) {
            let data: any
            if (searchParams.get('type') && searchParams.get('type') == "all")
                data = await db.collection("v_" + collection).findOne({ number: parseInt(searchParams.get('number') as string) })
            else
                data = await db.collection("v_" + collection).findOne({ number: parseInt(searchParams.get('number') as string), status: "Active" })
            if (data)
                return new Response(JSON.stringify({ status: "OK", data }), { status: 200, headers: { 'Content-Type': 'application/json' } })
            else
                return new Response(JSON.stringify({ status: "INVALID" }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        } else {
            let pageNum = searchParams.get('page') ? parseInt(searchParams.get('page') as string) : 1
            let dataPerPage = process.env.DATA_PER_PAGE ? parseInt(process.env.DATA_PER_PAGE) : 15
            const data = await db.collection("v_" + collection).find().sort('created.at', -1).skip(dataPerPage * (pageNum - 1)).limit(dataPerPage).toArray()
            const count = await db.collection("v_" + collection).countDocuments();
            data.forEach((v: any) => {
                if (v.created && v.created.by) {
                    delete v.created.by.password;
                    delete v.created.by.address;
                    delete v.created.by.reset;
                    delete v.created.by.status;
                }
                if (v.updated && v.updated.by) {
                    delete v.updated.by.password;
                    delete v?.updated?.by.address;
                    delete v?.updated?.by.reset;
                    delete v?.updated?.by.status;
                }
            });
            return new Response(JSON.stringify({ status: "OK", data, count, page: pageNum, dataPerPage, total: Math.ceil(count / dataPerPage) }), { status: 200, headers: { 'Content-Type': 'application/json' } })
        }

    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}

export async function POST(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()
        if (body.title.trim() == "" || !body.content || (body.content && body.content?.blocks.length < 1) || body.tags.length <= 0 || body.description.trim() == "" || body.categories.length <= 0 || body.thumbnail.trim() == "" || body.thumbnail_caption == "" || body.language == "") {
            if (body.title.trim() == "") errors.title = "please provide post title"
            if (!body.content || (body.content && body.content?.blocks.length < 1)) errors.content = "content can't empty"
            if (body.tags.length <= 0) errors.tags = "please add some tags"
            if (body.description.trim() == "") errors.description = "please add some description"
            if (body.categories.length <= 0) errors.categories = "please select category"
            if (body.thumbnail.trim() == "") errors.thumbnail = "please add image thumbnail"
            if (body.thumbnail_caption.trim() == "") errors.thumbnail_caption = "please add thumbnail caption"
            if (body.language.trim() == "") errors.language = "please select language"
            return new Response(JSON.stringify({ status: "Invalid", errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        } else {
            let slug = slugify(body.title.trim(), { lower: true, strict: true })
            let i = 1
            while (true) {
                let check = await db.collection('content_posts').findOne({ slug: slug })

                if (!check)
                    break
                else slug = slug + "-" + (i + 1).toString()
            }
            body.slug = slug
            body.categories = body.categories.map((cat: string) => new ObjectId(cat))
            body.status = "Draft"
            body.created = { 'at': new Date(), 'by': new ObjectId(session.user?.id) }
            const insert = await db.collection(collection).insertOne(body)

            VContentPosts()
            return new Response(JSON.stringify({ status: "OK", id: insert.insertedId, insert }), { status: 201, headers: { 'Content-Type': 'application/json' } })
        }
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}

export async function PUT(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()
        if (body.title.trim() == "" || !body.content || (body.content && body.content?.blocks.length < 1) || body.tags.length <= 0 || body.description.trim() == "" || body.categories.length <= 0 || body.thumbnail.trim() == "" || body.thumbnail_caption == "" || body.language == "") {
            if (body.title.trim() == "") errors.title = "please provide post title"
            if (!body.content || (body.content && body.content?.blocks.length < 1)) errors.content = "content can't empty"
            if (body.tags.length <= 0) errors.tags = "please add some tags"
            if (body.description.trim() == "") errors.description = "please add some description"
            if (body.categories.length <= 0) errors.categories = "please select category"
            if (body.thumbnail.trim() == "") errors.thumbnail = "please add image thumbnail"
            if (body.thumbnail_caption.trim() == "") errors.thumbnail_caption = "please add thumbnail caption"
            if (body.language.trim() == "") errors.language = "please select language"
            return new Response(JSON.stringify({ status: "Invalid", errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        } else {
            body.categories = body.categories.map((cat: string) => new ObjectId(cat))
            body.updated = { 'at': new Date(), 'by': new ObjectId(session.user.id) }
            let id = body.id
            delete body.id
            const update = await db.collection(collection).updateOne({ _id: new ObjectId(id) }, { $set: body })
            return new Response(JSON.stringify({ status: "OK", update }), { status: 201, headers: { 'Content-Type': 'application/json' } })
        }
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}

export async function OPTIONS(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()
        if (!body.id || (body.id && !ObjectId.isValid(body.id))) {
            errors.title = "ID Not Found"
        } else {
            body.status = "Published"
            body.published = { 'at': new Date(), 'by': new ObjectId(session.user?.id) }
            const publish = await db.collection('content_posts').updateOne({ _id: new ObjectId(body.id) }, { $set: body })
            const data = await db.collection('v_content_posts').findOne({ _id: new ObjectId(body.id) })
            return new Response(JSON.stringify({ status: "OK", publish, data }), { status: 201, headers: { 'Content-Type': 'application/json' } })
        }
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}
