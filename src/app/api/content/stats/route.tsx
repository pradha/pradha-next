import clientPromise from "@/lib/connections/mongo"
import { CheckDigit } from "@/lib/text"
import { ObjectId } from "mongodb"
import { getServerSession } from "next-auth"

export async function GET(request: Request) {
    const session = await getServerSession()
    if (session) {
        const db = (await clientPromise).db()

        let posts = JSON.parse(JSON.stringify(await db.collection('content_posts').aggregate([{
            $group: {
                _id: '$status',
                count: { $sum: 1 }
            }
        }]).toArray()))
        let categories = JSON.parse(JSON.stringify(await db.collection('v_content_posts').aggregate([
            {
                $unwind: '$categories'
            },
            {
                $group: {
                    _id: '$categories.name',
                    count: { $sum: 1 }
                }
            }
        ]).toArray()))

        let dailyPosts = JSON.parse(JSON.stringify(await db.collection('content_posts').aggregate([
            {
                $group: {
                    _id: { $dateTrunc: { date: "$published.at", unit: "day", timezone: "Asia/Jakarta" } },
                    total: { $count: {} }
                }
            },
            { $densify: { field: "_id", range: { step: 1, unit: "day", bounds: "full" } } },
            {
                $match: {
                    "_id": { $gte: new Date(new Date().getTime() - 7 * 24 * 60 * 60 * 1000) }
                }
            }
        ]).toArray()))
        return new Response(JSON.stringify({ statue: true, message: "OK", posts, categories, dailyPosts }), { status: 200, headers: { 'Content-Type': 'application/json' } })

    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}