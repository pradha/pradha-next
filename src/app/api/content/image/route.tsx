import clientPromise from "@/lib/connections/mongo";
import { ObjectId, Decimal128 } from "mongodb";
import { getServerSession } from "next-auth";
import { authOptions } from "@/lib/auth/options";
import path from 'path';
import fs from "fs";
import { createFolder } from "@/lib/folder";
import { NextRequest } from "next/server";
import slugify from "slugify";
import { VContentMedia } from "@/lib/connections/views/content";

const collection = 'content_media'
let errors = {
    name: "",
    description: "",
    tags: "",
    file: ""
}

export async function POST(req: NextRequest, res: Response) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        const folder = "./public/uploads/" + new Date().getFullYear() + "/" + (new Date().getMonth() + 1).toString().padStart(2, '0')
        createFolder(folder)

        const formData = await req.formData()
        const file = formData.get('file') as File
        let newName: string;
        let type: string | null;

        if (!file || typeof file === 'string') {
            if (!file || typeof file === 'string') errors.file = "No file selected"
            return new Response(JSON.stringify({ status: "Invalid", errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        }
        const ab = await file.arrayBuffer()
        const bf = Buffer.from(ab)

        let size = Buffer.byteLength(bf) / 1024
        if (size > (parseInt(process.env.MAX_IMAGE_UPLOAD_SIZE as string) || 200)) {
            return new Response(
                JSON.stringify({
                    errors: {file: 'File too large, Please select file less than ' + (parseFloat(process.env.MAX_IMAGE_UPLOAD_SIZE as string) || 200) + 'KB' }
                }),
                {
                    status: 406
                }
            )
        }

        const allowedExt = process.env.ALLOWED_IMAGE_FILE_EXT?.split(",") || ["jpg", "jpeg", "webp", "png", "gif"]
        if (!allowedExt.includes(path.parse(file.name).ext.replace(".", ""))) {
            return new Response(
                JSON.stringify({
                    errors: {file: 'Allowed file type is ' + allowedExt.join(", ")}
                }),
                {
                    status: 406
                }
            )
        }

        newName = file.name
        if (file?.name) {
            newName = slugify(path.parse(file?.name).name, { lower: true, strict: true }) + path.parse(file?.name).ext
            let i = 1;
            while (true) {
                if (fs.existsSync(path.resolve(folder, newName))) {
                    i++;
                    newName = slugify(path.parse(file?.name).name, { lower: true, strict: true }) + '-' + i + path.parse(file?.name).ext
                } else break
            }
        }
        await fs.promises.writeFile(path.join(folder, newName), bf, { encoding: 'binary' })

        let filePath = path.posix.join(folder, newName).slice(path.posix.join(folder, newName).indexOf("/", 1))
        db.collection("content_media").insertOne({
            name: file.name,
            description: file.name,
            tags: path.parse(file.name as string).name?.split(" "),
            ext: path.parse(file.name as string).ext,
            original_file_name: file.name,
            type: path.parse(file.name).ext.replace(".", ""),
            size: size,
            path: filePath,
            uploaded: { 'at': new Date(), 'by': new ObjectId(session.user.id) }
        })

        VContentMedia();

        return new Response(JSON.stringify({ message: 'Your file has been uploaded!', success: 1, file: { url: process.env.STATIC_FILES + filePath } }), { status: 201, headers: { 'Content-Type': 'application/json' } })
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}
