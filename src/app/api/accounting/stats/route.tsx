import clientPromise from "@/lib/connections/mongo"
import { CheckDigit } from "@/lib/text"
import { ObjectId } from "mongodb"
import { getServerSession } from "next-auth"

export async function GET(request: Request) {
    const session = await getServerSession()
    if (session) {
        const db = (await clientPromise).db()

        let statement = JSON.parse(JSON.stringify(await db.collection('v_accounting_transactions').aggregate([
            {
                $unwind: "$journals",
            },
            {
                $match: {
                    status: "Confirmed",
                    'confirmed.at': { $gte: new Date(new Date().getTime() - 365 * 24 * 60 * 60 * 1000) }
                },
            },
            {
                $group: {
                    _id: {
                        year: {
                            $year: { date: "$confirmed.at", timezone: "Asia/Jakarta"}
                        },
                        month: {
                            $month: { date:"$confirmed.at", timezone: "Asia/Jakarta"}
                        },
                        coa: {
                            $toInt:  "$journals.account.coa.parent.parent"
                        }
                    },
                    debit: {
                        $sum: "$journals.debit",
                    },
                    credit: {
                        $sum: "$journals.credit",
                    },
                },
            },
            { $sort: {"_id.month": 1, "_id.year": 1, "_id.coa": 1}}
        ]).toArray()))
        //console.log(statement)
        return new Response(JSON.stringify({ status: true, message: "OK", statement }), { status: 200, headers: { 'Content-Type': 'application/json' } })

    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}