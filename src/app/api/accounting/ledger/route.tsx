import clientPromise from "@/lib/connections/mongo"
import { ObjectId } from "mongodb"
import { getServerSession } from "next-auth"

export async function POST(request: Request) {
    let errors = {
        account: ""
    }
    const session = await getServerSession()
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()
        let check = await db.collection('accounting_accounts').findOne({ number: Number(body.number) })
        //console.log(body)
        errors.account = "Account not found"
        if (check) {
            let toDate = new Date(body.dateRange[1])
            toDate.setHours(23)
            toDate.setMinutes(59)
            toDate.setSeconds(59)
            let transactions = JSON.parse(JSON.stringify(await db.collection('v_accounting_transaction_details').find({ "journals.account._id": new ObjectId(check._id), status: "Confirmed", "confirmed.at": { $gte: new Date(body.dateRange[0]), $lt: toDate } }).sort('confirmed.at', 1).toArray()))
            return new Response(JSON.stringify({ status: "OK", account:check, transactions }), { status: 200, headers: { 'Content-Type': 'application/json' } })
        } else return new Response(JSON.stringify({ message: 'Error', errors }), { status: 406, headers: { 'Content-Type': 'application/json' } }) 
        
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}