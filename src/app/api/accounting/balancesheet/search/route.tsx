import clientPromise from "@/lib/connections/mongo"
import { getServerSession } from "next-auth"
import { authOptions } from "@/lib/auth/options";

export async function POST(request: Request) {
    let errors = {
        search: ""
    }
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()
        if (body?.date && (body?.date != "" || body?.date!="")) {
            const balances = JSON.parse(JSON.stringify(await db.collection('v_accounting_accounts').aggregate([
                {
                    $group: {
                        _id: "$coa.code",
                        balance: {
                            $sum: "$balance"
                        }
                    }
                },
                { "$sort": { "_id": 1 } }
            ]).toArray()))
            let date = new Date(body.date)
            date.setHours(23)
            date.setMinutes(59)
            date.setSeconds(59)
            const trxs = JSON.parse(JSON.stringify(await db.collection('v_accounting_transaction_details').aggregate([
                {
                    $match: {
                        status: "Confirmed", "confirmed.at":{$lt: date} 
                    }
                },
                {
                    $group: {
                        _id: "$journals.account.coa.code",
                        debit: {
                            $sum: "$journals.debit"
                        },
                        credit: {
                            $sum: "$journals.credit"
                        }
                    }
                },
                { "$sort": { "_id": 1 } }
            ]).toArray()))
            return new Response(JSON.stringify({ status: "OK", data: {balances, trxs} }), {status: 200, headers: {'Content-Type': 'application/json'} })
        } else {
            return new Response(JSON.stringify({message: 'Error', errors }), {status: 406, headers: {'Content-Type': 'application/json'} })
        }
    } else {
        return new Response(JSON.stringify({message: 'Unauthorized'}), {status: 403, headers: {'Content-Type': 'application/json'} })
    }
}