import clientPromise from "@/lib/connections/mongo"
import { ObjectId } from "mongodb"
import { getServerSession } from "next-auth"
import { authOptions } from "@/lib/auth/options";

export async function POST(request: Request) {
    let errors = {
        coa: ""
    }
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()
        let check = await db.collection('accounting_coa').findOne({ _id: new ObjectId(body.coa) })

        errors.coa = "CoA Not Found"
        if (check) {
            let fromDate = new Date(body.dateRange[0])
            fromDate.setHours(0)
            fromDate.setMinutes(0)
            fromDate.setSeconds(0)

            let dateBefore = fromDate
            dateBefore.setDate(dateBefore.getDate() - 1)

            let toDate = new Date(body.dateRange[1])
            toDate.setHours(23)
            toDate.setMinutes(59)
            toDate.setSeconds(59)

            const trxs = JSON.parse(JSON.stringify(await db.collection('v_accounting_transaction_details').aggregate([
                {
                    $match: {
                        "type": "IV",
                        "journals.type": "Cr",
                        "journals.account.coa.code": check.code,
                        status: 'Confirmed',
                        "confirmed.at": { $gte: new Date(body.dateRange[0]), $lt: toDate },
                        "reversed.transaction" : {$exists: false}
                    }
                },
                {
                    $group: {
                        _id: { number: "$journals.account.number", name: "$journals.account.name" },
                        qty: {
                            $sum: "$journals.quantity"
                        },
                        debit: {
                            $sum: "$journals.debit"
                        },
                        credit: {
                            $sum: "$journals.credit"
                        },
                    }
                },
                { "$sort": { "_id": 1 } }
            ]).toArray()))
            check.previous_balance = trxs ? trxs[0] : null
            return new Response(JSON.stringify({ status: "OK", account: check, trxs }), { status: 200, headers: { 'Content-Type': 'application/json' } })
        } else return new Response(JSON.stringify({ message: 'Error', errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })

    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}