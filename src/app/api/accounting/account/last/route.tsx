import clientPromise from "@/lib/connections/mongo"
import { CheckDigit } from "@/lib/text"
import { ObjectId } from "mongodb"
import { getServerSession } from "next-auth"

export async function POST(request: Request) {
    let errors = {
        search: ""
    }
    const session = await getServerSession()
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()

        let coa = await db.collection('accounting_coa').findOne({ _id: new ObjectId(body.coa) })
        let last = await db.collection('accounting_accounts').find({ coa: new ObjectId(coa?._id) }).sort('number', -1).toArray()
        if (last.length >= 1){
            return new Response(JSON.stringify({ statue: true, message: "OK", data: { number: parseInt((parseInt(last[0].number.toString().substr(0, 7)) + 1).toString() +""+ CheckDigit((parseInt(last[0].number.toString().substr(0, 7))+1).toString()) )} }), { status: 200, headers: { 'Content-Type': 'application/json' } })
        } else
            return new Response(JSON.stringify({ statue: true, message: "OK", data: { number: parseInt(coa?.code + "00" + CheckDigit(coa?.code + "00")) } }), { status: 200, headers: { 'Content-Type': 'application/json' } })

    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}