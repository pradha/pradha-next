import clientPromise from "@/lib/connections/mongo";
import { ObjectId, Decimal128 } from "mongodb";
import { getServerSession } from "next-auth";
import { authOptions } from "@/lib/auth/options";
import { VAccountingAccounts } from "@/lib/connections/views/accounting";

const collection = 'accounting_accounts'
let errors = {
    id: "",
    coa: "",
    number: "",
    name: "",
    balance: "",
    description: "",
}
export async function GET(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        const { searchParams } = new URL(request.url)
        if (searchParams.get('id') && ObjectId.isValid(searchParams.get('id') as string)) {
            const data = await db.collection("v_" + collection).findOne({ _id: new ObjectId(searchParams.get('id') as string) })
            return new Response(JSON.stringify({ status: "OK", data }), { status: 200, headers: { 'Content-Type': 'application/json' } })
        } else if (searchParams.get('number') != null) {
            let data: any
            if (searchParams.get('type') && searchParams.get('type') == "all")
                data = await db.collection("v_" + collection).findOne({ number: parseInt(searchParams.get('number') as string) })
            else
                data = await db.collection("v_" + collection).findOne({ number: parseInt(searchParams.get('number') as string), status: "Active" })
            if (data)
                return new Response(JSON.stringify({ status: "OK", data }), { status: 200, headers: { 'Content-Type': 'application/json' } })
            else
                return new Response(JSON.stringify({ status: "INVALID" }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        } else {
            let pageNum = searchParams.get('page') ? parseInt(searchParams.get('page') as string) : 1
            let dataPerPage = process.env.DATA_PER_PAGE ? parseInt(process.env.DATA_PER_PAGE) : 15
            const data = await db.collection("v_" + collection).find().sort('created.at', -1).skip(dataPerPage * (pageNum - 1)).limit(dataPerPage).toArray()
            const count = await db.collection("v_" + collection).countDocuments();
            data.forEach((v: any) => {
                if (v.created && v.created.by) {
                    delete v.created.by.password;
                    delete v.created.by.address;
                    delete v.created.by.reset;
                    delete v.created.by.status;
                }
                if (v.updated && v.updated.by) {
                    delete v.updated.by.password;
                    delete v?.updated?.by.address;
                    delete v?.updated?.by.reset;
                    delete v?.updated?.by.status;
                }
            });
            return new Response(JSON.stringify({ status: "OK", data, count, page: pageNum, dataPerPage, total: Math.ceil(count / dataPerPage) }), { status: 200, headers: { 'Content-Type': 'application/json' } })
        }

    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}

export async function POST(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()
        const checkNumber = await db.collection(collection).findOne({ number: parseInt(body?.number) })
        if (body?.coa.trim() == "" || body?.number.trim() == "" || body?.name.trim() == "" || body?.description.trim() == "" || checkNumber) {
            if (checkNumber) errors.number = "Duplicate Account Number"
            if (body?.number.trim() == "") errors.number = "Account number can't empty"
            if (body?.coa.trim() == "") errors.coa = "Please select cart of account"
            if (body?.name.trim() == "") errors.name = "Name can't empty"
            if (body?.description.trim() == "") errors.description = "Description can't empty"
            return new Response(JSON.stringify({ status: "Invalid", errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        } else {
            body.coa = new ObjectId(body.coa)
            body.number = parseInt(body.number)
            body.balance = new Decimal128("0.00")
            body.locked_balance = new Decimal128("0.00")
            body.status = "Not Active"
            body.created = { 'at': new Date(), 'by': new ObjectId(session.user.id) }
            if (body.default_amount) body.default_amount = new Decimal128(parseFloat(body.default_amount).toFixed(2))
            else if (!body.default_amount || body?.default_amount == "") body.default_amount = new Decimal128("0.00")
            const insert = await db.collection(collection).insertOne(body)

            // Create View Account
            VAccountingAccounts()
            return new Response(JSON.stringify({ status: "OK", id:insert.insertedId, insert }), { status: 201, headers: { 'Content-Type': 'application/json' } })
        }
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}

export async function PUT(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()
        const checkNumber = await db.collection(collection).findOne({ number: parseInt(body?.number), _id: { $ne: new ObjectId(body.id) } })
        if (body?.coa.trim() == "" || body?.number.trim() == "" || body?.name.trim() == "" || body?.description.trim() == "" || checkNumber) {
            if (checkNumber) errors.number = "Duplicate Account Number"
            if (body?.number.trim() == "") errors.number = "Account number can't empty"
            if (body?.coa.trim() == "") errors.coa = "Please select cart of account"
            if (body?.name.trim() == "") errors.name = "Name can't empty"
            if (body?.description.trim() == "") errors.description = "Description can't empty"
            return new Response(JSON.stringify({ status: "Invalid", errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        } else {
            body.coa = new ObjectId(body.coa)
            body.number = parseInt(body.number)
            if (body.default_amount) body.default_amount = new Decimal128(parseFloat(body.default_amount).toFixed(2))
            else if (!body.default_amount || body?.default_amount == "") body.default_amount = new Decimal128("0.00")
            const id = body.id
            delete body.id
            body.updated = { 'at': new Date(), 'by': new ObjectId(session.user.id) }
            const update = await db.collection(collection).updateOne({ _id: new ObjectId(id) }, { $set: body })
            return new Response(JSON.stringify({ status: "OK", update }), { status: 201, headers: { 'Content-Type': 'application/json' } })
        }
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}

export async function PATCH(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()

        if (body?.id.trim() == "") {
            if (body?.id.trim() == "") errors.id = "Invalid ID"
            return new Response(JSON.stringify({ status: "Invalid", errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        } else {
            const activate = await db.collection(collection).updateOne({ _id: new ObjectId(body.id) }, { $set: { status: "Active", confirmed: { 'at': new Date(), 'by': new ObjectId(session.user.id) } } })
            return new Response(JSON.stringify({ status: "OK", activate }), { status: 201, headers: { 'Content-Type': 'application/json' } })
        }
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}

export async function DELETE(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()
        let check = await db.collection(collection).findOne({ _id: new ObjectId(body.id) })
        if (body?.id.trim() == "" || parseFloat(check?.balance) > 0 || parseFloat(check?.balance) < 0 || parseFloat(check?.locked_balance) > 0 || parseFloat(check?.locked_balance) < 0) {
            if (parseFloat(check?.balance) > 0 || parseFloat(check?.balance) < 0 || parseFloat(check?.locked_balance) > 0 || parseFloat(check?.locked_balance) < 0) errors.balance = "Balance is not 0"
            if (body?.id.trim() == "") errors.id = "Invalid ID"
            return new Response(JSON.stringify({ status: "Invalid", errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        } else {
            const closed = await db.collection(collection).updateOne({ _id: new ObjectId(body.id) }, { $set: { status: "Closed", closed: { 'at': new Date(), 'by': new ObjectId(session.user.id) } } })
            return new Response(JSON.stringify({ status: "OK", closed }), { status: 201, headers: { 'Content-Type': 'application/json' } })
        }
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}