import clientPromise from "@/lib/connections/mongo";
import { ObjectId, Decimal128 } from "mongodb";
import { getServerSession } from "next-auth";
import { useSearchParams } from "next/navigation";
import { authOptions } from "@/lib/auth/options";
import { CheckDigit } from "@/lib/text";
import { Confirm } from "@/lib/transaction/confirm";
import { VAccountingTransactionDetails, VAccountingTransactions } from "@/lib/connections/views/accounting";

const collection = 'accounting_transactions'
let errors = {
    id: "",
    type: "",
    information: "",
    journals: "",
    balance: ""
}
export async function GET(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        const { searchParams } = new URL(request.url)
        if (searchParams.get('id') && ObjectId.isValid(searchParams.get('id') as string)) {
            const data = await db.collection("v_" + collection).findOne({ _id: new ObjectId(searchParams.get('id') as string) })
            return new Response(JSON.stringify({ status: "OK", data }), { status: 200, headers: { 'Content-Type': 'application/json' } })
        } else {
            let pageNum = searchParams.get('page') ? parseInt(searchParams.get('page') as string) : 1
            let dataPerPage = process.env.DATA_PER_PAGE ? parseInt(process.env.DATA_PER_PAGE) : 15
            const data = await db.collection("v_" + collection).find().sort('created.at', -1).skip(dataPerPage * (pageNum - 1)).limit(dataPerPage).toArray()
            const count = await db.collection("v_" + collection).countDocuments();
            data.forEach((v: any) => {
                if (v.created && v.created.by) {
                    delete v.created.by.password;
                    delete v.created.by.address;
                    delete v.created.by.reset;
                    delete v.created.by.status;
                }
                if (v.updated && v.updated.by) {
                    delete v.updated.by.password;
                    delete v?.updated?.by.address;
                    delete v?.updated?.by.reset;
                    delete v?.updated?.by.status;
                }
            });
            return new Response(JSON.stringify({ status: "OK", data, count, page: pageNum, dataPerPage, total: Math.ceil(count / dataPerPage) }), { status: 200, headers: { 'Content-Type': 'application/json' } })
        }

    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}

export async function POST(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()
        if (body?.type.trim() == "" || body?.information.trim() == "" || body?.journals.length < 2) {
            if (body?.type.trim() == "") errors.type = "Please select transaction type"
            if (body?.information.trim() == "") errors.information = "Please add some information"
            if (body?.journals.length < 2) errors.journals = "Journals can't empty"
            return new Response(JSON.stringify({ status: "Invalid", errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        } else {
            let status = true;
            let journals: any[] = [];
            let tdebit: number = 0, tcredit: number = 0
            for (var j of body.journals) {
                let check = await db.collection('accounting_accounts').findOne({ number: j.number, status: "Active" })
                if (!check) {
                    status = false
                    errors.journals = `Account ${j.number} not found`
                    break
                }
                if (j?.price && j?.quantity)
                    journals.push({
                        id: journals.length + 1,
                        account: check._id,
                        type: j.debit != 0 ? "Db" : "Cr",
                        previous_balance: check.balance ? new Decimal128(parseFloat(check.balance).toFixed(2).toString()) : new Decimal128("0.00"),
                        price: new Decimal128(j.price.toString().replace(/\,/g, '')),
                        quantity: Number(j.quantity.toString().replace(",", ".").replace(/\,/g, '')),
                        debit: new Decimal128(j.debit.toString().replace(/\,/g, '')),
                        credit: new Decimal128(j.credit.toString().replace(/\,/g, '')),
                        final_balance: new Decimal128("0.00")
                    })
                else
                    journals.push({
                        id: journals.length + 1,
                        account: check._id,
                        type: j.debit != 0 ? "Db" : "Cr",
                        previous_balance: check.balance ? new Decimal128(parseFloat(check.balance).toFixed(2).toString()) : new Decimal128("0.00"),
                        debit: new Decimal128(j.debit.toString().replace(/\,/g, '')),
                        credit: new Decimal128(j.credit.toString().replace(/\,/g, '')),
                        final_balance: new Decimal128("0.00")
                    })
                tdebit = tdebit + Number(j.debit.toString().replace(/\,/g, ''))
                tcredit = tcredit + Number(j.credit.toString().replace(/\,/g, ''))
            }

            if (!status || tdebit != tcredit) {
                if (tdebit != tcredit) errors.balance = "Unbalance Transaction"
                return new Response(JSON.stringify({ status: "Invalid", errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
            } else {
                let code = ""
                let last = await db.collection('accounting_transactions').find({ type: body.type }).sort('code', -1).toArray()
                if (last.length >= 1)
                    code = (parseInt(last[0].code.substr(2, 11)) + 1).toString()
                else
                    code = "10000000000"

                let doc = {
                    type: body.type,
                    code: body.type + code + CheckDigit(code),
                    reference: body.reference,
                    information: body.information,
                    journals: journals,
                    status: "Pending",
                    created: { at: new Date(), by: new ObjectId(session.user.id) }
                }
                const insert = await db.collection(collection).insertOne(doc)

                // Create View Transacations & Details
                VAccountingTransactions()
                VAccountingTransactionDetails()
                return new Response(JSON.stringify({ status: "OK", id: insert.insertedId, insert }), { status: 201, headers: { 'Content-Type': 'application/json' } })
            }

        }
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}

export async function PUT(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()
        let status = true;
        let journals: any[] = [];
        let tdebit: number = 0, tcredit: number = 0
        for (var j of body.journals) {
            let check = await db.collection('accounting_accounts').findOne({ number: j.number, status: "Active" })
            if (!check) {
                status = false
                errors.journals = `Account ${j.number} not found`
                break
            }
            if (j?.price && j?.quantity)
                journals.push({
                    id: journals.length + 1,
                    account: check._id,
                    type: j.debit != 0 ? "Db" : "Cr",
                    previous_balance: check.balance ? new Decimal128(parseFloat(check.balance).toFixed(2).toString()) : new Decimal128("0.00"),
                    price: new Decimal128(j.price.toString().replace(/\,/g, '')),
                    quantity: Number(j.quantity.toString().replace(",", ".").replace(/\,/g, '')),
                    debit: new Decimal128(j.debit.toString().replace(/\,/g, '')),
                    credit: new Decimal128(j.credit.toString().replace(/\,/g, '')),
                    final_balance: new Decimal128("0.00")
                })
            else
                journals.push({
                    id: journals.length + 1,
                    account: check._id,
                    type: j.debit != 0 ? "Db" : "Cr",
                    previous_balance: check.balance ? new Decimal128(parseFloat(check.balance).toFixed(2).toString()) : new Decimal128("0.00"),
                    debit: new Decimal128(j.debit.toString().replace(/\,/g, '')),
                    credit: new Decimal128(j.credit.toString().replace(/\,/g, '')),
                    final_balance: new Decimal128("0.00")
                })
            tdebit = tdebit + Number(j.debit.toString().replace(/\,/g, ''))
            tcredit = tcredit + Number(j.credit.toString().replace(/\,/g, ''))
        }
        let check = db.collection(collection).findOne({ _id: new ObjectId(body.id) })

        if (!check || !status || tdebit != tcredit) {
            if (!check) errors.id = "Invalid Transaction"
            if (tdebit != tcredit) errors.balance = "Unbalance Transaction"
            return new Response(JSON.stringify({ status: "Invalid", errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        } else {
            let doc = {
                reference: body.reference,
                information: body.information,
                journals: journals,
                updated: { at: new Date(), by: new ObjectId(session.user.id) }
            }
            body.updated = { 'at': new Date(), 'by': new ObjectId(session.user.id) }
            const update = await db.collection(collection).updateOne({ _id: new ObjectId(body.id) }, { $set: doc })
            return new Response(JSON.stringify({ status: "OK", update }), { status: 201, headers: { 'Content-Type': 'application/json' } })
        }

    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}

export async function PATCH(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()

        const confirm = await Confirm(new ObjectId(body.id))
        if (!confirm.status) {
            errors.information = confirm.message
            return new Response(JSON.stringify({ status: "Invalid", errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        } else {
            return new Response(JSON.stringify({ status: "OK", confirm }), { status: 201, headers: { 'Content-Type': 'application/json' } })
        }
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}

export async function DELETE(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()
        let transaction = await db.collection(collection).findOne({ _id: new ObjectId(body.id) })
        if (body?.id.trim() == "" || !transaction) {
            if (body?.id.trim() == "") errors.id = "Invalid ID"
            return new Response(JSON.stringify({ status: "Invalid", errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        } else {
            let code = ""
            let last = await db.collection('accounting_transactions').find({ type: "RV" }).sort('code', -1).toArray()
            if (last.length >= 1)
                code = (parseInt(last[0].code.substr(2, 11)) + 1).toString()
            else
                code = "10000000000"

            let journals: any[] = [];
            let success = true

            await Promise.all(
                transaction.journals.map(async (journal: any, i: number) => {
                    let check = await db.collection('accounting_accounts').findOne({ _id: new ObjectId(journal.account), status: "Active" })
                    if (check)
                        if (journal?.price && journal?.quantity)
                            journals.push({
                                id: i,
                                account: journal.account,
                                type: journal.type == "Db" ? "Cr" : "Db",
                                previous_balance: check.balance ? new Decimal128(parseFloat(check.balance).toFixed(2).toString()) : new Decimal128("0.00"),
                                price: journal.price,
                                quantity: journal.quantity,
                                debit: journal.credit,
                                credit: journal.debit,
                                final_balance: new Decimal128("0.00")
                            })
                        else
                            journals.push({
                                id: i,
                                account: journal.account,
                                type: journal.type == "Db" ? "Cr" : "Db",
                                previous_balance: check.balance ? new Decimal128(parseFloat(check.balance).toFixed(2).toString()) : new Decimal128("0.00"),
                                debit: journal.credit,
                                credit: journal.debit,
                                final_balance: new Decimal128("0.00")
                            })
                    else success = false
                }))

            if (success) {
                let doc = {
                    type: "RV",
                    code: "RV" + code + CheckDigit(code),
                    reference: transaction.code,
                    information: "Reversed Transaction " + transaction.code,
                    journals: journals,
                    status: "Pending",
                    created: { at: new Date(), by: new ObjectId(session.user.id) }
                }
                const insert = await db.collection(collection).insertOne(doc)
                const confirm = await Confirm(new ObjectId(insert.insertedId))
                const update = await db.collection(collection).updateOne({ _id: new ObjectId(body.id) }, { $set: { reversed: { transaction: new ObjectId(insert.insertedId), 'at': new Date(), 'by': new ObjectId(session.user.id) } } })
                transaction = await db.collection("v_"+collection).findOne({ _id: new ObjectId(body.id) })
                return new Response(JSON.stringify({ status: "OK", insert, update, transaction }), { status: 201, headers: { 'Content-Type': 'application/json' } })
            } else {
                return new Response(JSON.stringify({ message: 'Failed' }), { status: 406, headers: { 'Content-Type': 'application/json' } })
            }
        }
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}