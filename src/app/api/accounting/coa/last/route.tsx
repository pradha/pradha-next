import clientPromise from "@/lib/connections/mongo"
import { ObjectId } from "mongodb"
import { getServerSession } from "next-auth"

export async function POST(request: Request) {
    let errors = {
        search: ""
    }
    const session = await getServerSession()
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()
        let last: any;
        let check: any;
        if (body?.parent.length == 1)
            last = await db.collection('accounting_coa').find({ parent: parseInt(body.parent as string) }).sort('code', -1).toArray()
        else {
            check = await db.collection('accounting_coa').findOne({ _id: new ObjectId(body.parent as string) })
            if (check.code.toString().length == 3)
                last = await db.collection('accounting_coa').find({ parent: check._id }).sort('code', -1).toArray()
            else
                last = await db.collection('accounting_coa').find({ _id: new ObjectId(body.parent as string) }).sort('code', -1).toArray()
        }
        if (last.length >= 1 && last[0].code.toString().length > 1)
            return new Response(JSON.stringify({ statue: true, message: "OK", data: { code: parseInt(JSON.parse(JSON.stringify(last[0])).code) } }), { status: 200, headers: { 'Content-Type': 'application/json' } })
        else
            if (check)
                return new Response(JSON.stringify({ statue: true, message: "OK", data: { code: parseInt(check.code + "00") } }), { status: 200, headers: { 'Content-Type': 'application/json' } })
            else
                return new Response(JSON.stringify({ statue: true, message: "OK", data: { code: parseInt(body.parent + "00") } }), { status: 200, headers: { 'Content-Type': 'application/json' } })
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}