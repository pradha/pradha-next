import clientPromise from "@/lib/connections/mongo";
import { ObjectId } from "mongodb";
import { getServerSession } from "next-auth";
import { authOptions } from "@/lib/auth/options";
import { VAccountingCoa } from "@/lib/connections/views/accounting";

const collection = 'accounting_coa'
let errors = {
    parent: "",
    name: "",
    description: "",
    position: "",
    code: ""
}
export async function GET(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        const { searchParams } = new URL(request.url)
        if (searchParams.get('id') && ObjectId.isValid(searchParams.get('id') as string)) {
            const data = await db.collection("v_" + collection).findOne({ _id: new ObjectId(searchParams.get('id') as string) })
            return new Response(JSON.stringify({ status: "OK", data }), { status: 200, headers: { 'Content-Type': 'application/json' } })
        } else {
            const data = await db.collection("v_" + collection).find().toArray()
            const count = await db.collection("v_" + collection).countDocuments();
            data.forEach((v: any) => {
                if (v.created && v.created.by) {
                    delete v.created.by.password;
                    delete v.created.by.address;
                    delete v.created.by.reset;
                    delete v.created.by.status;
                }
                if (v.updated && v.updated.by) {
                    delete v.updated.by.password;
                    delete v?.updated?.by.address;
                    delete v?.updated?.by.reset;
                    delete v?.updated?.by.status;
                }
            });
            return new Response(JSON.stringify({ status: "OK", data, count }), { status: 200, headers: { 'Content-Type': 'application/json' } })
        }

    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}

export async function POST(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()
        const checkCode = await db.collection(collection).findOne({ code: parseInt(body?.code) })
        if (body?.parent.trim() == "" || body?.name.trim() == "" || body?.description.trim() == "" || body?.position.trim() == "" || body?.code.trim == "" || checkCode) {
            if (checkCode) errors.code = "Duplicate CoA Code"
            if (body?.parent.trim() == "") errors.parent = "Please select parent"
            if (body?.name.trim() == "") errors.name = "Name can't empty"
            if (body?.description.trim() == "") errors.description = "Description can't empty"
            if (body?.position.trim() == "") errors.position = "Please select position"
            if (body?.code.trim() == "") errors.code = "CoA code can't empty"
            return new Response(JSON.stringify({ status: "OK", errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        } else {
            if (ObjectId.isValid(body?.parent))
                body.parent = new ObjectId(body.parent)
            if (Number(body?.parent))
                body.parent = Number(body.parent)
            body.code = parseInt(body.code)
            body.created = { 'at': new Date(), 'by': new ObjectId(session.user.id) }
            const insert = await db.collection(collection).insertOne(body)

            // Create View CoA
            VAccountingCoa()
            return new Response(JSON.stringify({ status: "OK", insert }), { status: 201, headers: { 'Content-Type': 'application/json' } })
        }
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}

export async function PUT(request: Request) {
    const session = await getServerSession()
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()
        if (body?.name.trim() == "" || body?.description.trim() == "") {
            if (body?.name.trim() == "") errors.name = "Name can't empty"
            if (body?.description.trim() == "") errors.description = "Description can't empty"
            return new Response(JSON.stringify({ status: "OK", errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        } else {
            body.updated = { 'at': new Date(), 'by': new ObjectId(session.user.id) }
            const id = body.id
            delete body.id
            const update = await db.collection(collection).updateOne({ _id: new ObjectId(id) }, { $set: body })
            return new Response(JSON.stringify({ status: "OK", update }), { status: 201, headers: { 'Content-Type': 'application/json' } })
        }
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}