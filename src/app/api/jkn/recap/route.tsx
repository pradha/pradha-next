import clientPromise from "@/lib/connections/mongo";
import bcrypt from 'bcrypt'
import { ObjectId } from "mongodb";
import jwt from 'jsonwebtoken'
export async function POST(req: Request) {
    const db = (await clientPromise).db()
    const header = req.headers
    let body = await req.json()
    if (header.get('x-token') && body?.kodepoli && body?.tanggalperiksa) {
        try {
            if (jwt.verify(header.get('x-token') as string, process.env.JWT_KEY as string)) {
                const now = Math.floor(Date.now() / 1000)
                const user = JSON.parse(JSON.stringify(jwt.decode(header.get('x-token') as string)))

                const service = await db.collection("queue_services").findOne({ code: body?.kodepoli })
                if (!service)
                    return new Response(JSON.stringify({ metadata: { message: "INVALID POLI", code: 406 } }), { status: 406, headers: { 'Content-Type': 'application/json' } })

                let startDay = new Date(body.tanggalperiksa)
                startDay.setHours(0)
                startDay.setMinutes(0)
                startDay.setSeconds(0)
                let lastDay = new Date(body.tanggalperiksa)
                lastDay.setHours(23)
                lastDay.setMinutes(59)
                lastDay.setSeconds(59)

                let served = await db.collection('queue_tickets').countDocuments({
                    service: new ObjectId(service._id),
                    'date': { $gte: startDay, $lt: lastDay },
                    called : { $exists: false } 
                })
                let total = await db.collection('queue_tickets').countDocuments({
                    service: new ObjectId(service._id),
                    'date': { $gte: startDay, $lt: lastDay },
                })

                return new Response(JSON.stringify({
                    response: {
                        jumlahterlayani: served,
                        lastupdate: new Date().getTime(),
                        namapoli: service.name,
                        totalantrean: total,
                        namadokter: "",
                    },
                    metadata: { code: 200, message: "OK" }
                }), { status: 200, headers: { 'Content-Type': 'application/json' } })
            } else return new Response(JSON.stringify({ metadata: { message: "INVALID TOKEN", code: 406 } }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        } catch (e: any) {
            console.log(e?.message)
            return new Response(JSON.stringify({ metadata: { message: e?.message.toUpperCase(), code: 406 } }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        }
    } else return new Response(JSON.stringify({ metadata: { message: "REQUIRED HEADERS & BODY NOT COMPLETED", code: 406 } }), { status: 406, headers: { 'Content-Type': 'application/json' } })
}