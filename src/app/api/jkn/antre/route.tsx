import clientPromise from "@/lib/connections/mongo";
import bcrypt from 'bcrypt'
import { ObjectId } from "mongodb";
import jwt from 'jsonwebtoken'
export async function POST(req: Request) {
    const db = (await clientPromise).db()
    const header = req.headers
    let body = await req.json()
    if (header.get('x-token') && body?.kodepoli && body?.tanggalperiksa) {
        try {
            if (jwt.verify(header.get('x-token') as string, process.env.JWT_KEY as string)) {
                const now = Math.floor(Date.now() / 1000)
                const user = JSON.parse(JSON.stringify(jwt.decode(header.get('x-token') as string)))

                const service = await db.collection("queue_services").findOne({ code: body?.kodepoli })
                if (!service)
                    return new Response(JSON.stringify({ metadata: { message: "INVALID POLI", code: 406 } }), { status: 406, headers: { 'Content-Type': 'application/json' } })

                let startDay = new Date(body.tanggalperiksa)
                startDay.setHours(0)
                startDay.setMinutes(0)
                startDay.setSeconds(0)
                let lastDay = new Date(body.tanggalperiksa)
                lastDay.setHours(23)
                lastDay.setMinutes(59)
                lastDay.setSeconds(59)

                let lastTicket = await db.collection('queue_tickets').findOne({
                    service: new ObjectId(service._id),
                    'date': { $gte: startDay, $lt: lastDay }
                }, { sort: { "created.at": -1 } })

                let last: any
                let number = lastTicket?.number + 1 || 1
                if (lastTicket) {
                    last = await db.collection('queue_tickets').insertOne({
                        health_number: body?.nomorkartu || "",
                        identity_number: body?.nik || "",
                        phone: body?.notelp || "",
                        name: body?.nama || "",
                        service: new ObjectId(service._id),
                        symbol: service.symbol,
                        number: number,
                        date: new Date(body.tanggalperiksa),
                        type: body?.jenisrequest || "JKN",
                        created: { at: new Date() }
                    })
                } else {
                    last = await db.collection('queue_tickets').insertOne({
                        health_number: body?.nomorkartu || "",
                        identity_number: body?.nik || "",
                        phone: body?.notelp || "",
                        name: body?.nama || "",
                        service: new ObjectId(service._id),
                        symbol: service.symbol,
                        number: number,
                        date: new Date(body.tanggalperiksa),
                        type: body?.jenisrequest || "JKN",
                        created: { at: new Date() }
                    })
                }
                return new Response(JSON.stringify({
                    response: {
                        nomorantrean: service.symbol + number,
                        kodebooking: last._id,
                        jenisantrean: body.jenisrequest,
                        estimasidilayani: new Date(body.tanggalperiksa).getTime(),
                        namapoli: service.name,
                        namadokter: "",
                    },
                    metadata: { code: 200, message: "OK" }
                }), { status: 200, headers: { 'Content-Type': 'application/json' } })
            } else return new Response(JSON.stringify({ metadata: { message: "INVALID TOKEN", code: 406 } }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        } catch (e: any) {
            console.log(e?.message)
            return new Response(JSON.stringify({ metadata: { message: e?.message.toUpperCase(), code: 406 } }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        }
    } else return new Response(JSON.stringify({ metadata: { message: "REQUIRED HEADERS & BODY NOT COMPLETED", code: 406 } }), { status: 406, headers: { 'Content-Type': 'application/json' } })
}