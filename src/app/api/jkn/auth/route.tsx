import clientPromise from "@/lib/connections/mongo";
import bcrypt from 'bcrypt'
import { ObjectId } from "mongodb";
import jwt from 'jsonwebtoken'
export async function GET(req: Request) {
    const db = (await clientPromise).db()
    const header = req.headers
    if (header.get('x-username') != null && header.get('x-password') != null) {
        const user = await db.collection('system_users').findOne({ username: header.get('x-username') })
        if (user && bcrypt.compareSync(header.get('x-password') as string, user.password)) {
            const now = Math.floor(Date.now() / 1000)
            const payload = {
                id: user._id,
                name: user.name,
                email: user.email,
                iat: now
            }

            const token = jwt.sign(
                payload,
                process.env.JWT_KEY as string,
                {
                    expiresIn: 60
                }
            )
            await db.collection('system_users').updateOne({ _id: new ObjectId(user._id) }, { $set: { token: token } })
            return new Response(JSON.stringify({ response: { token: token }, metadata: { code: 200, message: "OK" } }), { status: 200, headers: { 'Content-Type': 'application/json' } })
        } else return new Response(JSON.stringify({ status: "INVALID USERNAME/PASSWORD" }), { status: 406, headers: { 'Content-Type': 'application/json' } })
    } else return new Response(JSON.stringify({ status: "REQUIRED HEADERS NOT FOUND" }), { status: 406, headers: { 'Content-Type': 'application/json' } })
}