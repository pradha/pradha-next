import clientPromise from "@/lib/connections/mongo"
import url from 'url'
import { sendMail, subscribeMail } from "@/lib/mail";
import { NextRequest } from "next/server";

const collection = 'system_subscribers'
let errors = {
    email: ""
}

export async function POST(request: NextRequest) {
    const db = (await clientPromise).db()
    let body = await request.json()
    const check = await db.collection(collection).findOne({ email: body.email })
    if (!check || (check && (check.status != "confirmed" && check.status != "Pending"))) {
        const q = url.parse(request.nextUrl.href)
        body.registered = new Date()
        body.status = "Pending"
        body.confirmation = (Math.random()).toString(36).substring(2) + (Math.random()).toString(36).substring(2)

        let subscribe: any;
        if (check)
            subscribe = db.collection(collection).updateOne({ _id: check._id }, { $set: body })
        else
            subscribe = db.collection(collection).insertOne(body)

        let message = subscribeMail(body.email, process.env.NEXT_PUBLIC_SITE_URL + "/subscribe/confirm/" + body.confirmation)
        sendMail("Subscribe", body.email, "Subscribe Confirmation", message.text, message.html)

        return new Response(JSON.stringify({ status: "OK", subscribe }), { status: 201, headers: { 'Content-Type': 'application/json' } })
    } else {
        errors.email = "Email already registered"
        return new Response(JSON.stringify({ status: "Invalid", errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
    }
}