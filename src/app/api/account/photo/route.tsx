import clientPromise from "@/lib/connections/mongo"
import { ObjectId } from "mongodb"
import { getServerSession } from "next-auth"
import { authOptions } from "@/lib/auth/options";
import { NextRequest } from "next/server";
import { createFolder } from "@/lib/folder";
import path from 'path';
import fs from "fs";


export async function POST(req: NextRequest, res: Response) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()

        const folder = "./public/profile/photos"
        createFolder(folder)

        const formData = await req.formData()
        const file = formData.get('file') as File
        let newName: string;
        let type: string | null;

        if (!file || typeof file === 'string') {
            return new Response(
                JSON.stringify({
                    msg: 'no file'
                }),
                {
                    status: 400
                }
            )
        }
        const ab = await file.arrayBuffer()
        const bf = Buffer.from(ab)

        let size = Buffer.byteLength(bf) / 1024
        if (size > (parseInt(process.env.MAX_PROFILE_IMAGE_SIZE as string) || 200)) {
            return new Response(
                JSON.stringify({
                    msg: 'File too large, Please select file less than '+ (parseFloat(process.env.MAX_PROFILE_IMAGE_SIZE as string) || 200)+'KB'
                }),
                {
                    status: 406
                }
            )
        }

        const allowedExt = process.env.ALLOWED_IMAGE_FILE_EXT?.split(",") || ["jpg", "jpeg", "webp", "png", "gif"]
        if (!allowedExt.includes(path.parse(file.name).ext.replace(".", ""))) {
            return new Response(
                JSON.stringify({
                    msg: 'Allowed file type is ' + allowedExt.join(", ")
                }),
                {
                    status: 406
                }
            )
        }

        newName = session.user.id + path.parse(file.name).ext
        await fs.promises.writeFile(path.join(folder, newName), bf, { encoding: 'binary' })

        let filePath = path.posix.join(folder, newName).slice(path.posix.join(folder, newName).indexOf("/", 1))
        const update = await db.collection('system_users').updateOne({ _id: new ObjectId(session.user.id) }, { $set: { photo: filePath } })

        return new Response(JSON.stringify({ message: 'Your profile photo has been changed!', success: 1, file: { url: filePath } }), { status: 201, headers: { 'Content-Type': 'application/json' } })
    }
    else return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
}