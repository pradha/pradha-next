import clientPromise from "@/lib/connections/mongo"
import { VSystemGroups, VSystemRoles, VSystemUsers } from "@/lib/connections/views/system"
import { ObjectId } from "mongodb"
import url from 'url'
import { registrationMail, sendMail } from "@/lib/mail";
import bcrypt from 'bcrypt'
import { NextRequest } from "next/server";

const collection = 'system_users'
let errors = {
    recaptcha: "",
    email: "",
    username: "",
    name: "",
    password: "",
    repassword: ""
}

export async function POST(request: NextRequest) {
    const db = (await clientPromise).db()
    let body = await request.json()
    const reCaptcha = await fetch("https://www.google.com/recaptcha/api/siteverify", {
        method: "POST",
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
        },
        body: `secret=${process.env.RECAPTCHA_SECRET_KEY}&response=${body.gRecaptchaToken}`,
    })
    const verify = await reCaptcha.json()
    try {
        const email = await db.collection('system_users').findOne({ 'email': body.email })
        const username = await db.collection('system_users').findOne({ 'username': body.username })
        if (verify.score <= 0.5 || email || username || body.username.indexOf(' ') >= 0 || body.username.length < 5 || body.name.trim().length < 1 || body.name.trim().length < 1 || body.password.length < 8 || body.password != body.repassword) {
            if (verify.score <= 0.5) errors.recaptcha = "ReCaptcha score bellow 0.5"
            if (email) errors.email = "email is invalid"
            if (username) errors.username = "username is invalid"
            if (body.username.indexOf(' ') >= 0) errors.username = "username cannot contain spaces"
            if (body.username.length < 5) errors.username = "username minimum 5 characters"
            if (body.email.trim().length < 1) errors.email = "invalid email"
            if (body.name.trim().length < 1) errors.name = "invalid name"
            if (body.password.length < 8) errors.password = "password minimum 8 characters"
            if (body.password != body.repassword) errors.repassword = "invalid repeat password"
            return new Response(JSON.stringify({ status: "invalid", errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        } else {
            body.activation = (Math.random()).toString(36).substring(2) + (Math.random()).toString(36).substring(2)
            body.group = new ObjectId(process.env.DEFAULT_USER_GROUP)
            body.created = { 'at': new Date() }
            body.password = bcrypt.hashSync(body.password, bcrypt.genSaltSync(10))
            body.status = { 'active': process.env.DEFAULT_USER_STATUS_ACTIVE == "true" ? true : false }
            delete body.repassword
            delete body.gRecaptchaToken
            const register = await db.collection('system_users').insertOne(body)

            if (register) {
                const q = url.parse(request.nextUrl.href)

                let message = registrationMail(body.name, process.env.NEXT_PUBLIC_SITE_URL + "/account/verify/" + body.activation)
                sendMail("Registration", body.email, "Account Registration", message.text, message.html)

                // Check System Groups Collection
                const group = await db.collection("system_groups").findOne({})
                if (!group) {
                    await db.collection('system_groups').insertOne({
                        "_id": new ObjectId(process.env.DEFAULT_USER_GROUP),
                        "name": "Super Admin",
                        "description": "Super admin is super can access any roles",
                        "roles": [
                            new ObjectId('61e3de6ecc4029a9d11a1000'),
                            new ObjectId('61e3de6ecc4029a9d11a1001'),
                            new ObjectId('61e3de6ecc4029a9d11a1002'),
                            new ObjectId('61e3de6ecc4029a9d11a1003'),
                            new ObjectId('61e3de6ecc4029a9d11a1004'),
                            new ObjectId('61e3de6ecc4029a9d11a1005'),
                            new ObjectId('61e3de6ecc4029a9d11a1006'),
                            new ObjectId('61e3de6ecc4029a9d11a1007'),
                            new ObjectId('61e3de6ecc4029a9d11a2000'),
                            new ObjectId('61e3de6ecc4029a9d11a2001'),
                            new ObjectId('61e3de6ecc4029a9d11a2002'),
                            new ObjectId('61e3de6ecc4029a9d11a2003'),
                            new ObjectId('61e3de6ecc4029a9d11a2004'),
                            new ObjectId('61e3de6ecc4029a9d11a2005'),
                            new ObjectId('61e3de6ecc4029a9d11a2006'),
                            new ObjectId('61e3de6ecc4029a9d11a2007'),
                            new ObjectId('61e3de6ecc4029a9d11a2008'),
                            new ObjectId('61e3de6ecc4029a9d11a2009'),
                            new ObjectId('61e3de6ecc4029a9d11a2010'),
                            new ObjectId('61e3de6ecc4029a9d11a2011'),
                            new ObjectId('61e3de6ecc4029a9d11a2012')
                        ],
                        "created": { "at": new Date(), "by": new ObjectId(register.insertedId) }
                    });
                }

                // System Group View
                VSystemGroups()

                // System Roles View
                VSystemRoles()

                // System Users View
                VSystemUsers()
                return new Response(JSON.stringify({ status: "OK", register }), { status: 201, headers: { 'Content-Type': 'application/json' } })
            }
        }

    } catch (err) {
        return new Response(JSON.stringify({ status: "Error", error: 'failed register your data' }), { status: 500, headers: { 'Content-Type': 'application/json' } })
    }
}