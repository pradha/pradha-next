import clientPromise from "@/lib/connections/mongo"
import { NextRequest } from "next/server";

const collection = 'system_users'

export async function GET(request: NextRequest) {
    const db = (await clientPromise).db()
    
    const { searchParams } = new URL(request.url)
    if (searchParams.get('code')){
        let verify =  await db.collection("v_system_users").findOne({ activation: searchParams.get('code') });
        if (verify) {
            let update = await db.collection("system_users").updateOne({ _id: verify._id }, {$set: {'status.verified': true}, $unset: {activation: searchParams.get('code')}});
            return new Response(JSON.stringify({ status: "OK", update }), { status: 201, headers: { 'Content-Type': 'application/json' } })
        } else return new Response(JSON.stringify({ status: "Not Found" }), { status: 404, headers: { 'Content-Type': 'application/json' } })
    }
}