import clientPromise from "@/lib/connections/mongo";
import { ObjectId, Decimal128 } from "mongodb";
import { getServerSession } from "next-auth";
import { authOptions } from "@/lib/auth/options";
import { VCompanyRegions, VCompanyWarehouses } from "@/lib/connections/views/company";
import { VQueueServices } from "@/lib/connections/views/queue";
import { VSystemBilling } from "@/lib/connections/views/system";

const collection = 'system_billing'
let errors = {
    type: "",
    name: "",
    description: "",
    amount: "",
    payment_method: "",
    payment_amount: "",
    exist: ''
}

export async function GET(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        const { searchParams } = new URL(request.url)
        if (searchParams.get('id') && ObjectId.isValid(searchParams.get('id') as string)) {
            const data = await db.collection("v_" + collection).findOne({ _id: new ObjectId(searchParams.get('id') as string) })
            return new Response(JSON.stringify({ status: "OK", data }), { status: 200, headers: { 'Content-Type': 'application/json' } })
        } else if (searchParams.get('number') != null) {
            let data: any
            if (searchParams.get('type') && searchParams.get('type') == "all")
                data = await db.collection("v_" + collection).findOne({ number: parseInt(searchParams.get('number') as string) })
            else
                data = await db.collection("v_" + collection).findOne({ number: parseInt(searchParams.get('number') as string), status: "Active" })
            if (data)
                return new Response(JSON.stringify({ status: "OK", data }), { status: 200, headers: { 'Content-Type': 'application/json' } })
            else
                return new Response(JSON.stringify({ status: "INVALID" }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        } else {
            let pageNum = searchParams.get('page') ? parseInt(searchParams.get('page') as string) : 1
            let dataPerPage = process.env.DATA_PER_PAGE ? parseInt(process.env.DATA_PER_PAGE) : 15
            const data = await db.collection("v_" + collection).find({'created.by._id': new ObjectId(session.user.id)}).sort('created.at', -1).skip(dataPerPage * (pageNum - 1)).limit(dataPerPage).toArray()
            const count = await db.collection("v_" + collection).countDocuments();
            data.forEach((v: any) => {
                if (v.created && v.created.by) {
                    delete v.created.by.password;
                    delete v.created.by.address;
                    delete v.created.by.reset;
                    delete v.created.by.status;
                }
                if (v.updated && v.updated.by) {
                    delete v.updated.by.password;
                    delete v?.updated?.by.address;
                    delete v?.updated?.by.reset;
                    delete v?.updated?.by.status;
                }
            });
            return new Response(JSON.stringify({ status: "OK", data, count, page: pageNum, dataPerPage, total: Math.ceil(count / dataPerPage) }), { status: 200, headers: { 'Content-Type': 'application/json' } })
        }

    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}

export async function POST(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()
        
        let check = await db.collection(collection).findOne({'created.by': new ObjectId(session.user.id), type:body.type.trim(), $expr: { $eq: [{ $year: "$created.at" }, new Date().getFullYear()] }});
        console.log(check)
        if (body.type.trim().length < 1 || body.name.trim().length < 1 || body.description.trim().length < 1 || body.amount.toString().trim().length < 1 || body.payment_method.trim().length < 1 || body.payment_amount.toString().trim().length < 1 || check) {
            if (body.type.trim().length < 1) errors.type = "please fill bill type"
            if (body.name.trim().length < 1) errors.name = "please fill bill name"
            if (body.description.trim().length < 1) errors.description = "please fill bill description"
            if (body.amount.toString().trim().length < 1) errors.amount = "please fill the bill amount"
            if (body.payment_method.trim().length < 1) errors.payment_method = "please select payment method"
            if (body.payment_amount.toString().trim().length < 1) errors.payment_amount = "please fill payment amount"
            if (check) errors.exist = "Billing already exist"
            return new Response(JSON.stringify({ status: "Invalid", errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        } else {
            body.amount = new Decimal128(body.amount.toString())
            body.payment_amount = new Decimal128(body.payment_amount.toString())
            body.status = 'Pending'
            body.created = { 'at': new Date(), 'by': new ObjectId(session.user.id) }
            const insert = await db.collection(collection).insertOne(body)

            VSystemBilling()
            return new Response(JSON.stringify({ status: "OK", id: insert.insertedId, insert }), { status: 201, headers: { 'Content-Type': 'application/json' } })
        }
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}

export async function PUT(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()
        if (body.type.trim().length < 1 || body.name.trim().length < 1 || body.description.trim().length < 1 || body.amount.toString().trim().length < 1 || body.payment_type.trim().length < 1 || body.payment_amount.toString().trim().length < 1) {
            if (body.type.trim().length < 1) errors.type = "please fill bill type"
            if (body.name.trim().length < 1) errors.name = "please fill bill name"
            if (body.description.trim().length < 1) errors.description = "please fill bill description"
            if (body.amount.toString().trim().length < 1) errors.amount = "please fill the bill amount"
            if (body.payment_method.trim().length < 1) errors.payment_method = "please select payment method"
            if (body.payment_amount.toString().trim().length < 1) errors.payment_amount = "please fill payment amount"
            return new Response(JSON.stringify({ status: "Invalid", errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        } else {
            body.amount = new Decimal128(body.amount.toString())
            body.payment_amount = new Decimal128(body.payment_amount.toString())
            body.status = 'Pending'
            body.updated = { 'at': new Date(), 'by': new ObjectId(session.user.id) }
            let id = body.id
            delete body.id

            const update = await db.collection(collection).updateOne({ _id: new ObjectId(id) }, { $set: body })
            return new Response(JSON.stringify({ status: "OK", update }), { status: 201, headers: { 'Content-Type': 'application/json' } })
        }
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}
