import clientPromise from "@/lib/connections/mongo"
import { ObjectId } from "mongodb"
import { passwordChangedMail, sendMail } from "@/lib/mail";
import bcrypt from 'bcrypt'
import { NextRequest } from "next/server";

const collection = 'system_users'
let errors = {
    hash: "",
    password: "",
    repassword: "",
}

export async function POST(request: NextRequest) {
    const db = (await clientPromise).db()
    const { searchParams } = new URL(request.url)

    let code = searchParams.get('code') as string
    let verify = await db.collection("v_system_users").findOne({ reset: code });
    if (verify && (parseInt(code.toString().split("|")[0]) - Math.floor(new Date().getTime() / 1000)) > 0) {
        return new Response(JSON.stringify({ message: 'OK' }), { status: 201, headers: { 'Content-Type': 'application/json' } })
    } else return new Response(JSON.stringify({ message: 'Not Found' }), { status: 404, headers: { 'Content-Type': 'application/json' } })
}
export async function PUT(request: NextRequest) {
    const db = (await clientPromise).db()
    let body = await request.json()
    const { searchParams } = new URL(request.url)
    let code = searchParams.get('code') as string
    try {
        const account = await db.collection("v_system_users").findOne({ reset: code })
        if (account) {
            if ((parseInt(code.toString().split("|")[0]) - Math.floor(new Date().getTime() / 1000)) < 0 || body.password.length < 8 || body.password != body.repassword || bcrypt.compareSync(body.password, account.password)) {
                if ((parseInt(code.toString().split("|")[0]) - Math.floor(new Date().getTime() / 1000)) < 0) errors.hash = "hash expired, please try reset again"
                if (bcrypt.compareSync(body.password, account.password)) errors.password = "can't use old password"
                if (body.password.length < 8) errors.password = "password minimum 8 characters"
                if (body.password != body.repassword) errors.repassword = "invalid password repeat"
                return new Response(JSON.stringify({ status: "Invalid", errors }), { status: 404, headers: { 'Content-Type': 'application/json' } })
            } else {
                const reset = await db.collection('system_users').updateOne({ _id: new ObjectId(account._id) }, { $set: { password: bcrypt.hashSync(body.password, bcrypt.genSaltSync(10)) }, $unset: { reset: code } })
                let message = passwordChangedMail(account.name)
                sendMail("Password Changed", account.email, "Password Changed", message.text, message.html)
                return new Response(JSON.stringify({ status: "OK", reset }), { status: 201, headers: { 'Content-Type': 'application/json' } })
            }

        } else return new Response(JSON.stringify({ status: "Error", errors: { hash: "invalid hash, please try reset again" } }), { status: 404, headers: { 'Content-Type': 'application/json' } })
    } catch (err) {
        return new Response(JSON.stringify({ status: "Error", err }), { status: 501, headers: { 'Content-Type': 'application/json' } })
    }
}