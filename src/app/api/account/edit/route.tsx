import clientPromise from "@/lib/connections/mongo"
import { VSystemUsers } from "@/lib/connections/views/system"
import { ObjectId } from "mongodb"
import { getServerSession } from "next-auth"
import { authOptions } from "@/lib/auth/options";
import url from 'url'
import { registrationMail, sendMail } from "@/lib/mail";
import { NextRequest } from "next/server";

const collection = 'system_users'
let errors = {
    email: "",
    username: "",
    name: "",
    gender: "",
}

export async function PUT(request: NextRequest) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()
        try {
            const email = await db.collection('system_users').findOne({ 'email': body.email, _id: { $ne: new ObjectId(session.user.id) } })
            const username = await db.collection('system_users').findOne({ 'username': body.username, _id: { $ne: new ObjectId(session.user.id) } })
            if (email || username || body.username.indexOf(' ') >= 0 || body.username.length < 5 || body.email.trim().length < 1 || body.name.trim().length < 1) {
                if (email) errors.email = "email is invalid"
                if (username) errors.username = "username is invalid"
                if (body.username.indexOf(' ') >= 0) errors.username = "username cannot contain spaces"
                if (body.username.length < 5) errors.username = "username minimum 5 characters"
                if (body.email.trim().length < 1) errors.email = "invalid email"
                if (body.name.trim().length < 1) errors.name = "invalid name"
                if (body.gender.trim().length < 1) errors.gender = "invalid gender"
                return new Response(JSON.stringify({ status: "Invalid", errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
            } else {
                if (body.date_of_birth != ""){
                    body.date_of_birth = new Date(body.date_of_birth)
                }
                body.updated = { 'at': new Date(), 'by': new ObjectId(session?.user.id) }
                const update = await db.collection('system_users').updateOne({ _id: new ObjectId(session.user.id) }, { $set: body })
                return new Response(JSON.stringify({ status: "OK", update }), { status: 202, headers: { 'Content-Type': 'application/json' } })
             }
        } catch (err) {
            return new Response(JSON.stringify({ status: "Error", err }), { status: 501, headers: { 'Content-Type': 'application/json' } })
        }

    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}