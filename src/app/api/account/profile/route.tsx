import clientPromise from "@/lib/connections/mongo"
import { ObjectId } from "mongodb"
import { getServerSession } from "next-auth"
import { authOptions } from "@/lib/auth/options";
import { NextRequest } from "next/server";

const collection = 'system_users'
let errors = {
    hash: "",
    password: "",
    repassword: "",
}

export async function GET(request: NextRequest) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()

        const data = await db.collection("v_system_users").findOne({ _id: new ObjectId(session.user.id) })
        if (data) {
            delete data.password
            delete data?.updated?.by?.password
            delete data?.created?.by?.password
            delete data?.reset
        }
        return new Response(JSON.stringify({ data }), { status: 202, headers: { 'Content-Type': 'application/json' } })
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }

}