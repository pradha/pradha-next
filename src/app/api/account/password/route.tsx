import clientPromise from "@/lib/connections/mongo"
import { VSystemUsers } from "@/lib/connections/views/system"
import { ObjectId } from "mongodb"
import { getServerSession } from "next-auth"
import { authOptions } from "@/lib/auth/options";
import url from 'url'
import { passwordChangedMail, sendMail } from "@/lib/mail";
import bcrypt from 'bcrypt'
import { NextRequest } from "next/server";

const collection = 'system_users'
let errors = {
    oldpassword: "",
    password: "",
    repassword: "",
}

export async function PUT(request: NextRequest) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()
        try {
            const check = await db.collection("v_system_users").findOne({ _id: new ObjectId(session.user.id) })
            if (body.password.length < 8 || body.password != body.repassword || bcrypt.compareSync(body.oldpassword, check?.password) === false || bcrypt.compareSync(body.password, check?.password) === true) {
                if (bcrypt.compareSync(body.password, check?.password) === true) errors.password = "can't use old password"
                if (bcrypt.compareSync(body.oldpassword, check?.password) === false) errors.oldpassword = "invalid old password"
                if (body.password.length < 8) errors.password = "password minimum 8 characters"
                if (body.password != body.repassword) errors.repassword = "invalid password repeat"
                return new Response(JSON.stringify({ status: "OK", errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
            } else {
                const update = await db.collection('system_users').updateOne({ _id: new ObjectId(session.user.id) }, { $set: { password: bcrypt.hashSync(body.password, bcrypt.genSaltSync(10)) }, $unset: { reset: body.hash } })
                let message = passwordChangedMail(check?.name)
                sendMail("Password Changed", check?.email, "Password Changed", message.text, message.html)
                return new Response(JSON.stringify({ status: "OK", update }), { status: 202, headers: { 'Content-Type': 'application/json' } })
            }
        } catch (err) {
            return new Response(JSON.stringify({ status: "Error", err }), { status: 501, headers: { 'Content-Type': 'application/json' } })
        }

    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}