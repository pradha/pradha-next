import clientPromise from "@/lib/connections/mongo"
import { ObjectId } from "mongodb"
import { getServerSession } from "next-auth"
import { authOptions } from "@/lib/auth/options";
import url from 'url'
import { NextRequest } from "next/server";
import crypto from 'crypto'
import { resetPasswordMail, sendMail } from '@/lib/mail';

const collection = 'system_users'
let errors = {
    recaptcha: "",
    usermail: ""
}

export async function POST(request: NextRequest) {
    const session = await getServerSession(authOptions)

        const db = (await clientPromise).db()
        let body = await request.json()
        try {
            const reCaptcha = await fetch("https://www.google.com/recaptcha/api/siteverify", {
                method: "POST",
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                },
                body: `secret=${process.env.RECAPTCHA_SECRET_KEY}&response=${body.gRecaptchaToken}`,
            })
            const verify = await reCaptcha.json()
            if (body.usermail.trim() == "" || body.usermail.trim().length <= 3 || verify.score <= 0.5) {
                if (body.usermail.trim() == "" || body.usermail.trim().length <= 3) errors.usermail = "Invalid username or email"
                if (verify.score <= 0.5) errors.recaptcha = "Recaptcha score <= 0.5"
                return new Response(JSON.stringify({ status: "Invalid", errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
            } else {
                const user = await db.collection("v_system_users").findOne({ $or: [{ username: body.usermail }, { email: body.usermail }] })
                if (user) {
                    let code = Math.floor((new Date().getTime() / 1000) + 3600) + "-" + crypto.randomBytes(32).toString('hex')
                    const reset = await db.collection('system_users').updateOne({ _id: new ObjectId(user._id) }, { $set: { reset: code } })
                    if (reset) {
                        const q = url.parse(request.nextUrl.href)
                        let message = resetPasswordMail(user.name, process.env.NEXT_PUBLIC_SITE_URL + "/account/reset/" + code)
                        sendMail("Reset Password", user.email, "Reset Account Password", message.text, message.html)
                        return new Response(JSON.stringify({ status: "OK", reset }), { status: 202, headers: { 'Content-Type': 'application/json' } })
                    } else {
                        errors.usermail = "unknown"
                        return new Response(JSON.stringify({ status: "Error", errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
                    }
                } else {
                    errors.usermail = "account not found"
                    return new Response(JSON.stringify({ status: "Not Found", errors }), { status: 404, headers: { 'Content-Type': 'application/json' } })
                    //return res.status(200).json({ status: "invalid", errors }) 
                }
            }
        } catch (err) {
            return new Response(JSON.stringify({ status: "Error", err }), { status: 501, headers: { 'Content-Type': 'application/json' } })
        }
}