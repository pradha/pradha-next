import clientPromise from "@/lib/connections/mongo";

export async function GET(request: Request) {
    const { searchParams } = new URL(request.url);
    const db = (await clientPromise).db()
    let verses: any
    if (searchParams.get('s') && searchParams.get('v')) {
        verses = await db.collection("quran").aggregate([
            { $match: { 'number': parseInt(searchParams.get('s') as string) } },
            { $unwind: '$verses' },
            {
                $addFields: {
                    verses: {
                        name: '$name',
                        surahNum: '$number',
                        preBismillah: '$preBismillah',
                        tafsirSurah: '$tafsir',
                    }
                }
            },
            { $match: { 'verses.number.inSurah': parseInt(searchParams.get('v') as string) } },
            {
                $group: {
                    _id: '$verses.meta.page',
                    verses: { $push: '$verses' }
                }
            }
        ]).toArray()
        return new Response(JSON.stringify({ status: "OK", verses }), { status: 200, headers: { 'Content-Type': 'application/json' } })
    } else if (searchParams.get('s')) {
        verses = await db.collection("quran").aggregate([
            { $match: { 'number': parseInt(searchParams.get('s') as string) } },
            { $unwind: '$verses' },
            {
                $addFields: {
                    verses: {
                        name: '$name',
                        surahNum: '$number',
                        preBismillah: '$preBismillah',
                        tafsirSurah: '$tafsir',
                    }
                }
            },
            {
                $group: {
                    _id: '$verses.meta.page',
                    verses: { $push: '$verses' }
                }
            }
        ]).toArray()
        return new Response(JSON.stringify({ status: "OK", verses }), { status: 200, headers: { 'Content-Type': 'application/json' } })
    } else {
        return new Response(JSON.stringify({ status: "Not Found" }), { status: 404, headers: { 'Content-Type': 'application/json' } })
    }
}