import clientPromise from "@/lib/connections/mongo"
import { ObjectId, PullOperator, PushOperator } from "mongodb"
import { getServerSession } from "next-auth"
import { authOptions } from "@/lib/auth/options";
import { NextRequest } from "next/server";
import { createFolder } from "@/lib/folder";
import path from 'path';
import fs from "fs";
import slugify from "slugify";


export async function POST(req: NextRequest, res: Response) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        const formData = await req.formData()
        const id = formData.get('id') as string
        const file = formData.get('file') as File

        const folder = "./public/company/product/images/" + id
        createFolder(folder)
        await new Promise(resolve => setTimeout(resolve, 500));
        let newName: string;
        let type: string | null;

        if (!file || typeof file === 'string') {
            return new Response(
                JSON.stringify({
                    msg: 'no file'
                }),
                {
                    status: 400
                }
            )
        }
        const ab = await file.arrayBuffer()
        const bf = Buffer.from(ab)

        let size = Buffer.byteLength(bf) / 1024
        if (size > (parseInt(process.env.MAX_PROFILE_IMAGE_SIZE as string) || 200)) {
            return new Response(
                JSON.stringify({
                    msg: 'File too large, Please select file less than ' + (parseFloat(process.env.MAX_PROFILE_IMAGE_SIZE as string) || 200) + 'KB'
                }),
                {
                    status: 406
                }
            )
        }

        const allowedExt = process.env.ALLOWED_IMAGE_FILE_EXT?.split(",") || ["jpg", "jpeg", "webp", "png", "gif"]
        if (!allowedExt.includes(path.parse(file.name).ext.replace(".", ""))) {
            return new Response(
                JSON.stringify({
                    msg: 'Allowed file type is ' + allowedExt.join(", ")
                }),
                {
                    status: 406
                }
            )
        }
        newName = file.name
        if (file?.name) {
            newName = slugify(path.parse(file?.name).name, { lower: true, strict: true }) + path.parse(file?.name).ext
            let i = 1;
            while (true) {
                if (fs.existsSync(path.resolve(folder, newName))) {
                    i++;
                    newName = slugify(path.parse(file?.name).name, { lower: true, strict: true }) + '-' + i + path.parse(file?.name).ext
                } else break
            }
        }
        await fs.promises.writeFile(path.join(folder, newName), bf, { encoding: 'binary' })

        let filePath = path.posix.join(folder, newName).slice(path.posix.join(folder, newName).indexOf("/", 1))
        const update = await db.collection('company_products').updateOne({ _id: new ObjectId(id) }, { $push: { images: filePath } } as unknown as PushOperator<Document>)
        const product = await db.collection('v_company_products').findOne({ _id: new ObjectId(id) })
        return new Response(JSON.stringify({ message: 'Image has been successfully uploaded', success: 1, file: { url: filePath }, product }), { status: 201, headers: { 'Content-Type': 'application/json' } })
    }
    else return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
}

export async function GET(request: Request) {
    const { searchParams } = new URL(request.url);
    let data: any
    if (searchParams.get('id')) {
        const db = (await clientPromise).db()

        const product = await db.collection('v_company_products').findOne({ _id: new ObjectId(searchParams.get('id') as string) })
        
        return new Response(JSON.stringify({ images: product?.images ? product.images : []}), { status: 201, headers: { 'Content-Type': 'application/json' } })
    }
}



export async function DELETE(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()

        const update = await db.collection('company_products').updateOne({ _id: new ObjectId(body.id) }, { $pull: { images: body.image } } as unknown as PullOperator<Document>)
        const product = await db.collection('v_company_products').findOne({ _id: new ObjectId(body.id) })
        fs.unlink('./public' + body.image, (err: any) => {
            if (err) console.log("file not found: " + '.public' + body.image)
        })
        return new Response(JSON.stringify({ message: 'Image has been successfully deleted', success: 1, update, product }), { status: 201, headers: { 'Content-Type': 'application/json' } })
    } else return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
}
