import clientPromise from "@/lib/connections/mongo";
import { ObjectId, Decimal128 } from "mongodb";
import { getServerSession } from "next-auth";
import { authOptions } from "@/lib/auth/options";
import { VCompanyRegions } from "@/lib/connections/views/company";

const collection = 'company_regions'
let errors = {
    name: "",
    description: "",
    address: "",
}

export async function GET(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        const { searchParams } = new URL(request.url)
        if (searchParams.get('id') && ObjectId.isValid(searchParams.get('id') as string)) {
            const data = await db.collection("v_" + collection).findOne({ _id: new ObjectId(searchParams.get('id') as string) })
            return new Response(JSON.stringify({ status: "OK", data }), { status: 200, headers: { 'Content-Type': 'application/json' } })
        } else if (searchParams.get('number') != null) {
            let data: any
            if (searchParams.get('type') && searchParams.get('type') == "all")
                data = await db.collection("v_" + collection).findOne({ number: parseInt(searchParams.get('number') as string) })
            else
                data = await db.collection("v_" + collection).findOne({ number: parseInt(searchParams.get('number') as string), status: "Active" })
            if (data)
                return new Response(JSON.stringify({ status: "OK", data }), { status: 200, headers: { 'Content-Type': 'application/json' } })
            else
                return new Response(JSON.stringify({ status: "INVALID" }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        } else {
            let pageNum = searchParams.get('page') ? parseInt(searchParams.get('page') as string) : 1
            let dataPerPage = process.env.DATA_PER_PAGE ? parseInt(process.env.DATA_PER_PAGE) : 15
            const data = await db.collection("v_" + collection).find().sort('created.at', -1).skip(dataPerPage * (pageNum - 1)).limit(dataPerPage).toArray()
            const count = await db.collection("v_" + collection).countDocuments();
            data.forEach((v: any) => {
                if (v.created && v.created.by) {
                    delete v.created.by.password;
                    delete v.created.by.address;
                    delete v.created.by.reset;
                    delete v.created.by.status;
                }
                if (v.updated && v.updated.by) {
                    delete v.updated.by.password;
                    delete v?.updated?.by.address;
                    delete v?.updated?.by.reset;
                    delete v?.updated?.by.status;
                }
            });
            return new Response(JSON.stringify({ status: "OK", data, count, page: pageNum, dataPerPage, total: Math.ceil(count / dataPerPage) }), { status: 200, headers: { 'Content-Type': 'application/json' } })
        }

    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}

export async function POST(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()
        body.code = "01"
        let checkLast: any
        if (body.parent.trim() != "") {
            const parent = await db.collection('company_regions').findOne({ _id: new ObjectId(body.parent) })
            if (parent) {
                checkLast = JSON.parse(JSON.stringify(await db.collection('company_regions').find({ parent: new ObjectId(parent._id) }).sort('code', -1).limit(1).toArray()))
                if (checkLast[0]) {
                    body.code = "0" + (parseInt(checkLast[0].code) + 1).toString()
                } else {
                    if (parent.code.length == 2)
                        body.code = parent.code + "0" + (body.code).toString()
                    if (parent.code.length == 5)
                        body.code = parent.code + "00" + (body.code).toString()
                    if (parent.code.length == 9)
                        body.code = parent.code + "000" + (body.code).toString()
                }
            }
        } else {
            checkLast = JSON.parse(JSON.stringify(await db.collection('company_regions').find({ parent: { $exists: false } }).sort('code', -1).limit(1).toArray()))
            if (checkLast[0]) {
                body.code = "0" + (parseInt(checkLast[0].code) + 1).toString()
            }
        }
        if (body.name.trim().length < 1 || body.description.trim().length < 1 || body.address.trim().length < 1) {
            if (body.name.trim().length < 1) errors.name = "invalid name"
            if (body.description.trim().length < 1) errors.description = "invalid description"
            if (body.description.trim().length < 1) errors.address = "invalid address"
            return new Response(JSON.stringify({ status: "Invalid", errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        } else {
            if (body.parent != "") body.parent = new ObjectId(body.parent); else delete body.parent
            body.status = { 'active': (body.status === "1") }
            body.created = { 'at': new Date(), 'by': new ObjectId(session.user.id) }
            //delete body.status
            const insert = await db.collection(collection).insertOne(body)

            VCompanyRegions()
            return new Response(JSON.stringify({ status: "OK", id: insert.insertedId, insert }), { status: 201, headers: { 'Content-Type': 'application/json' } })
        }
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}

export async function PUT(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()
        if (body.name.trim().length < 1 || body.description.trim().length < 1 || body.address.trim().length < 1) {
            if (body.name.trim().length < 1) errors.name = "invalid name"
            if (body.description.trim().length < 1) errors.description = "invalid description"
            if (body.description.trim().length < 1) errors.address = "invalid address"
            return new Response(JSON.stringify({ status: "Invalid", errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        } else {
            if (body.parent != "") body.parent = new ObjectId(body.parent); else body.parent = null
            body.status = { 'active': (body.status === "1") }
            body.updated = { 'at': new Date(), 'by': new ObjectId(session.user.id) }
            let id = body.id
            delete body.id
            //delete body.status
            const update = await db.collection(collection).updateOne({ _id: new ObjectId(id) }, { $set: body })
            return new Response(JSON.stringify({ status: "OK", update }), { status: 201, headers: { 'Content-Type': 'application/json' } })
        }
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}
