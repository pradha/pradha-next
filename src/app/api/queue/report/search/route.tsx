import clientPromise from "@/lib/connections/mongo"
import { ObjectId } from "mongodb"
import { getServerSession } from "next-auth"

export async function POST(request: Request) {
    let errors = {
        search: ""
    }
    const session = await getServerSession()
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()
        let textSearch = new RegExp(body.search, "i")

        if (body.service.trim().length < 1 && body.date.trim().length < 1) {
            if (body.search.trim().length < 1) errors.search = "invalid search"
            return new Response(JSON.stringify({ message: 'Error', errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        } else {
            let startDay = new Date(body.date)
            startDay.setHours(0)
            startDay.setMinutes(0)
            startDay.setSeconds(0)
            let lastDay = new Date(body.date)
            lastDay.setHours(23)
            lastDay.setMinutes(59)
            lastDay.setSeconds(59)

            let search: any
            let groups: any
            if (body.service != "all") {
                search = await db.collection('v_queue_tickets').find({ "service._id": new ObjectId(body.service), 'created.at': { $gte: startDay, $lt: lastDay } }).sort('created.at', -1).toArray()
            } else {
                search = await db.collection('v_queue_tickets').find({ 'created.at': { $gte: startDay, $lt: lastDay } }).sort('created.at', -1).toArray()
                groups = await db.collection('v_queue_tickets').aggregate([
                    {
                        $match: { 'created.at': { $gte: startDay, $lt: lastDay } }
                    },
                    {
                        $group: {
                            _id: "$service.name",
                            count: {
                                $count : {}
                            }
                        }
                    }
                ]).toArray()
            }
            return new Response(JSON.stringify({ status: "OK", search, groups }), { status: 200, headers: { 'Content-Type': 'application/json' } })
        }
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}