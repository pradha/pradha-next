import clientPromise from "@/lib/connections/mongo";
import { ObjectId, Decimal128 } from "mongodb";
import { getServerSession } from "next-auth";
import { authOptions } from "@/lib/auth/options";
import { createFolder } from "@/lib/folder";
import path from 'path';
import fs from "fs";
import slugify from "slugify";
import { VQueueCounters } from "@/lib/connections/views/queue";
import { notFound } from "next/navigation";

const collection = 'queue_counters'
let errors = {
    name: "",
    description: "",
    display: "",
    service: "",
    sound: "",
    status: "",
    type: "",
}

export async function GET(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        const { searchParams } = new URL(request.url)
        if (searchParams.get('id') && ObjectId.isValid(searchParams.get('id') as string)) {
            const data = await db.collection("v_" + collection).findOne({ _id: new ObjectId(searchParams.get('id') as string) })
            return new Response(JSON.stringify({ status: "OK", data }), { status: 200, headers: { 'Content-Type': 'application/json' } })
        } else if (searchParams.get('number') != null) {
            let data: any
            if (searchParams.get('type') && searchParams.get('type') == "all")
                data = await db.collection("v_" + collection).findOne({ number: parseInt(searchParams.get('number') as string) })
            else
                data = await db.collection("v_" + collection).findOne({ number: parseInt(searchParams.get('number') as string), status: "Active" })
            if (data)
                return new Response(JSON.stringify({ status: "OK", data }), { status: 200, headers: { 'Content-Type': 'application/json' } })
            else
                return new Response(JSON.stringify({ status: "INVALID" }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        } else {
            let pageNum = searchParams.get('page') ? parseInt(searchParams.get('page') as string) : 1
            let dataPerPage = process.env.DATA_PER_PAGE ? parseInt(process.env.DATA_PER_PAGE) : 15
            const data = await db.collection("v_" + collection).find().sort('created.at', -1).skip(dataPerPage * (pageNum - 1)).limit(dataPerPage).toArray()
            const count = await db.collection("v_" + collection).countDocuments();
            data.forEach((v: any) => {
                if (v.created && v.created.by) {
                    delete v.created.by.password;
                    delete v.created.by.address;
                    delete v.created.by.reset;
                    delete v.created.by.status;
                }
                if (v.updated && v.updated.by) {
                    delete v.updated.by.password;
                    delete v?.updated?.by.address;
                    delete v?.updated?.by.reset;
                    delete v?.updated?.by.status;
                }
            });
            return new Response(JSON.stringify({ status: "OK", data, count, page: pageNum, dataPerPage, total: Math.ceil(count / dataPerPage) }), { status: 200, headers: { 'Content-Type': 'application/json' } })
        }

    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}

export async function POST(req: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        const folder = "./public/audio/" + new Date().getFullYear() + "/" + (new Date().getMonth() + 1).toString().padStart(2, '0')
        createFolder(folder)

        const formData = await req.formData()
        const file = formData.get('sound') as File
        let newName: string;
        if ((formData.get("name") as string).trim().length < 1 || (formData.get("description") as string).trim().length < 1 || (formData.get("display") as string).trim().length < 1 || (formData.get("status") as string).trim().length < 1 || (formData.get("type") as string).trim().length < 1 || (formData.get('services') || []).length <= 0) {
            if ((formData.get("name") as string).trim().length < 1) errors.name = "please fill service name"
            if ((formData.get("description") as string).trim().length < 1) errors.description = "please fill counter description"
            if ((formData.get("display") as string).trim().length < 1) errors.description = "please select display number"
            if ((formData.get('services') || []).length <= 0) errors.service = "please select counter service target"
            if ((formData.get("status") as string).trim().length < 1) errors.status = "please select counter status"
            if ((formData.get("type") as string).trim().length < 1) errors.type = "please select call type"
            return new Response(JSON.stringify({ status: "Invalid", errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        }
        const ab = await file.arrayBuffer()
        const bf = Buffer.from(ab)
        let size = Buffer.byteLength(bf) / 1024

        if (size > (parseInt(process.env.MAX_IMAGE_UPLOAD_SIZE as string) || 200)) {
            return new Response(
                JSON.stringify({
                    status: "Invalid",
                    errors: { sound: 'File too large, Please select file less than ' + (parseFloat(process.env.MAX_IMAGE_UPLOAD_SIZE as string) || 200) + 'KB' }
                }),
                {
                    status: 406
                }
            )
        }

        if (!["mp3"].includes(path.parse(file.name).ext.replace(".", ""))) {
            return new Response(
                JSON.stringify({
                    status: "Invalid",
                    errors: { sound: 'Allowed file type is *.mp3' }
                }),
                {
                    status: 406
                }
            )
        }

        newName = file.name
        if (file?.name) {
            newName = slugify(path.parse(file?.name).name, { lower: true, strict: true }) + path.parse(file?.name).ext
            let i = 1;
            while (true) {
                if (fs.existsSync(path.resolve(folder, newName))) {
                    i++;
                    newName = slugify(path.parse(file?.name).name, { lower: true, strict: true }) + '-' + i + path.parse(file?.name).ext
                } else break
            }
        }
        await fs.promises.writeFile(path.join(folder, newName), bf, { encoding: 'binary' })
        let filePath = path.posix.join(folder, newName).slice(path.posix.join(folder, newName).indexOf("/", 1))
        let services = formData.get('services') as string

        let insert = db.collection(collection).insertOne({
            name: formData.get('name'),
            description: formData.get('description'),
            display: formData.get('display'),
            audio: filePath,
            services: JSON.parse(services).map((service: string) => new ObjectId(service)),
            type: formData.get('type'),
            status: { 'active': (formData.get("status") === "1") },
            created: { 'at': new Date(), 'by': new ObjectId(session.user.id) }
        })
        VQueueCounters()
        return new Response(JSON.stringify({ message: 'Counter has been created', success: 1, insert }), { status: 201, headers: { 'Content-Type': 'application/json' } })
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}


export async function PUT(req: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        
        const folder = "./public/audio/" + new Date().getFullYear() + "/" + (new Date().getMonth() + 1).toString().padStart(2, '0')
        createFolder(folder)

        const formData = await req.formData()
        let data = await db.collection(collection).findOne({_id: new ObjectId(formData.get('id') as string)})
        if (!data)
            notFound()

        const file = formData.get('sound') as File
        let newName: string;
        if ((formData.get("name") as string).trim().length < 1 || (formData.get("description") as string).trim().length < 1 || (formData.get("display") as string).trim().length < 1 || (formData.get("status") as string).trim().length < 1 || (formData.get("type") as string).trim().length < 1 || (formData.get('services') || []).length <= 0) {
            if ((formData.get("name") as string).trim().length < 1) errors.name = "please fill service name"
            if ((formData.get("description") as string).trim().length < 1) errors.description = "please fill counter description"
            if ((formData.get("display") as string).trim().length < 1) errors.description = "please select display number"
            if ((formData.get('services') || []).length <= 0) errors.service = "please select counter service target"
            if ((formData.get("status") as string).trim().length < 1) errors.status = "please select counter status"
            if ((formData.get("type") as string).trim().length < 1) errors.type = "please select call type"
            return new Response(JSON.stringify({ status: "Invalid", errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        }

        let filePath = data.sound
        if (formData.get("sound") !== 'undefined') {
            console.log("file is provided")
            const ab = await file.arrayBuffer()
            const bf = Buffer.from(ab)
            let size = Buffer.byteLength(bf) / 1024

            if (size > (parseInt(process.env.MAX_IMAGE_UPLOAD_SIZE as string) || 200)) {
                return new Response(
                    JSON.stringify({
                        status: "Invalid",
                        errors: { sound: 'File too large, Please select file less than ' + (parseFloat(process.env.MAX_IMAGE_UPLOAD_SIZE as string) || 200) + 'KB' }
                    }),
                    {
                        status: 406
                    }
                )
            }

            if (!["mp3"].includes(path.parse(file.name).ext.replace(".", ""))) {
                return new Response(
                    JSON.stringify({
                        status: "Invalid",
                        errors: { sound: 'Allowed file type is *.mp3' }
                    }),
                    {
                        status: 406
                    }
                )
            }

            newName = file.name
            if (file?.name) {
                newName = slugify(path.parse(file?.name).name, { lower: true, strict: true }) + path.parse(file?.name).ext
                let i = 1;
                while (true) {
                    if (fs.existsSync(path.resolve(folder, newName))) {
                        i++;
                        newName = slugify(path.parse(file?.name).name, { lower: true, strict: true }) + '-' + i + path.parse(file?.name).ext
                    } else break
                }
            }
            await fs.promises.writeFile(path.join(folder, newName), bf, { encoding: 'binary' })

            filePath = path.posix.join(folder, newName).slice(path.posix.join(folder, newName).indexOf("/", 1))
        }
        let services = formData.get('services') as string

        let doc = {
            name: formData.get('name'),
            description: formData.get('description'),
            display: formData.get('display'),
            audio: filePath ? filePath : data.audio,
            services: JSON.parse(services).map((service: string) => new ObjectId(service)),
            type: formData.get('type'),
            status: { 'active': (formData.get("status") === "1") },
            updated: { 'at': new Date(), 'by': new ObjectId(session.user.id) }
        }
        let update = db.collection(collection).updateOne({ _id: new ObjectId(formData.get('id') as string) }, { $set: doc })
        data = await db.collection(collection).findOne({_id: new ObjectId(formData.get('id') as string)})
        return new Response(JSON.stringify({ message: 'Counter has been updated', success: 1, data }), { status: 201, headers: { 'Content-Type': 'application/json' } })
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}