import clientPromise from "@/lib/connections/mongo"
import { getServerSession } from "next-auth"

export async function POST(request: Request) {
    let errors = {
        search: ""
    }
    const session = await getServerSession()
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()
        let textSearch = new RegExp(body.search, "i")
        if (body.search.trim().length < 1) {
            if (body.search.trim().length < 1) errors.search = "invalid search"
            return new Response(JSON.stringify({message: 'Error', errors }), {status: 406, headers: {'Content-Type': 'application/json'} })
        } else {
            const search = await db.collection('v_queue_counters').find({ $or: [{ name: textSearch }, { description: textSearch }] }).toArray()
            return new Response(JSON.stringify({ status: "OK", search }), {status: 200, headers: {'Content-Type': 'application/json'} })
        }
    } else {
        return new Response(JSON.stringify({message: 'Unauthorized'}), {status: 403, headers: {'Content-Type': 'application/json'} })
    }
}