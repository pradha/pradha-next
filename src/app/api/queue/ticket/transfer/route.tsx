import clientPromise from "@/lib/connections/mongo"
import { ObjectId } from "mongodb"
import { notFound } from "next/navigation"
import { getServerSession } from "next-auth";
import { authOptions } from "@/lib/auth/options";

export async function POST(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()

        const service = await db.collection("queue_services").findOne({ _id: new ObjectId(body.service) })
        if (!service)
            return new Response(JSON.stringify({ status: "Invalid", errors:{service: "Invalid service ID"} }), { status: 201, headers: { 'Content-Type': 'application/json' } })
        let last = await db.collection('queue_tickets').insertOne({
            service: new ObjectId(body.service),
            transfered: new ObjectId(body.transfered),
            symbol: body.symbol,
            number: body.number,
            date: new Date(body.date),
            created: { at: new Date(), by: new ObjectId(session.user.id) }
        })
        return new Response(JSON.stringify({ status: "OK", last }), { status: 201, headers: { 'Content-Type': 'application/json' } })
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}