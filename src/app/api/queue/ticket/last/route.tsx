import { authOptions } from "@/lib/auth/options"
import clientPromise from "@/lib/connections/mongo"
import { ISODate } from "@/lib/time"
import { ObjectId } from "mongodb"
import { getServerSession } from "next-auth"
import { notFound } from "next/navigation"

export async function POST(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()

        let startDay = new Date(body.date)

        startDay.setHours(0, 0, 0, 0)
        let lastDay = new Date(body.date)
        lastDay.setHours(23, 59, 59, 999)

        const counter = await db.collection("queue_counters").findOne({ _id: new ObjectId(body.counter) })

        if (!counter)
            notFound()
        
        let lastTicket:any
        let waiting:any
        if (counter.type == "New Tickets Only") {
            lastTicket = await db.collection("v_queue_tickets").findOne({ "service._id": { $in: counter.services }, 'date': { $gte: startDay, $lt: lastDay }, "transfered": { $exists: false }, "called.by": { $exists: false } }, { sort: { "created.at": 1 } })
            waiting = JSON.parse(JSON.stringify(await db.collection("v_queue_tickets").find({ "service._id": { $in: counter.services }, 'date': { $gte: startDay, $lt: lastDay }, "transfered": { $exists: false }, "called.by": { $exists: false } }, { sort: { "created.at": 1 } }).toArray()))
        } else if (counter.type == "Transfered Only") {
            lastTicket = await db.collection("v_queue_tickets").findOne({ "service._id": { $in: counter.services }, 'date': { $gte: startDay, $lt: lastDay }, "transfered": { $exists: true }, "called.by": { $exists: false } }, { sort: { "created.at": 1 } })
            waiting = JSON.parse(JSON.stringify(await db.collection("v_queue_tickets").find({ "service._id": { $in: counter.services }, 'date': { $gte: startDay, $lt: lastDay }, "transfered": { $exists: true }, "called.by": { $exists: false } }, { sort: { "created.at": 1 } }).toArray()))
        } else {
            lastTicket = await db.collection("v_queue_tickets").findOne({ "service._id": { $in: counter.services }, 'date': { $gte: startDay, $lt: lastDay }, "called.by": { $exists: false } }, { sort: { "created.at": 1 } })
            waiting = JSON.parse(JSON.stringify(await db.collection("v_queue_tickets").find({ "service._id": { $in: counter.services }, 'date': { $gte: startDay, $lt: lastDay }, "called.by": { $exists: false } }, { sort: { "created.at": 1 } }).toArray()))
        }

        let transfered = lastTicket?._id ? await db.collection("queue_tickets").findOne({ "transfered": new ObjectId(lastTicket._id) }) : false
        return new Response(JSON.stringify({ status: "OK", lastTicket, waiting, startDay, transfered: transfered ? true : false }), { status: 201, headers: { 'Content-Type': 'application/json' } })
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}