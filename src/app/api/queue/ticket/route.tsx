import clientPromise from "@/lib/connections/mongo"
import { VQueueTickets } from "@/lib/connections/views/queue"
import { DateTime, DayDateFormat, FullShort } from "@/lib/time"
import { exec } from "child_process"
import { ObjectId } from "mongodb"
import { notFound } from "next/navigation"

export async function GET(request: Request) { }

export async function POST(request: Request) {
    const db = (await clientPromise).db()
    let body = await request.json()

    const service = await db.collection("queue_services").findOne({ _id: new ObjectId(body.service) })
    if (!service)
        notFound()
    let startDay = new Date(body.date)
    startDay.setHours(0)
    startDay.setMinutes(0)
    startDay.setSeconds(0)
    let lastDay = new Date(body.date)
    lastDay.setHours(23)
    lastDay.setMinutes(59)
    lastDay.setSeconds(59)

    let lastTicket = await db.collection('queue_tickets').findOne({
        service: new ObjectId(service._id),
        transfered: { $exists: false },
        'date': { $gte: startDay, $lt: lastDay }
    }, { sort: { "created.at": -1 } })

    let last: any
    let number = lastTicket?.number + 1 || 1
    if (lastTicket) {
        last = await db.collection('queue_tickets').insertOne({
            service: new ObjectId(service._id),
            symbol: service.symbol,
            number: number,
            date: new Date(body.date),
            created: { at: new Date() }
        })
    } else {
        last = await db.collection('queue_tickets').insertOne({
            service: new ObjectId(service._id),
            symbol: service.symbol,
            number: number,
            date: new Date(body.date),
            created: { at: new Date() }
        })
    }
    VQueueTickets()
    let waiting = await db.collection('queue_tickets').countDocuments({ service: new ObjectId(service._id), 'date': { $gte: startDay, $lt: lastDay }, called: { $exists: false } })
    if (body.print) {
        const ThermalPrinter = require("node-thermal-printer").printer;
        const PrinterTypes = require("node-thermal-printer").types;
        
        let printer = new ThermalPrinter({
          type: PrinterTypes.EPSON,
          interface: '/dev/usb/lp1',
        });
        printer.alignCenter();
        await printer.printImage('./public/img/favicon/apple-touch-icon.png')
        printer.setTextSize(1,1);
        printer.bold(true);
        printer.println("UPTD PUSKESMAS GEMPOL");
        printer.bold(false);
        printer.setTextSize(0,0);
        printer.println("Jl. KH. Agus Salim No.26, Kec. Gempol");
        printer.println("Kabupaten Cirebon, Jawa Barat 45161");
        printer.println("");
        printer.leftRight(DayDateFormat(new Date().toString()), new Date().getHours().toString().padStart(2,"0")+":"+new Date().getMinutes().toString().padStart(2,"0"));

        printer.bold(true);
        printer.setTextDoubleHeight();  
        printer.setTextDoubleWidth(); 
        printer.setTextSize(7,7);
        printer.println("");
        printer.println(service.symbol+number);
        printer.println("");
        printer.setTextSize(2,1);
        printer.println(service.name);
        printer.println("")
        printer.setTextNormal();
        printer.println("Antrian menunggu didepan anda : "+(waiting >= 1 ? waiting - 1 : 0));
        printer.println("Nomor antrian yang sudah terlewati,");
        printer.println("silahkan mengambil antrian baru");
        printer.println("")
        printer.printQR(last.insertedId.toString());
        printer.cut();

        try {
          let execute = await printer.execute()
          console.error("Print Executed!", execute);
        } catch (error) {
          console.log("Print failed:", error);
        }

    }

    return new Response(JSON.stringify({ status: "OK", service, number, waiting: waiting >= 1 ? waiting - 1 : 0 }), { status: 201, headers: { 'Content-Type': 'application/json' } })
}