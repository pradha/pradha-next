import { authOptions } from "@/lib/auth/options"
import clientPromise from "@/lib/connections/mongo"
import { ObjectId } from "mongodb"
import { getServerSession } from "next-auth"
import { notFound } from "next/navigation"

export async function POST(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()
        let startDay = new Date(body.date)
        startDay.setHours(0)
        startDay.setMinutes(0)
        startDay.setSeconds(0)
        let lastDay = new Date(body.date)
        lastDay.setHours(23)
        lastDay.setMinutes(59)
        lastDay.setSeconds(59)

        const counter = await db.collection("queue_counters").findOne({ _id: new ObjectId(body.counter) })

        if (!counter)
            notFound()
        let nextTicket: any
        let waiting:any
        if (counter.type == "New Tickets Only") {
            nextTicket = await db.collection("v_queue_tickets").findOne({ "service._id": { $in: counter.services }, 'date': { $gte: startDay, $lt: lastDay }, "transfered": { $exists: false }, _id: { $gt: new ObjectId(body.ticket) } }, { sort: { "created.at": 1 } })
            waiting = JSON.parse(JSON.stringify(await db.collection("v_queue_tickets").find({ "service._id": { $in: counter.services }, 'date': { $gte: startDay, $lt: lastDay }, "transfered": { $exists: false }, "called.by": { $exists: false } }, { sort: { "created.at": 1 } }).toArray()))
        } else if (counter.type == "Transfered Only") {
            nextTicket = await db.collection("v_queue_tickets").findOne({ "service._id": { $in: counter.services }, 'date': { $gte: startDay, $lt: lastDay }, "transfered": { $exists: true }, _id: { $gt: new ObjectId(body.ticket) } }, { sort: { "created.at": 1 } })
            waiting = JSON.parse(JSON.stringify(await db.collection("v_queue_tickets").find({ "service._id": { $in: counter.services }, 'date': { $gte: startDay, $lt: lastDay }, "transfered": { $exists: true }, "called.by": { $exists: false } }, { sort: { "created.at": 1 } }).toArray()))
        } else {
            nextTicket = await db.collection("v_queue_tickets").findOne({ "service._id": { $in: counter.services }, 'date': { $gte: startDay, $lt: lastDay }, _id: { $gt: new ObjectId(body.ticket) } }, { sort: { "created.at": 1 } })
            waiting = JSON.parse(JSON.stringify(await db.collection("v_queue_tickets").find({ "service._id": { $in: counter.services }, 'date': { $gte: startDay, $lt: lastDay }, "called.by": { $exists: false } }, { sort: { "created.at": 1 } }).toArray()))
        }

        let transfered = nextTicket?._id ? await db.collection("queue_tickets").findOne({ "transfered": new ObjectId(nextTicket._id) }) : false
        return new Response(JSON.stringify({ status: "OK", waiting, nextTicket, transfered: transfered ? true : false }), { status: 201, headers: { 'Content-Type': 'application/json' } })
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}