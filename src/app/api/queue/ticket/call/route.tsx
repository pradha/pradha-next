import { authOptions } from "@/lib/auth/options"
import clientPromise from "@/lib/connections/mongo"
import { ObjectId } from "mongodb"
import { getServerSession } from "next-auth"
import { notFound } from "next/navigation"

export async function POST(request: Request, response: Response) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()

        let startDay = new Date(body.date)
        startDay.setHours(0)
        startDay.setMinutes(0)
        startDay.setSeconds(0)
        let lastDay = new Date(body.date)
        lastDay.setHours(23)
        lastDay.setMinutes(59)
        lastDay.setSeconds(59)

        const ticket = await db.collection("queue_tickets").findOne({ _id: new ObjectId(body.ticket), 'created.at': { $gte: startDay, $lt: lastDay } })
        if (!ticket)
            notFound()
        let update = await db.collection("queue_tickets").updateOne({ _id: new ObjectId(ticket._id) }, { $set: { called: { counter: new ObjectId(body.counter), by: new ObjectId(session.user.id), at: new Date() } } })
        const lastTicket = await db.collection("v_queue_tickets").findOne({ _id: new ObjectId(ticket._id) })
        let transfered = lastTicket?._id ? await db.collection("queue_tickets").findOne({ "transfered": new ObjectId(lastTicket._id) }) : false
        return new Response(JSON.stringify({ status: "OK", lastTicket, transfered: transfered ? true : false  }), { status: 201, headers: { 'Content-Type': 'application/json' } })
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}