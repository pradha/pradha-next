import { authOptions } from "@/lib/auth/options"
import clientPromise from "@/lib/connections/mongo"
import { ObjectId } from "mongodb"
import { getServerSession } from "next-auth"
import { notFound } from "next/navigation"

export async function POST(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()
        let startDay = new Date(body.date)
        startDay.setHours(0)
        startDay.setMinutes(0)
        startDay.setSeconds(0)
        let lastDay = new Date(body.date)
        lastDay.setHours(23)
        lastDay.setMinutes(59)
        lastDay.setSeconds(59)

        const counter = await db.collection("queue_counters").findOne({ _id: new ObjectId(body.counter) })

        if (!counter)
            notFound()

        let previousTicket: any
        let waiting:any
        if (counter.type == "New Tickets Only") {
            previousTicket = await db.collection("v_queue_tickets").findOne({ "service._id": { $in: counter.services }, 'date': { $gte: startDay, $lt: lastDay }, "transfered": { $exists: false }, _id: { $lt: new ObjectId(body.ticket) } }, { sort: { "created.at": -1 } })
            waiting = JSON.parse(JSON.stringify(await db.collection("v_queue_tickets").find({ "service._id": { $in: counter.services }, 'date': { $gte: startDay, $lt: lastDay }, "transfered": { $exists: false }, "called.by": { $exists: false } }, { sort: { "created.at": 1 } }).toArray()))
        } else if (counter.type == "Transfered Only") {
            previousTicket = await db.collection("v_queue_tickets").findOne({ "service._id": { $in: counter.services }, 'date': { $gte: startDay, $lt: lastDay }, "transfered": { $exists: true }, _id: { $lt: new ObjectId(body.ticket) } }, { sort: { "created.at": -1 } })
            waiting = JSON.parse(JSON.stringify(await db.collection("v_queue_tickets").find({ "service._id": { $in: counter.services }, 'date': { $gte: startDay, $lt: lastDay }, "transfered": { $exists: true }, "called.by": { $exists: false } }, { sort: { "created.at": 1 } }).toArray()))
        } else {
            previousTicket = await db.collection("v_queue_tickets").findOne({ "service._id": { $in: counter.services }, 'date': { $gte: startDay, $lt: lastDay }, _id: { $lt: new ObjectId(body.ticket) } }, { sort: { "created.at": -1 } })
            waiting = JSON.parse(JSON.stringify(await db.collection("v_queue_tickets").find({ "service._id": { $in: counter.services }, 'date': { $gte: startDay, $lt: lastDay }, "called.by": { $exists: false } }, { sort: { "created.at": 1 } }).toArray()))
        }

        let transfered = previousTicket?._id ? await db.collection("queue_tickets").findOne({ "transfered": new ObjectId(previousTicket._id) }) : false
        return new Response(JSON.stringify({ status: "OK", waiting, previousTicket, transfered: transfered ? true : false }), { status: 201, headers: { 'Content-Type': 'application/json' } })
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}