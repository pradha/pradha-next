import clientPromise from "@/lib/connections/mongo";
import { ObjectId, Decimal128 } from "mongodb";
import { getServerSession } from "next-auth";
import { authOptions } from "@/lib/auth/options";
import { VCompanyRegions, VCompanyWarehouses } from "@/lib/connections/views/company";
import { VQueueServices } from "@/lib/connections/views/queue";

const collection = 'queue_settings'
let errors = {
    name: "",
    address: "",
    phone: "",
}

export async function PUT(request: Request) {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        let body = await request.json()
        if (body.name.trim().length < 1 || body.address.trim().length < 1 || body.phone.trim().length < 1) {
            if (body.name.trim().length < 1) errors.name = "please fill queue name"
            if (body.address.trim().length < 1) errors.address = "please fill queue address"
            if (body.phone.trim().length < 1) errors.phone = "please fill the queue phone"
            return new Response(JSON.stringify({ status: "Invalid", errors }), { status: 406, headers: { 'Content-Type': 'application/json' } })
        } else {
            body.video = body.video === "1"
            body.ticker = { 'display': (body.display_ticker === "1"), 'touch': (body.touch_ticker === "1") }
            body.updated = { 'at': new Date(), 'by': new ObjectId(session.user.id) }
            delete body.display_ticker
            delete body.touch_ticker
            let check = await db.collection(collection).findOne({})
            let update:any
            if (check)
                update = await db.collection(collection).updateOne({ _id: new ObjectId(check._id) }, { $set: body })
            else
                update = await db.collection(collection).insertOne(body)
            return new Response(JSON.stringify({ status: "OK", update }), { status: 201, headers: { 'Content-Type': 'application/json' } })
        }
    } else {
        return new Response(JSON.stringify({ message: 'Unauthorized' }), { status: 403, headers: { 'Content-Type': 'application/json' } })
    }
}