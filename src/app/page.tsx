
import { Dialog, Menu, Transition } from "@headlessui/react";
import { ArrowLeftOnRectangleIcon, ArrowRightOnRectangleIcon, Bars3Icon, BriefcaseIcon, ChartPieIcon, HomeIcon, KeyIcon, MagnifyingGlassIcon, PhoneIcon, PresentationChartLineIcon, QuestionMarkCircleIcon, UserIcon, XMarkIcon } from "@heroicons/react/24/outline";
import { useSession, signOut } from "next-auth/react";
import Link from "next/link";
import { Fragment } from "react";
import Image from "next/image";
import PublicLayout from "@/components/ui/layouts/public";
import { Truncate } from "@/lib/text";
import { FullShort } from "@/lib/time";
import { PostList } from "@/components/ui/layouts/posts";
import clientPromise from "@/lib/connections/mongo";

export const revalidate = 10
export default async function Home(props: any) {
    const db = (await clientPromise).db()
    let dataPerPage = process.env.DATA_PER_PAGE ? parseInt(process.env.DATA_PER_PAGE) : 15
    const posts = JSON.parse(JSON.stringify(await db.collection("v_content_posts").find({ status: 'Published' }).sort('published.at', -1).limit(dataPerPage).toArray()))
    return (
        <>
            <PublicLayout>
                <div className="container grid lg:grid-cols-2 gap-4 grid-cols-2 max-w-6xl mx-auto">
                    <PostList posts={posts} />
                </div>
            </PublicLayout>
        </>
    )
}
