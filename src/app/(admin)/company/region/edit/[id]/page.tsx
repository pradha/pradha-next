'use client'
import { useSession } from "next-auth/react"
import { redirect, usePathname } from 'next/navigation';
import { FormEvent, Fragment, useState } from 'react'
import Link from "next/link";
import { UserIcon, KeyIcon, ArrowPathIcon, ListBulletIcon, PlusIcon, IdentificationIcon, TagIcon, LinkIcon, PencilIcon } from "@heroicons/react/24/outline";
import { Success, Warning } from "@/components/ui/alert";
import { Spinner } from "@/components/ui/loader";
import path from "path";

export const revalidate = 5;
export const dynamic = 'force-dynamic';
export default function Page(props: any) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })

    const pathName = usePathname()

    let errors: string[] = []
    const [data, setData] = useState(props.params.data)
    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState(errors)
    const [success, setSuccess] = useState({ title: "", description: "" })

    const submit = async (e: FormEvent) => {
        e.preventDefault()
        setWarning(errors)
        setSpinner(true)

        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement
        const submit = await fetch(path.posix.join("/api/", pathName as string, '../..'), {
            method: 'PUT',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                id: props.params.id,
                name: (document.getElementById("name") as HTMLInputElement).value,
                description: target.description.value,
                address: target.address.value,
                status: target.status.value,
            })
        });
        //await new Promise(resolve => setTimeout(resolve, 3000));
        const res = await submit.json()
        if (submit.ok) {
            props.params.data.name = (document.getElementById("name") as HTMLInputElement).value,
            props.params.data.description = target.description.value,
            props.params.data.address = target.address.value,
            props.params.data.status = target.status.value
            setData(props.params.data)
            setSuccess({ title: "Success", description: "Company region has been updated" })
        } else {
            errors = await res.errors
            setWarning(Object.values(errors).filter(a => a != ""))
            errors = []
        }
        setSpinner(false)
    }
    return (
        <main>
            <div className="flex flex-col md:flex-row md:space-y-0 space-y-4 mb-4 justify-between">
                <div>
                    <h1 className="text-xl lg:text-2xl font-semibold">Edit Region</h1>
                    <div className="text-sm text-slate-500">Edit Company Region</div>
                </div>
                <div className="flex items-center space-x-2">
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11ab002") &&
                        <Link href={"/company/region/create"} title="New Region" className="btn-circle" aria-label="region-edit">
                            <PlusIcon width={20} height={20} />
                        </Link>
                    }
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11ab001") &&
                        <Link href={"/company/region"} title="Region data" className="btn-circle" aria-label="region-data">
                            <ListBulletIcon width={20} height={20} />
                        </Link>
                    }
                </div>
            </div>

            <div className="relative bg-white shadow-lg w-full rounded-tl-3xl rounded-br-3xl p-6 dark:bg-slate-700">
                {spinner && <Spinner className="rounded-tl-3xl rounded-br-3xl" />}
                <Success data={success} hide={() => setSuccess({ title: "", description: "" })} />
                <form onSubmit={submit} className="ng-untouched ng-pristine ng-valid">
                    <Warning className="" errors={warning} hide={() => setWarning(errors)} />

                    <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                        <div className="flex-1 mb-2">
                            <label className="label" htmlFor="group">Parent</label>
                            <div className="relative inline-block w-full text-gray-700">
                                <select name="parent" id="parent" className="select" defaultValue={data?.parent} disabled>
                                    <option key={Math.random()} value="">- No Parent -</option>
                                    {props.params?.parents.map((item: any, i: number) =>
                                        !item.parent &&
                                        <Fragment key={Math.random()}>
                                            <option key={item._id} value={item._id}>{item.code} | {item.name}</option>
                                            {props.params?.parents.map((sub: any, s: number) =>
                                                sub.parent?._id == item._id &&
                                                <Fragment key={Math.random()}>
                                                    <option key={sub._id} value={sub._id}>{sub.code} | {sub.name}</option>
                                                    {props.params?.parents.map((sub2: any, s2: number) =>
                                                        sub2.parent?._id == sub._id &&
                                                        <Fragment key={Math.random()}>
                                                            <option key={sub2._id} value={sub2._id}>{sub2.code} | {sub2.name}</option>
                                                        </Fragment>
                                                    )}
                                                </Fragment>
                                            )}
                                        </Fragment>
                                    )}
                                </select>
                            </div>
                        </div>
                        <div className="flex-1 mb-2">
                            <label className="label" htmlFor="name">Name <sup className="text-rose-500">*</sup></label>
                            <div className="relative mb-2">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <IdentificationIcon className="text-gray-400 w-5 h-5" />
                                </div>
                                <input type="name" id="name" name='name' className={"input-icon"} placeholder="Region Name" defaultValue={data.name} />
                            </div>
                        </div>
                    </div>
                    <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                        <div className="flex-1 mb-2">
                            <label htmlFor="description" className="label">Description <sup className="text-rose-500">*</sup></label>
                            <textarea name="description" id="description" placeholder="Region Description" className={"input"} rows={3} defaultValue={data.description}></textarea>
                        </div>
                        <div className="flex-1 mb-2">
                            <label htmlFor="address" className="label">Address <sup className="text-rose-500">*</sup></label>
                            <textarea name="address" id="address" placeholder="Region Address" className={"input"} rows={3} defaultValue={data.address}></textarea>
                        </div>
                    </div>
                    <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                        <div className="lg:w-1/4 w-full mb-2">
                            <label className="label" htmlFor="status">Status</label>
                            <div className="relative inline-block w-full text-gray-700">
                                <select name="status" id="status" className="select" defaultValue={data.status.active ? "1":"0"}>
                                    <option value="1">Active</option>
                                    <option value="0">InActive</option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <div className="">
                        <button type="submit" className="btn btn-primary flex items-center space-x-2">
                            <ArrowPathIcon width={20} height={20} className="mr-2" />
                            Update Region
                        </button>
                    </div>
                </form>
            </div>
        </main>
    )
}