import DashboardLayout from "@/components/ui/layouts/dashboard"
import { getServerSession } from "next-auth";
import { notFound, redirect } from "next/navigation";

export const revalidate = 0
export const dynamic = 'force-dynamic'
export default async function RootLayout(props: any) {
    const session = await getServerSession();
    if (!session)
        notFound()

    return (
        <DashboardLayout>{props.children}</DashboardLayout>
    )
}

const title = 'Company Product'
const description = 'Company Products'
const keywords = ["company", "product", "products", "company products"]
const url = process.env.SITE_URL + '/company/product'
export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: false,
        follow: false,
        nocache: false,
        googleBot: {
            index: false,
            follow: false
        },
    },
}