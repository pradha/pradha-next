'use client'
import { useSession } from "next-auth/react"
import { redirect, usePathname } from 'next/navigation';
import { FormEvent, Fragment, useState } from 'react'
import Link from "next/link";
import { UserIcon, KeyIcon, ArrowPathIcon, ListBulletIcon, PlusIcon, IdentificationIcon, TagIcon, LinkIcon, HashtagIcon, BanknotesIcon, ClockIcon, Square3Stack3DIcon } from "@heroicons/react/24/outline";
import { Success, Warning } from "@/components/ui/alert";
import { Spinner } from "@/components/ui/loader";
import path from "path";
import { FormatCurrency } from "@/lib/text";
import { useRouter } from "next/navigation";

export const revalidate = 5;
export const dynamic = 'force-dynamic';
export default function Page(props: any) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })
    const router = useRouter()

    const pathName = usePathname()

    let errors: string[] = []
    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState(errors)
    const [success, setSuccess] = useState({ title: "", description: "" })

    const submit = async (e: FormEvent) => {
        e.preventDefault()
        setWarning(errors)
        setSpinner(true)

        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement
        const submit = await fetch(path.posix.join("/api/", pathName as string, '..'), {
            method: 'POST',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                code: target.code.value,
                name: (document.getElementById("name") as HTMLInputElement).value,
                price: target.price.value,
                description: target.description.value,
                lead_time: target.lead_time.value,
                minimum_quantity: target.minimum_quantity.value,
                status: target.status.value,
            })
        });
        //await new Promise(resolve => setTimeout(resolve, 3000));
        const res = await submit.json()
        if (submit.ok) {
            target.code.value = "";
            (document.getElementById("name") as HTMLInputElement).value = "";
            target.price.value = "";
            target.description.value = "";
            target.status.value = "";
            router.replace('/company/product/detail/'+res.id+'?created')
            setSuccess({ title: "Success", description: "New product has been created" })
        } else {
            errors = await res.errors
            setWarning(Object.values(errors).filter(a => a != ""))
            errors = []
        }
        setSpinner(false)
    }
    return (
        <main>
            <div className="flex flex-col md:flex-row md:space-y-0 space-y-4 mb-4 justify-between">
                <div>
                    <h1 className="text-xl lg:text-2xl font-semibold">New Product</h1>
                    <div className="text-sm text-slate-500">Create New Company Product</div>
                </div>
                <div className="flex items-center space-x-2">
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a3101") &&
                        <Link href={path.join(pathName as string, "..")} title="Product data" className="btn-circle" aria-label="product-data">
                            <ListBulletIcon width={20} height={20} />
                        </Link>
                    }
                </div>
            </div>

            <div className="relative bg-white shadow-lg w-full rounded-tl-3xl rounded-br-3xl p-6 dark:bg-slate-700">
                {spinner && <Spinner className="rounded-tl-3xl rounded-br-3xl" />}
                <Success data={success} hide={() => setSuccess({ title: "", description: "" })} />
                <form onSubmit={submit} className="ng-untouched ng-pristine ng-valid">
                    <Warning className="" errors={warning} hide={() => setWarning(errors)} />

                    <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                        <div className="lg:w-1/4 w-full mb-2">
                            <label className="label" htmlFor="code">Prod ID / Code (SKU) <sup className="text-rose-500">*</sup></label>
                            <div className="relative mb-2">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <HashtagIcon className="text-gray-400 w-5 h-5" />
                                </div>
                                <input type="text" id="code" name='code' className={"input-icon"} placeholder="Prod ID / Code (SKU)" />
                            </div>
                        </div>
                        <div className="lg:w-1/2 w-full mb-2">
                            <label className="label" htmlFor="name">Name <sup className="text-rose-500">*</sup></label>
                            <div className="relative mb-2">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <IdentificationIcon className="text-gray-400 w-5 h-5" />
                                </div>
                                <input type="text" id="name" name='name' className={"input-icon"} placeholder="Product Name" />
                            </div>
                        </div>
                        <div className="lg:w-1/4 w-full mb-2">
                            <label className="label" htmlFor="code">Unit Price <sup className="text-rose-500">*</sup></label>
                            <div className="relative mb-2">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <BanknotesIcon className="text-gray-400 w-5 h-5" />
                                </div>
                                <input type="text" id="price" name='price' className={"input-icon"} placeholder="Price per unit" onBlur={(e) => FormatCurrency(e.target, "", 'blur')} onKeyUp={(e) => FormatCurrency(e.target as HTMLInputElement, "", "")} />
                            </div>
                        </div>
                    </div>
                    <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                        <div className="flex-1 mb-2">
                            <label htmlFor="description" className="label">Description</label>
                            <textarea name="description" id="description" placeholder="Product Description" className={"input"} rows={3}></textarea>
                        </div>
                    </div>
                    <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                        <div className="lg:w-1/4 w-full mb-2">
                            <label className="label" htmlFor="lead_time">Lead Time <sup className="text-rose-500">*</sup></label>
                            <div className="relative mb-2">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <ClockIcon className="text-gray-400 w-5 h-5" />
                                </div>
                                <input type="number" id="lead_time" name='lead_time' className={"input-icon"} placeholder="Lead Time" defaultValue={1} />
                            </div>
                        </div>
                        <div className="lg:w-1/4 w-full mb-2">
                            <label className="label" htmlFor="minimum_quantity">Minimum Purchase Quantity <sup className="text-rose-500">*</sup></label>
                            <div className="relative mb-2">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <Square3Stack3DIcon className="text-gray-400 w-5 h-5" />
                                </div>
                                <input type="number" id="minimum_quantity" name='minimum_quantity' defaultValue={1} className={"input-icon"} placeholder="Minimum Quantity" />
                            </div>
                        </div>
                    </div>
                    <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                        <div className="lg:w-1/4 w-full mb-2">
                            <label className="label" htmlFor="status">Status <sup className="text-rose-500">*</sup></label>
                            <div className="relative inline-block w-full text-gray-700">
                                <select name="status" id="status" className="select" defaultValue={"0"}>
                                    <option value="1">Active</option>
                                    <option value="0">InActive</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="">
                        <button type="submit" className="btn btn-primary flex items-center space-x-2">
                            <PlusIcon width={20} height={20} className="mr-2" />
                            Create Product
                        </button>
                    </div>
                </form>
            </div>
        </main>
    )
}