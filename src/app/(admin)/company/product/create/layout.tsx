import DashboardLayout from "@/components/ui/layouts/dashboard"
import clientPromise from "@/lib/connections/mongo";
import { getServerSession } from "next-auth";
import { redirect } from "next/navigation";

export const revalidate = 0
export const dynamic = 'force-dynamic'
export default async function RootLayout(props:any) {
    const session = await getServerSession();
    if (!session)
        redirect('/');

    return (
        <>{props.children}</>
        
    )
}

const title = 'Create New Product'
const description = 'Create New Company Product'
const keywords = ["company", "product", "company product", "products", "create"]
const url = process.env.SITE_URL + '/company/product/create'
export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: false,
        follow: false,
        nocache: false,
        googleBot: {
            index: false,
            follow: false
        },
    },
}