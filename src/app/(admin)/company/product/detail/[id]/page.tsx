'use client'
import { useSession } from "next-auth/react"
import { redirect, usePathname } from 'next/navigation';
import { FormEvent, Fragment, useEffect, useState } from 'react'
import Link from "next/link";
import { ListBulletIcon, PlusIcon, PencilIcon, NoSymbolIcon, CheckIcon, XMarkIcon, TrashIcon } from "@heroicons/react/24/outline";
import { Success, Warning } from "@/components/ui/alert";
import { Spinner } from "@/components/ui/loader";
import path from "path";
import { FormatCurrency, FormatMoney } from "@/lib/text";
import { DateTimeShort } from "@/lib/time";
import { Dialog, Transition } from "@headlessui/react";

export const revalidate = 0;
export const dynamic = 'force-dynamic';
export default function Page(props: any) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })

    const pathName = usePathname()
    let errors: string[] = []
    const [data, setData] = useState(props.params.data)
    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState(errors)
    const [success, setSuccess] = useState({ title: "", description: "" })
    const [showImage, setShowImage] = useState(false)
    const [image, setImage] = useState("")

    const deleteImage = async (e: FormEvent) => {
        e.preventDefault()
        setWarning(errors)
        setSpinner(true)

        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement
        const submit = await fetch("/api/company/product/image", {
            method: 'DELETE',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                id: (document.getElementById("id") as HTMLInputElement).value,
                image: target.image.value,
            })
        });
        //await new Promise(resolve => setTimeout(resolve, 3000));
        const res = await submit.json()
        if (submit.ok) {
            setData(res.product)
            setShowImage(false)
            setSuccess({ title: "Success", description: "Image has been successfully deleted" })
        } else {
            errors = await res.errors
            setWarning(Object.values(errors).filter(a => a != ""))
            errors = []
        }
        setSpinner(false)
    }
    const addImage = async (e: FormEvent) => {
        e.preventDefault()
        setWarning(errors)
        setSpinner(true)
        setSuccess({ title: "", description: "" })

        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }
        const body = new FormData()
        const file = e.target as HTMLInputElement
        if (file && file.files) {
            body.append("id", data._id)
            body.append("file", file.files[0])
            try {
                const submit = await fetch("/api/company/product/image", {
                    method: "POST",
                    body
                });
                const res = await submit.json()

                if (submit.ok) {
                    setData(res.product)
                    setSuccess({ title: "Success", description: res.message })
                } else {
                    setWarning([res.msg])
                }
            } catch (err: any) {
                errors = [await err.message]
                setWarning(errors)
            }
            file.value = ""
        }
        setSpinner(false)
    }
    const getImage = async () => {
        setSpinner(true);
        const data = await fetch('/api/' + (pathName ? pathName : "..") + "../../../image?id=" + props.params.data._id, {
            cache: 'no-store',
            headers: { 'Content-Type': 'application/json' },
            method: 'GET'
        })
        setSpinner(false);
        return data.json()
    }
    let loaded = false
    useEffect(() => {
        if (!loaded) {
            (async () => {
                let images = await getImage()
                let newData = data
                newData.images = images.images
                setData(newData)
            })();
            loaded = true
            return
        }
    }, [])
    return (
        <main>
            <div className="flex flex-col md:flex-row md:space-y-0 space-y-4 mb-4 justify-between">
                <div>
                    <h1 className="text-xl lg:text-2xl font-semibold">Product Detail</h1>
                    <div className="text-sm text-slate-500">Company Product Detail</div>
                </div>
                <div className="flex items-center space-x-2">
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11ab017") &&
                        <Link href={path.join(pathName as string, "../../create")} title="Create New Product" className="btn-circle" aria-label="product-create">
                            <PlusIcon width={20} height={20} />
                        </Link>
                    }
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11ab018") &&
                        <Link href={path.join(pathName as string, "../../edit/" + data._id)} title="Edit Product" className="btn-circle" aria-label="product-edit">
                            <PencilIcon width={20} height={20} />
                        </Link>
                    }
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11ab015") &&
                        <Link href={path.join(pathName as string, "../..")} title="Product data" className="btn-circle" aria-label="product-data">
                            <ListBulletIcon width={20} height={20} />
                        </Link>
                    }
                </div>
            </div>

            <Transition appear show={showImage} as={Fragment}>
                <Dialog as="div" className="relative z-10" onClose={() => setShowImage(false)}>
                    <Transition.Child
                        as={Fragment}
                        enter="ease-out duration-300"
                        enterFrom="opacity-0"
                        enterTo="opacity-100"
                        leave="ease-in duration-200"
                        leaveFrom="opacity-100"
                        leaveTo="opacity-0"
                    >
                        <div className="fixed inset-0 bg-white dark:bg-black dark:bg-opacity-75 blur-lg bg-opacity-75" />
                    </Transition.Child>

                    <div className="fixed inset-0 overflow-y-auto">
                        <div className="flex min-h-full items-center justify-center p-4 text-center">
                            <Transition.Child
                                as={Fragment}
                                enter="ease-out duration-300"
                                enterFrom="opacity-0 scale-95"
                                enterTo="opacity-100 scale-100"
                                leave="ease-in duration-200"
                                leaveFrom="opacity-100 scale-100"
                                leaveTo="opacity-0 scale-95"
                            >
                                <Dialog.Panel className="w-full max-w-3xl transform overflow-hidden rounded-2xl bg-white p-6 text-left align-middle shadow-xl transition-all">
                                    <Dialog.Title as="h3" className="text-lg flex justify-between font-medium leading-6 text-gray-900" >
                                        Product Image
                                        <XMarkIcon title="close" width={16} className="cursor-pointer  text-slate-400" onClick={() => setShowImage(false)} />
                                    </Dialog.Title>
                                    <div className="mt-2">
                                        <img className="object-contain h-96 w-full transition-all ease-in-out border-none overflow-hidden" src={image} />
                                    </div>

                                    <form onSubmit={deleteImage} className="flex space-x-2 justify-end mt-4">
                                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11ab020") &&
                                            <Fragment>
                                                <input type="hidden" name="image" value={image} />
                                                <input type="hidden" name="id" id="id" value={data._id} />
                                                <button type="submit" className="btn flex items-center space-x-2" title="Delete Image">
                                                    <TrashIcon width={20} height={20} className="mr-2 text-red-700" />
                                                </button>
                                            </Fragment>
                                        }
                                        <button
                                            type="button"
                                            className="inline-flex justify-center rounded-md border border-transparent bg-blue-100 px-4 py-2 text-sm font-medium text-blue-900 hover:bg-blue-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-blue-500 focus-visible:ring-offset-2"
                                            onClick={() => setShowImage(false)}
                                        >
                                            Close
                                        </button>
                                    </form>
                                </Dialog.Panel>
                            </Transition.Child>
                        </div>
                    </div>
                </Dialog>
            </Transition>

            <div className="relative">
                {spinner && <Spinner className="rounded-tl-3xl rounded-br-3xl" />}
                <Success data={success} hide={() => setSuccess({ title: "", description: "" })} />
                <Warning className="" errors={warning} hide={() => setWarning(errors)} />
                <div className="flex flex-col md:flex-row md:space-x-4 space-x-0 md:space-y-0 space-y-4 relative z-0 w-full">
                    <div className="w-full md:w-2/3 bg-white dark:bg-slate-700 shadow-lg rounded-tl-3xl rounded-br-3xl p-4">
                        <div className="flex mb-4">
                            {data?.images && data.images.map((img: string) =>
                                <div key={Math.random()} onClick={() => { setImage(img), setShowImage(true) }} className="relative first:ml-0 ml-2 my-2 mr-2 group bg-gray-50 dark:bg-slate-600 lg:w-1/4 rounded-lg overflow-hidden hover:bg-slate-400">
                                    <img className="object-contain h-48 w-full transition-all ease-in-out border-none overflow-hidden" src={img} />
                                </div>
                            )}
                            {('images' in data === false || (data?.images && data.images.length < 4)) && session?.user.group.roles.includes("61e3de6ecc4029a9d11ab019") &&
                                <div onClick={() => (document.getElementById("image") as HTMLInputElement).click()} className="first:ml-0 ml-2 my-2 mr-2 h-48 w-1/4 grid place-content-center text-gray-400 group cursor-pointer bg-gray-50 lg:w-1/4 border border-dashed border-gray-100 rounded-md text-center">
                                    <PlusIcon width={24} height={24} className="group-hover:text-gray-600  mx-auto" />
                                    <div className="group-hover:text-gray-600  mx-auto">Add Image</div>
                                    <input onChange={addImage} type="file" id="image" name="image" className="hidden" accept="image/png, image/gif, image/jpeg, image/webp" />
                                </div>
                            }

                        </div>
                        <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2 p-4'>
                            <div className="lg:w-1/2 w-full mb-2">
                                <div className="relative mb-4">
                                    <label className="label mb-1" >Prod ID / Code (SKU)</label>
                                    <div className="font-light">{data.code}</div>
                                </div>
                                <div className="relative mb-4">
                                    <label className="label mb-1">Name</label>
                                    <div className="font-light">{data.name}</div>
                                </div>
                                <div className="relative mb-4">
                                    <label className="label mb-1">Price</label>
                                    <div className="font-light">{data?.price ? FormatMoney(data.price.$numberDecimal) : "0.00"}</div>
                                </div>
                                <div className="relative mb-4">
                                    <label className="label mb-1">Description</label>
                                    <div className="font-light">
                                        {data?.description.split("\n").map((line: any) => <Fragment key={Math.random()}>{line}<br /></Fragment>)}
                                    </div>
                                </div>
                            </div>
                            <div className="lg:w-1/2 w-full mb-2">
                                <div className="relative mb-4">
                                    <label className="label mb-1">Lead Time</label>
                                    <div className="font-light">{data.lead_time} day{data.lead_time > 1 ? "s" : ""}</div>
                                </div>
                                <div className="relative mb-4">
                                    <label className="label mb-1">Minimum Purchase Quantity</label>
                                    <div className="font-light">{data.minimum_quantity} pcs</div>
                                </div>
                                <div className="relative mb-4">
                                    <label className="label mb-1">Price</label>
                                    <div className="font-light">{data?.price ? FormatMoney(data.price.$numberDecimal) : "0.00"}</div>
                                </div>
                                <div className="relative mb-4">
                                    <label className="label mb-1">Status</label>
                                    <div className="font-light">{data.status?.active ? <CheckIcon title="Active" width={20} className="text-green-600" /> : <NoSymbolIcon title="InActive" width={20} className="text-red-600" />}</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div className="w-full md:w-1/3 bg-white dark:bg-slate-700 shadow-lg rounded-tl-3xl rounded-br-3xl p-4">
                        <h2 className="font-semibold text-lg md:mb-4 mb-2 border-b py-1 border-gray-100 dark:border-slate-600">Creation & Status Info</h2>
                        <div className="px-2">
                            <div className="flex flex-col mb-4">
                                <label className="text-xs md:text-sm">Created</label>
                                <div className="text-xs">date <span className="font-semibold text-base">{DateTimeShort(data.created.at)}</span></div>
                                <div className="text-xs">by <span className="font-semibold text-base">{data.created.by.name}</span></div>
                            </div>
                            {data?.updated && data.updated?.by &&
                                <div className="flex flex-col mb-4">
                                    <label className="text-xs md:text-sm">Updated</label>
                                    <div className="text-xs">date <span className="font-semibold text-base">{DateTimeShort(data.updated.at)}</span></div>
                                    <div className="text-xs">by <span className="font-semibold text-base">{data.updated.by.name}</span></div>
                                </div>
                            }

                        </div>
                    </div>
                </div>
            </div >
        </main >
    )
}