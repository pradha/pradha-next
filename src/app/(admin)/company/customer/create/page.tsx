'use client'
import { useSession } from "next-auth/react"
import { redirect, usePathname } from 'next/navigation';
import { FormEvent, Fragment, useState } from 'react'
import Link from "next/link";
import { UserIcon, KeyIcon, ArrowPathIcon, ListBulletIcon, PlusIcon, IdentificationIcon, TagIcon, LinkIcon } from "@heroicons/react/24/outline";
import { Success, Warning } from "@/components/ui/alert";
import { Spinner } from "@/components/ui/loader";
import path from "path";

export const revalidate = 5;
export const dynamic = 'force-dynamic';
export default function Page(props: any) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })

    const pathName = usePathname()

    let errors: string[] = []
    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState(errors)
    const [success, setSuccess] = useState({ title: "", description: "" })

    const submit = async (e: FormEvent) => {
        e.preventDefault()
        setWarning(errors)
        setSpinner(true)

        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement
        const submit = await fetch(path.posix.join("/api/", pathName as string, '..'), {
            method: 'POST',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                parent: target.parent.value,
                name: (document.getElementById("name") as HTMLInputElement).value,
                description: target.description.value,
                address: target.address.value,
                status: target.status.value,
            })
        });
        //await new Promise(resolve => setTimeout(resolve, 3000));
        const res = await submit.json()
        if (submit.ok) {
            target.parent.value = "";
            (document.getElementById("name") as HTMLInputElement).value = "";
            target.description.value = "";
            target.address.value = "";
            target.status.value = "";

            setSuccess({ title: "Success", description: "New customer has been created" })
        } else {
            errors = await res.errors
            setWarning(Object.values(errors).filter(a => a != ""))
            errors = []
        }
        setSpinner(false)
    }
    return (
        <main>
            <div className="flex flex-col md:flex-row md:space-y-0 space-y-4 mb-4 justify-between">
                <div>
                    <h1 className="text-xl lg:text-2xl font-semibold">New Customer</h1>
                    <div className="text-sm text-slate-500">Create New Company Customer</div>
                </div>
                <div className="flex items-center space-x-2">
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11ab005") &&
                        <Link href={path.posix.join(pathName ? pathName : "", "..")} as={path.posix.join(pathName ? pathName : "", "..")} title="Customer data" className="btn-circle" aria-label="category-data">
                            <ListBulletIcon width={20} height={20} />
                        </Link>
                    }
                </div>
            </div>

            <div className="relative bg-white shadow-lg w-full rounded-tl-3xl rounded-br-3xl p-6 dark:bg-slate-700">
                {spinner && <Spinner className="rounded-tl-3xl rounded-br-3xl" />}
                <Success data={success} hide={() => setSuccess({ title: "", description: "" })} />
                <form onSubmit={submit} className="ng-untouched ng-pristine ng-valid">
                    <Warning className="" errors={warning} hide={() => setWarning(errors)} />
                    <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                        <div className="lg:w-1/2 w-full mb-2">
                            <label className="label" htmlFor="identity_number">Identity Number</label>
                            <div className="relative inline-block w-full text-gray-700">
                                <input type="text" id="identity_number" name='identity_number' className={"input"} placeholder="Identity Number" />
                            </div>
                        </div>
                        <div className="lg:w-1/4 w-full mb-2">
                            <label className="label" htmlFor="identity_type">ID Type <sup className="text-rose-500">*</sup></label>
                            <div className="relative inline-block w-full text-gray-700">
                                <select name="identity_type" id="identity_type" className="select" defaultValue={""}>
                                    <option key="None" value="None">None</option>
                                    <option key="ID_Card" value="ID Card">ID Card</option>
                                    <option key="Driver_license" value="Driver License">Driver License</option>
                                    <option key="Pasport" value="Pasport">Pasport</option>
                                    <option key="Business_License" value="Business License">Business License</option>
                                </select>
                            </div>
                        </div>
                        <div className="lg:w-1/4 w-full mb-2">
                            <label className="label" htmlFor="customer_type">Customer Type <sup className="text-rose-500">*</sup></label>
                            <div className="relative inline-block w-full text-gray-700">
                                <select name="customer_type" id="customer_type" className="select" defaultValue={""}>
                                    <option key="Individual" value="Individual">Individual</option>
                                    <option key="Representative" value="Representative">Representative</option>
                                    <option key="Company" value="F">Company</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="border-b border-slate-300 dark:border-slate-600 lg:mt-8 mt-4 mb-4 text-slate-400 text-xs">Personal Identities</div>
                    <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                        <div className="lg:w-1/4 w-full mb-2">
                            <label className="label" htmlFor="prefix_name">Prefix Name</label>
                            <div className="relative inline-block w-full text-gray-700">
                                <input type="text" id="prefix_name" name='prefix_name' className={"input"} placeholder="Prefix Name (Mr. Ms. Dr. etc)" />
                            </div>
                        </div>
                        <div className="lg:w-1/2 w-full mb-2">
                            <label className="label" htmlFor="name">Full Name <sup className="text-rose-500">*</sup></label>
                            <div className="relative mb-2">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <IdentificationIcon className="text-gray-400 w-5 h-5" />
                                </div>
                                <input type="name" id="name" name='name' className={"input-icon"} placeholder="Full Name" />
                            </div>
                        </div>
                        <div className="lg:w-1/4 w-full mb-2">
                            <label className="label" htmlFor="suffix_name">Suffix Name</label>
                            <div className="relative inline-block w-full text-gray-700">
                                <input type="text" id="suffix_name" name='suffix_name' className={"input"} placeholder="Suffix Name (S.Kom, M.Si. etc)" />
                            </div>
                        </div>
                    </div>
                    <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                        <div className="lg:w-1/2 w-full mb-2">
                            <label className="label" htmlFor="nick_name">Nick Name <sup className="text-rose-500">*</sup></label>
                            <div className="relative inline-block w-full text-gray-700">
                                <input type="text" id="nick_name" name='nick_name' className={"input"} placeholder="Nick Name" />
                            </div>
                        </div>
                        <div className="lg:w-1/4 w-full mb-2">
                            <label className="label" htmlFor="gender">Gender <sup className="text-rose-500">*</sup></label>
                            <div className="relative inline-block w-full text-gray-700">
                                <select name="gender" id="gender" className="select" defaultValue={"U"}>
                                    <option key="U" value="U">Unknown</option>
                                    <option key="M" value="M">Male</option>
                                    <option key="F" value="F">Female</option>
                                </select>
                            </div>
                        </div>
                        <div className="lg:w-1/4 w-full mb-2">
                            <label className="label" htmlFor="religion">Religion <sup className="text-rose-500">*</sup></label>
                            <div className="relative inline-block w-full text-gray-700">
                                <select name="religion" id="religion" className="select" defaultValue={""}>
                                    <option key="" value="">None</option>
                                    <option key="Islam" value="Islam">Islam</option>
                                    <option key="KristenM" value="Kristen">Kristen</option>
                                    <option key="Katholik" value="Katholik">Katholik</option>
                                    <option key="Hindu" value="FHindu">Hindu</option>
                                    <option key="Budha" value="Budha">Budha</option>
                                    <option key="Konghuchu" value="Konghuchu">Konghuchu</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div className="border-b border-slate-300 dark:border-slate-600 lg:mt-8 mt-4 mb-4 text-slate-400 text-xs">Personal Address</div>
                    <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                        <div className="flex-1 mb-2">
                            <label htmlFor="address" className="label">Address <sup className="text-rose-500">*</sup></label>
                            <textarea name="address" id="address" placeholder="Region Address" className={"input"} rows={3}></textarea>
                        </div>
                    </div>

                    <div className='flex flex-col md:flex-row space-x-0 md:space-x-4 md:space-y-0 space-y-2  mb-2'>
                        <div className="lg:w-1/4 md:w-1/2 w-full mb-2">
                            <label className="label" htmlFor="province">Province <sup className="text-rose-500">*</sup></label>
                            <div className="relative inline-block w-full text-gray-700">
                                <select name="province" id="province" className="select" defaultValue={""}>
                                    <option value={""}>- Select Province -</option>
                                </select>
                            </div>
                        </div>
                        <div className="lg:w-1/4 md:w-1/2 w-full mb-2">
                            <label className="label" htmlFor="district">District <sup className="text-rose-500">*</sup></label>
                            <div className="relative inline-block w-full text-gray-700">
                                <select name="district" id="district" className="select" defaultValue={""}>
                                    <option value={""}>- Select District -</option>
                                </select>
                            </div>
                        </div>
                        <div className="lg:w-1/4 md:w-1/2 w-full mb-2">
                            <label className="label" htmlFor="sub_district">Sub District <sup className="text-rose-500">*</sup></label>
                            <div className="relative inline-block w-full text-gray-700">
                                <select name="sub_district" id="sub_district" className="select" defaultValue={""}>
                                    <option value={""}>- Select Sub District -</option>
                                </select>
                            </div>
                        </div>
                        <div className="lg:w-1/4 md:w-1/2 w-full mb-2">
                            <label className="label" htmlFor="village">Village <sup className="text-rose-500">*</sup></label>
                            <div className="relative inline-block w-full text-gray-700">
                                <select name="village" id="village" className="select" defaultValue={""}>
                                    <option value={""}>- Select Village -</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                        <div className="lg:w-1/4 w-full mb-2">
                            <label className="label" htmlFor="postal_code">Postal Code</label>
                            <div className="relative inline-block w-full text-gray-700">
                                <div className="relative inline-block w-full text-gray-700">
                                    <input type="text" id="postal_code" name='postal_code' className={"input"} placeholder="Postal Code" />
                                </div>
                            </div>
                        </div>
                        <div className="flex flex-row lg:w-1/2 w-full space-x-4">
                            <div className="lg:w-1/2 w-full mb-2">
                                <label className="label" htmlFor="rt">RT</label>
                                <div className="relative inline-block w-full text-gray-700">
                                    <div className="relative inline-block w-full text-gray-700">
                                        <input type="text" id="rt" name='rt' className={"input"} placeholder="RT" />
                                    </div>
                                </div>
                            </div>
                            <div className="lg:w-1/2 w-full mb-2">
                                <label className="label" htmlFor="rw">RW</label>
                                <div className="relative inline-block w-full text-gray-700">
                                    <div className="relative inline-block w-full text-gray-700">
                                        <input type="text" id="rw" name='rw' className={"input"} placeholder="RW" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="lg:w-1/4 w-full mb-2"></div>
                    </div>

                    <div className="border-b border-slate-300 dark:border-slate-600 lg:mt-8 mt-4 mb-4 text-slate-400 text-xs">Contact</div>
                    <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                        <div className="lg:w-3/4 w-full mb-2">
                            <label className="label" htmlFor="email">E-Mail</label>
                            <div className="relative inline-block w-full text-gray-700">
                                <div className="relative inline-block w-full text-gray-700">
                                    <input type="text" id="email" name='email' className={"input"} placeholder="Email Address" />
                                </div>
                            </div>
                        </div>
                        <div className="lg:w-1/4 w-full mb-2">
                            <label className="label" htmlFor="phone">Phone</label>
                            <div className="relative inline-block w-full text-gray-700">
                                <div className="relative inline-block w-full text-gray-700">
                                    <input type="text" id="phone" name='phone' className={"input"} placeholder="Phone No" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="">
                        <button type="submit" className="btn btn-primary flex items-center space-x-2">
                            <PlusIcon width={20} height={20} className="mr-2" />
                            Create Customer
                        </button>
                    </div>
                </form>
            </div>
        </main>
    )
}