'use client'
import { useSession } from "next-auth/react"
import { redirect } from 'next/navigation';
import { ChangeEvent, FormEvent, Fragment, useEffect, useState } from 'react'
import Link from "next/link";
import { PlusIcon, PencilIcon, MagnifyingGlassIcon, XMarkIcon, ChevronDoubleRightIcon, XCircleIcon, AdjustmentsHorizontalIcon, IdentificationIcon, CheckIcon, NoSymbolIcon, PlayIcon, HandRaisedIcon, ArrowPathIcon, PrinterIcon, MegaphoneIcon, ChevronLeftIcon, ChevronRightIcon } from "@heroicons/react/24/outline";
import { Spinner } from "@/components/ui/loader";
import { FullShort } from "@/lib/time";
import { usePathname } from 'next/navigation';
import { Pagination } from "@/components/ui/pagination";
import path from "path";
import { FormatMoney, FormatNumber } from "@/lib/text";
import { Warning } from "@/components/ui/alert";

export default function Display(props: any) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })
    const pathName = usePathname()



    return (
        <main>
            <div className="flex flex-col justify-between mb-4 space-y-4 md:flex-row md:space-y-0">
                <div>
                    <h1 className="text-xl font-semibold lg:text-2xl">Display</h1>
                    <div className="text-sm text-slate-500">Queue Displays</div>
                </div>
                <div className="flex items-center space-x-2">
                </div>
            </div>

            <div className="relative text-gray-600  ">
                <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                    <Link href={'/queue/display/1'} target="_blank" className="lg:w-1/4 w-full h-40 flex items-center text-center bg-white shadow-lg rounded-md dark:bg-slate-700 dark:text-slate-300 transform transition-all ease-in-out">
                        <div className="text-center mx-auto">Display 1</div>
                    </Link>
                    <Link href={'/queue/display/2'} target="_blank" className="lg:w-1/4 w-full h-40 flex place-items-center text-center items-center bg-white shadow-lg rounded-md dark:bg-slate-700 dark:text-slate-300 transform transition-all ease-in-out">
                        <div className="text-center mx-auto">Display 2</div>
                    </Link>
                    <Link href={'/queue/display/3'} target="_blank" className="lg:w-1/4 w-full h-40 flex place-items-center text-center items-center bg-white shadow-lg rounded-md dark:bg-slate-700 dark:text-slate-300 transform transition-all ease-in-out">
                        <div className="text-center mx-auto">Display 3</div>
                    </Link>
                   
                </div>
                <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                    <Link href={'/queue/display/4'} target="_blank" className="lg:w-1/4 w-full h-40 flex place-items-center text-center items-center bg-white shadow-lg rounded-md dark:bg-slate-700 dark:text-slate-300 transform transition-all ease-in-out">
                        <div className="text-center mx-auto">Display 4</div>
                    </Link>
                    <Link href={'/queue/display/5'} target="_blank" className="lg:w-1/4 w-full h-40 flex place-items-center text-center items-center bg-white shadow-lg rounded-md dark:bg-slate-700 dark:text-slate-300 transform transition-all ease-in-out">
                        <div className="text-center mx-auto">Display 5</div>
                    </Link>
                </div>
            </div>
        </main>
    )
}