'use client'
import { useSession } from "next-auth/react"
import { redirect } from 'next/navigation';
import { FormEvent, Fragment, useEffect, useState } from 'react'
import Link from "next/link";
import { PlusIcon, PencilIcon, MagnifyingGlassIcon, XMarkIcon, ChevronDoubleRightIcon, XCircleIcon, AdjustmentsHorizontalIcon, IdentificationIcon, CheckIcon, NoSymbolIcon, PlayIcon, HandRaisedIcon, ArrowPathIcon, PrinterIcon, ArrowDownIcon } from "@heroicons/react/24/outline";
import { Spinner } from "@/components/ui/loader";
import { FullShort } from "@/lib/time";
import { usePathname } from 'next/navigation';
import { Pagination } from "@/components/ui/pagination";
import path from "path";
import { FormatMoney, FormatNumber } from "@/lib/text";
import { Warning } from "@/components/ui/alert";

export default function Ticket(props: any) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })
    const pathName = usePathname()

    let errors: string[] = []
    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState(errors)
    const [ticket, setTicket] = useState({
        service: "",
        symbol: "",
        number: "",
        waiting: "",
    })

    const submit = async (e: FormEvent) => {
        e.preventDefault()
        setWarning(errors)
        setSpinner(true)

        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement
        const submit = await fetch(path.posix.join("/api/", pathName as string), {
            method: 'POST',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                date: new Date(),
                service: target.service.value,
            })
        });
        //await new Promise(resolve => setTimeout(resolve, 3000));
        const res = await submit.json()
        setSpinner(false)
        if (submit.ok) {

            setTicket({
                service: res.service.name,
                symbol: res.service.symbol,
                number: res.number,
                waiting: res.waiting
            })

        } else {
            errors = await res.errors
            setWarning(Object.values(errors).filter(a => a != ""))
            errors = []
        }
        setSpinner(false)
    }
    const clearTicket = () => {
        setTicket({
            service: "",
            symbol: "",
            number: "",
            waiting: ""
        })
    }
    const printTicket = () => {
        let printContents = (document.getElementById('print') as HTMLElement).innerHTML;
        const printWindow = window.open("", "", "height=1000,width=1000");
        printWindow?.document.write(printContents);
        printWindow?.print();
        printWindow?.close()

    }
    return (
        <main>
            <div className="flex flex-col justify-between mb-4 space-y-4 md:flex-row md:space-y-0">
                <div>
                    <h1 className="text-xl font-semibold lg:text-2xl">Ticket</h1>
                    <div className="text-sm text-slate-500">Queue Ticket Print</div>
                </div>
                <div className="flex items-center space-x-2">
                    <Link target={"_blank"} href={path.posix.join(pathName ? pathName : "", "touch")} as={path.posix.join(pathName ? pathName : "", "touch")} title="Queue Ticket Touch" className="btn-circle" aria-label="new-service">
                        <HandRaisedIcon width={20} height={20} />
                    </Link>

                </div>
            </div>

            <div className="relative max-w-xl text-gray-600 bg-white shadow-lg rounded-tl-3xl rounded-br-3xl dark:bg-slate-700 dark:text-slate-300 transform transition-all ease-in-out">
                {spinner && <Spinner className=" rounded-tl-3xl rounded-br-3xl" />}
                <form onSubmit={submit} className="ng-untouched ng-pristine ng-valid">
                    <Warning className="" errors={warning} hide={() => setWarning(errors)} />
                    <div className="px-6 py-3 text-sm tracking-wide">

                        <div className="flex-1 w-full mb-2">
                            <label className="label" htmlFor="service">Select Service<sup className="text-rose-500">*</sup></label>
                            <div className="relative inline-block w-full text-gray-700">
                                <select name="service" id="service" className="select" defaultValue={""} onChange={() => clearTicket()}>
                                    {props.params?.services && props.params.services.map((service: any) => (<option key={service._id} value={service._id}>{service.name} ({service.symbol})</option>))}
                                </select>
                            </div>
                        </div>
                        <div className="flex flex-1 w-full mb-2">
                            <button type="submit" className="btn btn-primary flex items-center space-x-2 w-full">
                                <ArrowDownIcon width={20} height={20} className="mr-2" />
                                Get Ticket
                            </button>
                        </div>
                    </div>

                </form>
                {ticket.service != "" &&
                    <Fragment>
                        <div id="print" className="border-t-2 my-4 p-4" style={{ textAlign: "center", margin: "0 auto" }}>
                            <div style={{ textAlign: "center", fontSize: "2em" }}>Nomor Antrian</div>
                            <div style={{ textAlign: "center", fontSize: "7em" }}>{ticket.symbol}{ticket.number}</div>
                            <div style={{ textAlign: "center", fontSize: "2em" }}>{ticket.service}</div>
                            <div style={{ textAlign: "center", fontSize: "1em" }}>Antrian Didepan Anda: {ticket.waiting}</div>
                        </div>
                        <button type="button" className="btn btn-primary flex items-center space-x-2 w-full" onClick={() => printTicket()}>
                            <PrinterIcon width={20} height={20} className="mr-2" />
                            Print
                        </button>
                    </Fragment>
                }
            </div>
        </main>
    )
}