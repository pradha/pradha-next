'use client'
import { useSession } from "next-auth/react"
import { redirect, usePathname } from 'next/navigation';
import { FormEvent, Fragment, useState } from 'react'
import Link from "next/link";
import { UserIcon, KeyIcon, ArrowPathIcon, ListBulletIcon, PlusIcon, IdentificationIcon, TagIcon, LinkIcon, HashtagIcon } from "@heroicons/react/24/outline";
import { Success, Warning } from "@/components/ui/alert";
import { Spinner } from "@/components/ui/loader";
import path from "path";

export const revalidate = 5;
export const dynamic = 'force-dynamic';
export default function Page(props: any) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })

    const pathName = usePathname()

    let errors: string[] = []
    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState(errors)
    const [success, setSuccess] = useState({ title: "", description: "" })

    const submit = async (e: FormEvent) => {
        e.preventDefault()
        setWarning(errors)
        setSpinner(true)

        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement
        const submit = await fetch(path.posix.join("/api/", pathName as string, '..'), {
            method: 'POST',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                code: target.code.value,
                name: (document.getElementById("name") as HTMLInputElement).value,
                symbol: target.symbol.value,
                description: target.description.value,
                print: target.print.value,
                status: target.status.value,
            })
        });
        //await new Promise(resolve => setTimeout(resolve, 3000));
        const res = await submit.json()
        if (submit.ok) {
            (document.getElementById("name") as HTMLInputElement).value = "";
            target.code.value = "";
            target.symbol.value = "A";
            target.description.value = "";
            target.print.value = "0";
            target.status.value = "0";

            setSuccess({ title: "Success", description: "New queue service has been created" })
        } else {
            errors = await res.errors
            setWarning(Object.values(errors).filter(a => a != ""))
            errors = []
        }
        setSpinner(false)
    }
    return (
        <main>
            <div className="flex flex-col md:flex-row md:space-y-0 space-y-4 mb-4 justify-between">
                <div>
                    <h1 className="text-xl lg:text-2xl font-semibold">New Service</h1>
                    <div className="text-sm text-slate-500">Create New Queue Service</div>
                </div>
                <div className="flex items-center space-x-2">
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a8001") &&
                        <Link href={path.join(pathName as string, "..")} title="Service data" className="btn-circle" aria-label="service-data">
                            <ListBulletIcon width={20} height={20} />
                        </Link>
                    }
                </div>
            </div>

            <div className="relative bg-white shadow-lg w-full rounded-tl-3xl rounded-br-3xl p-6 dark:bg-slate-700">
                {spinner && <Spinner className="rounded-tl-3xl rounded-br-3xl" />}
                <Success data={success} hide={() => setSuccess({ title: "", description: "" })} />
                <form onSubmit={submit} className="ng-untouched ng-pristine ng-valid">
                    <Warning className="" errors={warning} hide={() => setWarning(errors)} />

                    <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                        <div className="lg:w-1/4 w-full mb-2">
                            <label className="label" htmlFor="code">Code <sup className="text-rose-500">*</sup></label>
                            <div className="relative mb-2">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <HashtagIcon className="text-gray-400 w-5 h-5" />
                                </div>
                                <input type="text" id="code" name='code' className={"input-icon"} placeholder="Service Code" />
                            </div>
                        </div>
                        <div className="lg:w-1/2 w-full mb-2">
                            <label className="label" htmlFor="name">Name <sup className="text-rose-500">*</sup></label>
                            <div className="relative mb-2">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <IdentificationIcon className="text-gray-400 w-5 h-5" />
                                </div>
                                <input type="text" id="name" name='name' className={"input-icon"} placeholder="Service Name" />
                            </div>
                        </div>
                        <div className="lg:w-1/4 w-full mb-2">
                            <label className="label" htmlFor="status">Symbol <sup className="text-rose-500">*</sup></label>
                            <div className="relative inline-block w-full text-gray-700">
                                <select name="symbol" id="symbol" className="select" defaultValue={""}>
                                    {Array.from(Array(26)).map((e, i) => i + 65).map((x) => <option key={String.fromCharCode(x)} value={String.fromCharCode(x)}>{String.fromCharCode(x)}... ({String.fromCharCode(x)}1, {String.fromCharCode(x)}2, ..., {String.fromCharCode(x)}35, ...)</option>)}
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                        <div className="flex-1 mb-2">
                            <label htmlFor="description" className="label">Description <sup className="text-rose-500">*</sup></label>
                            <textarea name="description" id="description" placeholder="Service Description" className={"input"} rows={3}></textarea>
                        </div>
                    </div>
                    <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                        <div className="lg:w-1/4 w-full mb-2">
                            <label className="label" htmlFor="print">Print Ticket <sup className="text-rose-500">*</sup></label>
                            <div className="relative inline-block w-full text-gray-700">
                                <select name="print" id="print" className="select" defaultValue={"1"}>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                        <div className="lg:w-1/4 w-full mb-2">
                            <label className="label" htmlFor="status">Status <sup className="text-rose-500">*</sup></label>
                            <div className="relative inline-block w-full text-gray-700">
                                <select name="status" id="status" className="select" defaultValue={"0"}>
                                    <option value="1">Active</option>
                                    <option value="0">InActive</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="">
                        <button type="submit" className="btn btn-primary flex items-center space-x-2">
                            <PlusIcon width={20} height={20} className="mr-2" />
                            Create Service
                        </button>
                    </div>
                </form>
            </div>
        </main>
    )
}