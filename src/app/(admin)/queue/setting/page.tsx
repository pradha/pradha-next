'use client'
import { useSession } from "next-auth/react"
import { redirect } from 'next/navigation';
import { FormEvent, Fragment, useEffect, useState } from 'react'
import Link from "next/link";
import { PlusIcon, PencilIcon, MagnifyingGlassIcon, XMarkIcon, ChevronDoubleRightIcon, XCircleIcon, AdjustmentsHorizontalIcon, IdentificationIcon, CheckIcon, NoSymbolIcon, PlayIcon, HandRaisedIcon, ArrowPathIcon, PrinterIcon, ArrowDownIcon, ArrowDownCircleIcon, ArrowPathRoundedSquareIcon, EnvelopeIcon, PhoneIcon } from "@heroicons/react/24/outline";
import { Spinner } from "@/components/ui/loader";
import { usePathname } from 'next/navigation';
import path from "path";
import { Success, Warning } from "@/components/ui/alert";
import Image from "next/image"

export default function Ticket(props: any) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })
    const pathName = usePathname()

    let errors: string[] = []
    const [data, setData] = useState(props.params.setting)
    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState(errors)
    const [success, setSuccess] = useState({ title: "", description: "" })
    const [photo, setPhoto] = useState(props.params?.setting?.photo ? props.params.setting.photo : "/img/no-image.png")

    const submit = async (e: FormEvent) => {
        e.preventDefault()
        setWarning(errors)
        setSpinner(true)

        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement
        const submit = await fetch(path.posix.join("/api/", pathName as string), {
            method: 'PUT',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                name: (document.getElementById("name") as HTMLInputElement).value,
                address: target.address.value,
                phone: target.phone.value,
                email: target.email.value,
                information: target.information.value,
                video: target.video.value,
                display_ticker: target.display_ticker.value,
                touch_ticker: target.touch_ticker.value,
            })
        });
        //await new Promise(resolve => setTimeout(resolve, 3000));
        const res = await submit.json()
        if (submit.ok) {
            props.params.setting.name = (document.getElementById("name") as HTMLInputElement).value,
                props.params.setting.address = target.address.value,
                props.params.setting.phone = target.phone.value,
                props.params.setting.email = target.email.value,
                props.params.setting.information = target.email.value,
                props.params.setting.video = target.video.value == "1" ? true : false,
                props.params.setting.ticker.display = target.display_ticker.value == "1" ? true : false,
                props.params.setting.ticker.touch = target.touch_ticker.value == "1" ? true : false,
                setData(props.params.setting)
            setSuccess({ title: "Success", description: "New queue setting has been updated" })
        } else {
            errors = await res.errors
            setWarning(Object.values(errors).filter(a => a != ""))
            errors = []
        }
        setSpinner(false)
    }

    const changePhoto = async (e: FormEvent) => {
        e.preventDefault()
        setWarning(errors)

        setSuccess({ title: "", description: "" })

        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }
        const body = new FormData()
        const file = e.target as HTMLInputElement
        if (file && file.files) {
            setSpinner(true)
            body.append("file", file.files[0])
            try {
                const submit = await fetch("/api/queue/setting/photo", {
                    method: "POST",
                    body
                });
                const res = await submit.json()

                if (submit.ok) {
                    setPhoto(res.file.url + "?" + Math.random())
                    setSuccess({ title: "Success", description: res.message })
                } else {
                    setWarning([res.msg])
                }
            } catch (err: any) {
                errors = [await err.message]
                setWarning(errors)
            }
            file.value = ""
        }
        setSpinner(false)
    }
    return (
        <main>
            <div className="flex flex-col justify-between mb-4 space-y-4 md:flex-row md:space-y-0">
                <div>
                    <h1 className="text-xl font-semibold lg:text-2xl">Settings</h1>
                    <div className="text-sm text-slate-500">Queue System Settings</div>
                </div>
                <div className="flex items-center space-x-2">

                </div>
            </div>

            <div className="relative bg-white shadow-lg w-full rounded-tl-3xl rounded-br-3xl p-6 dark:bg-slate-700">
                {spinner && <Spinner className="rounded-tl-3xl rounded-br-3xl" />}
                <Success data={success} hide={() => setSuccess({ title: "", description: "" })} />
                <form onSubmit={submit} className="ng-untouched ng-pristine ng-valid">
                    <Warning className="" errors={warning} hide={() => setWarning(errors)} />

                    <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                        <div className="lg:w-1/4 w-full mb-2">
                            <div className="relative overflow-hidden py-6 bg-white shadow-lg rounded-tl-3xl rounded-br-3xl group h-72 lg:h-full">
                                {spinner && <Spinner className="rounded-tl-3xl rounded-br-3xl" />}
                                <Image
                                    className="object-cover object-center w-full h-full rounded-tl-3xl rounded-br-3xl"
                                    src={photo}
                                    alt="Queue"
                                    fill
                                    priority
                                    sizes="(max-width: 768px) 100vw,(max-width: 1200px) 50vw,33vw"
                                />
                                {session?.user.group.roles.includes("61e3de6ecc4029a9d11a1004") &&
                                    <>
                                        <button type="button" onClick={() => (document.getElementById("photo") as HTMLInputElement).click()} className="absolute hidden w-32 px-1 py-2 mx-auto text-sm text-center transform -translate-x-1/2 bg-gray-200 border border-gray-500 rounded-lg shadow group-hover:block bottom-10 left-1/2 right-1/2 dark:text-slate-600">Change Photo</button>
                                        <input onChange={changePhoto} type="file" id="photo" name="photo" className="hidden" accept="image/png, image/gif, image/jpeg, image/webp" />
                                    </>
                                }
                            </div>
                        </div>
                        <div className="lg:w-3/4 w-full mb-2">
                            <div className="flex-1 mb-4">
                                <label className="label" htmlFor="status">Name <sup className="text-rose-500">*</sup></label>
                                <div className="relative mb-2">
                                    <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                        <IdentificationIcon className="text-gray-400 w-5 h-5" />
                                    </div>
                                    <input type="text" id="name" name='name' className={"input-icon"} placeholder="Queue Name" defaultValue={data.name} />
                                </div>
                            </div>
                            <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-4'>
                                <div className="lg:w-1/2 w-full mb-">
                                    <label className="label" htmlFor="phone">Phone <sup className="text-rose-500">*</sup></label>
                                    <div className="relative mb-2">
                                        <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                            <PhoneIcon className="text-gray-400 w-5 h-5" />
                                        </div>
                                        <input type="text" id="phone" name='phone' className={"input-icon"} placeholder="Contact Phone" defaultValue={data?.phone} />
                                    </div>
                                </div>
                                <div className="lg:w-1/2 w-full mb-2">
                                    <label className="label" htmlFor="email">Email <sup className="text-rose-500">*</sup></label>
                                    <div className="relative mb-2">
                                        <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                            <EnvelopeIcon className="text-gray-400 w-5 h-5" />
                                        </div>
                                        <input type="text" id="email" name='mail' className={"input-icon"} placeholder="Contact Mail" defaultValue={data.email} />
                                    </div>
                                </div>
                            </div>
                            <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2 mb-4'>
                                <div className="flex-1 mb-2">
                                    <label htmlFor="address" className="label">Address <sup className="text-rose-500">*</sup></label>
                                    <textarea name="address" id="address" placeholder="Queue Address" className={"input"} rows={3} defaultValue={data.address}></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                        <div className="flex-1 mb-2">
                            <label htmlFor="information" className="label">Informations <sup className="text-rose-500">*</sup></label>
                            <textarea name="information" id="information" placeholder="Queue Information Ticker" className={"input"} rows={5} defaultValue={data.information}></textarea>
                        </div>
                    </div>
                    <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                        <div className="lg:w-1/4 w-full mb-2">
                            <label className="label" htmlFor="video">Video on Display <sup className="text-rose-500">*</sup></label>
                            <div className="relative inline-block w-full text-gray-700">
                                <select name="video" id="video" className="select" defaultValue={data.video ? "1" : "0"}>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                        <div className="lg:w-1/4 w-full mb-2">
                            <label className="label" htmlFor="display_ticker">Ticker on Display <sup className="text-rose-500">*</sup></label>
                            <div className="relative inline-block w-full text-gray-700">
                                <select name="display_ticker" id="display_ticker" className="select" defaultValue={data.ticker.display ? "1" : "0"}>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                        <div className="lg:w-1/4 w-full mb-2">
                            <label className="label" htmlFor="touch_ticker">Touch Ticker <sup className="text-rose-500">*</sup></label>
                            <div className="relative inline-block w-full text-gray-700">
                                <select name="touch_ticker" id="touch_ticker" className="select" defaultValue={data.ticker.touch ? "1" : "0"}>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="">
                        <button type="submit" className="btn btn-primary flex items-center space-x-2">
                            <ArrowPathRoundedSquareIcon width={20} height={20} className="mr-2" />
                            Update Settings
                        </button>
                    </div>
                </form>
            </div>
        </main>
    )
}