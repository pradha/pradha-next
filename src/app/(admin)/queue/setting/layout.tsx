import DashboardLayout from "@/components/ui/layouts/dashboard"
import clientPromise from "@/lib/connections/mongo";
import { ObjectId } from "mongodb";
import { getServerSession } from "next-auth";
import { notFound, redirect } from "next/navigation";

export const revalidate = 0
export const dynamic = 'force-dynamic'
export default async function RootLayout(props: any) {
    const session = await getServerSession();
    if (!session)
        notFound()

    const db = (await clientPromise).db()
    const data = await db.collection("queue_settings").findOne()
    props.params.setting=JSON.parse(JSON.stringify(data))
    return (
        <DashboardLayout>{props.children}</DashboardLayout>
    )
}

const title = 'Queue Settings'
const description = 'Queue System Settings'
const keywords = ["queue", "setting", "settings", "queue settings"]
const url = process.env.SITE_URL + '/queue/setting'
export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: false,
        follow: false,
        nocache: false,
        googleBot: {
            index: false,
            follow: false
        },
    },
}