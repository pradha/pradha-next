import DashboardLayout from "@/components/ui/layouts/dashboard"
import clientPromise from "@/lib/connections/mongo";
import { ObjectId } from "mongodb";
import { getServerSession } from "next-auth";
import { notFound, redirect } from "next/navigation";

export const revalidate = 0
export const dynamic = 'force-dynamic'
export default async function RootLayout(props: any) {
    const session = await getServerSession();
    if (!session)
        redirect('/');
    const db = (await clientPromise).db()
    const data = await db.collection("queue_counters").findOne({ _id: new ObjectId(props.params.id) })


    if (!data)
        notFound()
    const services = await db.collection("queue_services").find({ 'status.active': true }).toArray()
    props.params.services = JSON.parse(JSON.stringify(services))
    props.params.data = JSON.parse(JSON.stringify(data))
    return (
        <>{props.children}</>

    )
}

const title = 'Edit Counter'
const description = 'Edit Queue Counter'
const keywords = ["queue", "Counter", "queue Counter", "Counters", "edit"]
const url = process.env.SITE_URL + '/queue/counter/edit'
export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: false,
        follow: false,
        nocache: false,
        googleBot: {
            index: false,
            follow: false
        },
    },
}