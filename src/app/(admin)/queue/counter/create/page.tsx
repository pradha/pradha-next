'use client'
import { useSession } from "next-auth/react"
import { redirect, usePathname } from 'next/navigation';
import { FormEvent, Fragment, useState } from 'react'
import Link from "next/link";
import { UserIcon, KeyIcon, ArrowPathIcon, ListBulletIcon, PlusIcon, IdentificationIcon, TagIcon, LinkIcon, HashtagIcon } from "@heroicons/react/24/outline";
import { Success, Warning } from "@/components/ui/alert";
import { Spinner } from "@/components/ui/loader";
import path from "path";

export const revalidate = 0;
export const dynamic = 'force-dynamic';
export default function Page(props: any) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })

    const pathName = usePathname()

    let errors: string[] = []
    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState(errors)
    const [success, setSuccess] = useState({ title: "", description: "" })

    const submit = async (e: FormEvent) => {
        e.preventDefault()
        setWarning(errors)
        setSpinner(true)
        setSuccess({ title: "", description: "" })

        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement

        const body = new FormData()
        const file = target.sound.files[0]
        if (file) {
            let services = Array.from(document.querySelectorAll('input[name="service"]:checked')).map(x => (x as HTMLInputElement).value)
            body.append("name", (document.getElementById("name") as HTMLInputElement).value)
            body.append("description", target.description.value)
            body.append("display", target.display.value)
            body.append("services", JSON.stringify(services))
            body.append("sound", file)
            body.append("type", target.type.value)
            body.append("status", target.status.value)
            const submit = await fetch(path.posix.join("/api/", pathName as string, '..'), {
                method: "POST",
                body
            });
            try {
                const res = await submit.json()
                if (submit.ok) {
                    (document.getElementById("name") as HTMLInputElement).value = ""
                    target.description.value = ""
                    target.display.value = "1"
                    target.sound.value = ""
                    target.status.value = "0"
                    Array.from(document.querySelectorAll('input[name="service"]:checked')).map(x => (x as HTMLInputElement).checked = false)
                    setSuccess({ title: "Success", description: "Counter has been successfully created" })
                } else {
                    if (await res.errors) errors = await res.errors
                    setWarning(Object.values(errors).filter(a => a != ""))
                }
            } catch (err: any) {
                errors = await err.errors
                setWarning(Object.values(errors).filter(a => a != ""))
            }
        } else {
            errors = ["Please select sound file"]
            setWarning(Object.values(errors).filter(a => a != ""))
            console.log(errors)
        }
        setSpinner(false)
    }
    return (
        <main>
            <div className="flex flex-col md:flex-row md:space-y-0 space-y-4 mb-4 justify-between">
                <div>
                    <h1 className="text-xl lg:text-2xl font-semibold">New Counter</h1>
                    <div className="text-sm text-slate-500">Create New Queue Counter</div>
                </div>
                <div className="flex items-center space-x-2">
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a8005") &&
                        <Link href={path.join(pathName as string, "..")} title="Service data" className="btn-circle" aria-label="service-data">
                            <ListBulletIcon width={20} height={20} />
                        </Link>
                    }
                </div>
            </div>

            <div className="relative bg-white shadow-lg w-full rounded-tl-3xl rounded-br-3xl p-6 dark:bg-slate-700">
                {spinner && <Spinner className="rounded-tl-3xl rounded-br-3xl" />}
                <Success data={success} hide={() => setSuccess({ title: "", description: "" })} />
                <form onSubmit={submit} className="ng-untouched ng-pristine ng-valid">
                    <Warning className="" errors={warning} hide={() => setWarning(errors)} />
                    <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2 lg:divide-x divide-slate-300 dark:divide-slate-600'>
                        <div className="lg:w-3/4 w-full mb-2">
                            <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                                <div className="lg:w-3/4 w-full mb-2">
                                    <label className="label" htmlFor="name">Name <sup className="text-rose-500">*</sup></label>
                                    <div className="relative mb-2">
                                        <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                            <IdentificationIcon className="text-gray-400 w-5 h-5" />
                                        </div>
                                        <input type="text" id="name" name='name' className={"input-icon"} placeholder="Counter Name" />
                                    </div>
                                </div>
                                <div className="lg:w-1/4 w-full mb-2">
                                    <label className="label" htmlFor="display">Display <sup className="text-rose-500">*</sup></label>
                                    <div className="relative inline-block w-full text-gray-700">
                                        <select name="display" id="display" className="select" defaultValue={"1"}>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                                <div className="flex-1 mb-2">
                                    <label htmlFor="description" className="label">Description <sup className="text-rose-500">*</sup></label>
                                    <textarea name="description" id="description" placeholder="Counter Description" className={"input"} rows={3}></textarea>
                                </div>
                            </div>
                            <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                                <div className="lg:w-3/4 w-full mb-2">
                                    <label className="label" htmlFor="sound">Sound File <sup className="text-rose-500">*</sup></label>
                                    <div className="relative inline-block w-full text-gray-700">
                                        <input type="file" name="sound" id="sound" className="input text-xs" accept="audio/mpeg3" />
                                    </div>
                                </div>
                                <div className="lg:w-1/4 w-full mb-2">
                                    <label className="label" htmlFor="status">Status <sup className="text-rose-500">*</sup></label>
                                    <div className="relative inline-block w-full text-gray-700">
                                        <select name="status" id="status" className="select" defaultValue={"0"}>
                                            <option value="1">Active</option>
                                            <option value="0">InActive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="lg:w-1/4 w-full mb-2 lg:px-4">
                            <div className="mb-4">
                                <label className="label" htmlFor="type">Call Type <sup className="text-rose-500">*</sup></label>
                                <div className="relative inline-block w-full text-gray-700">
                                    <select name="type" id="type" className="select" defaultValue={"All Tickets"}>
                                        <option value="All Tickets">All Tickets</option>
                                        <option value="New Tickets Only">New Tickets Only</option>
                                        <option value="Transfered Only">Transfered Only</option>
                                    </select>
                                </div>
                            </div>
                            <div className="mb-4">
                                <label className="label" htmlFor="status">Services <sup className="text-rose-500">*</sup></label>
                                {props.params?.services && props.params.services.map((item: any, i: number) =>
                                    <div key={item._id} className="flex items-center mb-1">
                                        <input type="checkbox" id={item._id} name="service" title={item.description} value={item._id} placeholder={item.name} className="checkbox" />
                                        <label htmlFor={item._id} title={item.description} className="pl-2 text-xs font-medium text-gray-600 dark:text-gray-400 cursor-pointer">{item.name}</label>
                                    </div>
                                )}
                            </div>
                        </div>
                    </div>

                    <div className="">
                        <button type="submit" className="btn btn-primary flex items-center space-x-2">
                            <PlusIcon width={20} height={20} className="mr-2" />
                            Create Counter
                        </button>
                    </div>
                </form>
            </div>
        </main>
    )
}