import DashboardLayout from "@/components/ui/layouts/dashboard"
import clientPromise from "@/lib/connections/mongo";
import { getServerSession } from "next-auth";
import { notFound, redirect } from "next/navigation";

export const revalidate = 0
export const dynamic = 'force-dynamic'
export default async function RootLayout(props: any) {
    const session = await getServerSession();
    if (!session)
        notFound()

    const db = (await clientPromise).db()
    const services = await db.collection("queue_services").find({ 'status.active': true }).toArray()

    props.params.services = JSON.parse(JSON.stringify(services))

    return (
        <DashboardLayout>{props.children}</DashboardLayout>
    )
}

const title = 'Queue Report'
const description = 'Queue Report'
const keywords = ["queue", "report", "reports", "queue report"]
const url = process.env.SITE_URL + '/queue/report'
export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: false,
        follow: false,
        nocache: false,
        googleBot: {
            index: false,
            follow: false
        },
    },
}