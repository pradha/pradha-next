'use client'
import { useSession } from "next-auth/react"
import { redirect } from 'next/navigation';
import { FormEvent, useEffect, useState } from 'react'
import Link from "next/link";
import { PlusIcon, PencilIcon, MagnifyingGlassIcon, XMarkIcon, ChevronDoubleRightIcon, XCircleIcon, AdjustmentsHorizontalIcon, IdentificationIcon, CheckIcon, NoSymbolIcon, ArrowPathIcon } from "@heroicons/react/24/outline";
import { Spinner } from "@/components/ui/loader";
import { FullShort, ISODate } from "@/lib/time";
import { usePathname } from 'next/navigation';
import { Pagination } from "@/components/ui/pagination";
import path from "path";
import { FormatMoney, FormatNumber } from "@/lib/text";
import { setgroups } from "process";

export default function Report(props: any) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })
    const pathName = usePathname()
    let fill: any
    let errors: string[] = []
    const [spinner, setSpinner] = useState(false)
    const [data, setData] = useState(fill)
    const [src, setSrc] = useState(false)
    const [pageNum, setPageNum] = useState(1)
    const [groups, setGroups] = useState<object[]>([])


    const search = async (e: FormEvent) => {
        e.preventDefault()
        setSpinner(true);
        if ((document.getElementById("date") as HTMLInputElement).value.length > 0) {
            const res = await fetch(path.posix.join('/api/', pathName ? pathName : "", '/search'), {
                body: JSON.stringify({
                    service: (document.getElementById("service") as HTMLInputElement).value,
                    date: new Date((document.getElementById("date") as HTMLInputElement).value)
                }),
                headers: {
                    'Content-Type': 'application/json',
                },
                method: 'POST'
            })

            const result = await res.json()
            if (result?.groups)
                setGroups(result.groups)
            else setGroups([])
            setData({ data: result.search, count: result.search.length })
            setSrc(true)
        } else {
            console.log("Please select date!")
        }
        setSpinner(false);
    }

    const getData = async (page: number) => {
        setSpinner(true);
        setPageNum(page)
        const data = await fetch('/api/' + (pathName ? pathName : "") + "?page=" + (page ? page : pageNum), {
            headers: { 'Content-Type': 'application/json' },
            method: 'GET'
        })
        setSpinner(false);
        let result = await data.json()
        if (result?.groups)
            setGroups(result.groups)
        else setGroups([])
        return result
    }

    const getPage = async (page: number) => {
        setSpinner(true);
        let data = await getData(page)
        setData(data)
        setSpinner(false);
    }
    var loaded = false;
    useEffect(() => {
        if (!loaded) {
            (async () => {
                let data = await getData(pageNum)
                setData(data)

            })();
            loaded = true
            return
        }
    }, [])
    const toTime = (miliseconds: number) => {
        return new Date(miliseconds).toISOString().slice(11, 19);
    }
    return (
        <main>
            <div className="flex flex-col justify-between mb-4 space-y-4 md:flex-row md:space-y-0">
                <div>
                    <h1 className="text-xl font-semibold lg:text-2xl">Report</h1>
                    <div className="text-sm text-slate-500">Queue Report</div>
                </div>
                <div className="flex items-center space-x-2">
                    <form onSubmit={search} className="flex space-x-2 items-center flex-1 ">
                        <div className="w-full">
                            <input type="date" id="date" name="date" required min="2020-01-01" max={ISODate((new Date()).toString())} className="input" placeholder="Search..." x-model="search" onChange={search} />
                        </div>
                        <div className="w-full">
                            <div className="relative inline-block w-full text-gray-700">
                                <select name="service" id="service" className="select text-sm pr-8" defaultValue={"all"} onChange={search}>
                                    <option value="all">All Services</option>
                                    {props.params?.services && props.params.services.map((item: any, i: number) => <option key={item._id} value={item._id}>{item.name} ({item.symbol})</option>)}
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            {groups.length >= 1 &&
                <div className="mb-4">
                    <table className=" dark:border-slate-500 divide-y divide-gray-200 table-auto border border-gray-300 rounded-lg border-collapse">
                        <tbody className="rounded-lg">
                            {groups.map((group: any) =>
                                <tr key={group._id}>
                                    <td className="thead thead-column">{group._id}</td>
                                    <td className="text-right px-4 text-sm">{group.count} Ticket{group.count > 1 ? "s" : ""}</td>
                                    <td className="text-right px-4 text-sm">{parseFloat((group.count / data?.count * 100).toString()).toFixed(2)}%</td>
                                </tr>
                            )}
                        </tbody>
                    </table>
                </div>
            }
            <div className="relative w-full text-gray-600 bg-white shadow-lg rounded-tl-3xl rounded-br-3xl dark:bg-slate-700 dark:text-slate-300 transform transition-all ease-in-out">
                {spinner && <Spinner className=" rounded-tl-3xl rounded-br-3xl" />}
                <div className="flex items-center px-6 py-3 space-x-2 text-sm tracking-wide">
                    <div>Total {src ? "Search result" : "Data"} :</div>
                    <div className="font-semibold">{data?.count ? data.count : "0"}</div>
                    <div>Item{data?.count > 1 ? "s" : ""}</div>
                    {src &&
                        <button className="p-1 mx-2 text-slate-100 rounded-md shadow-sm bg-gradient-to-br from-sky-300 to-sky-400 hover:from-sky-300 hover:to-sky-300" onClick={() => { setSrc(false); getPage(pageNum); (document.getElementById("date") as HTMLInputElement).value = ""; (document.getElementById("service") as HTMLInputElement).value = "all" }} type="button">
                            <XMarkIcon width={16} />
                        </button>}
                </div>
                <div className="relative overflow-x-auto">
                    <table className="min-w-full border-b border-gray-300 dark:border-slate-500 divide-y divide-gray-200 table-auto">
                        <thead className="thead">
                            <tr className="">
                                <th className="thead-column">Service</th>
                                <th className="thead-column">Number</th>
                                <th className="thead-column">Printed Ticket</th>
                                <th className="thead-column">Transfered</th>
                                <th className="thead-column">Last Call</th>
                                <th className="thead-column">Waiting Time</th>
                            </tr>
                        </thead>
                        <tbody>
                            {typeof data !== undefined && data?.data?.map((item: any, i: number) =>
                                <tr key={item._id} className="transition-all ease-in-out delay-200 border-b border-gray-200 divide-slate-200 bg-gradient-to-br from-slate-50 odd:to-gray-100 even:to-slate-200 hover:from-sky-50 hover:to-sky-100">
                                    <td className="tbody-column">{item.service.name}</td>
                                    <td className="tbody-column">{item.symbol}{item.number}</td>
                                    
                                    <td className="tbody-column">
                                        <div className="flex flex-col text-xs font-light">
                                            <div>{FullShort(item.created.at)}</div>
                                            <div><span className="font-semibold">{item.created?.by?.name}</span></div>
                                        </div>
                                    </td>
                                    <td className="tbody-column">
                                        {item?.transfered ? "Yes" : "No"}
                                    </td>
                                    <td className="tbody-column">{item.called?.at ?
                                        <div className="flex flex-col text-xs font-light">
                                            <div>{FullShort(item.called.at)}</div>
                                            <div><span className="font-semibold"><small>by</small> {item.called?.by?.name}</span></div>
                                            <div><span className="font-semibold"><small>from</small> {item.called?.counter?.name}</span></div>
                                        </div> : <span className="flex items-center text-yellow-700"><ArrowPathIcon width={20} className="animate-spin mr-2"></ArrowPathIcon>Waiting...</span>}</td>
                                    <td className="tbody-column">{
                                        item.called?.at ?
                                            toTime((new Date(item.called.at).getTime() - new Date(item.created.at).getTime()))
                                            : <span className="flex items-center text-yellow-700"><ArrowPathIcon width={20} className="animate-spin mr-2"></ArrowPathIcon>Waiting...</span>
                                    }
                                    </td>
                                </tr>
                            )}
                            {(!data || data?.count <= 0) &&
                                <tr>
                                    <td className="p-6 text-center italic text-sm" colSpan={5}>no data at this time</td>
                                </tr>
                            }
                        </tbody>
                    </table>
                    {!src && <Pagination count={data?.count} page={data?.page} total={data?.total} click={getPage} />}
                </div>
            </div>
        </main>
    )
}