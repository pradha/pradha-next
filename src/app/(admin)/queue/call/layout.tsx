import DashboardLayout from "@/components/ui/layouts/dashboard"
import clientPromise from "@/lib/connections/mongo";
import { ObjectId } from "mongodb";
import { getServerSession } from "next-auth";
import { notFound, redirect } from "next/navigation";

export const revalidate = 0
export const dynamic = 'force-dynamic'
export default async function RootLayout(props: any) {
    const session = await getServerSession();
    if (!session)
        notFound()

    const db = (await clientPromise).db()
    const data = await db.collection("queue_counters").find({ 'status.active': true }).toArray()
    const services = await db.collection("queue_services").find({ 'status.active': true }).toArray()
    props.params.counters=JSON.parse(JSON.stringify(data))
    props.params.services=JSON.parse(JSON.stringify(services))
    return (
        <DashboardLayout>{props.children}</DashboardLayout>
    )
}

const title = 'Call Queue'
const description = 'Call Queue Ticket'
const keywords = ["queue", "ticket", "tickets", "queue tickets", "call"]
const url = process.env.SITE_URL + '/queue/call'
export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: false,
        follow: false,
        nocache: false,
        googleBot: {
            index: false,
            follow: false
        },
    },
}