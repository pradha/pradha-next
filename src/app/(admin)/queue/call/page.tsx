'use client'
import { useSession } from "next-auth/react"
import { redirect } from 'next/navigation';
import { ChangeEvent, FormEvent, Fragment, useEffect, useState } from 'react'
import Link from "next/link";
import { MegaphoneIcon, ChevronLeftIcon, ChevronRightIcon, PaperAirplaneIcon, XMarkIcon, CheckIcon, ArrowPathRoundedSquareIcon } from "@heroicons/react/24/outline";
import { Spinner } from "@/components/ui/loader";
import { FullShort } from "@/lib/time";
import { usePathname } from 'next/navigation';
import path from "path";
import { Warning } from "@/components/ui/alert";
import { io } from "socket.io-client"
import { Dialog, Transition } from "@headlessui/react";

export default function Call(props: any) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })
    const pathName = usePathname()

    let errors: string[] = []
    const [connected, setConnected] = useState<boolean>(false)
    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState(errors)
    const [success, setSuccess] = useState({ title: "", description: "" })
    const [services, setServices] = useState(props.params.services)
    const [transfer, setTransfer] = useState(false)
    const [waitings, setWaitings] = useState<any[]>([])
    const [ticket, setTicket] = useState({
        service: "",
        symbol: "",
        number: "",
        waiting: "",
    })
    const [call, setCall] = useState({
        id: "",
        service_id: "",
        service_name: "",
        symbol: "",
        number: "",
        name: "",
        called: {
            counter: {
                name: "",
            },
            at: "",
            by: {
                name: "",
                photo: ""
            }
        },
        transfered: false
    })
    const [socket, setSocket] = useState<any>(null)

    useEffect(() => {
        const socketIO = io(process.env.NEXT_PUBLIC_SOCKET_SERVER_URL as string, {
            transports: ["websocket"],
        });
        socketIO.on("connect", () => {
            console.log("SOCKET CONNECTED!", socketIO.id)
            setConnected(true)
        });
        socketIO.on("disconnect", () => {
            console.log("SOCKET DISCONNECTED!")
            setConnected(false)
        })

        setSocket(socketIO)
        return () => {
            console.log("Disconnecting from WebSocket server...");
            socketIO.disconnect();
            setConnected(false)
        };
    }, [])

    const getLast = async (id: string) => {
        setSpinner(true)
        if (id != "") {
            const submit = await fetch(path.posix.join("/api/", pathName as string, '../ticket/last'), {
                method: 'POST',
                credentials: 'include',
                cache: 'no-cache',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    date: new Date(),
                    counter: id
                })
            });
            const res = await submit.json()
            if (res.waiting)
                setWaitings(res.waiting)
            if (submit.ok && res.lastTicket != null) {
                setCall({
                    id: res.lastTicket._id,
                    service_id: res.lastTicket.service._id,
                    service_name: res.lastTicket.service.name,
                    symbol: res.lastTicket.symbol,
                    number: res.lastTicket.number,
                    name: res.lastTicket.name,
                    called: {
                        counter: {
                            name: res.lastTicket.called?.counter?.name || "",
                        },
                        at: res.lastTicket.called?.at || "",
                        by: {
                            name: res.lastTicket.called?.by?.name || "",
                            photo: res.lastTicket.called?.by?.photo || ""
                        }
                    },
                    transfered: res.transfered
                })

            } else {
                setCall({
                    id: "",
                    service_id: "",
                    service_name: "",
                    symbol: "",
                    number: "",
                    name: "",
                    called: {
                        counter: {
                            name: "",
                        },
                        at: "",
                        by: {
                            name: "",
                            photo: ""
                        }
                    },
                    transfered: false
                })
            }

        }
        setSpinner(false)
    }

    const last = async (e: ChangeEvent<HTMLSelectElement>) => {
        let id = e.target.value
        getLast(id)
    }

    const callQueue = async (id: string) => {

        setSpinner(true)
        const submit = await fetch(path.posix.join("/api/", pathName as string, '../ticket/call'), {
            method: 'POST',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                date: new Date(),
                ticket: id,
                counter: (document.getElementById('counter') as HTMLSelectElement).value
            })
        });
        const res = await submit.json()
        if (submit.ok) {
            socket.emit("call", res.lastTicket)
            console.log("Sending Queue Call")
            setCall({
                id: res.lastTicket._id,
                service_id: res.lastTicket.service._id,
                service_name: res.lastTicket.service.name,
                symbol: res.lastTicket.symbol,
                number: res.lastTicket.number,
                name: res.lastTicket.name,
                called: {
                    counter: {
                        name: res.lastTicket.called?.counter?.name || "",
                    },
                    at: res.lastTicket.called?.at || "",
                    by: {
                        name: res.lastTicket.called?.by?.name || "",
                        photo: res.lastTicket.called?.by?.photo || ""
                    }
                },
                transfered: res.transfered
            })
        } else {
            setCall({
                id: "",
                service_id: "",
                service_name: "",
                symbol: "",
                number: "",
                name: "",
                called: {
                    counter: {
                        name: "",
                    },
                    at: "",
                    by: {
                        name: "",
                        photo: ""
                    }
                },
                transfered: false
            })
        }
        setSpinner(false)
    }

    const nextQueue = async (id: string) => {
        setSpinner(true)
        const submit = await fetch(path.posix.join("/api/", pathName as string, '../ticket/next'), {
            method: 'POST',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                date: new Date(),
                counter: (document.getElementById('counter') as HTMLSelectElement).value,
                ticket: id
            })
        });
        const res = await submit.json()
        if (res.waiting)
            setWaitings(res.waiting)
        if (submit.ok && res.nextTicket != null) {
            setCall({
                id: res.nextTicket._id,
                service_id: res.nextTicket.service._id,
                service_name: res.nextTicket.service.name,
                symbol: res.nextTicket.symbol,
                number: res.nextTicket.number,
                name: res.nextTicket.name,
                called: {
                    counter: {
                        name: res.nextTicket.called?.counter?.name || "",
                    },
                    at: res.nextTicket.called?.at || "",
                    by: {
                        name: res.nextTicket.called?.by?.name || "",
                        photo: res.nextTicket.called?.by?.photo || ""
                    }
                },
                transfered: res.transfered
            })
        } else {
            setCall({
                id: "",
                service_id: "",
                service_name: "",
                symbol: "",
                number: "",
                name: "",
                called: {
                    counter: {
                        name: "",
                    },
                    at: "",
                    by: {
                        name: "",
                        photo: ""
                    }
                },
                transfered: false
            })
        }
        setSpinner(false)
    }
    
    const previousQueue = async (id: string) => {
        setSpinner(true)
        const submit = await fetch(path.posix.join("/api/", pathName as string, '../ticket/previous'), {
            method: 'POST',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                date: new Date(),
                counter: (document.getElementById('counter') as HTMLSelectElement).value,
                ticket: id
            })
        });
        const res = await submit.json()
        if (res.waiting)
            setWaitings(res.waiting)
        if (submit.ok && res.previousTicket != null) {
            setCall({
                id: res.previousTicket._id,
                service_id: res.previousTicket.service._id,
                service_name: res.previousTicket.service.name,
                symbol: res.previousTicket.symbol,
                number: res.previousTicket.number,
                name: res.previousTicket.name,
                called: {
                    counter: {
                        name: res.previousTicket.called?.counter?.name || "",
                    },
                    at: res.previousTicket.called?.at || "",
                    by: {
                        name: res.previousTicket.called?.by?.name || "",
                        photo: res.previousTicket.called?.by?.photo || ""
                    }
                },
                transfered: res.transfered
            })
        } else {
            setCall({
                id: "",
                service_id: "",
                service_name: "",
                symbol: "",
                number: "",
                name: "",
                called: {
                    counter: {
                        name: "",
                    },
                    at: "",
                    by: {
                        name: "",
                        photo: ""
                    }
                },
                transfered: false
            })
        }
        setSpinner(false)
    }

    const transferTicket = async (e: FormEvent) => {
        e.preventDefault()
        setWarning(errors)
        setSpinner(true)
        setSuccess({ title: "", description: "" })
        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement
        const submit = await fetch(path.posix.join("/api/", pathName as string, '../ticket/transfer'), {
            method: 'POST',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                transfered: call.id,
                service: target.service.value,
                symbol: call.symbol,
                number: call.number,
                date: new Date()
            })
        });

        const res = await submit.json()
        setTransfer(false)
        if (submit.ok) {
            setSuccess({ title: "Success", description: "Ticket has been transfered" })
            setCall({
                id: call.id,
                service_id: call.service_id,
                service_name: call.service_name,
                symbol: call.symbol,
                number: call.number,
                name: call.name,
                called: {
                    counter: {
                        name: call.called?.counter?.name || "",
                    },
                    at: call.called?.at || "",
                    by: {
                        name: call.called?.by?.name || "",
                        photo: call.called?.by?.photo || ""
                    }
                },
                transfered: true
            })
        } else {
            errors = await res.errors
            setWarning(Object.values(errors).filter(a => a != ""))
        }
        setSpinner(false)
    }

    return (
        <main>
            <div className="flex flex-col justify-between mb-4 space-y-4 md:flex-row md:space-y-0">
                <div>
                    <h1 className="text-xl font-semibold lg:text-2xl">Call</h1>
                    <div className="text-sm text-slate-500">Call Queue Ticket</div>
                </div>
                <div className="flex items-center space-x-2">
                </div>
            </div>

            <div className="flex lg:flex-row flex-col lg:space-x-2 space-x-0 lg:space-y-0 space-y-2">
                <div className="relative xl:w-1/2 lg:w-1/2 md:w-1/3 w-full">
                    <div className="w-full text-gray-600 bg-white shadow-lg rounded-tl-3xl rounded-br-3xl dark:bg-slate-700 dark:text-slate-300 transform transition-all ease-in-out">
                        {(spinner || !connected) && <Spinner className=" rounded-tl-3xl rounded-br-3xl" />}
                        <form className="ng-untouched ng-pristine ng-valid">
                            <Warning className="" errors={warning} hide={() => setWarning(errors)} />
                            <div className="px-6 py-3 text-sm tracking-wide">

                                <div className="flex-1 w-full mb-2">
                                    <label className="label" htmlFor="counter">Counter<sup className="text-rose-500">*</sup></label>
                                    <div className="relative inline-block w-full text-gray-700">
                                        <select name="counter" id="counter" className="select" defaultValue={""} onChange={(e) => last(e)}>
                                            <option value={""}>- Select Counter -</option>
                                            {props.params?.counters && props.params.counters.map((counter: any) => (<option key={counter._id} value={counter._id}>{counter.name}</option>))}
                                        </select>
                                    </div>
                                </div>
                                {call.id != "" &&
                                    <Fragment>
                                        <div id="print" className="border-t-2 border-slate-200 dark:border-slate-600 my-4 p-4 text-center">
                                            <div className="text-2xl">Nomor Antrian</div>
                                            <div className="text-8xl my-4">{call.symbol}{call.number}</div>
                                            <div className="text-3xl">{call.service_name}</div>
                                            <div className="text-2xl">{call.name}</div>
                                        </div>

                                        {call.called.at != "" &&
                                            <div className="text-center mb-4">
                                                Last Call :
                                                <div className="text-lg">{FullShort(call.called.at)}</div>
                                                <div className="text-lg font-semibold">{call.called.by.name}</div>
                                                <div className="text-2xl font-bold my-4">{call.called.counter?.name}</div>
                                            </div>
                                        }
                                        <div className="flex flex-row space-x-4 w-full mb-2">
                                            <div className="w-1/4">
                                                <button type="button" onClick={() => previousQueue(call.id)} className="btn btn-primary flex items-center space-x-2 w-full">
                                                    <ChevronLeftIcon width={20} height={20} className="lg:mr-4" />
                                                    Prev
                                                </button>
                                            </div>
                                            <div className="w-1/2">
                                                <button type="button" onClick={() => callQueue(call.id)} className="btn btn-primary flex items-center space-x-2 w-full">
                                                    <MegaphoneIcon width={20} height={20} className="mr-2" />
                                                    {call.called.at != "" ? "Redial" : "Call"}
                                                </button>
                                            </div>
                                            <div className="w-1/4">
                                                <button type="button" onClick={() => nextQueue(call.id)} className="btn btn-primary flex items-center space-x-2 w-full">
                                                    Next
                                                    <ChevronRightIcon width={20} height={20} className="lg:ml-4" />
                                                </button>
                                            </div>
                                        </div>
                                        {call.called.at != "" && !call.transfered &&
                                            <Fragment>
                                                <div className="w-full">
                                                    <button type="button" onClick={() => setTransfer(true)} className="btn btn-primary flex items-center space-x-2 w-full">
                                                        <PaperAirplaneIcon width={20} height={20} className="mr-2" />
                                                        Transfer
                                                    </button>
                                                </div>

                                                <Transition appear show={transfer} as={Fragment}>
                                                    <Dialog as="div" className="relative z-10" onClose={() => setTransfer(false)}>
                                                        <Transition.Child
                                                            as={Fragment}
                                                            enter="ease-out duration-300"
                                                            enterFrom="opacity-0"
                                                            enterTo="opacity-100"
                                                            leave="ease-in duration-200"
                                                            leaveFrom="opacity-100"
                                                            leaveTo="opacity-0"
                                                        >
                                                            <div className="fixed inset-0 bg-white dark:bg-black dark:bg-opacity-75 blur-lg bg-opacity-75" />
                                                        </Transition.Child>

                                                        <div className="fixed inset-0 overflow-y-auto">
                                                            <div className="flex min-h-full items-center justify-center p-4 text-center">
                                                                <Transition.Child
                                                                    as={Fragment}
                                                                    enter="ease-out duration-300"
                                                                    enterFrom="opacity-0 scale-95"
                                                                    enterTo="opacity-100 scale-100"
                                                                    leave="ease-in duration-200"
                                                                    leaveFrom="opacity-100 scale-100"
                                                                    leaveTo="opacity-0 scale-95"
                                                                >
                                                                    <Dialog.Panel className="w-full relative max-w-md transform overflow-hidden rounded-2xl bg-white p-6 text-left align-middle shadow-xl transition-all">
                                                                        <Dialog.Title
                                                                            as="h3"
                                                                            className="text-lg flex justify-between font-medium leading-6 text-gray-900"
                                                                        >
                                                                            Transfer Queue Ticket
                                                                            <XMarkIcon title="close" width={16} className="cursor-pointer  text-slate-400" onClick={() => setTransfer(false)} />
                                                                        </Dialog.Title>
                                                                        {(spinner || !connected) && <Spinner className=" rounded-tl-3xl rounded-br-3xl" />}
                                                                        <div className="my-4 text-center">
                                                                            <div className="text-2xl">Nomor Antrian</div>
                                                                            <div className="text-8xl my-4">{call.symbol}{call.number}</div>
                                                                            <div className="text-">from </div>
                                                                            <div className="text-3xl"> {call.called.counter?.name}</div>
                                                                        </div>

                                                                        <form onSubmit={transferTicket} className=" mt-4">
                                                                            <div className="flex-1 w-full mb-2">
                                                                                <label className="label" htmlFor="service">To Service<sup className="text-rose-500">*</sup></label>
                                                                                <div className="relative inline-block w-full text-gray-700">
                                                                                    <select name="service" id="service" className="select" defaultValue={call.service_id}>
                                                                                        {services.map((service: any) => <option value={service._id}>{service.name}</option>)}

                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div className="flex space-x-2 justify-end">
                                                                                <button className="btn btn-primary flex flex-1 items-center space-x-2">
                                                                                    <CheckIcon width={20} height={20} className="mr-2" />
                                                                                    Transfer
                                                                                </button>
                                                                                <button
                                                                                    type="button"
                                                                                    className="inline-flex justify-center rounded-md border border-transparent bg-blue-100 px-4 py-2 text-sm font-medium text-blue-900 hover:bg-blue-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-blue-500 focus-visible:ring-offset-2"
                                                                                    onClick={() => setTransfer(false)}
                                                                                >
                                                                                    Close
                                                                                </button>
                                                                            </div>
                                                                        </form>
                                                                    </Dialog.Panel>
                                                                </Transition.Child>
                                                            </div>
                                                        </div>
                                                    </Dialog>
                                                </Transition>
                                            </Fragment>
                                        }
                                        {call.called.at != "" && call.transfered && <div className="text-center">Already Transfered</div>}
                                    </Fragment>
                                }
                                {call.id == "" && <div className="flex">No Ticket Right Now, Please check other counter |&nbsp; <button type="button" className="flex flex-row space-x-2 items-baseline inline-" onClick={() => getLast((document.getElementById('counter') as HTMLSelectElement).value)}> reload</button></div>}
                            </div>

                        </form>
                    </div>
                </div>
                <div className="relative flex-1 ">
                    <div className="text-gray-600 bg-white shadow-lg rounded-tl-3xl rounded-br-3xl dark:bg-slate-700 dark:text-slate-300 transform transition-all ease-in-out">
                        <h3 className="p-2 font-semibold">Waiting List : {waitings.length} Tiket{waitings.length>1 ? "s" : ""}</h3>
                        <table className="min-w-full border-b border-gray-300 dark:border-slate-500 divide-y divide-gray-200 table-auto rounded-tl-3xl rounded-br-3xl">
                            <thead className="thead">
                                <tr className="rounded-tl-3xl rounded-br-3xl">
                                    <th className="thead-column">Printed</th>
                                    <th className="thead-column">Ticket Number</th>
                                    <th className="thead-column">Service</th>
                                </tr>
                            </thead>
                            <tbody>
                                {waitings?.map((waiting: any) =>
                                    <tr key={waiting._id} id={waiting.id} className={"transition-all rounded-tl-3xl rounded-br-3xl ease-in-out delay-200 border-b border-gray-200 divide-slate-200 bg-gradient-to-br  hover:from-sky-50 hover:to-sky-100 " + (call.id == waiting._id ? "from-slate-200 odd:to-slate-400 even:to-slate-400" : "from-slate-50 odd:to-gray-100 even:to-slate-200")}>
                                        <td className="tbody-column">{waiting.created.at}</td>
                                        <td className="tbody-column">{waiting.symbol}{waiting.number}</td>
                                        <td className="tbody-column">{waiting.service.name}</td>
                                    </tr>
                                )}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </main>
    )
}