import DashboardLayout from "@/components/ui/layouts/dashboard"
import clientPromise from "@/lib/connections/mongo";
import { getServerSession } from "next-auth";
import { redirect } from "next/navigation";

export default async function RootLayout(props:any) {
    const session = await getServerSession();
    if (!session)
        redirect('/');

    return (
        <DashboardLayout>{props.children}</DashboardLayout>
    )
}
