'use client'
import { useSession } from "next-auth/react"
import { redirect, usePathname } from 'next/navigation';
import { FormEvent, useState } from 'react'
import Link from "next/link";
import { UserIcon, KeyIcon, ArrowPathIcon, ListBulletIcon, PlusIcon, IdentificationIcon, TagIcon, LinkIcon } from "@heroicons/react/24/outline";
import { Success, Warning } from "@/components/ui/alert";
import { Spinner } from "@/components/ui/loader";
import path from "path";

export default function Page(props: any) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })

    const pathName = usePathname()

    let errors = {
        name: "",
        description: ""
    }

    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState({ errors: errors })
    const [success, setSuccess] = useState({ title: "", description: "" })

    const submit = async (e: FormEvent) => {
        e.preventDefault()
        setWarning({ errors: errors })
        setSpinner(true)

        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement
        const submit = await fetch(path.posix.join("/api/", pathName as string, '..'), {
            method: 'POST',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                name: (document.getElementById("name") as HTMLInputElement).value,
                description: target.description.value
            })
        });
        //await new Promise(resolve => setTimeout(resolve, 3000));
        const res = await submit.json()
        if (submit.ok) {
            (document.getElementById("name") as HTMLInputElement).value = ""
            target.description.value = ""

            setSuccess({ title: "Success", description: "New group has been created" })
        } else {
            errors = await res.errors
            setWarning({ errors: errors })
            errors = {
                name: "",
                description: ""
            }
        }
        setSpinner(false)
    }
    return (
        <main>
            <div className="flex flex-col md:flex-row md:space-y-0 space-y-4 mb-4 justify-between">
                <div>
                    <h1 className="text-xl lg:text-2xl font-semibold">Settings</h1>
                    <div className="text-sm text-slate-500">System App Settings</div>
                </div>
            </div>

            <div className="relative bg-white shadow-lg w-full rounded-tl-3xl rounded-br-3xl p-6 dark:bg-slate-700">
                {spinner && <Spinner className="rounded-tl-3xl rounded-br-3xl" />}
                <Success data={success} hide={() => setSuccess({ title: "", description: "" })} />
                <form onSubmit={submit} className="ng-untouched ng-pristine ng-valid">
                    <Warning className="" errors={Object.values(warning.errors).filter(a => a != "")} hide={() => setWarning({ errors: errors })} />

                    <div className="mb-4">
                        <label className="label" htmlFor="name">Name <sup className="text-rose-500">*</sup></label>
                        <div className="relative mb-2">
                            <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                <IdentificationIcon className="text-gray-400 w-5 h-5" />
                            </div>
                            <input type="name" id="name" name='name' className={warning?.errors.name ? "input-icon-error" : "input-icon"} placeholder="Group Name" />
                        </div>
                    </div>
                    <div className="mb-4">
                        <label htmlFor="description" className="label">Description <sup className="text-rose-500">*</sup></label>
                        <textarea name="description" id="description" placeholder="Group Description" className={warning?.errors.description ? "input-error" : "input"} rows={3}></textarea>
                    </div>
                    <div className="">
                        <button type="submit" className="btn btn-primary flex items-center space-x-2">
                            <ArrowPathIcon width={20} height={20} className="mr-2" />
                            Save Settings
                        </button>
                    </div>
                </form>
            </div>
        </main>
    )
}