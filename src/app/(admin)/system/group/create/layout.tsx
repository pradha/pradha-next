import DashboardLayout from "@/components/ui/layouts/dashboard"
import clientPromise from "@/lib/connections/mongo";
import { getServerSession } from "next-auth";
import { redirect } from "next/navigation";

export default async function RootLayout(props:any) {
    const session = await getServerSession();
    if (!session)
        redirect('/');

    return (
        <>{props.children}</>
    )
}

const title = 'New Group'
const description = 'Create New Account Group'
const keywords = ["category", "content", "system group", "group", "create"]
const url = process.env.SITE_URL + '/content/category'
export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: false,
        follow: false,
        nocache: false,
        googleBot: {
            index: false,
            follow: false
        },
    },
}