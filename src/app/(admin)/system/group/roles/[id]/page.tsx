'use client'
import { useSession } from "next-auth/react"
import { redirect, usePathname } from 'next/navigation';
import { ChangeEvent, FormEvent, useEffect, useState } from 'react'
import Link from "next/link";
import { UserIcon, KeyIcon, ArrowPathIcon, PencilSquareIcon, ListBulletIcon, AdjustmentsHorizontalIcon, PlusIcon, PencilIcon } from "@heroicons/react/24/outline";
import { Warning } from "@/components/ui/alert";
import { Spinner } from "@/components/ui/loader";
import path from "path";

export default function Page(props: any) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })

    const pathName = usePathname()

    let errors: string[] = []
    const [data, setData] = useState(props.params.data)
    const [title, setTitle] = useState({ name: data.name, description: data.description })
    const [roles, setRoles] = useState(props.params.roles)
    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState(errors)
    
    useEffect(() => {
        fetch(path.posix.join('/api/', pathName as string, "../..", "?id=" + props.params.id))
            .then((data) => data.json())
            .then((res) => {
                setTitle({ name: res.data.name, description: res.data.description })
            })
    }, [data.name]);
    const update = async (e: ChangeEvent) => {

        setWarning(errors)
        setSpinner(true)

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLInputElement
        const submit = await fetch("/api/system/group", {
            method: 'OPTIONS',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'X-CSRF-Token': '',
            },
            body: JSON.stringify({
                group: data._id,
                role: target.value,
                type: target.checked,
            })
        });

        const res = await submit.json()
        if (submit.ok) {
            setData(res.data)
            props.params.data = res.data
        }
        setSpinner(false)
    }
    return (
        <main>
            <div className="flex flex-col md:flex-row md:space-y-0 space-y-4 mb-4 justify-between">
                <div>
                    <h1 className="text-xl lg:text-2xl font-semibold">Roles of Group</h1>
                    <div className="text-sm text-slate-500">Edit Roles of Group : {title.name} - {title.description}</div>
                </div>
                <div className="flex items-center space-x-2">
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a2006") &&
                        <Link href={"/system/group/create"} title="Create New Group" className="btn-circle" aria-label="create-group">
                            <PlusIcon width={20} height={20} />
                        </Link>
                    }
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a2007") &&
                        <Link href={path.join(pathName as string, "../..", "edit", data._id)} title="Edit Group" className="btn-circle" aria-label="edit-group">
                            <PencilIcon width={20} height={20} />
                        </Link>
                    }
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a2005") &&
                        <Link href={"/system/group/"} title="Group Data" className="btn-circle" aria-label="group-data">
                            <ListBulletIcon width={20} height={20} />
                        </Link>
                    }
                </div>
            </div>

            <div className="relative bg-white shadow-lg w-full rounded-tl-3xl rounded-br-3xl p-6 dark:bg-slate-700">
                {spinner && <Spinner className="rounded-tl-3xl rounded-br-3xl" />}

                <div className="flex flex-wrap dark:text-slate-300">
                    {roles && roles.map((item: any, i: number) => {
                        if (!item.parent)
                            return <div key={item._id} className="bg-white dark:bg-slate-600 m-2 rounded-md p-4">
                                <div className="flex items-center mb-1">
                                    <input checked={data?.roles && data?.roles.includes(item._id) ? true : false} onChange={(e) => update(e)} type="checkbox" id={item._id} name={item._id} value={item._id} placeholder={item.name} className="appearance-none checkbox" />
                                    <label htmlFor={item._id} className="pl-2 text-xs font-medium text-gray-600 dark:text-gray-400 cursor-pointer">{item.name}</label>
                                </div>
                                {roles.map((sub: any, j: number) => {
                                    if (sub.parent == item._id)
                                        return <div key={sub._id} className="flex items-center ml-6 mb-1">
                                            <input checked={data?.roles && data?.roles.includes(sub._id) ? true : false} type="checkbox" onChange={(e) => update(e)} id={sub._id} name={sub._id} value={sub._id} placeholder={sub.name} className="appearance-none checkbox" />
                                            <label htmlFor={sub._id} className="pl-2 text-xs font-medium text-gray-600 dark:text-gray-400 cursor-pointer">{sub.name}</label>
                                        </div>
                                })}

                            </div>
                    })}
                </div>
            </div>
        </main>
    )
}