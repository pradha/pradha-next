'use client'
import { useSession } from "next-auth/react"
import { redirect, usePathname } from 'next/navigation';
import { FormEvent, useEffect, useState } from 'react'
import Link from "next/link";
import { UserIcon, KeyIcon, ArrowPathIcon, ListBulletIcon, PlusIcon, IdentificationIcon, TagIcon, LinkIcon, AdjustmentsHorizontalIcon } from "@heroicons/react/24/outline";
import { Success, Warning } from "@/components/ui/alert";
import { Spinner } from "@/components/ui/loader";
import path from "path";

export default function Page(props: any) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })

    const pathName = usePathname()

    let errors = {
        name: "",
        description: ""
    }
    const [data, setData] = useState(props.params.data)
    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState({ errors: errors })
    const [success, setSuccess] = useState({ title: "", description: "" })

    useEffect(() => {
        fetch(path.posix.join('/api/', pathName as string, "../..", "?id=" + props.params.id as string))
            .then((data) => data.json())
            .then((res) => {
                props.params.data = res.data;
                setData(res.data);
                (document.getElementById('name') as HTMLInputElement).value = res.data.name;
                (document.getElementById('description') as HTMLInputElement).value = res.data.description;
            })
    }, []);
    const submit = async (e: FormEvent) => {
        e.preventDefault()
        setWarning({ errors: errors })
        setSpinner(true)

        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement
        const submit = await fetch(path.posix.join("/api/", pathName as string, '../..'), {
            method: 'PUT',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                id: data._id,
                name: (document.getElementById("name") as HTMLInputElement).value,
                description: target.description.value
            })
        });
        //await new Promise(resolve => setTimeout(resolve, 3000));
        const res = await submit.json()
        if (submit.ok) {
            setSuccess({ title: "Success", description: "Group has been updated" })
        } else {
            errors = await res.errors
            setWarning({ errors: errors })
            errors = {
                name: "",
                description: ""
            }
        }
        setSpinner(false)
    }
    return (
        <main>
            <div className="flex flex-col md:flex-row md:space-y-0 space-y-4 mb-4 justify-between">
                <div>
                    <h1 className="text-xl lg:text-2xl font-semibold">Edit Group</h1>
                    <div className="text-sm text-slate-500">Edit Account Group</div>
                </div>
                <div className="flex items-center space-x-2">
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a2006") &&
                        <Link href={path.posix.join(pathName as string, "../..", "create")} title="Create Group" className="btn-circle" aria-label="create-group">
                            <PlusIcon width={20} height={20} />
                        </Link>
                    }
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a2008") &&
                        <Link href={path.posix.join(pathName as string, "../..", "roles", data?._id ? data._id : "")} title="Group Roles" className="btn-circle" aria-label="group-roles">
                            <AdjustmentsHorizontalIcon width={20} height={20} />
                        </Link>
                    }
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a2005") &&
                        <Link href={path.posix.join(pathName as string, "../..")} title="Group data" className="btn-circle" aria-label="group-data">
                            <ListBulletIcon width={20} height={20} />
                        </Link>
                    }
                </div>
            </div>

            <div className="relative bg-white shadow-lg w-full rounded-tl-3xl rounded-br-3xl p-6 dark:bg-slate-700">
                {spinner && <Spinner className="rounded-tl-3xl rounded-br-3xl" />}
                <Success data={success} hide={() => setSuccess({ title: "", description: "" })} />
                <form onSubmit={submit} className="ng-untouched ng-pristine ng-valid">
                    <Warning className="" errors={Object.values(warning.errors).filter(a => a != "")} hide={() => setWarning({ errors: errors })} />

                    <div className="mb-4">
                        <label className="label" htmlFor="name">Name <sup className="text-rose-500">*</sup></label>
                        <div className="relative mb-2">
                            <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                <IdentificationIcon className="text-gray-400 w-5 h-5" />
                            </div>
                            <input type="name" id="name" name='name' defaultValue={data.name} className={warning?.errors.name ? "input-icon-error" : "input-icon"} placeholder="Group Name" />
                        </div>
                    </div>
                    <div className="mb-4">
                        <label htmlFor="description" className="label">Description <sup className="text-rose-500">*</sup></label>
                        <textarea name="description" id="description" placeholder="Group Description" defaultValue={data.description} className={warning?.errors.description ? "input-error" : "input"} rows={3}></textarea>
                    </div>
                    <div className="">
                        <button type="submit" className="btn btn-primary flex items-center space-x-2">
                            <ArrowPathIcon width={20} height={20} className="mr-2" />
                            Update Group
                        </button>
                    </div>
                </form>
            </div>
        </main>
    )
}