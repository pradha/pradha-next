'use client'
import { useSession } from "next-auth/react"
import { redirect, usePathname } from 'next/navigation';
import { FormEvent, Fragment, useState } from 'react'
import Link from "next/link";
import { UserIcon, KeyIcon, ArrowPathIcon, PencilSquareIcon, ListBulletIcon, AdjustmentsHorizontalIcon, UserCircleIcon, ClockIcon, PlusIcon, IdentificationIcon, InformationCircleIcon, XMarkIcon, CheckIcon, BanknotesIcon, CalendarIcon, CreditCardIcon, PrinterIcon } from "@heroicons/react/24/outline";
import { Success, Warning } from "@/components/ui/alert";
import { Spinner } from "@/components/ui/loader";
import Image from "next/image";
import { DateTimeShort, DayDateFormat, FullShort, FullShortDay, ISO8601DateTime } from "@/lib/time";
import path from "path";
import { FormatCurrency, FormatMoney } from "@/lib/text";
import { Dialog, Transition } from "@headlessui/react";

export default function Page(props: any) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })

    const pathName = usePathname()
    let errors: string[] = []
    const [data, setData] = useState(props.params.data)
    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState(errors)
    const [success, setSuccess] = useState({ title: "", description: "" })
    const [payment, setPayment] = useState(false)
    const [showProof, setShowProof] = useState(false)
    const [confirm, setConfirm] = useState(false)
    const [print, setPrint] = useState(false)
    const [thumbnail, setThumbnail] = useState<string>("/img/no-image.png")

    const confirmPayment = async (e: FormEvent) => {
        e.preventDefault()
        setWarning(errors)
        setSpinner(true)

        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement
        const submit = await fetch(path.posix.join("/api/system/billing"), {
            method: 'PUT',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                id: data._id,
            })
        });
        //await new Promise(resolve => setTimeout(resolve, 3000));
        const res = await submit.json()
        if (submit.ok) {
            props.params.data = res.data
            setData(res.data)
            setConfirm(false)
            setSuccess({ title: "Success", description: "Payment has been confirmed" })
        } else {
            errors = await res.errors
            setWarning(Object.values(errors).filter(a => a != ""))
            errors = []
        }
        setSpinner(false)
    }
    const submitPayment = async (e: FormEvent) => {
        e.preventDefault()
        setWarning(errors)
        setSpinner(true)
        setSuccess({ title: "", description: "" })

        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement

        const body = new FormData()
        const file = target.file.files[0]
        if (file) {
            body.append("id", data._id)
            body.append("payment_method", target.payment_method.value)
            body.append("payment_amount", target.payment_amount.value)
            body.append("pay_date", target.pay_date.value)
            body.append("file", file)
            const submit = await fetch('/api/system/billing', {
                method: "POST",
                body
            });
            try {
                const res = await submit.json()
                if (submit.ok) {
                    setThumbnail("/img/no-image.png")
                    setSuccess({ title: "Success", description: "Payment has been submitted" })
                    setData(res.data)
                    setPayment(false)
                } else {
                    if (await res.errors) errors = await res.errors
                    setWarning(res.errors)
                }

            } catch (err: any) {
                setWarning([err])
            }
        } else {
            setWarning(["no image provided"])
        }
        setSpinner(false)
    }

    const previewImage = (e: FormEvent) => {
        var reader = new FileReader();
        let target = e.target as HTMLInputElement
        if (target.files) {
            if (!target.files[0].type.match(/image\/(png|jpg|jpeg|webp)/i)) {
                alert("Image mime type is not valid");
                return;
            }
            reader.onloadend = (e) => {
                setThumbnail(reader.result as string)
            }
            const fileData = reader.readAsDataURL(target.files[0])
        }

    }
    return (
        <main>
            <div className="flex flex-col md:flex-row md:space-y-0 space-y-4 mb-4 justify-between">
                <div>
                    <h1 className="text-xl lg:text-2xl font-semibold">Billing Detail</h1>
                    <div className="text-sm text-slate-500">System Billing Detail</div>
                </div>

                <div className="flex items-center space-x-2">
                    <div onClick={() => setPrint(true)} title="Print Billing" className="btn-circle" aria-label="print-billing">
                        <PrinterIcon width={20} height={20} />
                    </div>
                    <Transition appear show={print} as={Fragment}>
                        <Dialog as="div" className="relative z-10" onClose={() => setPrint(false)}>
                            <Transition.Child
                                as={Fragment}
                                enter="ease-out duration-300"
                                enterFrom="opacity-0"
                                enterTo="opacity-100"
                                leave="ease-in duration-200"
                                leaveFrom="opacity-100"
                                leaveTo="opacity-0"
                            >
                                <div className="fixed inset-0 bg-white dark:bg-black dark:bg-opacity-75 blur-lg bg-opacity-75" />
                            </Transition.Child>

                            <div className="fixed inset-0 overflow-y-auto">
                                <div className="flex min-h-full items-center justify-center p-4 text-center">
                                    <Transition.Child
                                        as={Fragment}
                                        enter="ease-out duration-300"
                                        enterFrom="opacity-0 scale-95"
                                        enterTo="opacity-100 scale-100"
                                        leave="ease-in duration-200"
                                        leaveFrom="opacity-100 scale-100"
                                        leaveTo="opacity-0 scale-95"
                                    >
                                        <Dialog.Panel className="w-full max-w-5xl border border-slate-100 dark:border-slate-600 transform overflow-hidden rounded-2xl bg-white dark:bg-slate-700 p-6 text-left align-middle shadow-xl transition-all">
                                            <Dialog.Title
                                                as="h3"
                                                className="text-lg flex justify-between font-medium leading-6 text-gray-900 dark:text-slate-100"
                                            >
                                                Print Invoice
                                                <XMarkIcon title="close" width={16} className="cursor-pointer  text-slate-400" onClick={() => setPrint(false)} />
                                            </Dialog.Title>
                                            <object data={"/api/system/billing/print?id=" + data._id}
                                                width="100%"
                                                height="750" />
                                        </Dialog.Panel>
                                    </Transition.Child>
                                </div>
                            </div>
                        </Dialog>
                    </Transition>
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a2020") &&
                        <Link href={"/system/billing/"} title="System Billing" className="btn-circle" aria-label="billing-data">
                            <ListBulletIcon width={20} height={20} />
                        </Link>
                    }
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a1007") &&
                        <Link href={"/account/billing/"} title="My Billing" className="btn-circle" aria-label="my-billing">
                            <CreditCardIcon width={20} height={20} />
                        </Link>
                    }
                    {(session?.user.group.roles.includes("61e3de6ecc4029a9d11a2022") && data.status != "Confirmed") &&
                        <Fragment>
                            <button onClick={() => setConfirm(true)} type="button" title="Confirm Payment" className="btn-circle" aria-label="confirm Payment">
                                <CheckIcon width={20} height={20} />
                            </button>
                            <Transition appear show={confirm} as={Fragment}>
                                <Dialog as="div" className="relative z-10" onClose={() => setConfirm(false)}>
                                    <Transition.Child
                                        as={Fragment}
                                        enter="ease-out duration-300"
                                        enterFrom="opacity-0"
                                        enterTo="opacity-100"
                                        leave="ease-in duration-200"
                                        leaveFrom="opacity-100"
                                        leaveTo="opacity-0"
                                    >
                                        <div className="fixed inset-0 bg-white dark:bg-black dark:bg-opacity-75 blur-lg bg-opacity-75" />
                                    </Transition.Child>

                                    <div className="fixed inset-0 overflow-y-auto">
                                        <div className="flex min-h-full items-center justify-center p-4 text-center">
                                            <Transition.Child
                                                as={Fragment}
                                                enter="ease-out duration-300"
                                                enterFrom="opacity-0 scale-95"
                                                enterTo="opacity-100 scale-100"
                                                leave="ease-in duration-200"
                                                leaveFrom="opacity-100 scale-100"
                                                leaveTo="opacity-0 scale-95"
                                            >
                                                <Dialog.Panel className="w-full max-w-xl border border-slate-100 dark:border-slate-600 transform overflow-hidden rounded-2xl bg-white dark:bg-slate-700 p-6 text-left align-middle shadow-xl transition-all">
                                                    <Dialog.Title
                                                        as="h3"
                                                        className="text-lg flex justify-between font-medium leading-6 text-gray-900 dark:text-slate-100"
                                                    >
                                                        Confirm Payment
                                                        <XMarkIcon title="close" width={16} className="cursor-pointer  text-slate-400" onClick={() => setConfirm(false)} />
                                                    </Dialog.Title>
                                                    <form onSubmit={confirmPayment} className="mt-4">
                                                        <div className="dark:text-slate-100 text-slate-800 my-8">
                                                            Are you sure to confirm this payment? <br /><strong>{data._id}<br /><br />Amount : {FormatMoney(data.payment_amount.$numberDecimal)}</strong>
                                                        </div>


                                                        <div className='flex flex-row space-x-4 mb-2'>
                                                            <button type="submit" className="btn btn-primary flex items-center space-x-2">
                                                                <CheckIcon width={20} height={20} className="mr-2" />
                                                                Confirm
                                                            </button>
                                                            <button
                                                                type="button"
                                                                className="inline-flex justify-center rounded-md border border-transparent bg-blue-100 px-4 py-2 text-sm font-medium text-blue-900 hover:bg-blue-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-blue-500 focus-visible:ring-offset-2"
                                                                onClick={() => setConfirm(false)}
                                                            >
                                                                Close
                                                            </button>
                                                        </div>
                                                    </form>
                                                </Dialog.Panel>
                                            </Transition.Child>
                                        </div>
                                    </div>
                                </Dialog>
                            </Transition>
                        </Fragment>
                    }
                    {(data.status != "Confirmed" && session?.user.group.roles.includes("61e3de6ecc4029a9d11a1009")) &&
                        <Fragment>
                            <div title="Confirm Payment" className="btn-circle" aria-label="confirm payment" onClick={() => setPayment(true)} >
                                <BanknotesIcon width={20} height={20} />
                            </div>
                            <Transition appear show={payment} as={Fragment}>
                                <Dialog as="div" className="relative z-10" onClose={() => setPayment(false)}>
                                    <Transition.Child
                                        as={Fragment}
                                        enter="ease-out duration-300"
                                        enterFrom="opacity-0"
                                        enterTo="opacity-100"
                                        leave="ease-in duration-200"
                                        leaveFrom="opacity-100"
                                        leaveTo="opacity-0"
                                    >
                                        <div className="fixed inset-0 bg-white dark:bg-black dark:bg-opacity-75 blur-lg bg-opacity-75" />
                                    </Transition.Child>

                                    <div className="fixed inset-0 overflow-y-auto">
                                        <div className="flex min-h-full items-center justify-center p-4 text-center">
                                            <Transition.Child
                                                as={Fragment}
                                                enter="ease-out duration-300"
                                                enterFrom="opacity-0 scale-95"
                                                enterTo="opacity-100 scale-100"
                                                leave="ease-in duration-200"
                                                leaveFrom="opacity-100 scale-100"
                                                leaveTo="opacity-0 scale-95"
                                            >
                                                <Dialog.Panel className="w-full max-w-xl border border-slate-100 dark:border-slate-600 transform overflow-hidden rounded-2xl bg-white dark:bg-slate-700 p-6 text-left align-middle shadow-xl transition-all">
                                                    <Dialog.Title
                                                        as="h3"
                                                        className="text-lg flex justify-between font-medium leading-6 text-gray-900 dark:text-slate-100"
                                                    >
                                                        Payment
                                                        <XMarkIcon title="close" width={16} className="cursor-pointer  text-slate-400" onClick={() => setPayment(false)} />
                                                    </Dialog.Title>
                                                    {spinner && <Spinner className=" rounded-tl-3xl rounded-br-3xl" />}

                                                    <form onSubmit={submitPayment} className="mt-4">
                                                        <Warning className="" errors={warning} hide={() => setWarning(errors)} />
                                                        <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                                                            <div className="flex-1 mb-2">
                                                                <label className="label" htmlFor="payment_method">Payment Method</label>
                                                                <div className="relative inline-block w-full text-gray-700">
                                                                    <select name="payment_method" id="payment_method" className="select" defaultValue={data.payment_method}>
                                                                        <option value="">- Select Billing -</option>
                                                                        <option value="BCA">(manual) Bank BCA 299-093-2959 a.n Aditya Y Pradhana</option>
                                                                        <option value="Mandiri">(manual) Bank Mandiri 134-000-690-049-1 a.n Aditya Y Pradhana</option>
                                                                        <option value="Jago">(manual) Bank Jago 5075-4305-7144 a.n Aditya Y Pradhana</option>
                                                                        <option value="BSI">(manual) Bank BSI 1016-8534-25 a.n Aditya Y Pradhana</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                                                            <div className="flex-1 mb-2">
                                                                <label className="label" htmlFor="payment_amount">Payment Amount</label>
                                                                <div className="relative mb-2">
                                                                    <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                                                        <IdentificationIcon className="text-gray-400 w-5 h-5" />
                                                                    </div>
                                                                    <input type="text" id="payment_amount" name='payment_amount' className={"input-icon"} placeholder="Payment Amount" defaultValue={FormatMoney(data.payment_amount.$numberDecimal)} onBlur={(e) => FormatCurrency(e.target, "", 'blur')} onKeyUp={(e) => FormatCurrency(e.target as HTMLInputElement, "", "")} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <label className="label" htmlFor="payment_method">Payment Method</label>
                                                        <div className="relative flex-1 group bg-white shadow-lg w-full rounded-lg dark:bg-slate-700 mb-4">
                                                            <img
                                                                className="object-contain object-center w-full rounded-lg"
                                                                src={thumbnail}
                                                                alt="File"
                                                            />
                                                            <button type="button" onClick={() => (document.getElementById("file") as HTMLInputElement).click()} className="hidden absolute group-hover:block mx-auto bottom-1/2 top-1/2 left-1/2 right-1/ h-16 transform -translate-x-1/2 text-center w-32 border border-gray-500 py-2 px-1 rounded-lg shadow bg-gray-200 text-sm dark:text-slate-600">Select File</button>
                                                            <input type="file" id="file" name="file" onChange={previewImage} className="hidden" accept="image/png, image/gif, image/jpeg, image/webp" />
                                                        </div>
                                                        <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                                                            <div className="flex-1 mb-2">
                                                                <label className="label" htmlFor="pay_date">Pay Date</label>
                                                                <div className="relative mb-2">
                                                                    <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                                                        <CalendarIcon className="text-gray-400 w-5 h-5" />
                                                                    </div>
                                                                    <input type="date" id="pay_date" name='pay_date' className={"input-icon"} placeholder="Pay Date" defaultValue={new Date().toISOString().substring(0, 10)} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className='flex flex-row space-x-4 mb-2'>

                                                            <button className="btn btn-primary flex items-center space-x-2">
                                                                <CheckIcon width={20} height={20} className="mr-2" />
                                                                Confirm
                                                            </button>
                                                            <button
                                                                type="button"
                                                                className="inline-flex justify-center rounded-md border border-transparent bg-blue-100 px-4 py-2 text-sm font-medium text-blue-900 hover:bg-blue-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-blue-500 focus-visible:ring-offset-2"
                                                                onClick={() => setPayment(false)}
                                                            >
                                                                Close
                                                            </button>
                                                        </div>
                                                    </form>
                                                </Dialog.Panel>
                                            </Transition.Child>
                                        </div>
                                    </div>
                                </Dialog>
                            </Transition>
                        </Fragment>
                    }
                </div>
            </div>
            <Success data={success} hide={() => setSuccess({ title: "", description: "" })} />
            <div className="relative flex flex-col items-stretch mb-4 space-x-0 lg:flex-row lg:space-x-4 ">

                <div className="p-6 w-3/4 mt-4 mb-2 bg-white shadow-lg rounded-tl-3xl rounded-br-3xl lg:mt-0 dark:bg-slate-700 dark:text-slate-300 lg:mb-4">
                    <div className="flex items-center content-center py-2 mb-4 space-x-2 border-b border-slate-200 dark:border-slate-500">
                        <InformationCircleIcon width={24} height={24} />
                        <span className="text-lg text-semibold">Information</span>
                    </div>
                    <div className="flex flex-col px-2 space-x-0 lg:flex-row lg:space-x-4">
                        <div className="flex-1">
                            <div className="flex flex-col mb-2">
                                <div className="text-sm">Billing ID</div>
                                <div className="font-semibold">{data?._id}</div>
                            </div>
                            <div className="flex flex-col mb-2">
                                <div className="text-sm">Billing Type</div>
                                <div className="font-semibold">{data?.type}</div>
                            </div>
                            <div className="flex flex-col mb-2">
                                <div className="text-sm">Billing Name</div>
                                <div className="font-semibold">{data?.name}</div>
                            </div>
                            <div className="flex flex-col mb-2">
                                <div className="text-sm">Description</div>
                                <div className="font-semibold">{data?.description}</div>
                            </div>
                            <div className="flex flex-col mb-2">
                                <div className="text-sm">Amount</div>
                                <div className="font-semibold">{FormatMoney(data?.amount.$numberDecimal)}</div>
                            </div>
                        </div>
                        <div className="flex-1">
                            <div className="flex flex-col mb-2">
                                <div className="text-sm">Payment Method</div>
                                <div className="font-semibold">{data?.payment_method}</div>
                            </div>
                            <div className="flex flex-col mb-2">
                                <div className="text-sm">Payment Amount</div>
                                <div className="font-semibold">{FormatMoney(data?.payment_amount.$numberDecimal)}</div>
                            </div>
                            {data?.pay_date &&
                                <div className="flex flex-col mb-2">
                                    <div className="text-sm">Pay Date</div>
                                    <div className="font-semibold">{DayDateFormat(data.pay_date)}</div>
                                </div>
                            }
                            {data?.proof_of_payment &&
                                <div className="flex flex-col mb-2">
                                    <div className="text-sm">Proof of Payment</div>
                                    <Image onClick={() => setShowProof(true)} src={data?.proof_of_payment ? data.proof_of_payment : "/img/no-image.png"} width={150} height={150} loading="lazy" className="object-cover cursor-pointer w-24 h-24 rounded-lg shadow hover:shadow-xl" alt="Proof of payment" />
                                    <Transition appear show={showProof} as={Fragment}>
                                        <Dialog as="div" className="relative z-10" onClose={() => setShowProof(false)}>
                                            <Transition.Child
                                                as={Fragment}
                                                enter="ease-out duration-300"
                                                enterFrom="opacity-0"
                                                enterTo="opacity-100"
                                                leave="ease-in duration-200"
                                                leaveFrom="opacity-100"
                                                leaveTo="opacity-0"
                                            >
                                                <div className="fixed inset-0 bg-white dark:bg-black dark:bg-opacity-75 blur-lg bg-opacity-75" />
                                            </Transition.Child>

                                            <div className="fixed inset-0 overflow-y-auto">
                                                <div className="flex min-h-full items-center justify-center p-4 text-center">
                                                    <Transition.Child
                                                        as={Fragment}
                                                        enter="ease-out duration-300"
                                                        enterFrom="opacity-0 scale-95"
                                                        enterTo="opacity-100 scale-100"
                                                        leave="ease-in duration-200"
                                                        leaveFrom="opacity-100 scale-100"
                                                        leaveTo="opacity-0 scale-95"
                                                    >
                                                        <Dialog.Panel className="w-full max-w-xl border border-slate-100 dark:border-slate-600 transform overflow-hidden rounded-2xl bg-white dark:bg-slate-700 p-6 text-left align-middle shadow-xl transition-all">
                                                            <Dialog.Title
                                                                as="h3"
                                                                className="text-lg flex justify-between font-medium leading-6 text-gray-900 dark:text-slate-100"
                                                            >
                                                                Proof of Payment
                                                                <XMarkIcon title="close" width={16} className="cursor-pointer  text-slate-400" onClick={() => setShowProof(false)} />
                                                            </Dialog.Title>
                                                            <Image src={data?.proof_of_payment ? data.proof_of_payment : "/img/no-image.png"} width={150} height={150} loading="lazy" className="object-cover w-full my-4 rounded-lg shadow hover:shadow-xl" alt="Proof of payment" />


                                                            <div className='flex flex-row space-x-4 mb-2'>
                                                                <button
                                                                    type="button"
                                                                    className="inline-flex justify-center rounded-md border border-transparent bg-blue-100 px-4 py-2 text-sm font-medium text-blue-900 hover:bg-blue-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-blue-500 focus-visible:ring-offset-2"
                                                                    onClick={() => setShowProof(false)}
                                                                >
                                                                    Close
                                                                </button>
                                                            </div>
                                                        </Dialog.Panel>
                                                    </Transition.Child>
                                                </div>
                                            </div>
                                        </Dialog>
                                    </Transition>
                                </div>
                            }

                        </div>
                    </div>
                </div>
                <div className="p-6 w-1/4 mt-4 mb-2 bg-white shadow-lg rounded-tl-3xl rounded-br-3xl lg:mt-0 dark:bg-slate-700 dark:text-slate-300 lg:mb-4">
                    <div className="flex items-center content-center py-2 mb-4 space-x-2 border-b border-slate-200 dark:border-slate-500">
                        <ClockIcon width={24} height={24} />
                        <span className="text-lg text-semibold">Status &amp; Creation Info</span>
                    </div>
                    <div className="flex flex-col px-2 space-x-0 lg:flex-row lg:space-x-4">
                        <div className="flex-1">
                            <div className="flex flex-col mb-2">
                                <div className="text-sm">Status</div>
                                <div className="font-semibold">{data?.status}</div>
                            </div>
                            <div className="flex flex-col mb-2">
                                <div className="text-sm">Created</div>
                                {data?.created &&
                                    <>
                                        <div className="font-semibold">{FullShortDay(data?.created?.at)}</div>
                                        {data?.created?.by && <div className="font-semibold">{data?.created?.by.name}</div>}
                                    </>
                                }
                            </div>
                            <div className="flex flex-col mb-2">
                                <div className="text-sm">Updated</div>
                                {data?.updated?.by ?
                                    <>
                                        <div className="font-semibold">{FullShortDay(data?.updated?.at)}</div>
                                        {data?.updated?.by && <div className="font-semibold">{data?.updated?.by.name}</div>}
                                    </> : <div className="font-semibold">Never</div>
                                }
                            </div>
                            <div className="flex flex-col mb-2">
                                <div className="text-sm">Confirmed</div>
                                {data?.confirmed?.by ?
                                    <>
                                        <div className="font-semibold">{FullShortDay(data?.confirmed?.at)}</div>
                                        {data?.confirmed?.by && <div className="font-semibold">{data?.confirmed?.by.name}</div>}
                                    </> : <div className="font-semibold">Never</div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    )
}