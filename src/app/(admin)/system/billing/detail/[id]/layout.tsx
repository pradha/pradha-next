import DashboardLayout from "@/components/ui/layouts/dashboard"
import { authOptions } from "@/lib/auth/options";
import clientPromise from "@/lib/connections/mongo";
import { ObjectId } from "mongodb";
import { getServerSession } from "next-auth";
import { notFound, redirect } from "next/navigation";

export default async function RootLayout(props: any) {
    const session = await getServerSession(authOptions);
    if (!session)
        redirect('/');
    
    const db = (await clientPromise).db()
    
    let data: any
    if (session?.user.group.roles.includes("61e3de6ecc4029a9d11a2020"))
        data = await db.collection("v_system_billing").findOne({ _id: new ObjectId(props.params.id) })
    else
        data = await db.collection("v_system_billing").findOne({ _id: new ObjectId(props.params.id), 'created.by._id': new ObjectId(session?.user.id) })

    if (!data)
        notFound()

    props.params.data = JSON.parse(JSON.stringify(data))
    return (
        <>{props.children}</>
    )
}

const title = 'Billing Detail'
const description = 'System Billing Detail'
const keywords = ["system", "billing", "detail", "system billing detail"]
const url = process.env.SITE_URL + '/system/billing/detail'
export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: false,
        follow: false,
        nocache: false,
        googleBot: {
            index: false,
            follow: false
        },
    },
}