import DashboardLayout from "@/components/ui/layouts/dashboard"
import { getServerSession } from "next-auth";
import { redirect } from "next/navigation";

export default async function RootLayout({ children }: { children: React.ReactNode }) {
    const session = await getServerSession();
    if (!session)
        redirect('/');
        
    return (
        <DashboardLayout>{children}</DashboardLayout>
    )
}

const title = 'System Billing'
const description = 'System Billing'
const keywords = ["system", "billing", "system billing"]
const url = process.env.SITE_URL + '/system/billing'
export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: false,
        follow: false,
        nocache: false,
        googleBot: {
            index: false,
            follow: false
        },
    },
}