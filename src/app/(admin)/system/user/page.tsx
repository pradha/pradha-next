'use client'
import { useSession } from "next-auth/react"
import { redirect, usePathname } from 'next/navigation';
import { FormEvent, useEffect, useState } from 'react'
import Link from "next/link";
import { UserIcon, KeyIcon, ArrowPathIcon, PlusIcon, XMarkIcon, MagnifyingGlassIcon, PencilIcon, IdentificationIcon } from "@heroicons/react/24/outline";
import { Warning } from "@/components/ui/alert";
import { Spinner } from "@/components/ui/loader";
import path from "path";
import { FullShort } from "@/lib/time";
import { Pagination } from "@/components/ui/pagination";

export default function Page() {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })

    const pathName = usePathname()

    let fill: any
    let errors: string[] = []
    const [spinner, setSpinner] = useState(false)
    const [data, setData] = useState(fill)
    const [src, setSrc] = useState(false)
    const [pageNum, setPageNum] = useState(1)


    const search = async (e: FormEvent) => {
        e.preventDefault()
        let target = e.target as HTMLFormElement

        if (target.search.value.length > 0) {
            setSpinner(true);
            const res = await fetch(path.posix.join('/api/', pathName ? pathName : "", '/search'), {
                body: JSON.stringify({
                    search: target.search.value
                }),
                headers: {
                    'Content-Type': 'application/json',
                },
                method: 'POST'
            })

            const result = await res.json()
            setData({ data: result.search, count: result.search.length })
            setSrc(true)
            setSpinner(false);
        }
    }

    const getData = async (page: number) => {
        setSpinner(true);
        setPageNum(page)
        const data = await fetch('/api/' + (pathName ? pathName : "") + "?page=" + (page ? page : pageNum), {
            headers: { 'Content-Type': 'application/json' },
            method: 'GET'
        })
        setSpinner(false);
        return data.json()
    }

    const getPage = async (page: number) => {
        setSpinner(true);
        let data = await getData(page)
        setData(data)
        setSpinner(false);
    }

    useEffect(() => {
        (async () => {
            let data = await getData(pageNum)
            setData(data)

        })();
        return
    }, [])
    return (
        <main>
            <div className="flex flex-col md:flex-row md:space-y-0 space-y-4 mb-4 justify-between">
                <div>
                    <h1 className="text-xl lg:text-2xl font-semibold">User Accounts</h1>
                    <div className="text-sm text-slate-500">System User Accounts</div>
                </div>
                <div className="flex items-center space-x-2">
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a2012") &&
                        <Link href={path.posix.join(pathName ? pathName : "", "create")} title="Create new user" className="btn-circle" aria-label="new-user">
                            <PlusIcon width={20} height={20} />
                        </Link>
                    }
                    <form onSubmit={search} className="flex items-center flex-1 bg-white rounded-full h-9">
                        <div className="w-full">
                            <input type="search" id="search" name="search" required className="w-full px-4 py-1 text-sm border-none rounded-full outline-1 focus:ring-0 focus:outline-sky-100" placeholder="Search..." x-model="search" />
                        </div>
                        <div className="-ml-8">
                            <button type="submit" className="flex items-center justify-center w-8 h-8 rounded-full text-slate-100 bg-sky-500" >
                                <MagnifyingGlassIcon width={18} height={18} />
                            </button>
                        </div>
                    </form>
                </div>
            </div>

            <div className="relative bg-white shadow-lg w-full rounded-tl-3xl rounded-br-3xl dark:bg-slate-700">
                {spinner && <Spinner className=" rounded-tl-3xl rounded-br-3xl" />}
                <div className="flex items-center px-6 py-3 space-x-2 text-sm tracking-wide">
                    <div>Total {src ? "Search result" : "Data"} :</div>
                    <div className="font-semibold">{data?.count ? data.count : "0"}</div>
                    <div>Item{data?.count > 1 ? "s" : ""}</div>
                    {src &&
                        <button className="p-1 mx-2 text-slate-100 rounded-md shadow-sm bg-gradient-to-br from-sky-300 to-sky-400 hover:from-sky-300 hover:to-sky-300" onClick={() => { setSrc(false); getPage(pageNum); (document.getElementById("search") as HTMLInputElement).value = "" }} type="button">
                            <XMarkIcon width={16} />
                        </button>}
                </div>
                <div className="relative overflow-x-auto">
                    <table className="table-auto min-w-full divide-y divide-gray-200 border-b border-gray-300">
                        <thead className="thead">
                            <tr className="">
                                <th className="thead-column">Name</th>
                                <th className="thead-column">Group</th>
                                <th className="thead-column">Gender</th>
                                <th className="thead-column">Biography</th>
                                <th className="thead-column">Status</th>
                                <th className="thead-column">Created</th>
                                {(session?.user.group.roles.includes("61e3de6ecc4029a9d11a2013") || session?.user.group.roles.includes("61e3de6ecc4029a9d11a2014")) &&
                                    <th className="thead-column">Action</th>
                                }
                            </tr>
                        </thead>
                        <tbody>

                            {typeof data !== undefined && data?.data.map((item: any, i: number) =>
                                <tr key={item._id} className="border-b border-gray-200 divide-slate-200 bg-gradient-to-br from-slate-50 odd:to-gray-100 even:to-slate-200  hover:from-sky-50 hover:to-sky-100 transition-all ease-in-out delay-200">
                                    <td className="tbody-column">
                                        <div className="flex gap-3 font-normal text-gray-900">
                                            <div className="relative h-10 w-10">
                                                <img
                                                    className="h-full w-full rounded-full object-cover object-center"
                                                    src={item?.photo ? process.env.NEXT_PUBLIC_STATIC_FILES + item.photo : "/img/person.jpg"}
                                                    alt="Person"
                                                />
                                                <span className={"absolute right-0 bottom-0 h-2 w-2 rounded-full ring ring-white " + (item?.status?.active ? "bg-green-400" : "bg-red-400")}></span>
                                            </div>
                                            <div className="text-sm">
                                                <div className="font-medium text-gray-700">{item.name}</div>
                                                <div className="text-gray-400">{item.email}</div>
                                            </div>
                                        </div>
                                    </td>
                                    <td className="tbody-column">{item?.group?.name}</td>
                                    <td className="tbody-column">{item?.gender ? item.gender : "Unknown"}</td>
                                    <td className="tbody-column">{item?.biography ? item?.biography : "-"}</td>
                                    <td className="tbody-column">{item?.status?.verified ? "Verified" : "Unverified"}</td>

                                    <td className="tbody-column">
                                        <div className="flex flex-col  font-light text-xs">
                                            <div>{FullShort(item.created.at)}</div>
                                            <div><span className="font-semibold">{item?.created?.by?.name}</span></div>
                                        </div>
                                    </td>
                                    {(session?.user.group.roles.includes("61e3de6ecc4029a9d11a2013") || session?.user.group.roles.includes("61e3de6ecc4029a9d11a2014")) &&
                                        <td className="tbody-column  max-w-32">
                                            <div className="flex flex-row space-x-2">
                                                {session?.user.group.roles.includes("61e3de6ecc4029a9d11a2013") &&
                                                    <Link href={path.posix.join(pathName ? pathName : "", 'edit', item._id)} title="Edit" className="p-1 rounded-md shadow-sm bg-gradient-to-br from-sky-400 to-sky-500 hover:from-sky-400 hover:to-sky-400">
                                                        <PencilIcon className="w-3 h-3 text-white" />
                                                    </Link>
                                                }
                                                {session?.user.group.roles.includes("61e3de6ecc4029a9d11a2014") &&
                                                    <Link href={path.posix.join(pathName ? pathName : "", 'detail', item._id)} title="Detail" className="p-1 rounded-md shadow-sm bg-gradient-to-br from-sky-400 to-sky-500 hover:from-sky-400 hover:to-sky-400">
                                                        <IdentificationIcon className="w-3 h-3 text-white" />
                                                    </Link>
                                                }
                                            </div>
                                        </td>
                                    }
                                </tr>
                            )}
                            {(!data || data?.count <= 0) &&
                                <tr>
                                    <td className="p-6 text-center italic text-sm" colSpan={7}>no data at this time</td>
                                </tr>
                            }
                        </tbody>
                    </table>
                    {!src && <Pagination count={data?.count} page={data?.page} total={data?.total} click={getPage} />}
                </div>
            </div>
        </main>
    )
}