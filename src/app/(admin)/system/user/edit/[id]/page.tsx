'use client'
import { useSession } from "next-auth/react"
import { redirect, usePathname } from 'next/navigation';
import { FormEvent, useEffect, useState } from 'react'
import Link from "next/link";
import { UserIcon, KeyIcon, ArrowPathIcon, PencilSquareIcon, ListBulletIcon, AdjustmentsHorizontalIcon, IdentificationIcon, MapPinIcon, CalendarDaysIcon, EnvelopeIcon, PlusIcon } from "@heroicons/react/24/outline";
import { Success, Warning } from "@/components/ui/alert";
import { Spinner } from "@/components/ui/loader";
import path from "path";
import { ISODate } from "@/lib/time";

export default function Page(props: any) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })

    const pathName = usePathname()

    let errors = {
        group: "",
        name: "",
        username: "",
        email: "",
        gender: "",
    }
    const [data, setData] = useState(props.params.data)
    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState({ errors: errors })
    const [success, setSuccess] = useState({ title: "", description: "" })

    useEffect(() => {
        setSpinner(true)
        fetch(path.posix.join('/api/', pathName as string, "../..", "?id=" + props.params.id))
            .then((data) => data.json())
            .then((res) => {
                props.params.data = res.data;
                setData(res.data);
                (document.getElementById('group') as HTMLInputElement).value = res.data.group._id;
                (document.getElementById('name') as HTMLInputElement).value = res.data.name;
                (document.getElementById('username') as HTMLInputElement).value = res.data.username;
                (document.getElementById('email') as HTMLInputElement).value = res.data.email;
                (document.getElementById('biography') as HTMLInputElement).value = res.data.biography ? res.data.biography : "";
                (document.getElementById('date_of_birth') as HTMLInputElement).value = res.data.date_of_birth ? ISODate(res.data.date_of_birth) : "";
                (document.getElementById('location') as HTMLInputElement).value = res.data.location ? res.data.location : "";
            })
        setSpinner(false)
    }, []);

    const submit = async (e: FormEvent) => {
        e.preventDefault()
        setWarning({ errors: errors })
        setSpinner(true)

        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement
        const submit = await fetch(path.posix.join("/api/", pathName as string, '../..'), {
            method: 'PUT',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                id: data._id,
                group: target.group.value,
                name: (document.getElementById("name") as HTMLInputElement).value,
                username: target.username.value,
                email: target.email.value,
                gender: target.gender.value,
                biography: target.biography.value,
                date_of_birth: target.date_of_birth.value,
                location: target.location.value
            })
        });
        //await new Promise(resolve => setTimeout(resolve, 3000));
        const res = await submit.json()
        if (submit.ok) {
            setSuccess({ title: "Success", description: "User account has been successfully updated" })
        } else {
            setWarning({ errors: await res.errors })
            errors = {
                group: "",
                name: "",
                username: "",
                email: "",
                gender: "",
            }
        }
        setSpinner(false)
    }
    return (
        <main>
            <div className="flex flex-col md:flex-row md:space-y-0 space-y-4 mb-4 justify-between">
                <div>
                    <h1 className="text-xl lg:text-2xl font-semibold">Edit User</h1>
                    <div className="text-sm text-slate-500">Edit System User Account</div>
                </div>
                <div className="flex items-center space-x-2">
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a2012") &&
                        <Link href={path.posix.join(pathName as string, "../..", "create")} title="Create User" className="btn-circle" aria-label="create-user">
                            <PlusIcon width={20} height={20} />
                        </Link>
                    }
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a2014") &&
                        <Link href={path.posix.join(pathName as string, "../..", "detail", data?._id as string)} title="User Detail" className="btn-circle" aria-label="user-detail">
                            <IdentificationIcon width={20} height={20} />
                        </Link>
                    }
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a2010") &&
                        <Link href={path.posix.join(pathName as string, "../..")} title="User Account data" className="btn-circle" aria-label="user-data">
                            <ListBulletIcon width={20} height={20} />
                        </Link>
                    }
                </div>
            </div>

            <div className="relative w-full p-6 bg-white shadow-lg rounded-tl-3xl rounded-br-3xl dark:bg-slate-700">
                {spinner && <Spinner className=" rounded-tl-3xl rounded-br-3xl" />}
                <Success data={success} hide={() => setSuccess({ title: "", description: "" })} />
                <form onSubmit={submit} className="ng-untouched ng-pristine ng-valid">
                    <Warning className="" errors={Object.values(warning.errors).filter(a => a != "")} hide={() => setWarning({ errors: errors })} />
                    <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                        <div className="lg:w-1/4 w-full mb-2">
                            <label className="label" htmlFor="group">Group Level <sup className="text-rose-500">*</sup></label>
                            <div className="relative inline-block w-full text-gray-700">
                                <select name="group" id="group" className={warning?.errors.group ? "select-error" : "select"} defaultValue={data?.group._id}>
                                    {props.params?.groups.map((item: any, i: number) => <option key={item._id} value={item._id}>{item.name}</option>)}
                                </select>
                            </div>
                        </div>
                        <div className="flex-1 mb-2">
                            <label className="label" htmlFor="name">Full Name <sup className="text-rose-500">*</sup></label>
                            <div className="relative mb-2">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <IdentificationIcon className="w-5 h-5 text-gray-400" />
                                </div>
                                <input type="name" id="name" name='name' defaultValue={data?.name} className={warning?.errors.name ? "input-icon-error" : "input-icon"} placeholder="Full Name" />
                            </div>
                        </div>
                    </div>
                    <div className='flex flex-col mb-2 space-x-0 lg:flex-row lg:space-x-4'>
                        <div className="w-full mb-2 lg:w-1/4">
                            <label className="label" htmlFor="usermail">Username <sup className="text-rose-500">*</sup></label>
                            <div className="relative mb-2">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <UserIcon className="w-5 h-5 text-gray-400" />
                                </div>
                                <input type="text" id="username" name='username' defaultValue={data?.username} className={warning?.errors.username ? "input-icon-error" : "input-icon"} placeholder="Username" />
                            </div>
                        </div>
                        <div className="flex-1 mb-2">
                            <label className="label" htmlFor="email">Email <sup className="text-rose-500">*</sup></label>
                            <div className="relative mb-2">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <EnvelopeIcon className="w-5 h-5 text-gray-400" />
                                </div>
                                <input type="email" id="email" name='email' defaultValue={data?.email} className={warning?.errors.email ? "input-icon-error" : "input-icon"} placeholder="Email" />
                            </div>
                        </div>
                        <div className="w-full mb-2 lg:w-1/4">
                            <label className="label" htmlFor="gender">Gender <sup className="text-rose-500">*</sup></label>
                            <div className="relative inline-block w-full text-gray-700">
                                <select name="gender" id="gender" defaultValue={data?.gender ? data.gender : "Unknown"} className={warning?.errors.gender ? "select-error" : "select"}>
                                    <option value="Unknown">Unknown</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div className="mb-4">
                        <label htmlFor="biography" className="label">Biography</label>
                        <textarea name="biography" id="biography" placeholder="User Biography" className={"input"} rows={3} defaultValue={data.biography}></textarea>
                    </div>
                    <div className='flex flex-col mb-2 space-x-0 space-y-2 lg:flex-row lg:space-x-4 lg:space-y-0'>
                        <div className="w-full mb-2 lg:w-1/4">
                            <label className="label" htmlFor="date_of_birth">Date of Birth</label>
                            <div className="relative mb-2">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <CalendarDaysIcon className="w-5 h-5 text-gray-400" />
                                </div>
                                <input type="date" id="date_of_birth" defaultValue={data?.date_of_birth ? ISODate(data?.date_of_birth) : ""} name='date_of_birth' className="input-icon" placeholder="Date of Birth" />
                            </div>
                        </div>
                        <div className="flex-1 mb-2">
                            <label className="label" htmlFor="location">Location</label>
                            <div className="relative mb-2">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <MapPinIcon className="w-5 h-5 text-gray-400" />
                                </div>
                                <input type="location" id="location" name='location' defaultValue={data?.location} className={"input-icon"} placeholder="Location (ex. Jakarta, Indonesia)" />
                            </div>
                        </div>
                    </div>
                    <div className="">
                        <button type="submit" className="btn btn-primary">
                            <ArrowPathIcon width={20} height={20} className="mr-2" />
                            Update Account
                        </button>
                    </div>
                </form>
            </div>
        </main>
    )
}