'use client'
import { useSession } from "next-auth/react"
import { redirect, usePathname } from 'next/navigation';
import { FormEvent, useState } from 'react'
import Link from "next/link";
import { UserIcon, KeyIcon, ArrowPathIcon, PencilSquareIcon, ListBulletIcon, AdjustmentsHorizontalIcon, UserCircleIcon, ClockIcon, PlusIcon, IdentificationIcon } from "@heroicons/react/24/outline";
import { Warning } from "@/components/ui/alert";
import { Spinner } from "@/components/ui/loader";
import Image from "next/image";
import { DayDateFormat, FullShortDay } from "@/lib/time";
import path from "path";

export default function Page(props: any) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })

    const pathName = usePathname()
    let errors: string[] = []
    const [data, setData] = useState(props.params.data)
    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState(errors)

    const submit = async (e: FormEvent) => {
        e.preventDefault()
        setWarning(errors)
        setSpinner(true)

        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement
    }
    return (
        <main>
            <div className="flex flex-col md:flex-row md:space-y-0 space-y-4 mb-4 justify-between">
                <div>
                    <h1 className="text-xl lg:text-2xl font-semibold">User Details</h1>
                    <div className="text-sm text-slate-500">User Account Details</div>
                </div>
                <div className="flex items-center space-x-2">
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a2012") &&
                        <Link href={path.posix.join(pathName as string, "../..", "create")} title="Create User" className="btn-circle" aria-label="create-user">
                            <PlusIcon width={20} height={20} />
                        </Link>
                    }
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a2013") &&
                        <Link href={path.posix.join(pathName as string, "../..", "edit", data?._id as string)} title="Edit User" className="btn-circle" aria-label="user-edit">
                            <PencilSquareIcon width={20} height={20} />
                        </Link>
                    }
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a2010") &&
                        <Link href={path.posix.join(pathName as string, "../..")} title="User Account data" className="btn-circle" aria-label="user-data">
                            <ListBulletIcon width={20} height={20} />
                        </Link>
                    }
                </div>
            </div>

            <div className="relative flex flex-col items-stretch mb-4 space-x-0 lg:flex-row lg:space-x-4 ">
                <div className="relative w-full text-gray-700 lg:w-1/4 xl:w-1/6 dark:text-slate-300">

                    <div className="sticky top-4 ">

                        <div className="relative overflow-hidden py-6 bg-white shadow-lg rounded-tl-3xl rounded-br-3xl group h-72 lg:h-56">
                            {spinner && <Spinner className="rounded-tl-3xl rounded-br-3xl" />}
                            <Image
                                className="object-cover object-center w-full h-64 rounded-tl-3xl rounded-br-3xl"
                                src={data?.photo ? data?.photo : "/img/person.jpg"}
                                alt="Person"
                                fill
                                priority
                                sizes="(max-width: 768px) 100vw,(max-width: 1200px) 50vw,33vw"
                            />

                        </div>
                    </div>
                </div>
                <div className="flex-1 text-slate-700 lg:w-3/4 xl:w-5/6">
                    <div className="p-6 mt-4 mb-2 bg-white shadow-lg rounded-tl-3xl rounded-br-3xl lg:mt-0 dark:bg-slate-700 dark:text-slate-300 lg:mb-4">
                        <div className="flex items-center content-center py-2 mb-4 space-x-2 border-b border-slate-200">
                            <UserIcon width={24} height={24} />
                            <span className="text-lg text-semibold">About</span>
                        </div>
                        <div className="flex flex-col px-2 space-x-0 lg:flex-row lg:space-x-4">
                            <div className="flex-1">
                                <div className="flex flex-col mb-2">
                                    <div className="text-sm">User ID</div>
                                    <div className="font-semibold">{data?._id}</div>
                                </div>
                                <div className="flex flex-col mb-2">
                                    <div className="text-sm">Full Name</div>
                                    <div key={Math.random()} className="font-semibold">{data?.name}</div>
                                </div>
                                <div className="flex flex-col mb-2">
                                    <div className="text-sm">E-Mail</div>
                                    <div className="font-semibold">{data?.email}</div>
                                </div>
                                <div className="flex flex-col mb-2">
                                    <div className="text-sm">Date of Birth</div>
                                    <div className="font-semibold">{data?.date_of_birth ? DayDateFormat(data?.date_of_birth) : "-"}</div>
                                </div>
                            </div>
                            <div className="flex-1">
                                <div className="flex flex-col mb-2">
                                    <div className="text-sm">Username</div>
                                    <div className="font-semibold">{data?.username}</div>
                                </div>
                                <div className="flex flex-col mb-2">
                                    <div className="text-sm">Group Level</div>
                                    <div className="font-semibold">{data?.group?.name}</div>
                                </div>
                                <div className="flex flex-col mb-2">
                                    <div className="text-sm">Registered</div>
                                    <div className="font-semibold">{FullShortDay(data?.created?.at)}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="p-6 mt-4 mb-2 bg-white shadow-lg rounded-tl-3xl rounded-br-3xl lg:mt-0 dark:bg-slate-700 dark:text-slate-300 lg:mb-4">
                        <div className="flex items-center content-center py-2 mb-4 space-x-2 border-b border-slate-200">
                            <UserCircleIcon width={24} height={24} />
                            <span className="text-lg text-semibold">Biography</span>
                        </div>
                        <p>{data?.biography}</p>
                    </div>
                    <div className="p-6 mt-4 mb-2 bg-white shadow-lg rounded-tl-3xl rounded-br-3xl lg:mt-0 dark:bg-slate-700 dark:text-slate-300 lg:mb-4">
                        <div className="flex items-center content-center py-2 mb-4 space-x-2 border-b border-slate-200">
                            <ClockIcon width={24} height={24} />
                            <span className="text-lg text-semibold">Status &amp; Creation Info</span>
                        </div>
                        <div className="flex flex-col px-2 space-x-0 lg:flex-row lg:space-x-4">
                            <div className="flex-1">
                                <div className="flex flex-col mb-2">
                                    <div className="text-sm">Status</div>
                                    <div className="font-semibold">Verification : {data?.status?.verified ? "Verified" : "Unverified"}</div>
                                    <div className="font-semibold">Active : {data?.status?.active ? "Active" : "Inactive"}</div>
                                </div>
                            </div>
                            <div className="flex-1">
                                <div className="flex flex-col mb-2">
                                    <div className="text-sm">Created</div>
                                    {data?.created &&
                                        <>
                                            <div className="font-semibold">{FullShortDay(data?.created?.at)}</div>
                                            {data?.created?.by && <div className="font-semibold">{data?.created?.by}</div>}
                                        </>
                                    }
                                </div>
                                <div className="flex flex-col mb-2">
                                    <div className="text-sm">Updated</div>
                                    {data && data?.updated && data?.updated?.at ?
                                        <>
                                            <div className="font-semibold">{FullShortDay(data?.updated?.at)}</div>
                                            {data?.updated?.by && <div className="font-semibold">{data?.updated?.by?.name}</div>}
                                        </> : <div className="font-semibold">Never</div>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    )
}