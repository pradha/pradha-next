'use client'
import { useSession } from "next-auth/react"
import { redirect, usePathname } from 'next/navigation';
import { FormEvent, useState } from 'react'
import Link from "next/link";
import { ArrowPathIcon, CalendarDaysIcon, EnvelopeIcon, IdentificationIcon, ListBulletIcon, MapPinIcon, PlusIcon, UserIcon } from "@heroicons/react/24/outline";
import { Success, Warning } from "@/components/ui/alert";
import { Spinner } from "@/components/ui/loader";
import path from "path";

export default function Page(props: any) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })

    const pathName = usePathname()

    let errors = {
        group: "",
        name: "",
        username: "",
        email: "",
        gender: "",
    }
    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState({ errors: errors })
    const [success, setSuccess] = useState({ title: "", description: "" })

    const submit = async (e: FormEvent) => {
        e.preventDefault()
        setWarning({ errors: errors })
        setSpinner(true)

        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement
        const submit = await fetch(path.posix.join("/api/", pathName as string, '..'), {
            method: 'POST',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                group: target.group.value,
                name: (document.getElementById("name") as HTMLInputElement).value,
                username: target.username.value,
                email: target.email.value,
                gender: target.gender.value,
                biography: target.biography.value,
                date_of_birth: target.date_of_birth.value,
                location: target.location.value
            })
        });
        //await new Promise(resolve => setTimeout(resolve, 3000));
        const res = await submit.json()
        if (submit.ok) {
            setSuccess({ title: "Success", description: "New user account has been successfully created" })
        } else {
            setWarning({ errors: await res.errors })
            errors = {
                group: "",
                name: "",
                username: "",
                email: "",
                gender: "",
            }
        }
        setSpinner(false)
    }
    return (
        <main>
            <div className="flex flex-col md:flex-row md:space-y-0 space-y-4 mb-4 justify-between">
                <div>
                    <h1 className="text-xl lg:text-2xl font-semibold">Create User</h1>
                    <div className="text-sm text-slate-500">Create New User Account</div>
                </div>
                <div className="flex items-center space-x-2">
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a2010") &&
                        <Link href={"/system/user"} title="User data" className="btn-circle" aria-label="user-data">
                            <ListBulletIcon width={20} height={20} />
                        </Link>
                    }
                </div>
            </div>

            <div className="relative w-full p-6 bg-white shadow-lg rounded-tl-3xl rounded-br-3xl dark:bg-slate-700">
                {spinner && <Spinner className=" rounded-tl-3xl rounded-br-3xl" />}
                <Success data={success} hide={() => setSuccess({ title: "", description: "" })} />
                <form onSubmit={submit} className="ng-untouched ng-pristine ng-valid">
                    <Warning className="" errors={Object.values(warning.errors).filter(a => a != "")} hide={() => setWarning({ errors: errors })} />
                    <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                        <div className="lg:w-1/4 w-full mb-2">
                            <label className="label" htmlFor="group">Group Level <sup className="text-rose-500">*</sup></label>
                            <div className="relative inline-block w-full text-gray-700">
                                <select name="group" id="group" className={warning?.errors.group ? "select-error" : "select"} defaultValue={""}>
                                    {props.params?.groups.map((item: any, i: number) => <option key={item._id} value={item._id}>{item.name}</option>)}
                                </select>
                            </div>
                        </div>
                        <div className="flex-1 mb-2">
                            <label className="label" htmlFor="name">Full Name <sup className="text-rose-500">*</sup></label>
                            <div className="relative mb-2">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <IdentificationIcon className="w-5 h-5 text-gray-400" />
                                </div>
                                <input type="name" id="name" name='name' className={warning?.errors.name ? "input-icon-error" : "input-icon"} placeholder="Full Name" />
                            </div>
                        </div>
                    </div>
                    <div className='flex flex-col mb-2 space-x-0 lg:flex-row lg:space-x-4'>
                        <div className="w-full mb-2 lg:w-1/4">
                            <label className="label" htmlFor="usermail">Username <sup className="text-rose-500">*</sup></label>
                            <div className="relative mb-2">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <UserIcon className="w-5 h-5 text-gray-400" />
                                </div>
                                <input type="text" id="username" name='username' className={warning?.errors.username ? "input-icon-error" : "input-icon"} placeholder="Username" />
                            </div>
                        </div>
                        <div className="flex-1 mb-2">
                            <label className="label" htmlFor="email">Email <sup className="text-rose-500">*</sup></label>
                            <div className="relative mb-2">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <EnvelopeIcon className="w-5 h-5 text-gray-400" />
                                </div>
                                <input type="email" id="email" name='email' className={warning?.errors.email ? "input-icon-error" : "input-icon"} placeholder="Email" />
                            </div>
                        </div>
                        <div className="w-full mb-2 lg:w-1/4">
                            <label className="label" htmlFor="gender">Gender <sup className="text-rose-500">*</sup></label>
                            <div className="relative inline-block w-full text-gray-700">
                                <select name="gender" id="gender" className={warning?.errors.gender ? "select-error" : "select"}>
                                    <option value="Unknown">Unknown</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div className="mb-4">
                        <label htmlFor="biography" className="label">Biography</label>
                        <textarea name="biography" id="biography" placeholder="User Biography" className={"input"} rows={3}></textarea>
                    </div>
                    <div className='flex flex-col mb-2 space-x-0 space-y-2 lg:flex-row lg:space-x-4 lg:space-y-0'>
                        <div className="w-full mb-2 lg:w-1/4">
                            <label className="label" htmlFor="date_of_birth">Date of Birth</label>
                            <div className="relative mb-2">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <CalendarDaysIcon className="w-5 h-5 text-gray-400" />
                                </div>
                                <input type="date" id="date_of_birth" name='date_of_birth' className="input-icon" placeholder="Date of Birth" />
                            </div>
                        </div>
                        <div className="flex-1 mb-2">
                            <label className="label" htmlFor="location">Location</label>
                            <div className="relative mb-2">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <MapPinIcon className="w-5 h-5 text-gray-400" />
                                </div>
                                <input type="location" id="location" name='location' className={"input-icon"} placeholder="Location (ex. Jakarta, Indonesia)" />
                            </div>
                        </div>
                    </div>
                    <div className="">
                        <button type="submit" className="btn btn-primary">
                            <PlusIcon width={20} height={20} className="mr-2" />
                            Create Account
                        </button>
                    </div>
                </form>
            </div>
        </main>
    )
}