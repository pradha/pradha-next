'use client'
import { useSession } from "next-auth/react"
import { redirect, usePathname } from 'next/navigation';
import { FormEvent, useState } from 'react'
import Link from "next/link";
import { UserIcon, KeyIcon, ArrowPathIcon, ListBulletIcon, PlusIcon, IdentificationIcon, TagIcon, LinkIcon, HashtagIcon } from "@heroicons/react/24/outline";
import { Success, Warning } from "@/components/ui/alert";
import { Spinner } from "@/components/ui/loader";
import path from "path";
import { useRouter } from "next/navigation";

export const revalidate = 0;
export const dynamic = 'force-dynamic';
export default function Page(props: any) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })

    const pathName = usePathname()
    const route = useRouter()

    let errors = {
        parent: "",
        name: "",
        description: "",
        position: "",
        code: ""
    }
    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState({ errors: errors })
    const [data, setData] = useState(props.params.data)
    const [parents, setParents] = useState(props.params.parents)
    const [success, setSuccess] = useState({ title: "", description: "" })

    const submit = async (e: FormEvent) => {
        e.preventDefault()
        setWarning({ errors: errors })
        setSpinner(true)

        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement
        const submit = await fetch(path.posix.join("/api/", pathName as string, '../..'), {
            method: 'PUT',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                id: data._id,
                name: (document.getElementById("name") as HTMLInputElement).value,
                description: target.description.value,
            })
        });
        //await new Promise(resolve => setTimeout(resolve, 3000));
        const res = await submit.json()
        if (submit.ok) {
            route.refresh()
            setSuccess({ title: "Success", description: "Chart of account has been updated" })
        } else {
            errors = await res.errors
            setWarning({ errors: errors })
            errors = {
                parent: "",
                name: "",
                description: "",
                position: "",
                code: ""
            }
        }
        setSpinner(false)
    }

    const changeParent = async (e: FormEvent) => {
        const target = e.target as HTMLSelectElement
        const req = await fetch(path.join('/api', pathName as string, "..", "last"), {
            method: 'POST',
            body: JSON.stringify({ parent: target.value }),
            headers: {
                'Content-Type': 'application/json',
            },
        })
        const last = await req.json()
        let code = parseInt(last.data.code) + 1;
        (document.getElementById('code') as HTMLInputElement).value = code.toString()
        if (code.toString().substr(0, 1) == "1" || code.toString().substr(0, 1) == "5" || code.toString().substr(0, 1) == "6" || code.toString().substr(0, 1) == "8")
            (document.getElementById('position') as HTMLSelectElement).value = "Db"
        else
            (document.getElementById('position') as HTMLSelectElement).value = "Cr"
        return
    }
    return (
        <main>
            <div className="flex flex-col md:flex-row md:space-y-0 space-y-4 mb-4 justify-between">
                <div>
                    <h1 className="text-xl lg:text-2xl font-semibold">Edit CoA</h1>
                    <div className="text-sm text-slate-500">Edit Cart of Acount (CoA)</div>
                </div>
                <div className="flex items-center space-x-2">
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5001") &&
                        <Link href={path.join(pathName as string, "../..")} title="CoA data" className="btn-circle" aria-label="coa-data">
                            <ListBulletIcon width={20} height={20} />
                        </Link>
                    }
                </div>
            </div>

            <div className="relative bg-white shadow-lg w-full rounded-tl-3xl rounded-br-3xl p-6 dark:bg-slate-700">
                {spinner && <Spinner className="rounded-tl-3xl rounded-br-3xl" />}
                <Success data={success} hide={() => setSuccess({ title: "", description: "" })} />
                <form onSubmit={submit} className="ng-untouched ng-pristine ng-valid">
                    <Warning className="" errors={Object.values(warning.errors).filter(a => a != "")} hide={() => setWarning({ errors: errors })} />
                    <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                        <div className="lg:w-1/4 w-full mb-2">
                            <label className="label" htmlFor="group">Parent</label>
                            <div className="relative inline-block w-full text-gray-700">
                                <select name="parent" id="parent" disabled className={warning?.errors.position ? "select-error" : "select"} defaultValue={data.parent} onChange={changeParent}>
                                    <option value="1">&nbsp;&nbsp;&nbsp;&nbsp;1 - ASSET (Aktiva) - Db</option>
                                    {parents.filter((parent: any) => parent.parent == 1).map((parent: any) => <option key={parent._id} value={parent._id}>{parent.code} - {parent.name}</option>)}
                                    <option value="2">&nbsp;&nbsp;&nbsp;&nbsp;2 - LIABILITY - Cr</option>
                                    {parents.filter((parent: any) => parent.parent == 2).map((parent: any) => <option key={parent._id} value={parent._id}>{parent.code} - {parent.name}</option>)}
                                    <option value="3">&nbsp;&nbsp;&nbsp;&nbsp;3 - EQUITY (Modal) - Cr</option>
                                    {parents.filter((parent: any) => parent.parent == 3).map((parent: any) => <option key={parent._id} value={parent._id}>{parent.code} - {parent.name}</option>)}
                                    <option value="4">&nbsp;&nbsp;&nbsp;&nbsp;4 - REVENUE (Pendapatan) - Cr</option>
                                    {parents.filter((parent: any) => parent.parent == 4).map((parent: any) => <option key={parent._id} value={parent._id}>{parent.code} - {parent.name}</option>)}
                                    <option value="5">&nbsp;&nbsp;&nbsp;&nbsp;5 - HARGA POKOK PENJUALAN (HPP) - Db</option>
                                    {parents.filter((parent: any) => parent.parent == 5).map((parent: any) => <option key={parent._id} value={parent._id}>{parent.code} - {parent.name}</option>)}
                                    <option value="6">&nbsp;&nbsp;&nbsp;&nbsp;6 - EXPENSE (Biaya/Beban) - Db</option>
                                    {parents.filter((parent: any) => parent.parent == 6).map((parent: any) => <option key={parent._id} value={parent._id}>{parent.code} - {parent.name}</option>)}
                                    <option value="7">&nbsp;&nbsp;&nbsp;&nbsp;7 - PENDAPATAN LAIN-LAIN - Cr</option>
                                    {parents.filter((parent: any) => parent.parent == 7).map((parent: any) => <option key={parent._id} value={parent._id}>{parent.code} - {parent.name}</option>)}
                                </select>
                            </div>
                        </div>
                        <div className="flex-1 mb-2">
                            <label className="label" htmlFor="name">Name <sup className="text-rose-500">*</sup></label>
                            <div className="relative mb-2">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <IdentificationIcon className="text-gray-400 w-5 h-5" />
                                </div>
                                <input type="name" id="name" name='name' defaultValue={data.name} className={warning?.errors.name ? "input-icon-error" : "input-icon"} placeholder="CoA Name" />
                            </div>
                        </div>

                    </div>
                    <div className="mb-4">
                        <label htmlFor="description" className="label">Description <sup className="text-rose-500">*</sup></label>
                        <textarea name="description" id="description" defaultValue={data.description} placeholder="Category Description" className={warning?.errors.description ? "input-error" : "input"} rows={3}></textarea>
                    </div>
                    <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                        <div className="lg:w-1/4 w-full mb-2">
                            <label className="label" htmlFor="position">Position</label>
                            <div className="relative inline-block w-full text-gray-700">
                                <select name="position" id="position" disabled className={warning?.errors.position ? "select-error" : "select"} defaultValue={data.position}>
                                    <option value="Db">Debit (Db)</option>
                                    <option value="Cr">Credit (Cr)</option>
                                </select>
                            </div>
                        </div>
                        <div className="flex-1 mb-2">
                            <label className="label" htmlFor="code">Code <sup className="text-rose-500">*</sup></label>
                            <div className="relative mb-2">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <HashtagIcon className="text-gray-400 w-5 h-5" />
                                </div>
                                <input type="code" id="code" name='code' defaultValue={data.code} className={warning?.errors.code ? "input-icon-error" : "input-icon"} placeholder="CoA Code" readOnly />
                            </div>
                        </div>
                    </div>
                    <button type="submit" className="btn btn-primary flex items-center space-x-2">
                        <ArrowPathIcon width={20} height={20} className="mr-2" />
                        Update CoA
                    </button>
                </form>
            </div >
        </main >
    )
}