import DashboardLayout from "@/components/ui/layouts/dashboard"
import clientPromise from "@/lib/connections/mongo";
import { ObjectId } from "mongodb";
import { getServerSession } from "next-auth";
import { redirect } from "next/navigation";

export const revalidate = 0
export default async function RootLayout(props: any) {
    const session = await getServerSession();
    if (!session)
        redirect('/');

    const db = (await clientPromise).db()
    const parents = JSON.parse(JSON.stringify(await db.collection('accounting_coa').find({code: { "$lte": 999 }}).toArray()))
    props.params.parents = parents
    return (
        <>{props.children}</>
    )
}

const title = 'New CoA'
const description = 'Create New Cart of Account'
const keywords = ["accounting", "cart of account", "coa"]
const url = process.env.SITE_URL + '/accounting/coa'
export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: false,
        follow: false,
        nocache: false,
        googleBot: {
            index: false,
            follow: false
        },
    },
}