'use client'
import { useSession } from "next-auth/react"
import { redirect } from 'next/navigation';
import { FormEvent, Fragment, useEffect, useState } from 'react'
import Link from "next/link";
import { PlusIcon, PencilIcon, MagnifyingGlassIcon, XMarkIcon, ChevronDoubleRightIcon, XCircleIcon } from "@heroicons/react/24/outline";
import { Spinner } from "@/components/ui/loader";
import { FullShort } from "@/lib/time";
import { usePathname } from 'next/navigation';
import { Pagination } from "@/components/ui/pagination";
import path from "path";
import { MasterCoA } from "@/lib/accounting/masterCoA";



export const revalidate = 5;
export const dynamic = 'force-dynamic';

const getParents = async () => {

}
export default function Page(props: any) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })
    const pathName = usePathname()
    let fill: any
    let errors: string[] = []
    const [spinner, setSpinner] = useState(false)
    const [data, setData] = useState(fill)
    const [masters, setMasters] = useState<any[]>([])
    const [src, setSrc] = useState(false)
    const [pageNum, setPageNum] = useState(1)


    const search = async (e: FormEvent) => {
        e.preventDefault()
        let target = e.target as HTMLFormElement

        if (target.search.value.length > 0) {
            const res = await fetch(path.posix.join('/api/', pathName ? pathName : "", '/search'), {
                body: JSON.stringify({
                    search: target.search.value
                }),
                headers: {
                    'Content-Type': 'application/json',
                },
                method: 'POST'
            })

            const result = await res.json()
            setData({ data: result.search, count: result.search.length })
            setSrc(true)
            setMasters(MasterCoA)
        }
    }

    const getData = async (page: number) => {
        setSpinner(true);
        setPageNum(page)
        const data = await fetch('/api/' + (pathName ? pathName : "") + "?page=" + (page ? page : pageNum), {
            headers: { 'Content-Type': 'application/json' },
            method: 'GET'
        })
        setSpinner(false);
        return data.json()
    }

    const getPage = async (page: number) => {
        setSpinner(true);
        let data = await getData(page)
        setData(data)
        setSpinner(false);
    }
    var loaded = false;
    useEffect(() => {
        if (!loaded) {
            (async () => {
                let data = await getData(pageNum)
                setData(data)
                setMasters(MasterCoA)
            })();
            loaded = true
            return
        }
    }, [])

    return (
        <main>
            <div className="flex flex-col justify-between mb-4 space-y-4 md:flex-row md:space-y-0">
                <div>
                    <h1 className="text-xl font-semibold lg:text-2xl">Cart of Account</h1>
                    <div className="text-sm text-slate-500">Accounting Cart of Account (CoA)</div>
                </div>
                <div className="flex items-center space-x-2">
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5002") &&
                        <Link href={path.posix.join(pathName ? pathName : "", "create")} as={path.posix.join(pathName ? pathName : "", "create")} title="Create new CoA" className="btn-circle" aria-label="new-coa">
                            <PlusIcon width={20} height={20} />
                        </Link>
                    }
                    <form onSubmit={search} className="flex items-center flex-1 bg-white rounded-full h-9">
                        <div className="w-full">
                            <input type="search" id="search" name="search" required className="w-full px-4 py-1 text-sm border-none rounded-full outline-1 focus:ring-0 focus:outline-sky-100" placeholder="Search..." x-model="search" />
                        </div>
                        <div className="-ml-8">
                            <button type="submit" className="flex items-center justify-center w-8 h-8 rounded-full text-slate-100 bg-sky-500" >
                                <MagnifyingGlassIcon width={18} height={18} />
                            </button>
                        </div>
                    </form>
                </div>
            </div>

            <div className="relative w-full text-gray-600 bg-white shadow-lg rounded-tl-3xl rounded-br-3xl dark:bg-slate-700 dark:text-slate-300 transform transition-all ease-in-out">
                {spinner && <Spinner className=" rounded-tl-3xl rounded-br-3xl" />}
                <div className="flex items-center px-6 py-3 space-x-2 text-sm tracking-wide">
                    <div>Total {src ? "Search result" : "Data"} :</div>
                    <div className="font-semibold">{data?.count ? data.count : "0"}</div>
                    <div>Item{data?.count > 1 ? "s" : ""}</div>
                    {src &&
                        <button className="p-1 mx-2 text-slate-100 rounded-md shadow-sm bg-gradient-to-br from-sky-300 to-sky-400 hover:from-sky-300 hover:to-sky-300" onClick={() => { setSrc(false); getPage(pageNum); (document.getElementById("search") as HTMLInputElement).value = "" }} type="button">
                            <XMarkIcon width={16} />
                        </button>}
                </div>
                <div className="relative overflow-x-auto">
                    <table className="min-w-full border-b border-gray-300 divide-y divide-gray-200 table-auto">
                        <thead className="thead">
                            <tr className="">
                                <th className="thead-column">Code</th>
                                <th className="thead-column">Name</th>
                                <th className="thead-column">Description</th>
                                <th className="thead-column">Position</th>
                                <th className="thead-column">Created</th>
                                {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5003") &&
                                    <th className="thead-column">Action</th>
                                }
                            </tr>
                        </thead>
                        <tbody>
                            {masters.map((master: any) =>
                                <Fragment key={master.code}>
                                    <tr className="transition-all ease-in-out delay-200 border-b border-gray-200 divide-slate-200 bg-gradient-to-br from-slate-50 odd:to-gray-100 even:to-slate-200 hover:from-sky-50 hover:to-sky-100">
                                        <td className="tbody-column text-right">{master.code}</td>
                                        <td className="tbody-column">{master.name}</td>
                                        <td className="tbody-column">{master.description}</td>
                                        <td className="tbody-column">{master.position}</td>
                                        <td className="tbody-column"></td>
                                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5003") &&
                                            <td className="tbody-column"></td>
                                        }
                                    </tr>
                                    {data?.data?.filter((parent: any) => parent?.code.toString().substr(0, 1) == master.code && parent?.code.toString().length == 3).map((coa: any) => {
                                        return <Fragment key={coa._id}>
                                            <tr key={coa._id} className="transition-all ease-in-out delay-200 border-b border-gray-200 divide-slate-200 bg-gradient-to-br from-slate-50 odd:to-gray-100 even:to-slate-200 hover:from-sky-50 hover:to-sky-100">
                                                <td className="tbody-column text-right">{coa.code}</td>
                                                <td className="tbody-column">{coa.name}</td>
                                                <td className="tbody-column">{coa.description}</td>
                                                <td className="tbody-column">{coa.position}</td>
                                                <td className="tbody-column w-52">
                                                    <div className="flex flex-col text-xs font-light">
                                                        <div>{FullShort(coa.created.at)}</div>
                                                        <div><span className="font-semibold">{coa?.created?.by ? coa.created.by.name : ""}</span></div>
                                                    </div>
                                                </td>
                                                {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5003") &&
                                                    <td className="w-32 tbody-column">
                                                        <div className="flex flex-row space-x-2">
                                                            <Link href={path.posix.join(pathName ? pathName : "", 'edit', coa._id)} title="Edit" className="p-1 rounded-md shadow-sm bg-gradient-to-br from-sky-400 to-sky-500 hover:from-sky-400 hover:to-sky-400">
                                                                <PencilIcon className="w-3 h-3 text-white" />
                                                            </Link>
                                                        </div>
                                                    </td>
                                                }
                                            </tr>
                                            {data?.data?.filter((parent: any) => parent.code.toString().substr(0, 3) == coa.code && parent.code.toString().length == 5).map((sub: any) => <tr key={sub._id} className="transition-all ease-in-out delay-200 border-b border-gray-200 divide-slate-200 bg-gradient-to-br from-slate-50 odd:to-gray-100 even:to-slate-200 hover:from-sky-50 hover:to-sky-100">
                                                <td className="tbody-column text-right">{sub.code}</td>
                                                <td className="tbody-column">{sub.name}</td>
                                                <td className="tbody-column">{sub.description}</td>
                                                <td className="tbody-column">{sub.position}</td>
                                                <td className="tbody-column w-52">
                                                    <div className="flex flex-col text-xs font-light">
                                                        <div>{FullShort(sub.created.at)}</div>
                                                        <div><span className="font-semibold">{sub?.created?.by ? sub.created.by.name : ""}</span></div>
                                                    </div>
                                                </td>
                                                {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5003") &&
                                                    <td className="w-32 tbody-column">
                                                        <div className="flex flex-row space-x-2">
                                                            <Link href={path.posix.join(pathName ? pathName : "", 'edit', sub._id)} title="Edit" className="p-1 rounded-md shadow-sm bg-gradient-to-br from-sky-400 to-sky-500 hover:from-sky-400 hover:to-sky-400">
                                                                <PencilIcon className="w-3 h-3 text-white" />
                                                            </Link>
                                                        </div>
                                                    </td>
                                                }
                                            </tr>)}
                                        </Fragment>
                                    })}
                                </Fragment>
                            )}

                        </tbody>
                    </table>
                </div>
            </div>
        </main>
    )
}