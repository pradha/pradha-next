'use client'
import { useSession } from "next-auth/react"
import { redirect, usePathname, useRouter } from 'next/navigation';
import { FormEvent, Fragment, useState } from 'react'
import Link from "next/link";
import { UserIcon, KeyIcon, ArrowPathIcon, ListBulletIcon, PlusIcon, IdentificationIcon, TagIcon, LinkIcon, HashtagIcon, CalculatorIcon, BanknotesIcon, PencilIcon, CheckBadgeIcon, CheckIcon, XMarkIcon, ArrowsUpDownIcon, ArrowDownTrayIcon, ArrowUpTrayIcon, ReceiptPercentIcon, PrinterIcon } from "@heroicons/react/24/outline";
import { Success, Warning } from "@/components/ui/alert";
import { Spinner } from "@/components/ui/loader";
import path from "path";
import { MasterCoA } from "@/lib/accounting/masterCoA";
import { FormatCurrency, FormatMoney, FormatNumber } from "@/lib/text";
import { Dialog, Menu, Transition } from "@headlessui/react";
import { DateTimeShort } from "@/lib/time";

export const revalidate = 0;
export const dynamic = 'force-dynamic';
export default function Page(props: any) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })

    const pathName = usePathname()
    const router = useRouter()

    let errors = {
        coa: "",
        number: "",
        name: "",
        description: ""
    }
    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState({ errors: errors })
    const [success, setSuccess] = useState(props.searchParams?.status && props.searchParams?.status == "created" ? { title: "Success", description: "Transaction has been successfully created" } : props.searchParams?.status && props.searchParams?.status == "updated" ? { title: "Success", description: "Transaction has been successfully updated" } : props.searchParams?.status && props.searchParams?.status == "confirmed" ? { title: "Success", description: "Transaction has been confirmed" } : props.searchParams?.status && props.searchParams?.status == "aborted" ? { title: "Success", description: "Transaction has been successfully aborted" } : { title: "", description: "" })
    const [data, setData] = useState(props.params.data)
    const [confirm, setConfirm] = useState(false)
    const [close, setClose] = useState(false)
    const [print, setPrint] = useState(false)
    const [reverse, setReverse] = useState(false)

    let tdebit: number = 0, tcredit: number = 0
    let amount: number = 0.00

    const confirmTransaction = async (e: FormEvent) => {
        e.preventDefault()
        setWarning({ errors: errors })
        setSpinner(true)
        setConfirm(false)
        setSuccess({ title: "", description: "" })
        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement
        const submit = await fetch(path.posix.join("/api/", pathName as string, '../..'), {
            method: 'PATCH',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                id: data._id,
            })
        });

        const res = await submit.json()
        if (submit.ok) {
            let newData = data
            data.confirmed.at = new Date()
            data.confirmed.by = { _id: session?.user.id, name: session?.user.name }
            data.status = 'Confirmed'
            setData(newData)
            setSuccess({ title: "Success", description: "Transaction has been successfully confirmed" })
            router.refresh()
            router.replace(path.join(pathName as string, "../..", "detail", data._id + "?status=confirmed"))
        } else {
            errors = await res.errors
            setWarning({ errors: errors })
            errors = {
                coa: "",
                number: "",
                name: "",
                description: "",
            }
        }
        setSpinner(false)
    }

    const reverseTransaction = async (e: FormEvent) => {
        e.preventDefault()
        setWarning({ errors: errors })
        setSpinner(true)
        setConfirm(false)
        setSuccess({ title: "", description: "" })
        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement
        const submit = await fetch(path.posix.join("/api/", pathName as string, '../..'), {
            method: 'DELETE',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                id: data._id,
            })
        });

        const res = await submit.json()
        if (submit.ok) {
            let newData = res.transaction
            setData(newData)
            setSuccess({ title: "Success", description: "Transaction has been successfully reversed" })
            router.refresh()
            router.replace(path.join(pathName as string, "../..", "detail", data._id + "?status=reversed"))
            setReverse(false)
        } else {
            errors = await res.errors
            setWarning({ errors: errors })
            errors = {
                coa: "",
                number: "",
                name: "",
                description: "",
            }
        }
        setSpinner(false)
    }

    const abortTransaction = async (e: FormEvent) => {
        e.preventDefault()
        setWarning({ errors: errors })
        setSpinner(true)
        setClose(false)
        setSuccess({ title: "", description: "" })
        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement
        const submit = await fetch(path.posix.join("/api/", pathName as string, '../..'), {
            method: 'DELETE',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                id: data._id,
            })
        });

        const res = await submit.json()
        if (submit.ok) {
            let newData = data
            data.aborted.at = new Date()
            data.aborted.by = { _id: session?.user.id, name: session?.user.name }
            data.status = 'Aborted'
            setData(newData)
            setSuccess({ title: "Success", description: "Transaction has been successfully aborted" })
            router.refresh()
            router.replace(path.join(pathName as string, "../..", "detail", data._id + "?status=aborted"))
        } else {
            errors = await res.errors
            setWarning({ errors: errors })
            errors = {
                coa: "",
                number: "",
                name: "",
                description: "",
            }
        }
        setSpinner(false)
    }

    data?.journals.map((journal: any) => { amount = amount + Number(journal.debit.$numberDecimal) })

    return (
        <main>
            <div className="flex flex-col md:flex-row md:space-y-0 space-y-4 mb-4 justify-between">
                <div>
                    <h1 className="text-xl lg:text-2xl font-semibold">Transaction Detail</h1>
                    <div className="text-sm text-slate-500">Accounting Transaction Detail - {data?.code}</div>
                </div>
                <div className="flex items-center space-x-2">
                {data?.status == "Confirmed" && session?.user.group.roles.includes("61e3de6ecc4029a9d11a5025") && !data?.reversed &&
                        <Fragment>
                            <button type="button" title="Reverse Transaction" className="btn-circle-red" onClick={() => setReverse(true)} aria-label="reverse-transaction">
                                <ArrowPathIcon width={20} height={20} />
                            </button>

                            <Transition appear show={reverse} as={Fragment}>
                                <Dialog as="div" className="relative z-10" onClose={() => setReverse(false)}>
                                    <Transition.Child
                                        as={Fragment}
                                        enter="ease-out duration-300"
                                        enterFrom="opacity-0"
                                        enterTo="opacity-100"
                                        leave="ease-in duration-200"
                                        leaveFrom="opacity-100"
                                        leaveTo="opacity-0"
                                    >
                                        <div className="fixed inset-0 bg-white dark:bg-black dark:bg-opacity-75 blur-lg bg-opacity-75" />
                                    </Transition.Child>

                                    <div className="fixed inset-0 overflow-y-auto">
                                        <div className="flex min-h-full items-center justify-center p-4 text-center">
                                            <Transition.Child
                                                as={Fragment}
                                                enter="ease-out duration-300"
                                                enterFrom="opacity-0 scale-95"
                                                enterTo="opacity-100 scale-100"
                                                leave="ease-in duration-200"
                                                leaveFrom="opacity-100 scale-100"
                                                leaveTo="opacity-0 scale-95"
                                            >
                                                <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-2xl bg-white p-6 text-left align-middle shadow-xl transition-all">
                                                    <Dialog.Title
                                                        as="h3"
                                                        className="text-lg flex justify-between font-medium leading-6 text-gray-900"
                                                    >
                                                        Reverse Transaction
                                                        <XMarkIcon title="close" width={16} className="cursor-pointer  text-slate-400" onClick={() => setReverse(false)} />
                                                    </Dialog.Title>
                                                    <div className="mt-2">
                                                        <p className="text-sm text-gray-500">
                                                            Are you sure you want to reverse this transaction :
                                                        </p>
                                                        <blockquote className="italic m-4">
                                                            <div className="flex flex-row space-x-2">
                                                                <div className="w-20">TX Code</div>
                                                                <div>:</div>
                                                                <div><span className="font-semibold">{data?.code}</span></div>
                                                            </div>
                                                            <div className="flex flex-row space-x-2">
                                                                <div className="w-20">Ref</div>
                                                                <div>:</div>
                                                                <div><span className="font-semibold">{data.reference}</span></div>
                                                            </div>
                                                            <div className="flex flex-row space-x-2">
                                                                <div className="w-20">Information</div>
                                                                <div>:</div>
                                                                <div><span className="font-semibold">{data.information}</span></div>
                                                            </div>
                                                            <div className="flex flex-row space-x-2">
                                                                <div className="w-20">Amount</div>
                                                                <div>:</div>
                                                                <div><span className="font-semibold">{FormatMoney(amount)}</span></div>
                                                            </div>
                                                            <div className="flex flex-row space-x-2">
                                                                <div className="w-20">Created</div>
                                                                <div>:</div>
                                                                <div><span className="font-semibold">{DateTimeShort(data.created.at)}</span><br />by <span className="text-sm font-semibold">{data.created.by.name}</span></div>
                                                            </div>
                                                        </blockquote>
                                                    </div>

                                                    <form onSubmit={reverseTransaction} className="flex space-x-2 justify-end mt-4">
                                                        <button className="btn btn-primary flex items-center space-x-2">
                                                            <CheckIcon width={20} height={20} className="mr-2" />
                                                            Reverse
                                                        </button>
                                                        <button
                                                            type="button"
                                                            className="inline-flex justify-center rounded-md border border-transparent bg-blue-100 px-4 py-2 text-sm font-medium text-blue-900 hover:bg-blue-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-blue-500 focus-visible:ring-offset-2"
                                                            onClick={() => setReverse(false)}
                                                        >
                                                            Close
                                                        </button>
                                                    </form>
                                                </Dialog.Panel>
                                            </Transition.Child>
                                        </div>
                                    </div>
                                </Dialog>
                            </Transition>
                        </Fragment>
                    }
                    {data?.status == "Confirmed" &&
                        <Fragment>
                            <button type="button" title="Print Transaction" className="btn-circle-yellow" onClick={() => setPrint(true)} aria-label="print-transaction">
                                <PrinterIcon width={20} height={20} />
                            </button>

                            <Transition appear show={print} as={Fragment}>
                                <Dialog as="div" className="relative z-10" onClose={() => setPrint(false)}>
                                    <Transition.Child
                                        as={Fragment}
                                        enter="ease-out duration-300"
                                        enterFrom="opacity-0"
                                        enterTo="opacity-100"
                                        leave="ease-in duration-200"
                                        leaveFrom="opacity-100"
                                        leaveTo="opacity-0"
                                    >
                                        <div className="fixed inset-0 bg-white dark:bg-black dark:bg-opacity-75 blur-lg bg-opacity-75" />
                                    </Transition.Child>

                                    <div className="fixed inset-0 overflow-y-auto">
                                        <div className="flex min-h-full items-center justify-center p-4 text-center">
                                            <Transition.Child
                                                as={Fragment}
                                                enter="ease-out duration-300"
                                                enterFrom="opacity-0 scale-95"
                                                enterTo="opacity-100 scale-100"
                                                leave="ease-in duration-200"
                                                leaveFrom="opacity-100 scale-100"
                                                leaveTo="opacity-0 scale-95"
                                            >
                                                <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-2xl bg-white p-6 text-left align-middle shadow-xl transition-all">
                                                    <Dialog.Title
                                                        as="h3"
                                                        className="text-lg flex justify-between font-medium leading-6 text-gray-900"
                                                    >
                                                        Print
                                                        <XMarkIcon title="close" width={16} className="cursor-pointer  text-slate-400" onClick={() => setPrint(false)} />
                                                    </Dialog.Title>
                                                    <object data={"/api/accounting/print/pdf/pos?id=" + data._id}
                                                        width="100%"
                                                        height="750" />
                                                </Dialog.Panel>
                                            </Transition.Child>
                                        </div>
                                    </div>
                                </Dialog>
                            </Transition>
                        </Fragment>
                    }
                    {data?.status != "Confirmed" && data?.status != "Aborted" && session?.user.group.roles.includes("61e3de6ecc4029a9d11a5023") &&
                        <Fragment>
                            <button type="button" title="Confirm Account" className="btn-circle-green" onClick={() => setConfirm(true)} aria-label="confirm-account">
                                <CheckIcon width={20} height={20} />
                            </button>

                            <Transition appear show={confirm} as={Fragment}>
                                <Dialog as="div" className="relative z-10" onClose={() => setConfirm(false)}>
                                    <Transition.Child
                                        as={Fragment}
                                        enter="ease-out duration-300"
                                        enterFrom="opacity-0"
                                        enterTo="opacity-100"
                                        leave="ease-in duration-200"
                                        leaveFrom="opacity-100"
                                        leaveTo="opacity-0"
                                    >
                                        <div className="fixed inset-0 bg-white dark:bg-black dark:bg-opacity-75 blur-lg bg-opacity-75" />
                                    </Transition.Child>

                                    <div className="fixed inset-0 overflow-y-auto">
                                        <div className="flex min-h-full items-center justify-center p-4 text-center">
                                            <Transition.Child
                                                as={Fragment}
                                                enter="ease-out duration-300"
                                                enterFrom="opacity-0 scale-95"
                                                enterTo="opacity-100 scale-100"
                                                leave="ease-in duration-200"
                                                leaveFrom="opacity-100 scale-100"
                                                leaveTo="opacity-0 scale-95"
                                            >
                                                <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-2xl bg-white p-6 text-left align-middle shadow-xl transition-all">
                                                    <Dialog.Title
                                                        as="h3"
                                                        className="text-lg flex justify-between font-medium leading-6 text-gray-900"
                                                    >
                                                        Confirm Transaction
                                                        <XMarkIcon title="close" width={16} className="cursor-pointer  text-slate-400" onClick={() => setConfirm(false)} />
                                                    </Dialog.Title>
                                                    <div className="mt-2">
                                                        <p className="text-sm text-gray-500">
                                                            Are you sure you want to confirm this transaction :
                                                        </p>
                                                        <blockquote className="italic m-4">
                                                            <div className="flex flex-row space-x-2">
                                                                <div className="w-20">TX Code</div>
                                                                <div>:</div>
                                                                <div><span className="font-semibold">{data?.code}</span></div>
                                                            </div>
                                                            <div className="flex flex-row space-x-2">
                                                                <div className="w-20">Ref</div>
                                                                <div>:</div>
                                                                <div><span className="font-semibold">{data.reference}</span></div>
                                                            </div>
                                                            <div className="flex flex-row space-x-2">
                                                                <div className="w-20">Information</div>
                                                                <div>:</div>
                                                                <div><span className="font-semibold">{data.information}</span></div>
                                                            </div>
                                                            <div className="flex flex-row space-x-2">
                                                                <div className="w-20">Amount</div>
                                                                <div>:</div>
                                                                <div><span className="font-semibold">{FormatMoney(amount)}</span></div>
                                                            </div>
                                                            <div className="flex flex-row space-x-2">
                                                                <div className="w-20">Created</div>
                                                                <div>:</div>
                                                                <div><span className="font-semibold">{DateTimeShort(data.created.at)}</span><br />by <span className="text-sm font-semibold">{data.created.by.name}</span></div>
                                                            </div>
                                                        </blockquote>
                                                    </div>

                                                    <form onSubmit={confirmTransaction} className="flex space-x-2 justify-end mt-4">
                                                        <button className="btn btn-primary flex items-center space-x-2">
                                                            <CheckIcon width={20} height={20} className="mr-2" />
                                                            Confirm
                                                        </button>
                                                        <button
                                                            type="button"
                                                            className="inline-flex justify-center rounded-md border border-transparent bg-blue-100 px-4 py-2 text-sm font-medium text-blue-900 hover:bg-blue-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-blue-500 focus-visible:ring-offset-2"
                                                            onClick={() => setConfirm(false)}
                                                        >
                                                            Close
                                                        </button>
                                                    </form>
                                                </Dialog.Panel>
                                            </Transition.Child>
                                        </div>
                                    </div>
                                </Dialog>
                            </Transition>
                        </Fragment>
                    }
                    {data?.status == "Pending" && session?.user.group.roles.includes("61e3de6ecc4029a9d11a5024") &&
                        <Fragment>
                            <button type="button" title="Abort Transaction" className="btn-circle-red" onClick={() => setClose(true)} aria-label="close-account">
                                <XMarkIcon width={20} height={20} />
                            </button>

                            <Transition appear show={close} as={Fragment}>
                                <Dialog as="div" className="relative z-10" onClose={() => setClose(false)}>
                                    <Transition.Child
                                        as={Fragment}
                                        enter="ease-out duration-300"
                                        enterFrom="opacity-0"
                                        enterTo="opacity-100"
                                        leave="ease-in duration-200"
                                        leaveFrom="opacity-100"
                                        leaveTo="opacity-0"
                                    >
                                        <div className="fixed inset-0 bg-white dark:bg-black dark:bg-opacity-75 blur-lg bg-opacity-75" />
                                    </Transition.Child>

                                    <div className="fixed inset-0 overflow-y-auto">
                                        <div className="flex min-h-full items-center justify-center p-4 text-center">
                                            <Transition.Child
                                                as={Fragment}
                                                enter="ease-out duration-300"
                                                enterFrom="opacity-0 scale-95"
                                                enterTo="opacity-100 scale-100"
                                                leave="ease-in duration-200"
                                                leaveFrom="opacity-100 scale-100"
                                                leaveTo="opacity-0 scale-95"
                                            >
                                                <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-2xl bg-white p-6 text-left align-middle shadow-xl transition-all">
                                                    <Dialog.Title
                                                        as="h3"
                                                        className="text-lg flex justify-between font-medium leading-6 text-gray-900"
                                                    >
                                                        Abort Transaction
                                                        <XMarkIcon title="close" width={16} className="cursor-pointer  text-slate-400" onClick={() => setConfirm(false)} />
                                                    </Dialog.Title>
                                                    <div className="mt-2">
                                                        <p className="text-sm text-gray-500">
                                                            Are you sure you want to abort this transaction :
                                                        </p>
                                                        <blockquote className="italic m-4">
                                                            <div className="flex flex-row space-x-2">
                                                                <div className="w-20">TX Code</div>
                                                                <div>:</div>
                                                                <div><span className="font-semibold">{data?.code}</span></div>
                                                            </div>
                                                            <div className="flex flex-row space-x-2">
                                                                <div className="w-20">Ref</div>
                                                                <div>:</div>
                                                                <div><span className="font-semibold">{data.reference}</span></div>
                                                            </div>
                                                            <div className="flex flex-row space-x-2">
                                                                <div className="w-20">Information</div>
                                                                <div>:</div>
                                                                <div><span className="font-semibold">{data.information}</span></div>
                                                            </div>
                                                            <div className="flex flex-row space-x-2">
                                                                <div className="w-20">Amount</div>
                                                                <div>:</div>
                                                                <div><span className="font-semibold">{FormatMoney(amount)}</span></div>
                                                            </div>
                                                            <div className="flex flex-row space-x-2">
                                                                <div className="w-20">Created</div>
                                                                <div>:</div>
                                                                <div><span className="font-semibold">{DateTimeShort(data.created.at)}</span><br />by <span className="text-sm font-semibold">{data.created.by.name}</span></div>
                                                            </div>
                                                        </blockquote>
                                                    </div>

                                                    <form onSubmit={abortTransaction} className="flex space-x-2 justify-end mt-4">
                                                        <button className="btn btn-primary flex items-center space-x-2">
                                                            <XMarkIcon width={20} height={20} className="mr-2" />
                                                            Abort
                                                        </button>
                                                        <button
                                                            type="button"
                                                            className="inline-flex justify-center rounded-md border border-transparent bg-blue-100 px-4 py-2 text-sm font-medium text-blue-900 hover:bg-blue-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-blue-500 focus-visible:ring-offset-2"
                                                            onClick={() => setClose(false)}
                                                        >
                                                            Close
                                                        </button>
                                                    </form>
                                                </Dialog.Panel>
                                            </Transition.Child>
                                        </div>
                                    </div>
                                </Dialog>
                            </Transition>
                        </Fragment>
                    }
                    {(session?.user.group.roles.includes("61e3de6ecc4029a9d11a5017") || session?.user.group.roles.includes("61e3de6ecc4029a9d11a5018") || session?.user.group.roles.includes("61e3de6ecc4029a9d11a5019") || session?.user.group.roles.includes("61e3de6ecc4029a9d11a5020")) &&
                        <Menu as="div" className="relative inline-block text-left">
                            <div>
                                <Menu.Button className="btn-circle">
                                    <PlusIcon width={20} height={20} />
                                </Menu.Button>
                            </div>
                            <Transition
                                as={Fragment}
                                enter="transition ease-out duration-100"
                                enterFrom="transform opacity-0 scale-95"
                                enterTo="transform opacity-100 scale-100"
                                leave="transition ease-in duration-75"
                                leaveFrom="transform opacity-100 scale-100"
                                leaveTo="transform opacity-0 scale-95"
                            >
                                <Menu.Items className="absolute z-10 lg:right-0  mt-2 w-40 origin-top-right divide-y divide-gray-100 rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
                                    <div className="px-1 py-1 ">
                                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5017") &&
                                            <Menu.Item>
                                                {({ active }) => (
                                                    <Link href={'/accounting/transaction/journal'} as={'/accounting/transaction/journal'} className={`${active ? 'bg-sky-500 text-white' : 'text-gray-900'} group flex w-full space-x-2 items-center rounded-md px-2 py-2 text-sm`}>
                                                        <ArrowsUpDownIcon width={18} /> <span>Journal</span>
                                                    </Link>
                                                )}
                                            </Menu.Item>
                                        }
                                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5018") &&
                                            <Menu.Item>
                                                {({ active }) => (
                                                    <Link href={'/accounting/transaction/incoming'} as={'/accounting/transaction/incoming'} className={`${active ? 'bg-sky-500 text-white' : 'text-gray-900'} group flex w-full space-x-2 items-center rounded-md px-2 py-2 text-sm`}>
                                                        <ArrowDownTrayIcon width={18} /> <span>Incoming</span>
                                                    </Link>
                                                )}
                                            </Menu.Item>
                                        }
                                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5019") &&
                                            <Menu.Item>
                                                {({ active }) => (
                                                    <Link href={'/accounting/transaction/outgoing'} as={'/accounting/transaction/outgoing'} className={`${active ? 'bg-sky-500 text-white' : 'text-gray-900'} group flex w-full space-x-2 items-center rounded-md px-2 py-2 text-sm`}>
                                                        <ArrowUpTrayIcon width={18} /> <span>Outgoing</span>
                                                    </Link>
                                                )}
                                            </Menu.Item>
                                        }
                                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5020") &&
                                            <Menu.Item>
                                                {({ active }) => (
                                                    <Link href={'/accounting/transaction/invoice'} as={'/accounting/transaction/invoice'} className={`${active ? 'bg-sky-500 text-white' : 'text-gray-900'} group flex w-full space-x-2 items-center rounded-md px-2 py-2 text-sm`}>
                                                        <ReceiptPercentIcon width={18} /> <span>Invoice</span>
                                                    </Link>
                                                )}
                                            </Menu.Item>
                                        }
                                    </div>

                                </Menu.Items>
                            </Transition>
                        </Menu>
                    }
                    {data?.status == "Pending" && session?.user.group.roles.includes("61e3de6ecc4029a9d11a5017") &&
                        <Link href={data.type=="IV" ? path.join(pathName as string, "../../../transaction/invoice", "edit", data._id) : path.join(pathName as string, "../..", "edit", data._id)} title="Edit Account" className="btn-circle" aria-label="account-edit">
                            <PencilIcon width={20} height={20} />
                        </Link>
                    }
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5015") &&
                        <Link href={path.join(pathName as string, "../..")} title="Transaction data" className="btn-circle" aria-label="transaction-data">
                            <ListBulletIcon width={20} height={20} />
                        </Link>
                    }
                </div>
            </div>

            <div className="relative">
                {spinner && <Spinner className="rounded-tl-3xl rounded-br-3xl" />}
                <Success data={success} hide={() => setSuccess({ title: "", description: "" })} />
                <Warning className="" errors={Object.values(warning.errors).filter(a => a != "")} hide={() => setWarning({ errors: errors })} />
                <div className="flex flex-col md:flex-row md:space-x-4 space-x-0 md:space-y-0 space-y-4 relative z-0 w-full mb-4">
                    <div className="w-full md:w-2/3 bg-white dark:bg-slate-700 shadow-lg rounded-tl-3xl rounded-br-3xl p-4">
                        <h2 className="font-semibold text-lg md:mb-4 mb-2 border-b py-1 border-gray-100 dark:border-slate-600">Transaction Info</h2>
                        <div className="flex flex-col md:flex-row md:space-x-4 space-x-0 md:space-y-0 space-y-4 relative justify-between">
                            <div className="px-2">
                                <div className="flex flex-col mb-2">
                                    <label className="text-xs md:text-sm">Type</label>
                                    <div className="font-semibold text-base">{data.type}</div>
                                </div>
                                <div className="flex flex-col mb-2">
                                    <label className="text-xs md:text-sm">Transaction ID</label>
                                    <div className="font-semibold text-base">{data.code}</div>
                                </div>

                                <div className="flex flex-col mb-2">
                                    <label className="text-xs md:text-sm">Reference</label>
                                    <div className="font-semibold text-base">{data.reference}</div>
                                </div>
                                <div className="flex flex-col mb-2">
                                    <label className="text-xs md:text-sm">Information</label>
                                    <div className="font-semibold text-base">{data.information}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="w-full md:w-1/3 bg-white shadow-lg dark:bg-slate-700 rounded-tl-3xl rounded-br-3xl p-4">
                        <h2 className="font-semibold text-lg md:mb-4 mb-2 border-b py-1 border-gray-100 dark:border-slate-600">Creation & Status Info</h2>
                        <div className="px-2">
                            <div className="flex flex-col mb-4">
                                <label className="text-xs md:text-sm">Created</label>
                                <div className="text-xs">date <span className="font-semibold text-base">{DateTimeShort(data.created.at)}</span></div>
                                <div className="text-xs">by <span className="font-semibold text-base">{data.created.by.name}</span></div>
                            </div>
                            {data?.updated && data.updated?.by &&
                                <div className="flex flex-col mb-4">
                                    <label className="text-xs md:text-sm">Updated</label>
                                    <div className="text-xs">date <span className="font-semibold text-base">{DateTimeShort(data.updated.at)}</span></div>
                                    <div className="text-xs">by <span className="font-semibold text-base">{data.updated.by.name}</span></div>
                                </div>
                            }
                            {data?.confirmed && data.confirmed?.by &&
                                <div className="flex flex-col mb-4">
                                    <label className="text-xs md:text-sm">Confirmed</label>
                                    <div className="text-xs">date <span className="font-semibold text-base">{DateTimeShort(data.confirmed.at)}</span></div>
                                    <div className="text-xs">by <span className="font-semibold text-base">{data.confirmed.by.name}</span></div>
                                </div>
                            }
                            {data?.aborted && data.aborted?.by &&
                                <div className="flex flex-col mb-4">
                                    <label className="text-xs md:text-sm">Aborted</label>
                                    <div className="text-xs">date <span className="font-semibold text-base">{DateTimeShort(data.aborted.at)}</span></div>
                                    <div className="text-xs">by <span className="font-semibold text-base">{data.aborted.by.name}</span></div>
                                </div>
                            }
                            {data?.reversed && data.reversed?.by &&
                                <div className="flex flex-col mb-4">
                                    <label className="text-xs md:text-sm">Reversed</label>
                                    <div className="text-xs">date <span className="font-semibold text-base">{DateTimeShort(data.reversed.at)}</span></div>
                                    <div className="text-xs">by <span className="font-semibold text-base">{data.reversed.by.name}</span></div>
                                </div>
                            }
                            <div className="flex flex-col mb-4">
                                <label className="text-xs md:text-sm">Status</label>
                                <div className="font-semibold text-base flex space-x-2">
                                    {data?.status == "Pending" &&
                                        <>
                                            <ArrowPathIcon width={25} className="animate-spin" /> <span>{data?.status}</span>
                                        </>
                                    }
                                    {data?.status == "Confirmed" &&
                                        <>
                                            <CheckIcon width={25} className="text-green-600" /> <span>{data?.status}</span>
                                        </>
                                    }
                                    {data?.status == "Aborted" &&
                                        <>
                                            <XMarkIcon width={25} className="text-red-600" /> <span>{data?.status}</span>
                                        </>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="w-full bg-white dark:bg-slate-700 shadow-lg rounded-tl-3xl rounded-br-3xl p-4">
                    <h2 className="font-semibold text-lg md:mb-4 mb-2 border-b py-1 border-gray-100 dark:border-slate-600">Transaction Journals</h2>
                    <div className="overflow-x-auto">
                        <table className="min-w-full divide-y  divide-gray-200 border-b border-gray-300">
                            <thead className="bg-gray-50">
                                <tr>
                                    <th scope="col" className="thead-column">Acc No</th>
                                    <th scope="col" className="thead-column">Acc Name</th>
                                    {data?.type != "IV" &&
                                        <Fragment>
                                            <th scope="col" className="thead-column">Prev Balance</th>
                                            <th scope="col" className="thead-column">Debit</th>
                                        </Fragment>
                                    }
                                    {data?.type == "IV" &&
                                        <Fragment>
                                            <th scope="col" className="thead-column">Price</th>
                                            <th scope="col" className="thead-column">Qty</th>
                                        </Fragment>
                                    }
                                    <th scope="col" className="thead-column">Credit</th>
                                    <th scope="col" className="thead-column">Final Balance</th>
                                </tr>
                            </thead>
                            <tbody>
                                {data?.type == "IV" && data.journals.filter((journal: any) => { if (data?.type == "IV" && journal.credit.$numberDecimal > 0) return true }).map((journal: any) => {
                                    tdebit = tdebit + parseFloat(journal.debit.$numberDecimal);
                                    tcredit = tcredit + parseFloat(journal.credit.$numberDecimal);
                                    return <tr key={"journal-" + journal.id} className="transition-all ease-in-out delay-200 border-b border-gray-200 divide-slate-200 bg-gradient-to-br from-slate-50 odd:to-gray-100 even:to-slate-200 hover:from-sky-50 hover:to-sky-100">
                                        <td className="tbody-column">{journal.account.number}</td>
                                        <td className="tbody-column">{journal.account.name}</td>
                                        {data?.type != "IV" &&
                                            <Fragment>
                                                <td className="tbody-column text-right">{FormatMoney(journal.previous_balance.$numberDecimal)}</td>
                                                <td className="tbody-column text-right">{FormatMoney(journal.debit.$numberDecimal)}</td>
                                            </Fragment>
                                        }
                                        {data?.type == "IV" &&
                                            <Fragment>
                                                <td className="tbody-column text-right">{journal?.price ? FormatMoney(journal?.price.$numberDecimal) : 0.00}</td>
                                                <td className="tbody-column text-center">{journal.quantity}</td>
                                            </Fragment>
                                        }
                                        <td className="tbody-column text-right">{FormatMoney(journal.credit.$numberDecimal)}</td>
                                        <td className="tbody-column text-right">{data?.status == "Pending" ? <div className="flex justify-start space-x-2 text-gray-400"><ArrowPathIcon width={20} className="animate-spin" /> <span>Waiting for confirmation</span></div> : data?.status == "Aborted" ? FormatMoney(journal.previous_balance.$numberDecimal) : FormatMoney(journal.final_balance.$numberDecimal)}</td>
                                    </tr>
                                })}
                                {data?.type != "IV" && data.journals.map((journal: any) => {
                                    tdebit = tdebit + parseFloat(journal.debit.$numberDecimal);
                                    tcredit = tcredit + parseFloat(journal.credit.$numberDecimal);
                                    return <tr key={"journal-" + journal.id} className="transition-all ease-in-out delay-200 border-b border-gray-200 divide-slate-200 bg-gradient-to-br from-slate-50 odd:to-gray-100 even:to-slate-200 hover:from-sky-50 hover:to-sky-100">
                                        <td className="tbody-column">{journal.account.number}</td>
                                        <td className="tbody-column">{journal.account.name}</td>
                                        {data?.type != "IV" &&
                                            <Fragment>
                                                <td className="tbody-column text-right">{FormatMoney(journal.previous_balance.$numberDecimal)}</td>
                                                <td className="tbody-column text-right">{FormatMoney(journal.debit.$numberDecimal)}</td>
                                            </Fragment>
                                        }
                                        {data?.type == "IV" &&
                                            <Fragment>
                                                <td className="tbody-column text-right">{journal?.price ? FormatMoney(journal?.price.$numberDecimal) : 0.00}</td>
                                                <td className="tbody-column text-center">{journal.quantity}</td>
                                            </Fragment>
                                        }
                                        <td className="tbody-column text-right">{FormatMoney(journal.credit.$numberDecimal)}</td>
                                        <td className="tbody-column text-right">{data?.status == "Pending" ? <div className="flex justify-start space-x-2 text-gray-400"><ArrowPathIcon width={20} className="animate-spin" /> <span>Waiting for confirmation</span></div> : data?.status == "Aborted" ? FormatMoney(journal.previous_balance.$numberDecimal) : FormatMoney(journal.final_balance.$numberDecimal)}</td>
                                    </tr>
                                })}
                            </tbody>

                            <tfoot className="bg-slate-100">
                                <tr className="">
                                    <td colSpan={data?.type != "IV" ? 3 : 4} className="thead-column text-right text-base">Total</td>
                                    {data?.type != "IV" && <td className="thead-column text-right text-base">{FormatMoney(tdebit)}</td>}
                                    <td className="thead-column text-right text-base">{FormatMoney(tcredit)}</td>
                                    <td className="thead-column  text-lg">{(tdebit == tcredit || data.type == "IV") ? <CheckIcon width={25} className="text-green-600" /> : <XMarkIcon width={25} className="text-red-600" />}</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div >
        </main >
    )
}