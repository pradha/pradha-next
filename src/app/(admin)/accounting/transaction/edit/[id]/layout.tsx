import DashboardLayout from "@/components/ui/layouts/dashboard"
import clientPromise from "@/lib/connections/mongo";
import { ObjectId } from "mongodb";
import { getServerSession } from "next-auth";
import { notFound, redirect } from "next/navigation";

export const revalidate = 0
export default async function RootLayout(props: any) {
    const session = await getServerSession();
    if (!session)
        redirect('/');

    const db = (await clientPromise).db()
    const data = JSON.parse(JSON.stringify(await db.collection("v_accounting_transactions").findOne({_id: new ObjectId(props.params.id)})))
    
    if (!data)
        notFound() 
    props.params.data = data
    return (
        <>{props.children}</>
    )
}

const title = 'Edit Transaction'
const description = 'Edit Accounting Transaction'
const keywords = ["accounting", "accounting", "accounting account", "edit"]
const url = process.env.SITE_URL + '/accounting/transaction/edit'
export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: false,
        follow: false,
        nocache: false,
        googleBot: {
            index: false,
            follow: false
        },
    },
}