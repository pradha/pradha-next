'use client'
import { useSession } from "next-auth/react"
import { redirect, useRouter } from 'next/navigation';
import { ChangeEvent, FormEvent, Fragment, useEffect, useState } from 'react'
import Link from "next/link";
import { PlusIcon, PencilIcon, MagnifyingGlassIcon, XMarkIcon, ChevronDoubleRightIcon, XCircleIcon, AdjustmentsHorizontalIcon, IdentificationIcon, CheckIcon, NoSymbolIcon, ArrowPathIcon, ChevronDownIcon, ArrowDownTrayIcon, ArrowUpTrayIcon, ArrowsUpDownIcon, ReceiptPercentIcon, ListBulletIcon, HashtagIcon } from "@heroicons/react/24/outline";
import { Spinner } from "@/components/ui/loader";
import { FullShort, ISODate } from "@/lib/time";
import { usePathname } from 'next/navigation';
import { Pagination } from "@/components/ui/pagination";
import path from "path";
import { FormatCurrency, FormatMoney, FormatNumber } from "@/lib/text";
import { Menu, Transition } from "@headlessui/react";
import { TransactionTypes } from "@/lib/accounting/transactionTypes";
import { Warning } from "@/components/ui/alert";

export default function Page(props: any) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })
    const pathName = usePathname()
    const router = useRouter()
    let fill: any
    let errors = {
        reference: "",
        information: "",
        name: "",
        description: ""
    }
    const [data, setData] = useState(props.params.data)
    const [spinner, setSpinner] = useState(false)
    const [success, setSuccess] = useState({ title: "", description: "" })
    const [warning, setWarning] = useState({ errors: errors })
    const [journals, setJournals] = useState<any[]>([])
    const [balance, setBalance] = useState(true)

    const getAccount = async (e: ChangeEvent) => {
        let acc = e.target as HTMLInputElement;
        let name = acc.parentNode?.parentNode?.nextSibling as HTMLElement;
        if (acc.value.length > 5 && parseInt(acc.value)) {
            const req = await fetch('/api/accounting/account?number=' + acc.value, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
            })
            const res = await req.json()
            if (req.ok) {
                name.getElementsByTagName('input')[0].value = res.data?.name
                acc.classList.remove('input-icon-error')
                acc.classList.add('input-icon')
            } else {
                name.getElementsByTagName('input')[0].value = "INVALID ACCOUNT NUMBER"
                acc.classList.add('input-icon-error')
                acc.classList.remove('input-icon')
            }

        }
    }

    const submit = async (e: FormEvent) => {
        e.preventDefault()
        setWarning({ errors: errors })
        setSpinner(true)
        setSuccess({ title: "", description: "" })
        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        let target = e.target as HTMLFormElement
        var journals = []
        for (var i = 0; i <= document.querySelectorAll('input[name="account"]').length - 1; i++) {
            if (parseInt((document.querySelectorAll('input[name="account"]')[i] as HTMLInputElement).value))
                journals.push({
                    number: parseInt((document.querySelectorAll('input[name="account"]')[i] as HTMLInputElement).value),
                    debit: (document.querySelectorAll('input[name="debit"]')[i] as HTMLInputElement).value,
                    credit: (document.querySelectorAll('input[name="credit"]')[i] as HTMLInputElement).value
                })
        }
        const req = await fetch('/api/accounting/transaction', {
            body: JSON.stringify({
                id: data._id,
                reference: target.reference.value,
                information: target.information.value,
                journals: journals,
            }),
            headers: {
                'Content-Type': 'application/json',
            },
            method: 'PUT'
        })
        const result = await req.json()
        if (req.ok) {
            router.replace(path.join(pathName as string, "../..", "detail", data._id + "?status=updated"))
        } else {
            errors = await result.errors
            setWarning({ errors: errors })
            errors = {
                reference: "",
                information: "",
                name: "",
                description: ""
            }
        }
        setSpinner(false)
    }

    const checkBalance = () => {
        if ((document.getElementById('tDebit') as HTMLInputElement).value != (document.getElementById('tCredit') as HTMLInputElement).value) setBalance(false)
        else setBalance(true)
    }
    const calculateTotal = () => {
        let debit = document.getElementsByName("debit")
        let credit = document.getElementsByName("credit")
        let tdebit = 0;
        let tcredit = 0;
        for (var db of debit) {
            tdebit = tdebit + parseFloat((db as HTMLInputElement).value.replaceAll(",", ""))
        }
        for (var cr of credit) {
            tcredit = tcredit + parseFloat((cr as HTMLInputElement).value.replaceAll(",", ""))
        }
        (document.getElementById('tDebit') as HTMLInputElement).value = FormatMoney(tdebit);
        (document.getElementById('tCredit') as HTMLInputElement).value = FormatMoney(tcredit);
        checkBalance();
    }

    const Journal = ({ index }: { index: string }) => (
        <Fragment>
            <div className="block md:hidden border border-gray-200 mb-2"></div>
            <div className="flex flex-col md:flex-row md:space-x-4 space-x-0 space-y-4 md:space-y-0">
                <div className="lg:w-1/4 w-full mb-2">
                    <label className="label block md:hidden" htmlFor="number">Account No <sup className="text-rose-500">*</sup></label>
                    <div className="relative mb-2">
                        <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                            <HashtagIcon className="text-gray-400 w-5 h-5" />
                        </div>
                        <input type="text" id="account" name='account' className={"input-icon"} placeholder="Account Number" onChange={(e) => getAccount(e)} />
                    </div>
                </div>
                <div className="lg:w-1/2 w-full mb-2">
                    <label className="label block md:hidden" htmlFor="number">Account Name <sup className="text-rose-500">*</sup></label>
                    <div className="relative mb-2">
                        <input type="text" id="name" name='name' className={"input"} placeholder="Account Name" readOnly />
                    </div>
                </div>
                <div className="w-full md:w-2/12 mb-2">
                    <label className="label block md:hidden" htmlFor="number">Debit <sup className="text-rose-500">*</sup></label>
                    <div className="relative mb-2">
                        <input type="text" id="debit" name='debit' className={"input text-right"} defaultValue="0.00" placeholder="Amount" onChange={calculateTotal} onBlur={(e) => FormatCurrency(e.target, "", 'blur')} onKeyUp={(e) => FormatCurrency(e.target as HTMLInputElement, "", "")} />
                    </div>
                </div>
                <div className="w-full md:w-2/12 mb-2">
                    <label className="label block md:hidden" htmlFor="number">Credit <sup className="text-rose-500">*</sup></label>
                    <div className="relative mb-2">
                        <input type="text" id="credit" name='credit' className={"input text-right"} defaultValue="0.00" placeholder="Amount" onChange={calculateTotal} onBlur={(e) => FormatCurrency(e.target, "", 'blur')} onKeyUp={(e) => FormatCurrency(e.target as HTMLInputElement, "", "")} />
                    </div>
                </div>
                <div className="w-full md:w-1/12 mb-2">
                    <button onClick={() => { removeJournal(index); calculateTotal }} type="button" className="btn btn-danger w-12">
                        <XMarkIcon width={20} />
                    </button>
                </div>
            </div>
        </Fragment>
    )
    const addJournal = () => {
        let id = "JRNL" + Math.random()
        setJournals([...journals, { id: id, content: <Journal index={id} key={id} /> }])
    }
    const removeJournal = async (index: string) => {
        setJournals((journals) => journals.filter((_, i) => _.id != index));
        await new Promise(resolve => setTimeout(resolve, 200));
        calculateTotal()
        checkBalance()
    }
    let tdebit: number = 0.00, tcredit: number = 0.00
    return (
        <main>
            <div className="flex flex-col justify-between mb-4 space-y-4 md:flex-row md:space-y-0">
                <div>
                    <h1 className="text-xl font-semibold lg:text-2xl">Edit Transaction</h1>
                    <div className="text-sm text-slate-500">Edit Journal Transaction</div>
                </div>
                <div className="flex items-center space-x-2">
                    {(session?.user.group.roles.includes("61e3de6ecc4029a9d11a5017") || session?.user.group.roles.includes("61e3de6ecc4029a9d11a5018") || session?.user.group.roles.includes("61e3de6ecc4029a9d11a5019") || session?.user.group.roles.includes("61e3de6ecc4029a9d11a5020")) &&
                        <Menu as="div" className="relative inline-block text-left">
                            <div>
                                <Menu.Button className="btn-circle">
                                    <PlusIcon width={20} height={20} />
                                </Menu.Button>
                            </div>
                            <Transition
                                as={Fragment}
                                enter="transition ease-out duration-100"
                                enterFrom="transform opacity-0 scale-95"
                                enterTo="transform opacity-100 scale-100"
                                leave="transition ease-in duration-75"
                                leaveFrom="transform opacity-100 scale-100"
                                leaveTo="transform opacity-0 scale-95"
                            >
                                <Menu.Items className="absolute z-10 right-0 mt-2 w-40 origin-top-right divide-y divide-gray-100 rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
                                    <div className="px-1 py-1 ">
                                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5017") &&
                                            <Menu.Item>
                                                {({ active }) => (
                                                    <Link href={'/accounting/transaction/journal'} as={'/accounting/transaction/journal'} className={`${active ? 'bg-sky-500 text-white' : 'text-gray-900'} group flex w-full space-x-2 items-center rounded-md px-2 py-2 text-sm`}>
                                                        <ArrowsUpDownIcon width={18} /> <span>Journal</span>
                                                    </Link>
                                                )}
                                            </Menu.Item>
                                        }
                                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5018") &&
                                            <Menu.Item>
                                                {({ active }) => (
                                                    <Link href={'/accounting/transaction/incoming'} as={'/accounting/transaction/incoming'} className={`${active ? 'bg-sky-500 text-white' : 'text-gray-900'} group flex w-full space-x-2 items-center rounded-md px-2 py-2 text-sm`}>
                                                        <ArrowDownTrayIcon width={18} /> <span>Incoming</span>
                                                    </Link>
                                                )}
                                            </Menu.Item>
                                        }
                                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5019") &&
                                            <Menu.Item>
                                                {({ active }) => (
                                                    <Link href={'/accounting/transaction/outgoing'} as={'/accounting/transaction/outgoing'} className={`${active ? 'bg-sky-500 text-white' : 'text-gray-900'} group flex w-full space-x-2 items-center rounded-md px-2 py-2 text-sm`}>
                                                        <ArrowUpTrayIcon width={18} /> <span>Outgoing</span>
                                                    </Link>
                                                )}
                                            </Menu.Item>
                                        }
                                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5020") &&
                                            <Menu.Item>
                                                {({ active }) => (
                                                    <Link href={'/accounting/transaction/invoice'} as={'/accounting/transaction/invoice'} className={`${active ? 'bg-sky-500 text-white' : 'text-gray-900'} group flex w-full space-x-2 items-center rounded-md px-2 py-2 text-sm`}>
                                                        <ReceiptPercentIcon width={18} /> <span>Invoice</span>
                                                    </Link>
                                                )}
                                            </Menu.Item>
                                        }
                                    </div>

                                </Menu.Items>
                            </Transition>
                        </Menu>
                    }
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5015") &&
                        <Link href={path.join(pathName as string, "..")} title="Transaction Data" className="btn-circle" aria-label="transaction-data">
                            <ListBulletIcon width={20} height={20} />
                        </Link>
                    }
                </div>
            </div>

            <div className="relative bg-white shadow-lg w-full rounded-tl-3xl rounded-br-3xl p-6 dark:bg-slate-700">
                {spinner && <Spinner className=" rounded-tl-3xl rounded-br-3xl" />}
                <Warning className="" errors={Object.values(warning.errors).filter(a => a != "")} hide={() => setWarning({ errors: errors })} />
                <form onSubmit={submit}>
                    <div className="flex flex-col lg:flex-row space-x-0 space-y-2 lg:space-y-0 lg:space-x-4 mb-4 md:mb-2">
                        <div className="lg:w-1/4 w-full mb-2">
                            <label className="label" htmlFor="coa">TRX Type <sup className="text-rose-500">*</sup></label>
                            <div className="relative inline-block w-full text-gray-700">
                                <select name="type" id="type" className="select" defaultValue={data.type} disabled>
                                    {TransactionTypes.map(type => <option key={type.code} value={type.code}>{type.code} - {type.name}</option>)}
                                </select>
                            </div>
                        </div>

                        <div className="lg:w-1/4 w-full mb-2">
                            <label className="label" htmlFor="number">Reference</label>
                            <div className="relative mb-2">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <HashtagIcon className="text-gray-400 w-5 h-5" />
                                </div>
                                <input type="text" id="reference" name='reference' defaultValue={data.reference} className={warning?.errors.reference ? "input-icon-error" : "input-icon"} placeholder="Reference" />
                            </div>
                        </div>
                    </div>
                    <div className="mb-4">
                        <label htmlFor="information" className="label">Information <sup className="text-rose-500">*</sup></label>
                        <textarea name="information" id="information" placeholder="TRX Information" defaultValue={data.information} className={warning?.errors.information ? "input-error" : "input"} rows={3}></textarea>
                    </div>

                    <h3 className='font-semibold'>Journals</h3>
                    <div className="my-2 border border-gray-200 dark:border-slate-600"></div>

                    {data.journals.map((journal: any, i: number) => {
                        tdebit = tdebit + Number(journal.debit.$numberDecimal)
                        tcredit = tcredit + Number(journal.credit.$numberDecimal)
                        return <div key={"journal-" + journal.id} className="flex flex-col md:flex-row md:space-x-4 space-x-0 space-y-4 md:space-y-0">
                            <div className="lg:w-1/4 w-full mb-2">
                                <label className="label block md:hidden" htmlFor="number">Account No <sup className="text-rose-500">*</sup></label>
                                <div className="relative mb-2">
                                    <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                        <HashtagIcon className="text-gray-400 w-5 h-5" />
                                    </div>
                                    <input type="text" id="account" name='account' defaultValue={journal.account.number} className={"input-icon"} placeholder="Account Number" onChange={(e) => getAccount(e)} />
                                </div>
                            </div>
                            <div className="lg:w-1/2 w-full mb-2">
                                <label className="label block md:hidden" htmlFor="number">Account Name <sup className="text-rose-500">*</sup></label>
                                <div className="relative mb-2">
                                    <input type="text" id="name" name='name' className={"input"} defaultValue={journal.account.name} placeholder="Account Name" readOnly />
                                </div>
                            </div>
                            <div className="w-full md:w-2/12 mb-2">
                                <label className="label block md:hidden" htmlFor="number">Debit <sup className="text-rose-500">*</sup></label>
                                <div className="relative mb-2">
                                    <input type="text" id="debit" name='debit' className={"input text-right"} defaultValue={FormatMoney(journal.debit.$numberDecimal)} placeholder="Amount" onChange={calculateTotal} onBlur={(e) => FormatCurrency(e.target, "", 'blur')} onKeyUp={(e) => FormatCurrency(e.target as HTMLInputElement, "", "")} />
                                </div>
                            </div>
                            <div className="w-full md:w-2/12 mb-2">
                                <label className="label block md:hidden" htmlFor="number">Credit <sup className="text-rose-500">*</sup></label>
                                <div className="relative mb-2">
                                    <input type="text" id="credit" name='credit' className={"input text-right"} defaultValue={FormatMoney(journal.credit.$numberDecimal)} placeholder="Amount" onChange={calculateTotal} onBlur={(e) => FormatCurrency(e.target, "", 'blur')} onKeyUp={(e) => FormatCurrency(e.target as HTMLInputElement, "", "")} />
                                </div>
                            </div>
                            <div className="w-full md:w-1/12 mb-2">
                                {i == 1 &&
                                    <button type="button" onClick={() => { addJournal() }} className="btn btn-primary w-12 h-8">
                                        <PlusIcon width={30} />
                                    </button>
                                }
                            </div>
                        </div>
                    }
                    )}


                    <div>
                        {journals.map(journal => journal.content)}
                    </div>
                    <div className="my-2 border border-gray-200 dark:border-slate-600"></div>
                    <div className="flex flex-col md:flex-row md:space-x-4 space-x-0 space-y-4 md:space-y-0">
                        <div className="w-full md:w-1/2 mb-2">
                            <button type="submit" className="btn btn-primary flex items-center space-x-2">
                                <ArrowPathIcon width={20} height={20} className="mr-2" />
                                Update Transaction
                            </button>
                        </div>
                        <div className="w-full md:w-1/4 mb-2 md:text-right">
                            <label htmlFor="debit" className="block font-semibold text-gray-800 dark:text-slate-300 px-1 w-full lg:mb-0 text-base md:py-2">Total</label>
                        </div>
                        <div className="w-full md:w-2/12 mb-2">
                            <label className="label block md:hidden" htmlFor="number">Debit</label>
                            <div className="relative mb-2">
                                <input type="text" id="tDebit" name='tDebit' className={"input text-right"} defaultValue={FormatMoney(tdebit)} placeholder="T Debit" readOnly />
                            </div>
                        </div>
                        <div className="w-full md:w-2/12 mb-2">
                            <label className="label block md:hidden" htmlFor="number">Credit</label>
                            <div className="relative mb-2">
                                <input type="text" id="tCredit" name='tCredit' className={"input text-right"} defaultValue={FormatMoney(tcredit)} placeholder="T Credit" readOnly />
                            </div>
                        </div>
                        <div className="w-full md:w-1/12 mb-2 py-4">
                            {!balance && <XMarkIcon width={20} className="text-red-600" />}
                            {balance && <CheckIcon width={20} className="text-green-600" />}
                        </div>

                    </div>

                </form>
            </div>
        </main>
    )
}