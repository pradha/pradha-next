import DashboardLayout from "@/components/ui/layouts/dashboard"
import clientPromise from "@/lib/connections/mongo";
import { ObjectId } from "mongodb";
import { getServerSession } from "next-auth";
import { notFound, redirect } from "next/navigation";

export const revalidate = 0
export default async function RootLayout(props: any) {
    const session = await getServerSession();
    if (!session)
        redirect('/');

    const db = (await clientPromise).db()
    const data = JSON.parse(JSON.stringify(await db.collection("v_accounting_transactions").findOne({ _id: new ObjectId(props.params.id), status: "Pending" })))
    const debit = await db.collection("v_accounting_accounts").find({ status: "Active", 'coa.parent.parent': 1 }).toArray()
    const credit = await db.collection("v_accounting_accounts").find({ status: "Active", 'coa.parent.parent': 4 }).toArray()


    if (!data)
        notFound()
    props.params.data = data
    props.params.DebitAcc = JSON.parse(JSON.stringify(debit))
    props.params.CreditAcc = JSON.parse(JSON.stringify(credit))
    return (
        <>{props.children}</>
    )
}

const title = 'Edit Invoice Transaction'
const description = 'Edit Invoice Transaction'
const keywords = ["accounting", "edit transaction", "accounting transaction", "edit"]
const url = process.env.SITE_URL + '/accounting/transaction/invoice/edit'
export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: false,
        follow: false,
        nocache: false,
        googleBot: {
            index: false,
            follow: false
        },
    },
}