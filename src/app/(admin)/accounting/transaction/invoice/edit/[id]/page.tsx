'use client'
import { useSession } from "next-auth/react"
import { redirect, useRouter } from 'next/navigation';
import { ChangeEvent, FormEvent, Fragment, useEffect, useState } from 'react'
import Link from "next/link";
import { PlusIcon, PencilIcon, MagnifyingGlassIcon, XMarkIcon, ChevronDoubleRightIcon, XCircleIcon, AdjustmentsHorizontalIcon, IdentificationIcon, CheckIcon, NoSymbolIcon, ArrowPathIcon, ChevronDownIcon, ArrowDownTrayIcon, ArrowUpTrayIcon, ArrowsUpDownIcon, ReceiptPercentIcon, ListBulletIcon, HashtagIcon } from "@heroicons/react/24/outline";
import { Spinner } from "@/components/ui/loader";
import { FullShort, ISODate } from "@/lib/time";
import { usePathname } from 'next/navigation';
import { Pagination } from "@/components/ui/pagination";
import path from "path";
import { FormatCurrency, FormatMoney, FormatNumber } from "@/lib/text";
import { Menu, Transition } from "@headlessui/react";
import { TransactionTypes } from "@/lib/accounting/transactionTypes";
import { Warning } from "@/components/ui/alert";

export default function Page(props: any) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })
    const pathName = usePathname()
    const router = useRouter()
    let fill: any
    let errors = {
        reference: "",
        information: "",
        name: "",
        description: ""
    }
    const [data, setData] = useState(props.params.data)
    const [spinner, setSpinner] = useState(false)
    const [success, setSuccess] = useState({ title: "", description: "" })
    const [warning, setWarning] = useState({ errors: errors })
    const [journals, setJournals] = useState<any[]>([])
    const [balance, setBalance] = useState(true)

    const getAccount = async (e: ChangeEvent) => {
        let acc = e.target as HTMLInputElement;
        let name = acc.parentNode?.parentNode?.nextSibling as HTMLElement;
        let amount = acc.parentNode?.parentNode?.nextSibling as HTMLElement;
        let qty = acc.parentNode?.parentNode?.nextSibling?.nextSibling as HTMLElement;
        let subTotal = acc.parentNode?.parentNode?.nextSibling?.nextSibling?.nextSibling as HTMLElement;
        if (acc.value.length > 5 && parseInt(acc.value)) {
            const req = await fetch('/api/accounting/account?number=' + acc.value, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
            })
            const res = await req.json()
            if (req.ok) {
                //name.getElementsByTagName('input')[0].value = res.data?.default_amount.$numberDecimal
                acc.classList.remove('input-icon-error')
                acc.classList.add('input-icon')
                amount.getElementsByTagName('input')[0].value = res.data?.default_amount ? FormatMoney(res.data?.default_amount.$numberDecimal) : "0.00"
                qty.getElementsByTagName('input')[0].value = "1"
            } else {
                //name.getElementsByTagName('input')[0].value = "INVALID ACCOUNT NUMBER"
                acc.classList.add('input-icon-error')
                acc.classList.remove('input-icon')
            }
            amount.getElementsByTagName('input')[0].dispatchEvent(new Event('change', { bubbles: true }));
            subTotal.getElementsByTagName('input')[0].value = parseFloat((Number(amount.getElementsByTagName('input')[0].value) * Number(qty.getElementsByTagName('input')[0].value)).toString()).toFixed(2)
            calculateTotal()

        }
    }

    const submit = async (e: FormEvent) => {
        e.preventDefault()
        setWarning({ errors: errors })
        setSpinner(true)
        setSuccess({ title: "", description: "" })
        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        let target = e.target as HTMLFormElement
        let sum = 0;
        for (var i = 0; i <= document.querySelectorAll('select[name="account"]').length - 1; i++) {
            sum = sum + Number((document.querySelectorAll('input[name="credit"]')[i] as HTMLInputElement).value.replace(/\,/g, ''))
        }
        var journals = []
        if (parseInt((document.getElementById('debit') as HTMLSelectElement).value))
            journals.push({
                number: parseInt((document.getElementById('debit') as HTMLSelectElement).value),
                debit: parseFloat(sum.toString()).toFixed(2),
                credit: 0.00,
            })
        for (var i = 0; i <= document.querySelectorAll('select[name="account"]').length - 1; i++) {
            if (Number((document.querySelectorAll('select[name="account"]')[i] as HTMLInputElement).value))
                journals.push({
                    number: Number((document.querySelectorAll('select[name="account"]')[i] as HTMLInputElement).value),
                    price: parseFloat((document.querySelectorAll('input[name="price"]')[i] as HTMLInputElement).value.replace(/\,/g, '')).toFixed(2),
                    quantity: Number((document.querySelectorAll('input[name="quantity"]')[i] as HTMLInputElement).value.replace(",", ".")),
                    debit: 0.00,
                    credit: parseFloat((document.querySelectorAll('input[name="credit"]')[i] as HTMLInputElement).value.replace(/\,/g, '')).toFixed(2)
                })
        }
        const req = await fetch('/api/accounting/transaction', {
            body: JSON.stringify({
                id:data._id,
                type: data.type,
                reference: target.reference.value,
                information: target.information.value,
                journals: journals,
            }),
            headers: {
                'Content-Type': 'application/json',
            },
            method: 'PUT'
        })
        const result = await req.json()

        if (req.ok) {
            router.push(path.join(pathName as string, "../../..", "detail", data._id + "?status=created"))
        } else {
            errors = await result.errors
            setWarning({ errors: errors })
            errors = {
                reference: "",
                information: "",
                name: "",
                description: ""
            }
        }
        setSpinner(false)
    }

    const changeQty = async (e: ChangeEvent) => {
        let qty = (e.target as HTMLInputElement).value
        qty = qty.replace(",", ".")
        let price = ((e.target as HTMLInputElement).parentNode?.parentNode?.previousSibling as HTMLElement).getElementsByTagName('input')[0].value;
        let total = ((e.target as HTMLInputElement).parentNode?.parentNode?.nextSibling as HTMLElement).getElementsByTagName('input')[0];
        total.value = FormatMoney((parseFloat(price.replace(/[,]/g, '')) * parseFloat(qty.replace(",", ".").replace(/[,]/g, ''))).toString())
        calculateTotal()
    }
    const changePrice = async (e: ChangeEvent) => {
        const price = (e.target as HTMLInputElement).value
        let qty = ((e.target as HTMLInputElement).parentNode?.parentNode?.nextSibling as HTMLElement).getElementsByTagName('input')[0].value;
        let total = ((e.target as HTMLInputElement).parentNode?.parentNode?.nextSibling?.nextSibling as HTMLElement).getElementsByTagName('input')[0];
        total.value = FormatMoney((parseFloat(price.replace(/[,]/g, '')) * parseFloat(qty.replace(",", ".").replace(/[,]/g, ''))).toString())
        calculateTotal()
    }
    const calculateTotal = () => {
        let credit = document.getElementsByName("credit")
        let tcredit = 0;

        for (var cr of credit) {
            tcredit = tcredit + parseFloat((cr as HTMLInputElement).value.replace(/\,/g, ''))
        }
        (document.getElementById('tCredit') as HTMLInputElement).value = FormatMoney(tcredit);

    }
    let debit = data.journals.filter((journal: any) => journal.type == "Db").map((journal: any) => journal.account.number)[0];

    const Journal = ({ index, account, price, qty }: { index: string, account: number, price: number, qty: number }) => (
        <Fragment>
            <div className="block md:hidden border border-gray-200 mb-2"></div>
            <div className="flex flex-col md:flex-row md:space-x-4 space-x-0 space-y-4 md:space-y-0">
                <div className="lg:w-1/2 w-full mb-2">
                    <label className="label block md:hidden" htmlFor="number">Product <sup className="text-rose-500">*</sup></label>
                    <div className="relative inline-block w-full text-gray-700">
                        <select name="account" id="account" className="select" onChange={(e) => getAccount(e)} defaultValue={account}>
                            <option value="">- Credit (Cr) Account -</option>
                            {props.params?.CreditAcc.map((acc: any) => <option key={acc._id} value={acc.number}>{acc.number} - {acc.name}</option>)}
                        </select>
                    </div>
                </div>
                <div className="lg:w-1/6 w-full mb-2">
                    <label className="label block md:hidden" htmlFor="price">Price <sup className="text-rose-500">*</sup></label>
                    <div className="relative mb-2">
                        <input type="text" id="price" name='price' className={"input text-right"} placeholder="Price" defaultValue={FormatMoney(price)} onChange={(e) => changePrice(e)} onBlur={(e) => FormatCurrency(e.target, "", 'blur')} onKeyUp={(e) => FormatCurrency(e.target as HTMLInputElement, "", "")} />
                    </div>
                </div>
                <div className="lg:w-1/12 w-full mb-2">
                    <label className="label block md:hidden" htmlFor="quantity">Qty <sup className="text-rose-500">*</sup></label>
                    <div className="relative mb-2">
                        <input type="number" id="quantity" name='quantity' step={0.5} className={"input text-center"} placeholder="Qty" defaultValue={qty} onChange={(e) => changeQty(e)} />
                    </div>
                </div>
                <div className="w-full md:w-1/6 mb-2">
                    <label className="label block md:hidden" htmlFor="number">Sub Total <sup className="text-rose-500">*</sup></label>
                    <div className="relative mb-2">
                        <input type="text" id="credit" name='credit' className={"input text-right"} placeholder="Amount" defaultValue={FormatMoney(price * qty)} onChange={calculateTotal} onBlur={(e) => FormatCurrency(e.target, "", 'blur')} onKeyUp={(e) => FormatCurrency(e.target as HTMLInputElement, "", "")} />
                    </div>
                </div>
                <div className="w-full md:w-1/12 mb-2">
                    <button onClick={() => { removeJournal(index); calculateTotal }} type="button" className="btn btn-danger w-12">
                        <XMarkIcon width={20} />
                    </button>
                </div>
            </div>
        </Fragment>
    )
    const addJournal = (account: number, price: number, qty: number) => {
        let id = "JRNL" + Math.random()
        setJournals([...journals, { id: id, content: <Journal index={id} key={id} account={account} price={price} qty={qty} /> }])
    }
    const removeJournal = async (index: string) => {
        setJournals((journals) => journals.filter((_, i) => _.id != index));
        await new Promise(resolve => setTimeout(resolve, 200));
        calculateTotal()
    }
    useEffect(() => {
        data.journals.filter((journal: any) => journal.type == "Cr").map((journal: any, i: number) => { if (i > 0) addJournal(journal.account.number, journal.price.$numberDecimal, journal.quantity) })
        calculateTotal()
    }, [])
    return (
        <main>
            <div className="flex flex-col justify-between mb-4 space-y-4 md:flex-row md:space-y-0">
                <div>
                    <h1 className="text-xl font-semibold lg:text-2xl">Edit Invoice {data.code}</h1>
                    <div className="text-sm text-slate-500">Edit Invoice Transaction {data.code}</div>
                </div>
                <div className="flex items-center space-x-2">
                    {(session?.user.group.roles.includes("61e3de6ecc4029a9d11a5017") || session?.user.group.roles.includes("61e3de6ecc4029a9d11a5019") || session?.user.group.roles.includes("61e3de6ecc4029a9d11a5020")) &&
                        <Menu as="div" className="relative inline-block text-left">
                            <div>
                                <Menu.Button className="btn-circle">
                                    <PlusIcon width={20} height={20} />
                                </Menu.Button>
                            </div>
                            <Transition
                                as={Fragment}
                                enter="transition ease-out duration-100"
                                enterFrom="transform opacity-0 scale-95"
                                enterTo="transform opacity-100 scale-100"
                                leave="transition ease-in duration-75"
                                leaveFrom="transform opacity-100 scale-100"
                                leaveTo="transform opacity-0 scale-95"
                            >
                                <Menu.Items className="absolute z-10 lg:right-0 mt-2 w-40 origin-top-right divide-y divide-gray-100 rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
                                    <div className="px-1 py-1 ">
                                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5017") &&
                                            <Menu.Item>
                                                {({ active }) => (
                                                    <Link href={'/accounting/transaction/journal'} as={'/accounting/transaction/journal'} className={`${active ? 'bg-sky-500 text-white' : 'text-gray-900'} group flex w-full space-x-2 items-center rounded-md px-2 py-2 text-sm`}>
                                                        <ArrowsUpDownIcon width={18} /> <span>Journal</span>
                                                    </Link>
                                                )}
                                            </Menu.Item>
                                        }
                                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5019") &&
                                            <Menu.Item>
                                                {({ active }) => (
                                                    <Link href={'/accounting/transaction/outgoing'} as={'/accounting/transaction/outgoing'} className={`${active ? 'bg-sky-500 text-white' : 'text-gray-900'} group flex w-full space-x-2 items-center rounded-md px-2 py-2 text-sm`}>
                                                        <ArrowUpTrayIcon width={18} /> <span>Outgoing</span>
                                                    </Link>
                                                )}
                                            </Menu.Item>
                                        }
                                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5020") &&
                                            <Menu.Item>
                                                {({ active }) => (
                                                    <Link href={'/accounting/transaction/invoice'} as={'/accounting/transaction/invoice'} className={`${active ? 'bg-sky-500 text-white' : 'text-gray-900'} group flex w-full space-x-2 items-center rounded-md px-2 py-2 text-sm`}>
                                                        <ReceiptPercentIcon width={18} /> <span>Invoice</span>
                                                    </Link>
                                                )}
                                            </Menu.Item>
                                        }
                                    </div>

                                </Menu.Items>
                            </Transition>
                        </Menu>
                    }
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5015") &&
                        <Link href={path.join(pathName as string, "../../..")} title="Transaction Data" className="btn-circle" aria-label="transaction-data">
                            <ListBulletIcon width={20} height={20} />
                        </Link>
                    }
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5015") &&
                        <Link href={path.join(pathName as string, "../../../detail/", data._id)} title="Transaction Data" className="btn-circle" aria-label="transaction-data">
                            <IdentificationIcon width={20} height={20} />
                        </Link>
                    }
                </div>
            </div>

            <div className="relative bg-white shadow-lg w-full rounded-tl-3xl rounded-br-3xl p-6 dark:bg-slate-700">
                {spinner && <Spinner className=" rounded-tl-3xl rounded-br-3xl" />}
                <Warning className="" errors={Object.values(warning.errors).filter(a => a != "")} hide={() => setWarning({ errors: errors })} />
                <form onSubmit={submit}>
                    <div className="flex flex-col lg:flex-row space-x-0 space-y-2 lg:space-y-0 lg:space-x-4 mb-4 md:mb-2">
                        <div className="lg:w-1/4 w-full mb-2">
                            <label className="label" htmlFor="coa">TRX Type <sup className="text-rose-500">*</sup></label>
                            <div className="relative inline-block w-full text-gray-700">
                                <select name="type" id="type" className="select" defaultValue={"IV"} disabled>
                                    {TransactionTypes.map(type => <option key={type.code} value={type.code}>{type.code} - {type.name}</option>)}
                                </select>
                            </div>
                        </div>
                        <div className="lg:w-1/2 w-full mb-2">
                            <label className="label" htmlFor="coa">Debit (Db) Account<sup className="text-rose-500">*</sup></label>
                            <div className="relative inline-block w-full text-gray-700">
                                <select name="debit" id="debit" className="select" multiple={false} defaultValue={debit}>
                                    <option value="">- Debit (Db) Account -</option>
                                    {props.params?.DebitAcc.map((acc: any) => <option key={acc._id} value={acc.number}>{acc.number} - {acc.name}</option>)}
                                </select>
                            </div>
                        </div>

                        <div className="lg:w-1/4 w-full mb-2">
                            <label className="label" htmlFor="number">Reference</label>
                            <div className="relative mb-2">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <HashtagIcon className="text-gray-400 w-5 h-5" />
                                </div>
                                <input type="text" id="reference" name='reference' defaultValue={data.refeence} className={warning?.errors.reference ? "input-icon-error" : "input-icon"} placeholder="Reference" />
                            </div>
                        </div>
                    </div>
                    <div className="mb-4">
                        <label htmlFor="information" className="label">Information <sup className="text-rose-500">*</sup></label>
                        <textarea name="information" id="information" defaultValue={data.information} placeholder="TRX Information" className={warning?.errors.information ? "input-error" : "input"} rows={3}></textarea>
                    </div>

                    <h3 className='font-semibold'>Credit Account</h3>
                    <div className="my-2 border border-gray-200 dark:border-slate-600"></div>
                    {data.journals.filter((journal: any) => journal.type == "Cr").map((journal: any, i: number) => {
                        if (i == 0)
                            return <div key={Math.random()} className="flex flex-col md:flex-row md:space-x-4 space-x-0 space-y-4 md:space-y-0">
                                <div className="lg:w-1/2 w-full mb-2">
                                    <label className="label block md:hidden" htmlFor="number">Product <sup className="text-rose-500">*</sup></label>
                                    <div className="relative inline-block w-full text-gray-700">
                                        <select name="account" id="account" className="select" onChange={(e) => getAccount(e)} defaultValue={journal.account.number}>
                                            <option value="">- Credit (Cr) Account -</option>
                                            {props.params?.CreditAcc.map((acc: any) => <option key={acc._id} value={acc.number}>{acc.number} - {acc.name}</option>)}
                                        </select>
                                    </div>
                                </div>
                                <div className="lg:w-1/6 w-full mb-2">
                                    <label className="label block md:hidden" htmlFor="price">Price <sup className="text-rose-500">*</sup></label>
                                    <div className="relative mb-2">
                                        <input type="text" id="price" name='price' className={"input text-right"} placeholder="Price" defaultValue={FormatMoney(journal.price.$numberDecimal)} onChange={(e) => changePrice(e)} onBlur={(e) => FormatCurrency(e.target, "", 'blur')} onKeyUp={(e) => FormatCurrency(e.target as HTMLInputElement, "", "")} />
                                    </div>
                                </div>
                                <div className="lg:w-1/12 w-full mb-2">
                                    <label className="label block md:hidden" htmlFor="quantity">Qty <sup className="text-rose-500">*</sup></label>
                                    <div className="relative mb-2">
                                        <input type="number" id="quantity" name='quantity' className={"input text-center"} step={0.5} placeholder="Qty" defaultValue={journal.quantity} onChange={(e) => changeQty(e)} />
                                    </div>
                                </div>
                                <div className="w-full md:w-1/6 mb-2">
                                    <label className="label block md:hidden" htmlFor="number">Sub Total <sup className="text-rose-500">*</sup></label>
                                    <div className="relative mb-2">
                                        <input type="text" id="credit" name='credit' className={"input text-right"} defaultValue={FormatMoney(journal.credit.$numberDecimal)} placeholder="Amount" onChange={calculateTotal} onBlur={(e) => FormatCurrency(e.target, "", 'blur')} onKeyUp={(e) => FormatCurrency(e.target as HTMLInputElement, "", "")} />
                                    </div>
                                </div>
                                <div className="w-full md:w-1/12 mb-2">
                                    <button type="button" onClick={() => { addJournal(0, 0.00, 1) }} className="btn btn-primary w-12 h-8">
                                        <PlusIcon width={30} />
                                    </button>
                                </div>
                            </div>
                    })}

                    <div>
                        {journals.map(journal => journal.content)}
                    </div>
                    <div className="my-2 border border-gray-20 dark:border-slate-600"></div>
                    <div className="flex flex-col md:flex-row md:space-x-4 space-x-0 space-y-4 md:space-y-0">
                        <div className="w-full md:w-1/2 mb-2">
                            <button type="submit" className="btn btn-primary flex items-center space-x-2">
                                <ArrowPathIcon width={20} height={20} className="mr-2" />
                                Update Transaction
                            </button>
                        </div>
                        <div className="w-full md:w-1/4 mb-2 md:text-right">
                            <label htmlFor="debit" className="block font-semibold text-gray-800 dark:text-slate-300 px-1 w-full lg:mb-0 text-base md:py-2">Total</label>
                        </div>
                        <div className="w-full md:w-1/6 mb-2">
                            <label className="label block md:hidden" htmlFor="number">Credit</label>
                            <div className="relative mb-2">
                                <input type="text" id="tCredit" name='tCredit' className={"input text-right"} defaultValue="0.00" placeholder="T Credit" readOnly />
                            </div>
                        </div>
                        <div className="w-full md:w-1/12 mb-2 py-4">
                            {!balance && <XMarkIcon width={20} className="text-red-600" />}
                            {balance && <CheckIcon width={20} className="text-green-600" />}
                        </div>

                    </div>

                </form>
            </div>
        </main>
    )
}