import DashboardLayout from "@/components/ui/layouts/dashboard"
import clientPromise from "@/lib/connections/mongo"

export const revalidate = 0
export const dynamic = 'force-dynamic'
export default async function RootLayout(props: any) {
    const db = (await clientPromise).db()
    const debit = await db.collection("v_accounting_accounts").find({ status: "Active", 'coa.parent.parent': 1 }).toArray()
    const credit = await db.collection("v_accounting_accounts").find({ status: "Active", 'coa.parent.parent': 4 }).toArray()
    
    props.params.DebitAcc = JSON.parse(JSON.stringify(debit))
    props.params.CreditAcc = JSON.parse(JSON.stringify(credit))
    return (
        <>{props.children}</>
    )
}

const title = 'Incoming Transaction'
const description = 'Create new incoming transaction'
const keywords = ["accounting", "transaction", "trx", "incoming"]
const url = process.env.SITE_URL + '/accounting/transaction/incoming'
export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: false,
        follow: false,
        nocache: false,
        googleBot: {
            index: false,
            follow: false
        },
    },
}