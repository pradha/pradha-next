import DashboardLayout from "@/components/ui/layouts/dashboard"

export const revalidate = 0
export const dynamic = 'force-dynamic'
export default async function RootLayout(props: any) {
    return (
        <>{props.children}</>
    )
}

const title = 'Journal Transaction'
const description = 'Create new journal transaction'
const keywords = ["accounting", "transaction", "trx"]
const url = process.env.SITE_URL + '/accounting/transaction/journal'
export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: false,
        follow: false,
        nocache: false,
        googleBot: {
            index: false,
            follow: false
        },
    },
}