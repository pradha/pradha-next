'use client'
import { useSession } from "next-auth/react"
import { redirect, usePathname } from 'next/navigation';
import { FormEvent, Fragment, useState } from 'react'
import Link from "next/link";
import { UserIcon, KeyIcon, ArrowPathIcon, ListBulletIcon, PlusIcon, IdentificationIcon, TagIcon, LinkIcon, HashtagIcon, CalculatorIcon, BanknotesIcon, PencilIcon, CheckBadgeIcon, CheckIcon, XMarkIcon } from "@heroicons/react/24/outline";
import { Success, Warning } from "@/components/ui/alert";
import { Spinner } from "@/components/ui/loader";
import path from "path";
import { useRouter } from "next/navigation";
import { MasterCoA } from "@/lib/accounting/masterCoA";
import { FormatCurrency, FormatMoney, FormatNumber } from "@/lib/text";
import { Dialog, Transition } from "@headlessui/react";
import { DateTimeShort } from "@/lib/time";

export const revalidate = 0;
export const dynamic = 'force-dynamic';
export default function Page(props: any) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })

    const pathName = usePathname()
    const route = useRouter()

    let errors = {
        coa: "",
        number: "",
        name: "",
        description: ""
    }
    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState({ errors: errors })
    const [coa, setCoa] = useState(props.params.coa)
    const [success, setSuccess] = useState({ title: "", description: "" })
    const [data, setData] = useState(props.params.data)
    const [confirm, setConfirm] = useState(false)
    const [close, setClose] = useState(false)

    const activate = async (e: FormEvent) => {
        e.preventDefault()
        setWarning({ errors: errors })
        setSpinner(true)
        setConfirm(false)
        setSuccess({ title: "", description: "" })
        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement
        const submit = await fetch(path.posix.join("/api/", pathName as string, '../..'), {
            method: 'PATCH',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                id: data._id,
            })
        });

        const res = await submit.json()
        if (submit.ok) {
            route.refresh()
            let newData = data
            data.confirmed.at = new Date()
            data.confirmed.by = { _id: session?.user.id, name: session?.user.name }
            data.status = 'Active'
            setData(newData)
            setSuccess({ title: "Success", description: "Account has been Confirmed & Activated for transaction" })
        } else {
            errors = await res.errors
            setWarning({ errors: errors })
            errors = {
                coa: "",
                number: "",
                name: "",
                description: "",
            }
        }
        setSpinner(false)
    }

    const closeAccount = async (e: FormEvent) => {
        e.preventDefault()
        setWarning({ errors: errors })
        setSpinner(true)
        setClose(false)
        setSuccess({ title: "", description: "" })
        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement
        const submit = await fetch(path.posix.join("/api/", pathName as string, '../..'), {
            method: 'DELETE',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                id: data._id,
            })
        });

        const res = await submit.json()
        if (submit.ok) {
            route.refresh()
            let newData = data
            data.closed.at = new Date()
            data.closed.by = { _id: session?.user.id, name: session?.user.name }
            data.status = 'Closed'
            setData(newData)
            setSuccess({ title: "Success", description: "Transaction has been aborted" })
        } else {
            errors = await res.errors
            setWarning({ errors: errors })
            errors = {
                coa: "",
                number: "",
                name: "",
                description: "",
            }
        }
        setSpinner(false)
    }

    return (
        <main>
            <div className="flex flex-col md:flex-row md:space-y-0 space-y-4 mb-4 justify-between">
                <div>
                    <h1 className="text-xl lg:text-2xl font-semibold">Transaction Detail</h1>
                    <div className="text-sm text-slate-500">Accounting Transaction Detail - {data.number}</div>
                </div>
                <div className="flex items-center space-x-2">
                    {data.status != "Active" && data.status != "Closed" && session?.user.group.roles.includes("61e3de6ecc4029a9d11a5010") &&
                        <Fragment>
                            <button type="button" title="Confirm Account" className="btn-circle-green" onClick={() => setConfirm(true)} aria-label="confirm-account">
                                <CheckIcon width={20} height={20} />
                            </button>

                            <Transition appear show={confirm} as={Fragment}>
                                <Dialog as="div" className="relative z-10" onClose={() => setConfirm(false)}>
                                    <Transition.Child
                                        as={Fragment}
                                        enter="ease-out duration-300"
                                        enterFrom="opacity-0"
                                        enterTo="opacity-100"
                                        leave="ease-in duration-200"
                                        leaveFrom="opacity-100"
                                        leaveTo="opacity-0"
                                    >
                                        <div className="fixed inset-0 bg-white dark:bg-black dark:bg-opacity-75 blur-lg bg-opacity-75" />
                                    </Transition.Child>

                                    <div className="fixed inset-0 overflow-y-auto">
                                        <div className="flex min-h-full items-center justify-center p-4 text-center">
                                            <Transition.Child
                                                as={Fragment}
                                                enter="ease-out duration-300"
                                                enterFrom="opacity-0 scale-95"
                                                enterTo="opacity-100 scale-100"
                                                leave="ease-in duration-200"
                                                leaveFrom="opacity-100 scale-100"
                                                leaveTo="opacity-0 scale-95"
                                            >
                                                <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-2xl bg-white p-6 text-left align-middle shadow-xl transition-all">
                                                    <Dialog.Title
                                                        as="h3"
                                                        className="text-lg flex justify-between font-medium leading-6 text-gray-900"
                                                    >
                                                        Confirm & Activate Account
                                                        <XMarkIcon title="close" width={16} className="cursor-pointer  text-slate-400" onClick={() => setConfirm(false)} />
                                                    </Dialog.Title>
                                                    <div className="mt-2">
                                                        <p className="text-sm text-gray-500">
                                                            Are you sure you want to confirm and activate this account :
                                                        </p>
                                                        <blockquote className="italic m-4">
                                                            <div className="flex flex-row space-x-2">
                                                                <div className="w-20">Number</div>
                                                                <div>:</div>
                                                                <div><span className="font-semibold">{data.number}</span></div>
                                                            </div>
                                                            <div className="flex flex-row space-x-2">
                                                                <div className="w-20">Name</div>
                                                                <div>:</div>
                                                                <div><span className="font-semibold">{data.name}</span></div>
                                                            </div>
                                                            <div className="flex flex-row space-x-2">
                                                                <div className="w-20">Created</div>
                                                                <div>:</div>
                                                                <div><span className="font-semibold">{DateTimeShort(data.created.at)}</span><br />by <span className="text-sm font-semibold">{data.created.by.name}</span></div>
                                                            </div>
                                                        </blockquote>
                                                    </div>

                                                    <form onSubmit={activate} className="flex space-x-2 justify-end mt-4">
                                                        <button className="btn btn-primary flex items-center space-x-2">
                                                            <CheckIcon width={20} height={20} className="mr-2" />
                                                            Confirm
                                                        </button>
                                                        <button
                                                            type="button"
                                                            className="inline-flex justify-center rounded-md border border-transparent bg-blue-100 px-4 py-2 text-sm font-medium text-blue-900 hover:bg-blue-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-blue-500 focus-visible:ring-offset-2"
                                                            onClick={() => setConfirm(false)}
                                                        >
                                                            Close
                                                        </button>
                                                    </form>
                                                </Dialog.Panel>
                                            </Transition.Child>
                                        </div>
                                    </div>
                                </Dialog>
                            </Transition>
                        </Fragment>
                    }
                    {data.status == "Active" && session?.user.group.roles.includes("61e3de6ecc4029a9d11a5011") &&
                        <Fragment>
                            <button type="button" title="Close Account" className="btn-circle-red" onClick={() => setClose(true)} aria-label="close-account">
                                <XMarkIcon width={20} height={20} />
                            </button>

                            <Transition appear show={close} as={Fragment}>
                                <Dialog as="div" className="relative z-10" onClose={() => setClose(false)}>
                                    <Transition.Child
                                        as={Fragment}
                                        enter="ease-out duration-300"
                                        enterFrom="opacity-0"
                                        enterTo="opacity-100"
                                        leave="ease-in duration-200"
                                        leaveFrom="opacity-100"
                                        leaveTo="opacity-0"
                                    >
                                        <div className="fixed inset-0 bg-white dark:bg-black dark:bg-opacity-75 blur-lg bg-opacity-75" />
                                    </Transition.Child>

                                    <div className="fixed inset-0 overflow-y-auto">
                                        <div className="flex min-h-full items-center justify-center p-4 text-center">
                                            <Transition.Child
                                                as={Fragment}
                                                enter="ease-out duration-300"
                                                enterFrom="opacity-0 scale-95"
                                                enterTo="opacity-100 scale-100"
                                                leave="ease-in duration-200"
                                                leaveFrom="opacity-100 scale-100"
                                                leaveTo="opacity-0 scale-95"
                                            >
                                                <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-2xl bg-white p-6 text-left align-middle shadow-xl transition-all">
                                                    <Dialog.Title
                                                        as="h3"
                                                        className="text-lg flex justify-between font-medium leading-6 text-gray-900"
                                                    >
                                                        Close & Disable
                                                        <XMarkIcon title="close" width={16} className="cursor-pointer  text-slate-400" onClick={() => setConfirm(false)} />
                                                    </Dialog.Title>
                                                    <div className="mt-2">
                                                        <p className="text-sm text-gray-500">
                                                            Are you sure you want to close & disable this account :
                                                        </p>
                                                        <blockquote className="italic m-4">
                                                            <div className="flex flex-row space-x-2">
                                                                <div className="w-20">Number</div>
                                                                <div>:</div>
                                                                <div><span className="font-semibold">{data.number}</span></div>
                                                            </div>
                                                            <div className="flex flex-row space-x-2">
                                                                <div className="w-20">Name</div>
                                                                <div>:</div>
                                                                <div><span className="font-semibold">{data.name}</span></div>
                                                            </div>
                                                            <div className="flex flex-row space-x-2">
                                                                <div className="w-20">Created</div>
                                                                <div>:</div>
                                                                <div><span className="font-semibold">{DateTimeShort(data.created.at)}</span><br />by <span className="text-sm font-semibold">{data.created.by.name}</span></div>
                                                            </div>
                                                        </blockquote>
                                                    </div>

                                                    <form onSubmit={closeAccount} className="flex space-x-2 justify-end mt-4">
                                                        <button className="btn btn-primary flex items-center space-x-2">
                                                            <CheckIcon width={20} height={20} className="mr-2" />
                                                            Confirm
                                                        </button>
                                                        <button
                                                            type="button"
                                                            className="inline-flex justify-center rounded-md border border-transparent bg-blue-100 px-4 py-2 text-sm font-medium text-blue-900 hover:bg-blue-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-blue-500 focus-visible:ring-offset-2"
                                                            onClick={() => setClose(false)}
                                                        >
                                                            Close
                                                        </button>
                                                    </form>
                                                </Dialog.Panel>
                                            </Transition.Child>
                                        </div>
                                    </div>
                                </Dialog>
                            </Transition>
                        </Fragment>
                    }
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5006") &&
                        <Link href={path.join(pathName as string, "../..", "create")} title="Create Account" className="btn-circle" aria-label="account-create">
                            <PlusIcon width={20} height={20} />
                        </Link>
                    }
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5007") &&
                        <Link href={path.join(pathName as string, "../..", "edit", data._id)} title="Edit Account" className="btn-circle" aria-label="account-edit">
                            <PencilIcon width={20} height={20} />
                        </Link>
                    }
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5005") &&
                        <Link href={path.join(pathName as string, "../..")} title="Account data" className="btn-circle" aria-label="account-data">
                            <ListBulletIcon width={20} height={20} />
                        </Link>
                    }
                </div>
            </div>

            <div className="relative">
                {spinner && <Spinner className="rounded-tl-3xl rounded-br-3xl" />}
                <Success data={success} hide={() => setSuccess({ title: "", description: "" })} />
                <Warning className="" errors={Object.values(warning.errors).filter(a => a != "")} hide={() => setWarning({ errors: errors })} />
                <div className="flex flex-col md:flex-row md:space-x-4 space-x-0 md:space-y-0 space-y-4 relative z-0 w-full">
                    <div className="w-full md:w-2/3 bg-white shadow-lg rounded-tl-3xl rounded-br-3xl p-4">
                        <h2 className="font-semibold text-lg md:mb-4 mb-2 border-b py-1 border-gray-100">Account Info</h2>
                        <div className="flex flex-col md:flex-row md:space-x-4 space-x-0 md:space-y-0 space-y-4 relative justify-between">
                            <div className="px-2">
                                <div className="flex flex-col mb-2">
                                    <label className="text-xs md:text-sm">Account Number</label>
                                    <div className="font-semibold text-base">{data.number}</div>
                                </div>
                                <div className="flex flex-col mb-2">
                                    <label className="text-xs md:text-sm">Account Name</label>
                                    <div className="font-semibold text-base">{data.name}</div>
                                </div>
                                <div className="flex flex-col mb-2">
                                    <label className="text-xs md:text-sm">Balance</label>
                                    <div className="font-semibold text-base">{data.name}</div>
                                </div>
                                <div className="flex flex-col mb-2">
                                    <label className="text-xs md:text-sm">Balance</label>
                                    <div className="font-semibold text-base text-right">{data?.balance ? FormatMoney(data.balance.$numberDecimal) : "0.00"}</div>
                                </div>
                                <div className="flex flex-col mb-2">
                                    <label className="text-xs md:text-sm">Locked Balance</label>
                                    <div className="font-semibold text-base text-right">{data?.locked_balance ? FormatMoney(data.locked_balance.$numberDecimal) : "0.00"}</div>
                                </div>
                                <div className="flex flex-col mb-2">
                                    <label className="text-xs md:text-sm">Default Amount</label>
                                    <div className="font-semibold text-base text-right">{data?.default_amount ? FormatMoney(data.default_amount.$numberDecimal) : "0.00"}</div>
                                </div>
                            </div>
                            <div className="px-2">
                                <div className="flex flex-col mb-2">
                                    <label className="text-xs md:text-sm">Parent (CoA)</label>
                                    <div className="font-semibold text-base">{data.coa.parent.code} / {data.coa.parent.name}</div>
                                </div>
                                <div className="flex flex-col mb-2">
                                    <label className="text-xs md:text-sm">Cart of Account (CoA) Name</label>
                                    <div className="font-semibold text-base">{data.coa.code} / {data.coa.name}</div>
                                </div>
                                <div className="flex flex-col mb-2">
                                    <label className="text-xs md:text-sm">Position</label>
                                    <div className="font-semibold text-base">{data.coa.position}</div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div className="w-full md:w-1/3 bg-white shadow-lg rounded-tl-3xl rounded-br-3xl p-4">
                        <h2 className="font-semibold text-lg md:mb-4 mb-2 border-b py-1 border-gray-100">Creation & Status Info</h2>
                        <div className="px-2">
                            <div className="flex flex-col mb-4">
                                <label className="text-xs md:text-sm">Created</label>
                                <div className="text-xs">date <span className="font-semibold text-base">{DateTimeShort(data.created.at)}</span></div>
                                <div className="text-xs">by <span className="font-semibold text-base">{data.created.by.name}</span></div>
                            </div>
                            {data?.updated && data.updated?.by &&
                                <div className="flex flex-col mb-4">
                                    <label className="text-xs md:text-sm">Updated</label>
                                    <div className="text-xs">date <span className="font-semibold text-base">{DateTimeShort(data.updated.at)}</span></div>
                                    <div className="text-xs">by <span className="font-semibold text-base">{data.updated.by.name}</span></div>
                                </div>
                            }
                            {data?.confirmed && data.confirmed?.by &&
                                <div className="flex flex-col mb-4">
                                    <label className="text-xs md:text-sm">Confirmed</label>
                                    <div className="text-xs">date <span className="font-semibold text-base">{DateTimeShort(data.confirmed.at)}</span></div>
                                    <div className="text-xs">by <span className="font-semibold text-base">{data.confirmed.by.name}</span></div>
                                </div>
                            }
                            {data?.closed && data.closed?.by &&
                                <div className="flex flex-col mb-4">
                                    <label className="text-xs md:text-sm">Closed</label>
                                    <div className="text-xs">date <span className="font-semibold text-base">{DateTimeShort(data.closed.at)}</span></div>
                                    <div className="text-xs">by <span className="font-semibold text-base">{data.closed.by.name}</span></div>
                                </div>
                            }
                            <div className="flex flex-col mb-4">
                                <label className="text-xs md:text-sm">Status</label>
                                <div className="font-semibold text-base">
                                    {data.status == "Not Active" &&
                                        <>
                                            <svg xmlns="http://www.w3.org/2000/svg" className="inline-block h-6 w-6 text-yellow-800" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z" />
                                            </svg> {data.status}
                                        </>
                                    }
                                    {data.status == "Active" &&
                                        <>
                                            <svg xmlns="http://www.w3.org/2000/svg" className="inline-block h-6 w-6 text-green-600" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 13l4 4L19 7" />
                                            </svg> {data.status}
                                        </>
                                    }
                                    {data.status == "Closed" &&
                                        <>
                                            <svg xmlns="http://www.w3.org/2000/svg" className="inline-block h-6 w-6 text-red-600" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M6 18L18 6M6 6l12 12" />
                                            </svg> {data.status}
                                        </>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        </main >
    )
}