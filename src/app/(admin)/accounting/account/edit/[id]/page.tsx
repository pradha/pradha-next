'use client'
import { useSession } from "next-auth/react"
import { redirect, usePathname } from 'next/navigation';
import { FormEvent, Fragment, useState } from 'react'
import Link from "next/link";
import { UserIcon, KeyIcon, ArrowPathIcon, ListBulletIcon, PlusIcon, IdentificationIcon, TagIcon, LinkIcon, HashtagIcon, CalculatorIcon, BanknotesIcon } from "@heroicons/react/24/outline";
import { Success, Warning } from "@/components/ui/alert";
import { Spinner } from "@/components/ui/loader";
import path from "path";
import { useRouter } from "next/navigation";
import { MasterCoA } from "@/lib/accounting/masterCoA";
import { FormatCurrency } from "@/lib/text";

export const revalidate = 0;
export const dynamic = 'force-dynamic';
export default function Page(props: any) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })

    const pathName = usePathname()
    const route = useRouter()

    let errors = {
        coa: "",
        number: "",
        name: "",
        description: ""
    }
    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState({ errors: errors })
    const [coa, setCoa] = useState(props.params.coa)
    const [success, setSuccess] = useState({ title: "", description: "" })
    const [data, setData] = useState(props.params.data)

    const submit = async (e: FormEvent) => {
        e.preventDefault()
        setWarning({ errors: errors })
        setSpinner(true)
        setSuccess({ title: "", description: "" })
        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement
        const submit = await fetch(path.posix.join("/api/", pathName as string, '../..'), {
            method: 'PUT',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                id: data._id,
                coa: target.coa.value,
                number: target.number.value,
                name: (document.getElementById("name") as HTMLInputElement).value,
                description: target.description.value,
                default_amount: target.default_amount.value.replaceAll(",", "")
            })
        });

        const res = await submit.json()
        if (submit.ok) {
            route.refresh()
            setSuccess({ title: "Success", description: "Account has been updated" })
        } else {
            errors = await res.errors
            setWarning({ errors: errors })
            errors = {
                coa: "",
                number: "",
                name: "",
                description: "",
            }
        }
        setSpinner(false)
    }

    const changeCoa = async (e: FormEvent) => {
        const target = e.target as HTMLSelectElement
        const req = await fetch(path.join('/api', pathName as string, "..", "last"), {
            method: 'POST',
            body: JSON.stringify({ coa: target.value }),
            headers: {
                'Content-Type': 'application/json',
            },
        })
        const last = await req.json()
        let num = parseInt(last.data.number);
        (document.getElementById('number') as HTMLInputElement).value = num.toString()
        return
    }
    return (
        <main>
            <div className="flex flex-col md:flex-row md:space-y-0 space-y-4 mb-4 justify-between">
                <div>
                    <h1 className="text-xl lg:text-2xl font-semibold">Edit Account</h1>
                    <div className="text-sm text-slate-500">Edit Accounting Account</div>
                </div>
                <div className="flex items-center space-x-2">
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5006") &&
                        <Link href={path.join(pathName as string, "../..", "create")} title="Create Account" className="btn-circle" aria-label="account-create">
                            <PlusIcon width={20} height={20} />
                        </Link>
                    }
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5009") &&
                        <Link href={path.join(pathName as string, "../..", "detail", data._id)} title="Account Detail" className="btn-circle" aria-label="account-detail">
                            <IdentificationIcon width={20} height={20} />
                        </Link>
                    }
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5005") &&
                        <Link href={path.join(pathName as string, "../..")} title="Account data" className="btn-circle" aria-label="account-data">
                            <ListBulletIcon width={20} height={20} />
                        </Link>
                    }
                </div>
            </div>

            <div className="relative bg-white shadow-lg w-full rounded-tl-3xl rounded-br-3xl p-6 dark:bg-slate-700">
                {spinner && <Spinner className="rounded-tl-3xl rounded-br-3xl" />}
                <Success data={success} hide={() => setSuccess({ title: "", description: "" })} />
                <form onSubmit={submit} className="ng-untouched ng-pristine ng-valid">
                    <Warning className="" errors={Object.values(warning.errors).filter(a => a != "")} hide={() => setWarning({ errors: errors })} />
                    <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                        <div className="lg:w-1/4 w-full mb-2">
                            <label className="label" htmlFor="coa">Cart of Account <sup className="text-rose-500">*</sup></label>
                            <div className="relative inline-block w-full text-gray-700">
                                <select name="coa" id="coa" className={warning?.errors.coa ? "select-error" : "select"} disabled defaultValue={data?.coa} onChange={changeCoa}>
                                    {MasterCoA.map((master: any) =>
                                        <Fragment key={master.code}>
                                            <option value={master.code} disabled>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{master.code} - {master.name} ({master.position})</option>
                                            {coa?.filter((parent: any) => parent.parent == master.code).map((parent: any) =>
                                                <Fragment key={parent._id}>
                                                    <option value={parent._id} disabled>&nbsp;&nbsp;&nbsp;&nbsp;{parent.code} - {parent.name} ({parent.position})</option>
                                                    {coa?.filter((sub: any) => sub.parent == parent._id).map((child: any) => <option key={child._id} value={child._id}>{child.code} - {child.name}</option>)}
                                                </Fragment>
                                            )}
                                        </Fragment>
                                    )}
                                </select>
                            </div>
                        </div>
                        <div className="lg:w-1/4 w-full mb-2">
                            <label className="label" htmlFor="number">Account Number<sup className="text-rose-500">*</sup></label>
                            <div className="relative mb-2">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <CalculatorIcon className="text-gray-400 w-5 h-5" />
                                </div>
                                <input type="number" id="number" name='number' defaultValue={data.number} className={warning?.errors.number ? "input-icon-error" : "input-icon"} placeholder="Account Number" />
                            </div>
                        </div>
                        <div className="flex-1 mb-2">
                            <label className="label" htmlFor="name">Name <sup className="text-rose-500">*</sup></label>
                            <div className="relative mb-2">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <IdentificationIcon className="text-gray-400 w-5 h-5" />
                                </div>
                                <input type="name" id="name" name='name' className={warning?.errors.name ? "input-icon-error" : "input-icon"} defaultValue={data.name} placeholder="Account Name" />
                            </div>
                        </div>
                    </div>
                    <div className="mb-4">
                        <label htmlFor="description" className="label">Description <sup className="text-rose-500">*</sup></label>
                        <textarea name="description" id="description" placeholder="Account Description" className={warning?.errors.description ? "input-error" : "input"} defaultValue={data.description} rows={3}></textarea>
                    </div>
                    <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                        <div className="lg:w-1/4 w-full mb-2">
                            <label className="label" htmlFor="default_amount">Default Amount </label>
                            <div className="relative mb-2">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <BanknotesIcon className="text-gray-400 w-5 h-5" />
                                </div>
                                <input type="text" id="default_amount" name='default_amount' defaultValue={data?.default_amount ? data.default_amount.$numberDecimal : "0.00"} className={"input-icon text-right"} placeholder="Default Amount" onBlur={(e) => FormatCurrency(e.target, "", 'blur')} onKeyUp={(e) => FormatCurrency(e.target as HTMLInputElement, "", "")} />
                            </div>
                        </div>
                        <div className="flex-1 mb-2">

                        </div>
                    </div>
                    <button type="submit" className="btn btn-primary flex items-center space-x-2">
                        <ArrowPathIcon width={20} height={20} className="mr-2" />
                        Update Account
                    </button>
                </form>
            </div >
        </main >
    )
}