import DashboardLayout from "@/components/ui/layouts/dashboard"
import clientPromise from "@/lib/connections/mongo";
import { ObjectId } from "mongodb";
import { getServerSession } from "next-auth";
import { notFound, redirect } from "next/navigation";

export const revalidate = 0
export default async function RootLayout(props: any) {
    const session = await getServerSession();
    if (!session)
        redirect('/');

    const db = (await clientPromise).db()
    const data = JSON.parse(JSON.stringify(await db.collection("accounting_accounts").findOne({_id: new ObjectId(props.params.id)})))
    
    if (!data)
        notFound() 
    const coa = JSON.parse(JSON.stringify(await db.collection('accounting_coa').find().toArray()))
    props.params.data = data
    props.params.coa = coa
    return (
        <>{props.children}</>
    )
}

const title = 'Edit Account'
const description = 'Edit Accounting account'
const keywords = ["accounting", "accounting", "accounting account", "edit"]
const url = process.env.SITE_URL + '/accounting/account/edit'
export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: false,
        follow: false,
        nocache: false,
        googleBot: {
            index: false,
            follow: false
        },
    },
}