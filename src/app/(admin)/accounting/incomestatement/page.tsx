'use client'
import { useSession } from "next-auth/react"
import { redirect } from 'next/navigation';
import { FormEvent, Fragment, useEffect, useState } from 'react'
import Link from "next/link";
import { PlusIcon, PencilIcon, MagnifyingGlassIcon, XMarkIcon, ChevronDoubleRightIcon, XCircleIcon, AdjustmentsHorizontalIcon, IdentificationIcon, CheckIcon, NoSymbolIcon, ArrowPathIcon, ChevronDownIcon, ArrowDownTrayIcon, ArrowUpTrayIcon, ArrowsUpDownIcon, ReceiptPercentIcon, CalendarIcon } from "@heroicons/react/24/outline";
import { Spinner } from "@/components/ui/loader";
import { FullDayDate, FullShort, ISODate } from "@/lib/time";
import { usePathname } from 'next/navigation';
import { Pagination } from "@/components/ui/pagination";
import path from "path";
import { FormatMoney, FormatNumber } from "@/lib/text";
import ReactDatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css'

export default function Page(props: any) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })
    const pathName = usePathname()
    let fill: any
    let errors: string[] = []
    const [spinner, setSpinner] = useState(false)
    const [data, setData] = useState(props.params.data)
    const [finalBalances, setFinalBalances] = useState(props.params.balances)
    const [transactions, setTransactions] = useState(props.params.transactions)
    const [src, setSrc] = useState(false)
    const [dateRange, setDateRange] = useState<any[]>([null, null]);
    const [startDate, endDate] = dateRange;
    const [isPeriod, setIsPeriod] = useState(false)

    const submit = async (e: FormEvent) => {
        e.preventDefault()
        setSpinner(true)
        let date: any[] = []
        date[0] = dateRange[0]
        date[1] = new Date(Number(new Date(dateRange[1].getTime())) - ((dateRange[1].getTimezoneOffset() - (dateRange[1].getTimezoneOffset())) * 60000)).toISOString();
        const res = await fetch(path.posix.join('/api/', pathName ? pathName : "", '/search'), {
            body: JSON.stringify({
                dateRange: date
            }),
            headers: {
                'Content-Type': 'application/json',
            },
            method: 'POST'
        })

        const result = await res.json()
        if (result.data) {
            setFinalBalances(result.data.balances)
            setTransactions(result.data.trxs)
            setIsPeriod(true)
        }

        setSpinner(false)
    }
    const clearSearch = () => {
        setDateRange([null, null])
        setIsPeriod(false)
        setTransactions(props.params.transactions)
    }
    let asset = 0, liability = 0, equity = 0, revenue = 0, expense = 0, tRevDebit = 0, tRevCredit = 0, tExpDebit = 0, tExpCredit = 0
    return (
        <main>
            <div className="flex flex-col justify-between mb-4 space-y-4 md:flex-row md:space-y-0">
                <div>
                    <h1 className="text-xl font-semibold lg:text-2xl">Income Statement</h1>
                    <div className="text-sm text-slate-500">Income Statement Report</div>
                </div>
                <div className="w-full lg:w-96 items-center space-x-2">

                    <form onSubmit={submit} className="flex items-center flex-1 bg-white rounded-full h-9">
                        <div className="w-full mr-10">
                            <ReactDatePicker selectsRange={true}
                                startDate={startDate}
                                isClearable={true}
                                endDate={endDate}
                                selected={null}
                                onChange={(update: any) => {
                                    let startDate = null
                                    let endDate = null
                                    if (update[0] != null)
                                        startDate = Number(new Date(update[0].getTime())) - (update[0].getTimezoneOffset() * 60000);
                                    if (update[1] != null)
                                        endDate = Number(new Date(update[1].getTime())) - (update[1].getTimezoneOffset() * 60000);
                                    setDateRange(update);
                                }} name="date_range" id="date_range" showYearDropdown scrollableMonthYearDropdown dateFormat="dd MMMM yyyy" maxDate={new Date()} className="w-full px-4 py-1 text-sm border-none rounded-full outline-1 focus:ring-0 focus:outline-sky-100" placeholderText="Date Period" />
                        </div>
                        <div className="-ml-8">
                            <button type="submit" className="flex items-center justify-center w-8 h-8 rounded-full text-slate-100 bg-sky-500" >
                                <MagnifyingGlassIcon width={18} height={18} />
                            </button>
                        </div>
                    </form>
                </div>
            </div>

            <div className="flex flex-wrap space-x-2 p-4 text-xs md:text-base">
                {isPeriod &&
                    <>
                        Period: <div><span className="font-semibold">{FullDayDate(dateRange[0])}</span> to <span className="font-semibold">{FullDayDate(dateRange[1])}</span></div>
                        <button type="button" className='button px-2 py-1 mx-2 flex-shrink rounded-md hover:bg-red-500 shadow-md bg-red-600 text-white' onClick={() => clearSearch()}>
                            <XMarkIcon width={16} />
                        </button>
                        <Link className='button px-2 py-1 mx-2 flex-shrink rounded-md hover:bg-green-500 shadow-md bg-green-600 text-sm text-white' href={"/accounting/incomestatement/download/" + ISODate(dateRange[0] as string)+"to"+ISODate(dateRange[1] as string)} title="Download"><ArrowDownTrayIcon width={16} /></Link>
                    </>
                }
                 {!isPeriod && <Link className='button px-2 py-1 mx-2 flex-shrink rounded-md hover:bg-green-500 shadow-md bg-green-600 text-sm text-white flex space-x-2 items-center' href={"/accounting/incomestatement/download/" + ISODate(new Date().toDateString())} title="Download"><ArrowDownTrayIcon width={16} /> <span>Download</span></Link>}
            </div>

            <div className="relative w-full text-gray-600 transform transition-all ease-in-out">
                {spinner && <Spinner className=" rounded-tl-3xl rounded-br-3xl" />}

                <div className="flex-1 bg-white shadow-lg w-full rounded-tl-3xl rounded-br-3xl mb-4">
                    <h2 className="font-bold py-2 px-6">REVENUE (Pendapatan)</h2>
                    <table className="min-w-full divide-y  divide-gray-200 border-b border-gray-300 mb-4">
                        <thead className="thead">
                            <tr>
                                <th scope="col" className="thead-column">Code</th>
                                <th scope="col" className="thead-column">Name</th>
                                <th scope="col" className="thead-column">Debit</th>
                                <th scope="col" className="thead-column">Credit</th>
                                <th scope="col" className="thead-column">Balance</th>
                            </tr>
                        </thead>
                        <tbody className="bg-white divide-y divide-gray-200 text-sm" id="data">
                            {data.filter((parent: any) => (parent.code.toString().substr(0, 1) == 4 || parent.code.toString().substr(0, 1) == 7) && parent.code.toString().length == 3).map((coa: any) =>
                                <Fragment key={coa._id}>
                                    <tr className='bg-gray-50 hover:bg-blue-50 font-semibold'>
                                        <td className="px-2 py-1 md:py-2 md:px-4 text-right">{coa.code}</td>
                                        <td className="px-2 py-1 md:py-2 md:px-4" title={coa.description}>{coa.name}</td>
                                        <td className="lg:table-cell hidden px-2 md:px-4 py-2 md:py-2"></td>
                                        <td className="lg:table-cell hidden px-2 md:px-4 py-2 md:py-2"></td>
                                        <td className="px-2 py-1 md:py-2 text-center"></td>
                                    </tr>
                                    {data.filter((parent: any) => parent.code.toString().substr(0, 3) == coa.code && parent.code.toString().length == 5).map((sub: any) =>
                                        <tr key={sub._id} className='bg-gray-50 hover:bg-blue-50 text-xs'>
                                            <td className="px-2 py-1 md:py-2 md:px-4 text-right">{sub.code}</td>
                                            <td className="px-2 py-1 md:py-2 md:px-4" title={sub.description}>{sub.name}</td>
                                            <td className="lg:table-cell hidden px-2 py-1 md:px-4 md:py-2 text-right">
                                                {transactions.filter((trx: any) => trx._id == sub.code).map((balance: any) => <Fragment key={balance._id}>{FormatMoney(balance.debit.$numberDecimal)}</Fragment>)}
                                            </td>
                                            <td className="lg:table-cell hidden px-2 py-1 md:px-4 md:py-2 text-right">
                                                {transactions.filter((trx: any) => trx._id == sub.code).map((balance: any) => <Fragment key={balance._id}>{FormatMoney(balance.credit.$numberDecimal)}</Fragment>)}
                                            </td>
                                            <td className="px-2 py-1 md:py-2 md:px-4 text-right">
                                                {!isPeriod &&
                                                    finalBalances.filter((final: any) => final._id == sub.code).map((balance: any) =>
                                                        <Fragment key={balance._id}>{FormatMoney(balance.balance.$numberDecimal)}</Fragment>
                                                    )}
                                                {!isPeriod && finalBalances.filter((final: any) => final._id == sub.code).map((balance: any) => { revenue = Number(revenue) + Number(balance.balance.$numberDecimal) })}
                                                {isPeriod && transactions.filter((trx: any) => trx._id == sub.code).map((trx: any) =>
                                                    <Fragment key={trx._id}>{FormatMoney(trx.credit.$numberDecimal - trx.debit.$numberDecimal)}</Fragment>
                                                )}
                                                {isPeriod && transactions.filter((trxs: any) => trxs._id == sub.code).map((trx: any) => { revenue = Number(revenue) + Number(trx.credit.$numberDecimal + trx.debit.$numberDecimal) })}
                                            </td>
                                        </tr>
                                    )}
                                </Fragment>
                            )}
                        </tbody>
                    </table>
                    <div className="p-3 text-right font-semibold">Total Revenue : {FormatMoney(revenue)}</div>
                </div>

                <div className="flex-1 bg-white shadow-lg w-full rounded-tl-3xl rounded-br-3xl mb-4">
                    <h2 className="font-bold py-2 px-6">EXPENSE (Beban/Biaya)</h2>
                    <table className="min-w-full divide-y  divide-gray-200 border-b border-gray-300 mb-4">
                        <thead className="thead">
                            <tr>
                                <th scope="col" className="thead-column">Code</th>
                                <th scope="col" className="thead-column">Name</th>
                                <th scope="col" className="thead-column">Debit</th>
                                <th scope="col" className="thead-column">Credit</th>
                                <th scope="col" className="thead-column">Balance</th>
                            </tr>
                        </thead>
                        <tbody className="bg-white divide-y divide-gray-200 text-sm" id="data">
                            {data.filter((parent: any) => (parent.code.toString().substr(0, 1) == 5 || parent.code.toString().substr(0, 1) == 6) && parent.code.toString().length == 3).map((coa: any) =>
                                <Fragment key={coa._id}>
                                    <tr className='bg-gray-50 hover:bg-blue-50 font-semibold'>
                                        <td className="px-2 py-1 md:py-2 md:px-4 text-right">{coa.code}</td>
                                        <td className="px-2 py-1 md:py-2 md:px-4" title={coa.description}>{coa.name}</td>
                                        <td className="lg:table-cell hidden px-2 md:px-4 py-2 md:py-2"></td>
                                        <td className="lg:table-cell hidden px-2 md:px-4 py-2 md:py-2"></td>
                                        <td className="px-2 py-1 md:py-2 text-center"></td>
                                    </tr>
                                    {data.filter((parent: any) => parent.code.toString().substr(0, 3) == coa.code && parent.code.toString().length == 5).map((sub: any) =>
                                        <tr key={sub._id} className='bg-gray-50 hover:bg-blue-50 text-xs'>
                                            <td className="px-2 py-1 md:py-2 md:px-4 text-right">{sub.code}</td>
                                            <td className="px-2 py-1 md:py-2 md:px-4" title={sub.description}>{sub.name}</td>
                                            <td className="lg:table-cell hidden px-2 py-1 md:px-4 md:py-2 text-right">
                                                {transactions.filter((trx: any) => trx._id == sub.code).map((balance: any) => <Fragment key={balance._id}>{FormatMoney(balance.debit.$numberDecimal)}</Fragment>)}
                                            </td>
                                            <td className="lg:table-cell hidden px-2 py-1 md:px-4 md:py-2 text-right">
                                                {transactions.filter((trx: any) => trx._id == sub.code).map((balance: any) => <Fragment key={balance._id}>{FormatMoney(balance.credit.$numberDecimal)}</Fragment>)}
                                            </td>
                                            <td className="px-2 py-1 md:py-2 md:px-4 text-right">
                                                {!isPeriod &&
                                                    finalBalances.filter((final: any) => final._id == sub.code).map((balance: any) =>
                                                        <Fragment key={balance._id}>{FormatMoney(balance.balance.$numberDecimal)}</Fragment>
                                                    )}
                                                {!isPeriod && finalBalances.filter((final: any) => final._id == sub.code).map((balance: any) => { expense = Number(expense) + Number(balance.balance.$numberDecimal) })}
                                                {isPeriod && transactions.filter((trx: any) => trx._id == sub.code).map((trx: any) =>
                                                    <Fragment key={trx._id}>{FormatMoney(trx.credit.$numberDecimal - trx.debit.$numberDecimal)}</Fragment>
                                                )}
                                                {isPeriod && transactions.filter((trxs: any) => trxs._id == sub.code).map((trx: any) => { expense = Number(expense) + Number(trx.credit.$numberDecimal + trx.debit.$numberDecimal) })}
                                            </td>
                                        </tr>
                                    )}
                                </Fragment>
                            )}
                        </tbody>
                    </table>
                    <div className="p-3 text-right font-semibold">Total Expense : {FormatMoney(expense)}</div>
                </div>
            </div>
        </main>
    )
}