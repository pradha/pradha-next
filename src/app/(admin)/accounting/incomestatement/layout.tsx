import DashboardLayout from "@/components/ui/layouts/dashboard"
import clientPromise from "@/lib/connections/mongo"
import { getServerSession } from "next-auth";
import { notFound, redirect } from "next/navigation";

export const revalidate = 0
export const dynamic = 'force-dynamic'
export default async function RootLayout(props: any) {
    const session = await getServerSession();
    if (!session)
        notFound()

    const db = (await clientPromise).db()
    const data = await db.collection('v_accounting_coa').find().sort('code', 1).toArray()
    const balances = await db.collection('v_accounting_accounts').aggregate([
        {
            $group: {
                _id: "$coa.code",
                balance: {
                    $sum: "$balance"
                }
            }
        },
        { "$sort": { "_id": 1 } }
    ]).toArray()
    const transactions = await db.collection('v_accounting_transaction_details').aggregate([
        { $match: { status: 'Confirmed' } },
        {
            $group: {
                _id: "$journals.account.coa.code",
                debit: {
                    $sum: "$journals.debit"
                },
                credit: {
                    $sum: "$journals.credit"
                }
            }
        },
        { "$sort": { "_id": 1 } }
    ]).toArray()

    props.params.data = JSON.parse(JSON.stringify(data))
    props.params.balances = JSON.parse(JSON.stringify(balances))
    props.params.transactions = JSON.parse(JSON.stringify(transactions))

    
    return (
        <DashboardLayout>{props.children}</DashboardLayout>
    )
}

const title = 'Income Statement'
const description = 'Accounting Incomestatement Report'
const keywords = ["accounting", "transaction", "incomestatement"]
const url = process.env.SITE_URL + '/accounting/incomestatement'
export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: false,
        follow: false,
        nocache: false,
        googleBot: {
            index: false,
            follow: false
        },
    },
}