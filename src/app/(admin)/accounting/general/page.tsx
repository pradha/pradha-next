'use client'
import { useSession } from "next-auth/react"
import { redirect } from 'next/navigation';
import { ChangeEvent, FormEvent, Fragment, useEffect, useState } from 'react'
import Link from "next/link";
import { PlusIcon, PencilIcon, MagnifyingGlassIcon, XMarkIcon, ChevronDoubleRightIcon, XCircleIcon, AdjustmentsHorizontalIcon, IdentificationIcon, CheckIcon, NoSymbolIcon, ArrowPathIcon, ChevronDownIcon, ArrowDownTrayIcon, ArrowUpTrayIcon, ArrowsUpDownIcon, ReceiptPercentIcon, CalculatorIcon, CalendarDaysIcon } from "@heroicons/react/24/outline";
import { Spinner } from "@/components/ui/loader";
import { DayDateFormat, FullShort, ISODate } from "@/lib/time";
import { usePathname } from 'next/navigation';
import path from "path";
import { FormatMoney, FormatNumber } from "@/lib/text";
import { Menu, Transition } from "@headlessui/react";
import ReactDatePicker from 'react-datepicker';
import { MasterCoA } from "@/lib/accounting/masterCoA";
import 'react-datepicker/dist/react-datepicker.css'

export default function Page(props: any) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })
    const pathName = usePathname()
    let fill: any
    let errors = {
        coa: "",
        number: "",
        name: "",
        date: ""
    }
    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState({ errors: errors })
    const [transactions, setTransactions] = useState<any[]>([])
    const [account, setAccount] = useState<any>({})
    const [src, setSrc] = useState(false)
    const [pageNum, setPageNum] = useState(1)
    const [dateRange, setDateRange] = useState<any[]>([null, null]);
    const [startDate, endDate] = dateRange;
    const [coa, setCoa] = useState(props.params.coa)

    const submit = async (e: FormEvent) => {
        e.preventDefault()
        setWarning({ errors: errors })
        setSpinner(true)
        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement
        let date: any[] = []
        date[0] = dateRange[0]
        date[1] = new Date(Number(new Date(dateRange[1].getTime())) - ((dateRange[1].getTimezoneOffset() - (dateRange[1].getTimezoneOffset())) * 60000)).toISOString();
        const submit = await fetch(path.posix.join("/api/", pathName as string), {
            method: 'POST',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                coa: target.coa.value,
                dateRange: date
            })
        });
        //await new Promise(resolve => setTimeout(resolve, 3000));
        const res = await submit.json()
        if (submit.ok) {
            //console.log(res.account.previous_balance)
            setAccount(res.account)
            setTransactions(res.transactions)
        } else {
            errors = await res.errors
            setWarning({ errors: errors })
            errors = {
                coa: "",
                number: "",
                name: "",
                date: ""
            }
        }
        setSpinner(false)
    }
    let tdebit: number = 0, tcredit: number = 0, qty:number= 0;
    if (account.position == "Db" && account?.previous_balance)
        tdebit = account.previous_balance.debit.$numberDecimal - account.previous_balance.credit.$numberDecimal
    if (account.position == "Cr" && account?.previous_balance)
        tcredit = account.previous_balance.credit.$numberDecimal - account.previous_balance.debit.$numberDecimal
    return (
        <main>
            <div className="flex flex-col justify-between mb-4 space-y-4 md:flex-row md:space-y-0">
                <div>
                    <h1 className="text-xl font-semibold lg:text-2xl">General Ledger</h1>
                    <div className="text-sm text-slate-500">Accounting General Ledger</div>
                </div>
                <div className="flex items-center space-x-2">
                    {(session?.user.group.roles.includes("61e3de6ecc4029a9d11a5017") || session?.user.group.roles.includes("61e3de6ecc4029a9d11a5018") || session?.user.group.roles.includes("61e3de6ecc4029a9d11a5019") || session?.user.group.roles.includes("61e3de6ecc4029a9d11a5020")) &&
                        <Menu as="div" className="relative inline-block text-left">
                            <div>
                                <Menu.Button className="btn-circle">
                                    <PlusIcon width={20} height={20} />
                                </Menu.Button>
                            </div>
                            <Transition
                                as={Fragment}
                                enter="transition ease-out duration-100"
                                enterFrom="transform opacity-0 scale-95"
                                enterTo="transform opacity-100 scale-100"
                                leave="transition ease-in duration-75"
                                leaveFrom="transform opacity-100 scale-100"
                                leaveTo="transform opacity-0 scale-95"
                            >
                                <Menu.Items className="absolute z-10 lg:right-0 mt-2 w-40 origin-top-right divide-y divide-gray-100 rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
                                    <div className="px-1 py-1 ">
                                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5017") &&
                                            <Menu.Item>
                                                {({ active }) => (
                                                    <Link href={'/accounting/transaction/journal'} as={'/accounting/transaction/journal'} className={`${active ? 'bg-sky-500 text-white' : 'text-gray-900'} group flex w-full space-x-2 items-center rounded-md px-2 py-2 text-sm`}>
                                                        <ArrowsUpDownIcon width={18} /> <span>Journal</span>
                                                    </Link>
                                                )}
                                            </Menu.Item>
                                        }
                                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5018") &&
                                            <Menu.Item>
                                                {({ active }) => (
                                                    <Link href={'/accounting/transaction/incoming'} as={'/accounting/transaction/incoming'} className={`${active ? 'bg-sky-500 text-white' : 'text-gray-900'} group flex w-full space-x-2 items-center rounded-md px-2 py-2 text-sm`}>
                                                        <ArrowDownTrayIcon width={18} /> <span>Incoming</span>
                                                    </Link>
                                                )}
                                            </Menu.Item>
                                        }
                                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5019") &&
                                            <Menu.Item>
                                                {({ active }) => (
                                                    <Link href={'/accounting/transaction/outgoing'} as={'/accounting/transaction/outgoing'} className={`${active ? 'bg-sky-500 text-white' : 'text-gray-900'} group flex w-full space-x-2 items-center rounded-md px-2 py-2 text-sm`}>
                                                        <ArrowUpTrayIcon width={18} /> <span>Outgoing</span>
                                                    </Link>
                                                )}
                                            </Menu.Item>
                                        }
                                        {session?.user.group.roles.includes("61e3de6ecc4029a9d11a5020") &&
                                            <Menu.Item>
                                                {({ active }) => (
                                                    <Link href={'/accounting/transaction/invoice'} as={'/accounting/transaction/invoice'} className={`${active ? 'bg-sky-500 text-white' : 'text-gray-900'} group flex w-full space-x-2 items-center rounded-md px-2 py-2 text-sm`}>
                                                        <ReceiptPercentIcon width={18} /> <span>Invoice</span>
                                                    </Link>
                                                )}
                                            </Menu.Item>
                                        }
                                    </div>

                                </Menu.Items>
                            </Transition>
                        </Menu>
                    }
                </div>
            </div>
            <div className="relative w-full text-gray-600 lg:w-3/4 mb-4 bg-white shadow-lg rounded-tl-3xl lg:p-6 p-4 rounded-br-3xl dark:bg-slate-700 dark:text-slate-300 transform transition-all ease-in-out">
                {spinner && <Spinner className=" rounded-tl-3xl rounded-br-3xl" />}
                <form onSubmit={submit}>
                    <div className='mb-2'>
                        <label className="label" htmlFor="coa">Cart of Account <sup className="text-rose-500">*</sup></label>
                        <div className="relative inline-block w-full text-gray-700">
                            <select name="coa" id="coa" className={warning?.errors.coa ? "select-error" : "select"} defaultValue={""}>
                                {MasterCoA.map((master: any) =>
                                    <Fragment key={master.code}>
                                        <option value={master.code} disabled>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{master.code} - {master.name} ({master.position})</option>
                                        {coa.filter((parent: any) => parent.parent == master.code).map((parent: any) =>
                                            <Fragment key={parent._id}>
                                                <option value={parent._id} disabled>&nbsp;&nbsp;&nbsp;&nbsp;{parent.code} - {parent.name} ({parent.position})</option>
                                                {coa.filter((sub: any) => sub.parent == parent._id).map((child: any) => <option key={child._id} value={child._id}>{child.code} - {child.name}</option>)}
                                            </Fragment>
                                        )}
                                    </Fragment>
                                )}
                            </select>
                        </div>
                    </div>
                    <div className="mb-4">
                        <label className="label" htmlFor="period">Date Range (Period) <sup className="text-rose-500">*</sup></label>
                        <ReactDatePicker selectsRange={true}
                            startDate={startDate}
                            isClearable={true}
                            endDate={endDate}
                            onChange={(update: any) => {
                                let startDate = null
                                let endDate = null
                                if (update[0] != null)
                                    startDate = Number(new Date(update[0].getTime())) - (update[0].getTimezoneOffset() * 60000);
                                if (update[1] != null)
                                    endDate = Number(new Date(update[1].getTime())) - (update[1].getTimezoneOffset() * 60000);
                                setDateRange(update);
                            }} name="date_range" id="date_range" showYearDropdown scrollableMonthYearDropdown dateFormat="dd MMMM yyyy" maxDate={new Date()} className="input flex-1 text-sm" placeholderText="Date Period" />
                    </div>
                    <button type="submit" className="btn btn-primary flex items-center space-x-2">
                        <MagnifyingGlassIcon width={20} height={20} className="mr-2" />
                        Show General Ledger
                    </button>
                </form>
            </div>
            <div className="w-full text-gray-600 bg-white shadow-lg rounded-tl-3xl rounded-br-3xl dark:bg-slate-700 dark:text-slate-300">
                {account.name &&
                    <div className="px-6 py-3 ">

                        <div className="flex items-center space-x-2 text-sm tracking-wide">
                            <div className="w-32">Code</div>
                            <div>:</div>
                            <div className="font-semibold">{account.code}</div>
                        </div>
                        <div className="flex items-center space-x-2 text-sm tracking-wide">
                            <div className="w-32">CoA Name</div>
                            <div>:</div>
                            <div className="font-semibold">{account.name}</div>
                        </div>
                        <div className="flex items-center space-x-2 text-sm tracking-wide">
                            <div className="w-32">Normal Position</div>
                            <div>:</div>
                            <div className="font-semibold">{account.position}</div>
                        </div>
                        <div className="flex items-center space-x-2 text-sm tracking-wide">
                            <div className="w-32">Previous Balance</div>
                            <div>:</div>
                            <div className="font-semibold">{account?.previous_balance ? FormatMoney(account.position == "Db" ? FormatMoney(account.previous_balance.debit.$numberDecimal - account.previous_balance.credit.$numberDecimal) : FormatMoney(account.previous_balance.credit.$numberDecimal - account.previous_balance.debit.$numberDecimal)) : "0.00"}</div>
                        </div>
                        <div className="flex items-center space-x-2 text-sm tracking-wide">
                            <div className="w-32">Total Data</div>
                            <div>:</div>
                            <div className="font-semibold">{transactions.length}</div>
                            <div>Journal{transactions.length > 1 ? "s" : ""} (<span className="font-semibold">{DayDateFormat(new Date(dateRange[0]).toString())}</span> to <span className="font-semibold">{DayDateFormat(new Date(dateRange[1]).toString())}</span>)</div>
                        </div>
                    </div>
                }
                <div className="overflow-x-auto">
                    <table className="min-w-full border-b border-gray-300 divide-y divide-gray-200 table-auto">
                        <thead className="thead">
                            <tr className="">
                                <th className="thead-column" rowSpan={2}>Efective Date</th>
                                <th className="thead-column" rowSpan={2}>TX Code</th>
                                <th className="thead-column w-72" rowSpan={2}>Information</th>
                                <th className="thead-column" rowSpan={2}>Debit</th>
                                <th className="thead-column" rowSpan={2}>Credit</th>
                                <th className="thead-column" colSpan={2}>Balance</th>
                            </tr>
                            <tr className="">
                                <th className="thead-column">Debit</th>
                                <th className="thead-column">Credit</th>
                            </tr>
                        </thead>
                        <tbody>
                            {typeof transactions !== undefined && transactions?.map((item: any, i: number) => {
                                tdebit = tdebit + Number(item.journals.debit.$numberDecimal)
                                tcredit = tcredit + Number(item.journals.credit.$numberDecimal)

                                return <Fragment key={item._id} >
                                    {i == 0 &&
                                        <tr className="transition-all ease-in-out delay-200 border-b border-gray-200 divide-slate-200 bg-gradient-to-br from-slate-300 to-slate-400 hover:from-slate-200 hover:to-slate-300">
                                            <td colSpan={5} className="tbody-column text-center">Transfer Balance</td>
                                            <td className="tbody-column text-right">{account?.previous_balance?.debit && (account.previous_balance.debit.$numberDecimal - account.previous_balance?.credit.$numberDecimal) >= 0 && account.position == "Db" ? FormatMoney(account.previous_balance.debit.$numberDecimal - account.previous_balance.credit.$numberDecimal) : "0.00"}</td>
                                            <td className="tbody-column text-right">{account?.previous_balance?.credit && (account.previous_balance.credit.$numberDecimal - account.previous_balance?.debit.$numberDecimal) >= 0 && account.position == "Cr" ? FormatMoney(account.previous_balance.credit.$numberDecimal - account.previous_balance.debit.$numberDecimal) : "0.00"}</td>
                                        </tr>
                                    }
                                    <tr className="transition-all ease-in-out delay-200 border-b border-gray-200 divide-slate-200 bg-gradient-to-br from-slate-50 odd:to-gray-100 even:to-slate-200 hover:from-sky-50 hover:to-sky-100">
                                        <td className="tbody-column"><div className='truncate w-16 md:w-max ' title={item.code}>{item?.confirmed.at ? ISODate(item.confirmed.at) : item?.status}</div></td>
                                        <td className="tbody-column"><Link target="_blank" href={"/accounting/transaction/detail/" + item._id} as={"/accounting/transaction/detail/" + item._id} title={item.code}>{item.code}</Link></td>
                                        <td className="tbody-column">{item.information} {(item.type == "IV" && item.journals.credit.$numberDecimal > 0.00) ? '/ ' + item.journals.account.number + ' - ' + item.journals.account.name + ` (${item.journals.quantity}Kg)` : ''}</td>
                                        <td className="tbody-column text-right">{FormatMoney(item.journals.debit.$numberDecimal)}</td>
                                        <td className="tbody-column text-right">{FormatMoney(item.journals.credit.$numberDecimal)}</td>
                                        <td className="tbody-column text-right">{account.position == "Db" && tdebit - tcredit >= 0 ? FormatMoney(tdebit - tcredit) : "0.00"}</td>
                                        <td className="tbody-column text-right">{account.position == "Cr" && tcredit - tdebit >= 0 ? FormatMoney(tcredit - tdebit) : "0.00"}</td>
                                    </tr>
                                </Fragment>
                            })}
                            {(!transactions || transactions?.length <= 0) &&
                                <tr>
                                    <td className="p-6 text-center italic text-sm" colSpan={7}>No Transaction</td>
                                </tr>
                            }
                        </tbody>
                    </table>
                  
                </div>
            </div>
        </main >
    )
}