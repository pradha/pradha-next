import clientPromise from "@/lib/connections/mongo";
import { ISODate } from "@/lib/time";
import excel from "excel4node";
export async function GET(request: Request, { params }: any) {
    let date = params.date
    let balances: any, transactions: any;
    const db = (await clientPromise).db()
    const data = await db.collection('v_accounting_coa').find().sort('code', 1).toArray()
    let isPeriod = false
    if (ISODate(new Date().toDateString()) == date) {
        balances = await db.collection('v_accounting_accounts').aggregate([
            {
                $group: {
                    _id: "$coa.code",
                    balance: {
                        $sum: "$balance"
                    }
                }
            },
            { "$sort": { "_id": 1 } }
        ]).toArray()
        transactions = await db.collection('v_accounting_transaction_details').aggregate([
            {
                $group: {
                    _id: "$journals.account.coa.code",
                    debit: {
                        $sum: "$journals.debit"
                    },
                    credit: {
                        $sum: "$journals.credit"
                    }
                }
            },
            { "$sort": { "_id": 1 } }
        ]).toArray()
    } else {
        isPeriod = true
        balances = await db.collection('v_accounting_accounts').aggregate([
            {
                $group: {
                    _id: "$coa.code",
                    balance: {
                        $sum: "$balance"
                    }
                }
            },
            { "$sort": { "_id": 1 } }
        ]).toArray()
        date = new Date(date)
        date.setHours(23)
        date.setMinutes(59)
        date.setSeconds(59)
        transactions = await db.collection('v_accounting_transaction_details').aggregate([
            {
                $match: {
                    status: "Confirmed", "confirmed.at": { $lt: date }
                }
            },
            {
                $group: {
                    _id: "$journals.account.coa.code",
                    debit: {
                        $sum: "$journals.debit"
                    },
                    credit: {
                        $sum: "$journals.credit"
                    }
                }
            },
            { "$sort": { "_id": 1 } }
        ]).toArray()
    }

    var wb = new excel.Workbook()
    var ws = wb.addWorksheet('Sheet 1');

    var titleStyle = wb.createStyle({
        font: {
            color: '#000000',
            size: 18,
            bold: true,
        },
        alignment: {
            horizontal: 'center',
        },
        numberFormat: '#,##0.00; (#,##0.00); -',
    });
    var headerStyle = wb.createStyle({
        font: {
            color: '#000000',
            size: 14,
            bold: true,
        },
        numberFormat: '#,##0.00; (#,##0.00); -',
    });
    var defaultStyle = wb.createStyle({
        font: {
            color: '#000000',
            size: 12,
        },
        numberFormat: '#,##0.00; (#,##0.00); -',
    });
    var totalStyle = wb.createStyle({
        font: {
            color: '#000000',
            size: 14,
            bold: true,
        },
        alignment: {
            horizontal: 'right',
        },
        numberFormat: '#,##0.00; (#,##0.00); -',
    });

    let asset = 0, liability = 0, equity = 0, revenue = 0, expense = 0, tRevDebit = 0, tRevCredit = 0, tExpDebit = 0, tExpCredit = 0

    ws.cell(1, 1, 1, 5, true).string("AKTIVA").style(titleStyle);
    ws.cell(2, 1).string("CODE").style(headerStyle);
    ws.cell(2, 2).string("NAME").style(headerStyle);
    ws.cell(2, 3).string("DEBIT").style(headerStyle);
    ws.cell(2, 4).string("CREDIT").style(headerStyle);
    ws.cell(2, 5).string("BALANCE").style(headerStyle);
    ws.column(2).setWidth(45);
    ws.column(3).setWidth(15);
    ws.column(4).setWidth(15);
    ws.column(5).setWidth(18);
    let line = 3;
    data.filter((parent: any) => parent.code.toString().substr(0, 1) == 1 && parent.code.toString().length == 3).map((coa: any) => {
        ws.cell(line, 1).number(coa.code);
        ws.cell(line, 2).string(coa.name).style(defaultStyle);

        line++;

        data.filter((parent: any) => parent.code.toString().substr(0, 3) == coa.code && parent.code.toString().length == 5).map((sub: any) => {
            ws.cell(line, 1).number(sub.code);
            ws.cell(line, 2).string(sub.name).style(defaultStyle);
            let balance = transactions.filter((trx: any) => trx._id == sub.code).map((balance: any) => balance)

            let debit = parseFloat(balance && balance.length > 0 && balance[0]?.debit ? balance[0].debit : 0)
            let credit = parseFloat(balance && balance.length > 0 && balance[0]?.credit ? balance[0].credit : 0)
            let finalBalance: number = 0
            let final: any
            if (!isPeriod) {
                final = balances.filter((final: any) => final._id == sub.code).map((balance: any) => balance)
                finalBalance = final.length >= 1 && final[0]?.balance ? parseFloat(final[0].balance) : 0
                asset = Number(asset) + Number(finalBalance)
            } else {
                final = transactions.filter((trxs: any) => trxs._id == sub.code).map((trx: any) => trx)
                finalBalance = final.length >= 1 ? parseFloat(final[0].debit) - parseFloat(final[0].credit) : 0
                asset = Number(asset) + Number(debit - credit)
            }

            ws.cell(line, 3).number(debit).style(defaultStyle);
            ws.cell(line, 4).number(credit).style(defaultStyle);
            ws.cell(line, 5).number(finalBalance).style(defaultStyle);

            line++;
        })

    })
    ws.cell(line, 1, line, 4, true).string("Total Aktiva (Assets)").style(totalStyle);
    ws.cell(line, 5).number(asset).style(totalStyle);

    line++
    line++
    ws.cell(line, 1, line, 5, true).string("PASIVA").style(titleStyle);
    line++
    ws.cell(line, 1).string("Liabilities").style(headerStyle);
    line++
    ws.cell(line, 1).string("CODE").style(headerStyle);
    ws.cell(line, 2).string("NAME").style(headerStyle);
    ws.cell(line, 3).string("DEBIT").style(headerStyle);
    ws.cell(line, 4).string("CREDIT").style(headerStyle);
    ws.cell(line, 5).string("BALANCE").style(headerStyle);

    line++;
    data.filter((parent: any) => parent.code.toString().substr(0, 1) == 2 && parent.code.toString().length == 3).map((coa: any) => {
        ws.cell(line, 1).number(coa.code);
        ws.cell(line, 2).string(coa.name).style(defaultStyle);

        line++;

        data.filter((parent: any) => parent.code.toString().substr(0, 3) == coa.code && parent.code.toString().length == 5).map((sub: any) => {
            ws.cell(line, 1).number(sub.code);
            ws.cell(line, 2).string(sub.name).style(defaultStyle);
            let balance = transactions.filter((trx: any) => trx._id == sub.code).map((balance: any) => balance)

            let debit = parseFloat(balance && balance.length > 0 && balance[0]?.debit ? balance[0].debit : 0)
            let credit = parseFloat(balance && balance.length > 0 && balance[0]?.credit ? balance[0].credit : 0)
            let finalBalance: number = 0
            let final: any
            if (!isPeriod) {
                final = balances.filter((final: any) => final._id == sub.code).map((balance: any) => balance)
                finalBalance = final.length >= 1 && final[0]?.balance ? parseFloat(final[0].balance) : 0
                liability = Number(liability) + Number(finalBalance)
            } else {
                final = transactions.filter((trxs: any) => trxs._id == sub.code).map((trx: any) => trx)
                finalBalance = final.length >= 1 ? parseFloat(final[0].credit) - parseFloat(final[0].debit) : 0
                liability = Number(liability) + Number(credit - debit)
            }

            ws.cell(line, 3).number(debit).style(defaultStyle);
            ws.cell(line, 4).number(credit).style(defaultStyle);
            ws.cell(line, 5).number(finalBalance).style(defaultStyle);

            line++;
        })

    })
    ws.cell(line, 1, line, 4, true).string("Total Liability").style(totalStyle);
    ws.cell(line, 5).number(liability).style(totalStyle);

    line++
    line++
    ws.cell(line, 1).string("Equity").style(headerStyle);
    line++
    ws.cell(line, 1).string("CODE").style(headerStyle);
    ws.cell(line, 2).string("NAME").style(headerStyle);
    ws.cell(line, 3).string("DEBIT").style(headerStyle);
    ws.cell(line, 4).string("CREDIT").style(headerStyle);
    ws.cell(line, 5).string("BALANCE").style(headerStyle);

    line++;
    data.filter((parent: any) => parent.code.toString().substr(0, 1) == 3 && parent.code.toString().length == 3).map((coa: any) => {
        ws.cell(line, 1).number(coa.code);
        ws.cell(line, 2).string(coa.name).style(defaultStyle);

        line++;

        data.filter((parent: any) => parent.code.toString().substr(0, 3) == coa.code && parent.code.toString().length == 5).map((sub: any) => {
            ws.cell(line, 1).number(sub.code);
            ws.cell(line, 2).string(sub.name).style(defaultStyle);
            let balance = transactions.filter((trx: any) => trx._id == sub.code).map((balance: any) => balance)

            let debit = parseFloat(balance && balance.length > 0 && balance[0]?.debit ? balance[0].debit : 0)
            let credit = parseFloat(balance && balance.length > 0 && balance[0]?.credit ? balance[0].credit : 0)
            let finalBalance: number = 0
            let final: any
            if (!isPeriod) {
                final = balances.filter((final: any) => final._id == sub.code).map((balance: any) => balance)
                finalBalance = final.length >= 1 && final[0]?.balance ? parseFloat(final[0].balance) : 0
                equity = Number(equity) + Number(finalBalance)
            } else {
                final = transactions.filter((trxs: any) => trxs._id == sub.code).map((trx: any) => trx)
                finalBalance = final.length >= 1 ? parseFloat(final[0].credit) - parseFloat(final[0].debit) : 0
                equity = Number(equity) + Number(credit - debit)
            }

            ws.cell(line, 3).number(debit).style(defaultStyle);
            ws.cell(line, 4).number(credit).style(defaultStyle);
            ws.cell(line, 5).number(finalBalance).style(defaultStyle);

            line++;

        })

        if (coa.code == 303) {
            if (!isPeriod) {
                balances.filter((final: any) => final._id.toString().substr(0, 1) == 4).map((rev: any) => { revenue = Number(revenue) + Number(rev.balance) })
                balances.filter((final: any) => final._id.toString().substr(0, 1) == 7).map((rev: any) => { revenue = Number(revenue) + Number(rev.balance) })
                balances.filter((final: any) => final._id.toString().substr(0, 1) == 5).map((rev: any) => { expense = Number(expense) + Number(rev.balance) })
                balances.filter((final: any) => final._id.toString().substr(0, 1) == 6).map((rev: any) => { expense = Number(expense) + Number(rev.balance) })
            } else {
                transactions.filter((trxs: any) => trxs._id.toString().substr(0, 1) == 4).map((trx: any) => { revenue = Number(revenue) + Number(trx.debit - trx.credit) })
                transactions.filter((trxs: any) => trxs._id.toString().substr(0, 1) == 7).map((trx: any) => { revenue = Number(revenue) + Number(trx.debit - trx.credit) })
                transactions.filter((trxs: any) => trxs._id.toString().substr(0, 1) == 5).map((trx: any) => { expense = Number(expense) + Number(trx.debit - trx.credit) })
                transactions.filter((trxs: any) => trxs._id.toString().substr(0, 1) == 6).map((trx: any) => { expense = Number(expense) + Number(trx.debit - trx.credit) })
            }
            ws.cell(line, 1).number(30301);
            ws.cell(line, 2).string("LABA/RUGI TOTAL").style(defaultStyle);

            transactions.filter((trxs: any) => trxs._id.toString().substr(0, 1) == 4 || trxs._id.toString().substr(0, 1) == 7).map((trx: any) => { tRevDebit = Number(tRevDebit) + Number(trx.debit) })
            transactions.filter((trxs: any) => trxs._id.toString().substr(0, 1) == 5 || trxs._id.toString().substr(0, 1) == 6).map((trx: any) => { tExpDebit = Number(tExpDebit) + Number(trx.debit) })
            ws.cell(line, 3).number(tRevDebit + tExpDebit).style(defaultStyle);

            transactions.filter((trxs: any) => trxs._id.toString().substr(0, 1) == 4 || trxs._id.toString().substr(0, 1) == 7).map((trx: any) => { tRevCredit = Number(tRevCredit) + Number(trx.credit) })
            transactions.filter((trxs: any) => trxs._id.toString().substr(0, 1) == 5 || trxs._id.toString().substr(0, 1) == 6).map((trx: any) => { tExpCredit = Number(tExpCredit) + Number(trx.credit) })
            ws.cell(line, 4).number(tRevCredit + tExpCredit).style(defaultStyle);
            
            ws.cell(line, 5).number(!isPeriod ? revenue - expense : (Number(tRevCredit + tExpCredit)) - (Number(tRevDebit + tExpDebit))).style(defaultStyle);
            line++
        }

    })
    ws.cell(line, 1, line, 4, true).string("Total Equity").style(totalStyle);
    ws.cell(line, 5).number(!isPeriod ? equity + revenue - expense : equity + (Number(tRevCredit + tExpCredit) - (Number(tRevDebit + tExpDebit)))).style(totalStyle);
    line++
    line++
    ws.cell(line, 1, line, 4, true).string("Total Pasiva").style(totalStyle);
    ws.cell(line, 5).number(liability + (!isPeriod ? equity + revenue - expense : equity + (Number(tRevCredit + tExpCredit) - (Number(tRevDebit + tExpDebit))))).style(totalStyle);

    return new Response(await wb.writeToBuffer().then((buffer: any) => buffer), {
        status: 200,
        headers: {
            'Content-Type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            "Content-Disposition": "attachment; filename=" + "Balancesheet_" + params.date.replaceAll("-", "") + ".xlsx"
        }
    });
}