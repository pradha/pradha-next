'use client'
import { useSession } from "next-auth/react"
import { redirect } from 'next/navigation';
import { FormEvent, Fragment, useEffect, useState } from 'react'
import Link from "next/link";
import { PlusIcon, PencilIcon, MagnifyingGlassIcon, XMarkIcon, ChevronDoubleRightIcon, XCircleIcon, AdjustmentsHorizontalIcon, IdentificationIcon, CheckIcon, NoSymbolIcon, ArrowPathIcon, ChevronDownIcon, ArrowDownTrayIcon, ArrowUpTrayIcon, ArrowsUpDownIcon, ReceiptPercentIcon, CalendarIcon, DocumentIcon } from "@heroicons/react/24/outline";
import { Spinner } from "@/components/ui/loader";
import { FullDayDate, FullShort, ISODate } from "@/lib/time";
import { usePathname } from 'next/navigation';
import { Pagination } from "@/components/ui/pagination";
import path from "path";
import { FormatMoney, FormatNumber } from "@/lib/text";

export default function Page(props: any) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })
    const pathName = usePathname()
    let fill: any
    let errors: string[] = []
    const [spinner, setSpinner] = useState(false)
    const [data, setData] = useState(props.params.data)
    const [finalBalances, setFinalBalances] = useState(props.params.balances)
    const [transactions, setTransactions] = useState(props.params.transactions)
    const [isPeriod, setIsPeriod] = useState(false)
    const [selectedDate, setSelectedDate] = useState<string | null>(null)

    const search = async () => {
        setSpinner(true)
        let target = document.getElementById('date') as HTMLInputElement
        if (target.value.length > 0) {
            setSelectedDate(target.value)
            const res = await fetch(path.posix.join('/api/', pathName ? pathName : "", '/search'), {
                body: JSON.stringify({
                    date: target.value
                }),
                headers: {
                    'Content-Type': 'application/json',
                },
                method: 'POST'
            })

            const result = await res.json()
            if (result.data) {
                setFinalBalances(result.data.balances)
                setTransactions(result.data.trxs)
                setIsPeriod(true)
            }
        }
        setSpinner(false)
    }

    const clearSearch = () => {
        setSelectedDate(null)
        setIsPeriod(false)
        setTransactions(props.params.transactions)
    }

    let asset = 0, liability = 0, equity = 0, revenue = 0, expense = 0, tRevDebit = 0, tRevCredit = 0, tExpDebit = 0, tExpCredit = 0
    return (
        <main>
            <div className="flex flex-col justify-between mb-4 space-y-4 md:flex-row md:space-y-0">
                <div>
                    <h1 className="text-xl font-semibold lg:text-2xl">Balancesheet</h1>
                    <div className="text-sm text-slate-500">Accounting Balancesheet Report</div>
                </div>
                <div className="flex w-1/3 items-center space-x-2">

                    <div className="flex items-center flex-1 bg-white rounded-full h-9">
                        <div className="w-full">
                            <input type="date" id="date" name="date" required min="2020-01-01" max={ISODate((new Date()).toString())} className="w-full px-4 py-1 text-sm border-none rounded-full outline-1 focus:ring-0 focus:outline-sky-100" placeholder="Search..." x-model="search" onChange={search} />
                        </div>

                    </div>
                </div>
            </div>

            <div className="flex flex-wrap space-x-2 p-4 text-xs md:text-base items-center">
                {isPeriod &&
                    <>
                        <div>Until:</div> <div><span className="font-semibold">{FullDayDate(selectedDate as string)}</span></div>
                        <button type="button" className='button px-2 py-1 mx-2 flex-shrink rounded-md hover:bg-red-500 shadow-md bg-red-600 text-white' onClick={() => clearSearch()}>
                            <XMarkIcon width={16} />
                        </button>
                        <Link className='button px-2 py-1 mx-2 flex-shrink rounded-md hover:bg-green-500 shadow-md bg-green-600 text-sm text-white' href={"/accounting/balancesheet/download/" + ISODate(selectedDate as string)} title="Download"><ArrowDownTrayIcon width={16} /></Link>
                    </>
                }
                {!isPeriod && <Link className='button px-2 py-1 mx-2 flex-shrink rounded-md hover:bg-green-500 shadow-md bg-green-600 text-sm text-white flex space-x-2 items-center' href={"/accounting/balancesheet/download/" + ISODate(new Date().toDateString())} title="Download"><ArrowDownTrayIcon width={16} /> <span>Download</span></Link>}
            </div>
            <div className="relative w-full text-gray-600 transform transition-all ease-in-out">
                {spinner && <Spinner className=" rounded-tl-3xl rounded-br-3xl" />}
                <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 space-y-4 lg:space-y-0'>
                    <div className="flex-1 bg-white shadow-lg w-full rounded-tl-3xl rounded-br-3xl py-4">
                        <h2 className="font-bold py-2 px-6">AKTIVA</h2>
                        <table className="min-w-full divide-y  divide-gray-200 border-b border-gray-300 mb-4">
                            <thead className="thead">
                                <tr>
                                    <th scope="col" className="thead-column">Code</th>
                                    <th scope="col" className="thead-column">Name</th>
                                    <th scope="col" className="thead-column">Debit</th>
                                    <th scope="col" className="thead-column">Credit</th>
                                    <th scope="col" className="thead-column">Balance</th>
                                </tr>
                            </thead>
                            <tbody className="bg-white divide-y divide-gray-200 text-xs" id="data">
                                {data.filter((parent: any) => parent.code.toString().substr(0, 1) == 1 && parent.code.toString().length == 3).map((coa: any) =>
                                    <Fragment key={coa._id}>
                                        <tr className='bg-gray-50 hover:bg-blue-50 font-semibold'>
                                            <td className="px-2 py-1 md:py-2 text-right">{coa.code}</td>
                                            <td className="px-2 py-1 md:py-2" title={coa.description}>{coa.name}</td>
                                            <td className="lg:table-cell hidden px-2 py-1 md:py-2"></td>
                                            <td className="lg:table-cell hidden px-2 py-1 md:py-2"></td>
                                            <td className="px-2 py-1 md:py-2 text-center"></td>
                                        </tr>
                                        {data.filter((parent: any) => parent.code.toString().substr(0, 3) == coa.code && parent.code.toString().length == 5).map((sub: any) =>
                                            <tr key={sub._id} className='bg-gray-50 hover:bg-blue-50'>
                                                <td className="px-2 py-1 md:py-2 text-right">{sub.code}</td>
                                                <td className="px-2 py-1 md:py-2" title={sub.description}>{sub.name}</td>
                                                <td className="lg:table-cell hidden px-2 py-1 md:py-2 text-right">
                                                    {transactions.filter((trx: any) => trx._id == sub.code).map((balance: any) => <Fragment key={balance._id}>{FormatMoney(balance.debit.$numberDecimal)}</Fragment>)}
                                                </td>
                                                <td className="lg:table-cell hidden px-2 py-1 md:py-2 text-right">
                                                    {transactions.filter((trx: any) => trx._id == sub.code).map((balance: any) => <Fragment key={balance._id}>{FormatMoney(balance.credit.$numberDecimal)}</Fragment>)}
                                                </td>
                                                <td className="px-2 py-1 md:py-2 text-right">
                                                    {!isPeriod &&
                                                        finalBalances.filter((final: any) => final._id == sub.code).map((balance: any) =>
                                                            <Fragment key={balance._id}>{FormatMoney(balance.balance.$numberDecimal)}</Fragment>
                                                        )}
                                                    {!isPeriod && finalBalances.filter((final: any) => final._id == sub.code).map((balance: any) => { asset = Number(asset) + Number(balance.balance.$numberDecimal) })}
                                                    {isPeriod && transactions.filter((trxs: any) => trxs._id == sub.code).map((trx: any) =>
                                                        <Fragment key={trx._id}>{FormatMoney(trx.debit.$numberDecimal - trx.credit.$numberDecimal)}</Fragment>
                                                    )}
                                                    {isPeriod && transactions.filter((trxs: any) => trxs._id == sub.code).map((trx: any) => { asset = Number(asset) + Number(trx.debit.$numberDecimal - trx.credit.$numberDecimal) })}
                                                </td>
                                            </tr>
                                        )}
                                    </Fragment>
                                )}
                            </tbody>
                        </table>
                        <div className="p-2 text-right font-semibold">Total Asset : {FormatMoney(asset)}</div>
                    </div>

                    <div className="flex-1 bg-white shadow-lg w-full rounded-tl-3xl rounded-br-3xl py-4">
                        <h2 className="font-bold py-2 px-6">PASIVA</h2>
                        <table className="min-w-full divide-y  divide-gray-200 border-b border-gray-300 mb-4">
                            <thead className="thead">
                                <tr>
                                    <th scope="col" className="thead-column">Code</th>
                                    <th scope="col" className="thead-column">Name</th>
                                    <th scope="col" className="thead-column">Debit</th>
                                    <th scope="col" className="thead-column">Credit</th>
                                    <th scope="col" className="thead-column">Balance</th>
                                </tr>
                            </thead>
                            <tbody className="bg-white divide-y divide-gray-200 text-xs" id="data">
                                {data.filter((parent: any) => parent.code.toString().substr(0, 1) == 2 && parent.code.toString().length == 3).map((coa: any) =>
                                    <Fragment key={coa._id}>
                                        <tr className='bg-gray-50 hover:bg-blue-50 font-semibold'>
                                            <td className="px-2 py-1 md:py-2 text-right">{coa.code}</td>
                                            <td className="px-2 py-1 md:py-2" title={coa.description}>{coa.name}</td>
                                            <td className="lg:table-cell hidden px-2 py-1 md:py-2"></td>
                                            <td className="lg:table-cell hidden px-2 py-1 md:py-2"></td>
                                            <td className="px-2 py-1 md:py-2 text-center"></td>
                                        </tr>
                                        {data.filter((parent: any) => parent.code.toString().substr(0, 3) == coa.code && parent.code.toString().length == 5).map((sub: any) =>
                                            <tr key={sub._id} className='bg-gray-50 hover:bg-blue-50'>
                                                <td className="px-2 py-1 md:py-2 text-right">{sub.code}</td>
                                                <td className="px-2 py-1 md:py-2" title={sub.description}>{sub.name}</td>
                                                <td className="lg:table-cell hidden px-2 py-1 md:py-2 text-right">
                                                    {transactions.filter((trx: any) => trx._id == sub.code).map((balance: any) => <Fragment key={balance._id}>{FormatMoney(balance.debit.$numberDecimal)}</Fragment>)}
                                                </td>
                                                <td className="lg:table-cell hidden px-2 py-1 md:py-2 text-right">
                                                    {transactions.filter((trx: any) => trx._id == sub.code).map((balance: any) => <Fragment key={balance._id}>{FormatMoney(balance.credit.$numberDecimal)}</Fragment>)}
                                                </td>
                                                <td className="px-2 py-1 md:py-2 text-right">
                                                    {!isPeriod &&
                                                        finalBalances.filter((final: any) => final._id == sub.code).map((balance: any) =>
                                                            <Fragment key={balance._id}>{FormatMoney(balance.balance.$numberDecimal)}</Fragment>
                                                        )}
                                                    {!isPeriod && finalBalances.filter((final: any) => final._id == sub.code).map((balance: any) => { liability = Number(liability) + Number(balance.balance.$numberDecimal) })}
                                                    {isPeriod && transactions.filter((trxs: any) => trxs._id == sub.code).map((trx: any) =>
                                                        <Fragment key={trx._id}>{FormatMoney(trx.debit.$numberDecimal - trx.credit.$numberDecimal)}</Fragment>
                                                    )}
                                                    {isPeriod && transactions.filter((trxs: any) => trxs._id == sub.code).map((trx: any) => { liability = Number(liability) + Number(trx.credit.$numberDecimal) - Number(trx.debit.$numberDecimal) })}
                                                </td>
                                            </tr>
                                        )}
                                    </Fragment>
                                )}
                            </tbody>
                        </table>
                        <div className="p-2 text-right font-semibold">Total Liability : {FormatMoney(liability)}</div>

                        <h2 className="font-bold py-2 px-6">EQUITY</h2>
                        <table className="min-w-full divide-y  divide-gray-200 border-b border-gray-300 mb-4">
                            <thead className="thead">
                                <tr>
                                    <th scope="col" className="thead-column">Code</th>
                                    <th scope="col" className="thead-column">Name</th>
                                    <th scope="col" className="thead-column">Debit</th>
                                    <th scope="col" className="thead-column">Credit</th>
                                    <th scope="col" className="thead-column">Balance</th>
                                </tr>
                            </thead>
                            <tbody className="bg-white divide-y divide-gray-200 text-xs" id="data">
                                {data.filter((parent: any) => parent.code.toString().substr(0, 1) == 3 && parent.code.toString().length == 3).map((coa: any) =>
                                    <Fragment key={coa._id}>
                                        <tr className='bg-gray-50 hover:bg-blue-50 font-semibold'>
                                            <td className="px-2 py-1 md:py-2 text-right">{coa.code}</td>
                                            <td className="px-2 py-1 md:py-2" title={coa.description}>{coa.name}</td>
                                            <td className="lg:table-cell hidden px-2 py-1 md:py-2"></td>
                                            <td className="lg:table-cell hidden px-2 py-1 md:py-2"></td>
                                            <td className="px-2 py-1 md:py-2 text-center"></td>
                                        </tr>
                                        {data.filter((parent: any) => parent.code.toString().substr(0, 3) == coa.code && parent.code.toString().length == 5).map((sub: any) =>
                                            <tr key={sub._id} className='bg-gray-50 hover:bg-blue-50'>
                                                <td className="px-2 py-1 md:py-2 text-right">{sub.code}</td>
                                                <td className="px-2 py-1 md:py-2" title={sub.description}>{sub.name}</td>
                                                <td className="lg:table-cell hidden px-2 py-1 md:py-2 text-right">
                                                    {transactions.filter((trx: any) => trx._id == sub.code).map((balance: any) => <Fragment key={balance._id}>{FormatMoney(balance.debit.$numberDecimal)}</Fragment>)}
                                                </td>
                                                <td className="lg:table-cell hidden px-2 py-1 md:py-2 text-right">
                                                    {transactions.filter((trx: any) => trx._id == sub.code).map((balance: any) => <Fragment key={balance._id}>{FormatMoney(balance.credit.$numberDecimal)}</Fragment>)}
                                                </td>
                                                <td className="px-2 py-1 md:py-2 text-right">
                                                    {!isPeriod &&
                                                        finalBalances.filter((final: any) => final._id == sub.code).map((balance: any) =>
                                                            <Fragment key={balance._id}>{FormatMoney(balance.balance.$numberDecimal)}</Fragment>
                                                        )}
                                                    {!isPeriod && finalBalances.filter((final: any) => final._id == sub.code).map((balance: any) => { equity = Number(equity) + Number(balance.balance.$numberDecimal) })}
                                                    {isPeriod && transactions.filter((trx: any) => trx._id == sub.code).map((trx: any) =>
                                                        <Fragment key={trx._id}>{FormatMoney(trx.credit.$numberDecimal - trx.debit.$numberDecimal)}</Fragment>
                                                    )}
                                                    {isPeriod && transactions.filter((trxs: any) => trxs._id == sub.code).map((trx: any) => { equity = Number(equity) + Number(trx.credit.$numberDecimal) + Number(trx.debit.$numberDecimal) })}
                                                </td>
                                            </tr>
                                        )}
                                        {coa.code == "303" &&
                                            <tr className='bg-gray-50 hover:bg-blue-50 font-normal'>
                                                {!isPeriod && finalBalances.filter((final: any) => final._id.toString().substr(0, 1) == 4).map((rev: any) => { revenue = Number(revenue) + Number(rev.balance.$numberDecimal) })}
                                                {!isPeriod && finalBalances.filter((final: any) => final._id.toString().substr(0, 1) == 7).map((rev: any) => { revenue = Number(revenue) + Number(rev.balance.$numberDecimal) })}
                                                {!isPeriod && finalBalances.filter((final: any) => final._id.toString().substr(0, 1) == 5).map((rev: any) => { expense = Number(expense) + Number(rev.balance.$numberDecimal) })}
                                                {!isPeriod && finalBalances.filter((final: any) => final._id.toString().substr(0, 1) == 6).map((rev: any) => { expense = Number(expense) + Number(rev.balance.$numberDecimal) })}


                                                {isPeriod && transactions.filter((trxs: any) => trxs._id.toString().substr(0, 1) == 4).map((trx: any) => { revenue = Number(revenue) + Number(trx.debit.$numberDecimal - trx.credit.$numberDecimal) })}
                                                {isPeriod && transactions.filter((trxs: any) => trxs._id.toString().substr(0, 1) == 7).map((trx: any) => { revenue = Number(revenue) + Number(trx.debit.$numberDecimal - trx.credit.$numberDecimal) })}
                                                {isPeriod && transactions.filter((trxs: any) => trxs._id.toString().substr(0, 1) == 5).map((trx: any) => { expense = Number(expense) + Number(trx.debit.$numberDecimal - trx.credit.$numberDecimal) })}
                                                {isPeriod && transactions.filter((trxs: any) => trxs._id.toString().substr(0, 1) == 6).map((trx: any) => { expense = Number(expense) + Number(trx.debit.$numberDecimal - trx.credit.$numberDecimal) })}
                                                <td className="px-2 py-1 md:py-2 text-right">30301</td>
                                                <td className="px-2 py-1 md:py-2">LABA RUGI TOTAL</td>
                                                <td className="lg:table-cell hidden px-2 py-1 md:py-2 text-right">
                                                    {transactions.filter((trxs: any) => trxs._id.toString().substr(0, 1) == 4 || trxs._id.toString().substr(0, 1) == 7).map((trx: any) => { tRevDebit = Number(tRevDebit) + Number(trx.debit.$numberDecimal) })}
                                                    {transactions.filter((trxs: any) => trxs._id.toString().substr(0, 1) == 5 || trxs._id.toString().substr(0, 1) == 6).map((trx: any) => { tExpDebit = Number(tExpDebit) + Number(trx.debit.$numberDecimal) })}
                                                    {FormatMoney(tRevDebit + tExpDebit)}
                                                </td>
                                                <td className="lg:table-cell hidden px-2 py-1 md:py-2 text-right">
                                                    {transactions.filter((trxs: any) => trxs._id.toString().substr(0, 1) == 4 || trxs._id.toString().substr(0, 1) == 7).map((trx: any) => { tRevCredit = Number(tRevCredit) + Number(trx.credit.$numberDecimal) })}
                                                    {transactions.filter((trxs: any) => trxs._id.toString().substr(0, 1) == 5 || trxs._id.toString().substr(0, 1) == 6).map((trx: any) => { tExpCredit = Number(tExpCredit) + Number(trx.credit.$numberDecimal) })}
                                                    {FormatMoney(tRevCredit + tExpCredit)}
                                                </td>
                                                <td className="px-2 py-1 md:py-2 text-right">
                                                    {!isPeriod && FormatMoney(revenue - expense)}
                                                    {isPeriod && FormatMoney((Number(tRevCredit + tExpCredit)) - (Number(tRevDebit + tExpDebit)))}

                                                </td>
                                            </tr>
                                        }
                                    </Fragment>
                                )}
                            </tbody>
                        </table>
                        <div className="p-2 text-right font-semibold">Total Equity :
                            {!isPeriod && FormatMoney(equity + revenue - expense)}
                            {isPeriod && FormatMoney(equity + (Number(tRevCredit + tExpCredit) - (Number(tRevDebit + tExpDebit))))}</div>
                    </div>

                </div>
            </div>
        </main>
    )
}