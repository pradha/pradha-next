import DashboardLayout from "@/components/ui/layouts/dashboard"
import { authOptions } from "@/lib/auth/options";
import { getServerSession } from "next-auth";
import { notFound } from "next/navigation";

export default async function RootLayout({ children }: { children: React.ReactNode }) {
    const session = await getServerSession(authOptions)
    if (!session)
        notFound()

    return (
        <DashboardLayout>{children}</DashboardLayout>

    )
}

const title = 'Dashboard'
const description = 'Your Dashboard Page'
const keywords = ["dashboard", "admin", "administrator"]
const url = process.env.SITE_URL + '/dashboard'
export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: false,
        follow: false,
        nocache: false,
        googleBot: {
            index: false,
            follow: false
        },
    },
}