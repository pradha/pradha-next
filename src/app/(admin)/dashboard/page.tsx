'use client'
import { useSession } from "next-auth/react"
import { redirect } from 'next/navigation';
import { Fragment, useEffect, useState } from 'react'
import { DayNameShort, FullDayDate, ISODate, MonthYearShort } from "@/lib/time"
import { Dialog, Transition } from "@headlessui/react";
//import { LineChart } from "@/lib/chart/lineChart";
import dynamic from 'next/dynamic'
import { ArrowsRightLeftIcon, EyeIcon, NewspaperIcon, TagIcon, UserGroupIcon, UserIcon } from "@heroicons/react/24/outline";

const BarChart = dynamic(() => import("@/components/ui/chart/barChart"), {
    ssr: false,
});
const PieChart = dynamic(() => import("@/components/ui/chart/pieChart"), {
    ssr: false,
});
const LineChart = dynamic(() => import("@/components/ui/chart/lineChart"), {
    ssr: false,
});


export default function Page() {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })

    const [monthly, setMonthly] = useState({
        labels: ["Jan 2023", "Feb 2023", "Mar 2023", "Apr 2023", "Mei 2023", "Jun 2023", "Jul 2023", "Aug 2023", "Sept 2023", "Oct 2023", "Nov 2023", "Des 2023"],
        datasets: [{
            label: 'Income',
            data: [1, 6, 4, 6, 8, 10, 11, 12, 10, 8, 16, 10],
            borderWidth: 3,
            tension: 0.8,
            borderColor: "rgba(100, 159,211, 0.9)",
            backgroundColor: "rgba(100, 159,211, 0.9)"
        }, {
            label: 'Expense',
            data: [2, 2, 4, 4, 8, 2, 11, 11, 14, 14, 14, 10],
            borderWidth: 3,
            tension: 0.8,
            borderColor: "rgba(225, 100 ,224, 0.9)",
            backgroundColor: "rgba(225, 100 ,224, 0.9)"
        }]
    })
    const [categories, setCategories] = useState<any[]>([])
    const [posts, setPosts] = useState<any[]>([])
    const [dailyPosts, setDailyPosts] = useState<any[]>([])
    const [users, setUsers] = useState<any[]>([])
    const [groups, setGroups] = useState<any[]>([])
    const [profitLoss, setProfitLoss] = useState<{ labels: Date[], datasets: any[] }>({ labels: [], datasets: [] })

    var loaded = false;
    useEffect(() => {
        if (!loaded) {
            (async () => {
                let req = await fetch('/api/content/stats', {
                    headers: { 'Content-Type': 'application/json' },
                    method: 'GET'
                })
                let res = await req.json()
                setCategories(res.categories)
                setPosts(res.posts)
                setDailyPosts(res.dailyPosts)
            })();
            (async () => {
                let req = await fetch('/api/system/stats', {
                    headers: { 'Content-Type': 'application/json' },
                    method: 'GET'
                })
                let res = await req.json()
                setUsers(res.users)
                setGroups(res.groups)
            })();
            (async () => {
                let req = await fetch('/api/accounting/stats', {
                    headers: { 'Content-Type': 'application/json' },
                    method: 'GET'
                })
                let res = await req.json()

                let pl: { labels: Date[], datasets: any[] } = { labels: [], datasets: [] }
                let date = new Date(new Date().getTime() - 365 * 24 * 60 * 60 * 1000)
                for (var m = 1; m <= 12; m++) {
                    pl.labels.push(new Date(date.setMonth(date.getMonth() + 1)))
                }

                let expense: any[] = []
                let revenue: any[] = []


                revenue = pl.labels.map((item: any) => {
                    const sttmn = res.statement.find((s: any) => (s._id.coa === 4 || s._id.coa === 7) && s._id.month === item.getMonth() + 1 && s._id.year === item.getFullYear())
                    return { ...item, ...sttmn }
                })
                expense = pl.labels.map((item: any) => {
                    const sttmn = res.statement.find((s: any) => (s._id.coa === 5 || s._id.coa === 6) && s._id.month === item.getMonth() + 1 && s._id.year === item.getFullYear())
                    return { ...item, ...sttmn }
                })

                pl.datasets.push({ label: 'Revenue', data: revenue.map((rev:any) => rev?.credit ? Number(rev.credit.$numberDecimal) : 0) })
                pl.datasets.push({ label: 'Expense', data: expense.map((rev:any) => rev?.debit ? Number(rev.debit.$numberDecimal) : 0) })
                setProfitLoss(pl)
            })();
            loaded = true
            return
        }
    }, [])

    let monthInLastYear: Date[] = []
    let date = new Date(new Date().getTime() - 365 * 24 * 60 * 60 * 1000)
    for (var m = 1; m <= 12; m++) {
        monthInLastYear.push(new Date(date.setMonth(date.getMonth() + 1)))
    }
    return (
        <div>
            <h1 className="text-2xl lg:text-3xl"> {session?.user.name ? `Hi...! ${session?.user.name}` : "Please wait..."}</h1>
            <div className="text-sm">Today is {FullDayDate(new Date().toISOString())}</div>

            <div className="flex lg:flex-row flex-col lg:space-x-4 space-x-0 space-y-4 lg:space-y-0 my-4">
                <div className="relative lg:w-2/3 w-full mb-4">
                    <div className="grid grid-cols-2 lg:grid-cols-4 gap-4 mb-4">
                        <div className="rounded-md shadow-md bg-white p-4 dark:bg-slate-700 hover:shadow-lg transition-all ease-in-out">
                            <div className="flex space-x-2 mb-4">
                                <UserGroupIcon width={20} />
                                <span className="font-semibold text-sm">Users</span>
                            </div>
                            <div className="text-xs text-slate-400">Total</div>
                            {users.reduce((accumulator, object) => {
                                return accumulator + object['count'];
                            }, 0)} Users
                        </div>
                        <div className="rounded-md shadow-md bg-white p-4 dark:bg-slate-700 hover:shadow-lg transition-all ease-in-out">
                            <div className="flex space-x-2 mb-4">
                                <UserGroupIcon width={20} />
                                <span className="font-semibold text-sm">Groups</span>
                            </div>
                            <div className="text-xs text-slate-400">Total</div>
                            {groups.reduce((accumulator, object) => {
                                return accumulator + object['count'];
                            }, 0)} Groups
                        </div>
                        <div className="rounded-md shadow-md bg-white p-4 dark:bg-slate-700 hover:shadow-lg transition-all ease-in-out">
                            <div className="flex space-x-2 mb-4">
                                <NewspaperIcon width={20} />
                                <span className="font-semibold text-sm">Posts</span>
                            </div>
                            <div className="text-xs text-slate-400">Total</div>
                            {posts.reduce((accumulator, object) => {
                                return accumulator + object['count'];
                            }, 0)} Posts
                        </div>
                        <div className="rounded-md shadow-md bg-white p-4 dark:bg-slate-700 hover:shadow-lg transition-all ease-in-out">
                            <div className="flex space-x-2 mb-4">
                                <TagIcon width={20} />
                                <span className="font-semibold text-sm">Categories</span>
                            </div>
                            <div className="text-xs text-slate-400">Total</div>
                            {categories.length} {categories.length > 1 ? "Categories" : "Category"}
                        </div>
                    </div>
                    <div className="rounded-md shadow-md bg-white px-4 py-2 dark:bg-slate-700 hover:shadow-lg transition-all ease-in-out">
                        <div className="flex space-x-2 mb-4 border-b border-slate-200 dark:border-slate-600 p-2">
                            <ArrowsRightLeftIcon width={20} />
                            <div className="font-semibold text-sm">Monthly Income & Expense <span className="text-xs font-sans font-normal">(Last 12 Months)</span></div>
                        </div>
                        <BarChart id={"monthly"} labels={profitLoss.labels.map((date:Date) => MonthYearShort(date.toDateString()))} datasets={profitLoss.datasets} />
                    </div>
                </div>
                <div className="relative lg:w-1/3 ">
                    <div className="rounded-md shadow-md bg-white px-4 py-2 dark:bg-slate-700 hover:shadow-lg transition-all ease-in-out mb-4">
                        <div className="flex space-x-2 mb-4 border-b border-slate-200 dark:border-slate-600 p-2">
                            <EyeIcon width={20} />
                            <div className="font-semibold text-sm">Published Posts</div>
                        </div>
                        <LineChart id={"dailyPosts"} labels={dailyPosts.map((daily: any) => DayNameShort(daily._id))} datasets={[{ label: 'Post(s)', data: dailyPosts.map((daily: any) => daily?.total ? daily.total : 0) }]} legend={false} />
                    </div>
                    <div className="rounded-md shadow-md bg-white px-4 py-2 dark:bg-slate-700 hover:shadow-lg transition-all ease-in-out mb-4">
                        <div className="flex space-x-2 mb-4 border-b border-slate-200 dark:border-slate-600 p-2">
                            <TagIcon width={20} />
                            <div className="font-semibold text-sm">Post in Category</div>
                        </div>
                        <PieChart id="categories" labels={categories.map(cat => cat._id)} datasets={[{
                            data: categories.map(cat => cat.count),
                            hoverOffset: 4,
                            borderWidth: 0
                        }]} />
                    </div>
                </div>
            </div>


        </div>
    )
}