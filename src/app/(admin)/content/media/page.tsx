'use client'
import { useSession } from "next-auth/react"
import { redirect } from 'next/navigation';
import { FormEvent, useEffect, useState } from 'react'
import Link from "next/link";
import { PlusIcon, PencilIcon, MagnifyingGlassIcon, XMarkIcon, ChevronDoubleRightIcon, XCircleIcon } from "@heroicons/react/24/outline";
import { Spinner } from "@/components/ui/loader";
import { FullShort } from "@/lib/time";
import { usePathname } from 'next/navigation';
import { Pagination } from "@/components/ui/pagination";
import path from "path";
import Image from "next/image";

export const revalidate = 5;
export const dynamic = 'force-dynamic';
export default function Page(props: any) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })
    const pathName = usePathname()
    let fill: any
    let errors: string[] = []
    const [spinner, setSpinner] = useState(false)
    const [data, setData] = useState(fill)
    const [src, setSrc] = useState(false)
    const [pageNum, setPageNum] = useState(1)


    const search = async (e: FormEvent) => {
        e.preventDefault()
        let target = e.target as HTMLFormElement

        if (target.search.value.length > 0) {
            const res = await fetch(path.posix.join('/api/', pathName ? pathName : "", '/search'), {
                body: JSON.stringify({
                    search: target.search.value
                }),
                headers: {
                    'Content-Type': 'application/json',
                },
                method: 'POST'
            })

            const result = await res.json()
            setData({ data: result.search, count: result.search.length })
            setSrc(true)
        }
    }

    const getData = async (page: number) => {
        setSpinner(true);
        setPageNum(page)
        const data = await fetch('/api/' + (pathName ? pathName : "") + "?page=" + (page ? page : pageNum), {
            headers: { 'Content-Type': 'application/json' },
            method: 'GET'
        })
        setSpinner(false);
        return data.json()
    }

    const getPage = async (page: number) => {
        setSpinner(true);
        let data = await getData(page)
        setData(data)
        setSpinner(false);
    }

    let loaded = false
    useEffect(() => {
        if (!loaded) {
            (async () => {
                let data = await getData(pageNum)
                setData(data)

            })();
            loaded = true
            return
        }
    }, [])

    return (
        <main>
            <div className="flex flex-col justify-between mb-4 space-y-4 md:flex-row md:space-y-0">
                <div>
                    <h1 className="text-xl font-semibold lg:text-2xl">Files & Media</h1>
                    <div className="text-sm text-slate-500">Content Files & Media</div>
                </div>
                <div className="flex items-center space-x-2">
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a3403") &&
                        <Link href={path.posix.join(pathName ? pathName : "", "upload")} title="Upload new Media" className="btn-circle" aria-label="new-media">
                            <PlusIcon width={20} height={20} />
                        </Link>
                    }
                    <form onSubmit={search} className="flex items-center flex-1 bg-white rounded-full h-9">
                        <div className="w-full">
                            <input type="search" id="search" name="search" required className="w-full px-4 py-1 text-sm border-none rounded-full outline-1 focus:ring-0 focus:outline-sky-100" placeholder="Search..." x-model="search" />
                        </div>
                        <div className="-ml-8">
                            <button type="submit" className="flex items-center justify-center w-8 h-8 rounded-full text-slate-100 bg-sky-500" >
                                <MagnifyingGlassIcon width={18} height={18} />
                            </button>
                        </div>
                    </form>
                </div>
            </div>

            <div className="relative w-full text-gray-600 bg-white shadow-lg rounded-tl-3xl rounded-br-3xl dark:bg-slate-700 dark:text-slate-300 transform transition-all ease-in-out">
                {spinner && <Spinner className=" rounded-tl-3xl rounded-br-3xl" />}
                <div className="flex items-center px-6 py-3 space-x-2 text-sm tracking-wide">
                    <div>Total {src ? "Search result" : "Data"} :</div>
                    <div className="font-semibold">{data?.count ? data.count : "0"}</div>
                    <div>Item{data?.count > 1 ? "s" : ""}</div>
                    {src &&
                        <button className="p-1 mx-2 text-slate-100 rounded-md shadow-sm bg-gradient-to-br from-sky-300 to-sky-400 hover:from-sky-300 hover:to-sky-300" onClick={() => { setSrc(false); getPage(pageNum); (document.getElementById("search") as HTMLInputElement).value = "" }} type="button">
                            <XMarkIcon width={16} />
                        </button>}
                </div>
                <div className="relative overflow-x-auto">
                    <div className="grid grid-cols-2 lg:grid-cols-4 xl:grid-cols-6 gap-4 p-6 border-b border-t border-gray-200 dark:border-slate-600">
                        {typeof data !== undefined && data?.data?.map((item: any, i: number) =>
                            <Link href={"/content/media/detail/" + item._id} key={item._id} replace={true} className="relative block group w-full h-24  xl:h-40 lg:h-32 overflow-hidden border  transition-all ease-in-out rounded-lg border-slate-100 dark:border-slate-600 bg-white shadow-md hover:shadow-lg">
                                <Image
                                    className="object-cover object-center w-full xl:h-40 lg:h-32 mx-auto align-middle"
                                    src={process.env.NEXT_PUBLIC_STATIC_FILES + item.path}
                                    alt={item.name}
                                    fill
                                    priority
                                    sizes="(max-width: 768px) 30vw,(max-width: 1200px) 16vw,12vw"
                                />
                                <div className="hidden absolute group-hover:block mx-auto bottom-2 p-2 bg-white text-sm transition-all ease-in-out shadow-md">
                                    {item.name}<br /><span className="text-xs">{parseFloat((item.size / 1000).toString()).toFixed(2)} KB</span>
                                </div>
                            </Link>
                        )}
                    </div>
                    <Pagination count={data?.count} page={data?.page} total={data?.total} click={getPage} />
                </div>
            </div>
        </main>
    )
}