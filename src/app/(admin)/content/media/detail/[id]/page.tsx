'use client'
import { useSession } from "next-auth/react"
import { redirect, usePathname } from 'next/navigation';
import { FormEvent, Fragment, useEffect, useState } from 'react'
import Link from "next/link";
import { UserIcon, KeyIcon, ArrowPathIcon, PencilSquareIcon, ListBulletIcon, PlusIcon, PencilIcon, EyeIcon, EyeSlashIcon, LinkIcon, CheckIcon, XMarkIcon, ClockIcon } from "@heroicons/react/24/outline";
import { Success, Warning } from "@/components/ui/alert";
import { Spinner } from "@/components/ui/loader";
import { ParseHTML } from "@/lib/parseHtml";
import { Dialog, Transition } from "@headlessui/react";
import path from "path";
import { FullDateTime } from "@/lib/time";
import Image from "next/image";

export const revalidate = 0
export const dynamic = 'force-dynamic'
export default function Page(props: any) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })

    const pathName = usePathname()

    let errors = {
        title: ""
    }
    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState({ errors: errors })
    const [success, setSuccess] = useState(props.searchParams?.status && props.searchParams?.status == "updated" ? { title: "Success", description: "Media has been successfully updated" } : { title: "", description: "" })
    const [data, setData] = useState(props.params.data)

    useEffect(() => {
        fetch(path.posix.join('/api/', pathName as string, "../..", "?id=" + props.params.id))
            .then((data) => data.json())
            .then((res) => {
                props.params.data = res.data;
                setData(res.data);
            })
    }, []);

    return (
        <main>
            <div className="flex flex-col md:flex-row md:space-y-0 space-y-4 mb-4 justify-between">
                <div>
                    <h1 className="text-xl lg:text-2xl font-semibold">Media Details</h1>
                    <div className="text-sm text-slate-500">Web Files / Media Details</div>
                </div>

                <div className="flex items-center space-x-2">
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a3403") &&
                        <Link href={"/content/media/upload/"} replace={true} title="Upload Media" className="btn-circle" aria-label="upload-media">
                            <PlusIcon width={20} height={20} />
                        </Link>
                    }
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a3404") &&
                        <Link href={"/content/media/edit/" + props.params.id} replace={true} title="Media Detail" className="btn-circle" aria-label="edit-media">
                            <PencilIcon width={20} height={20} />
                        </Link>
                    }
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a3401") &&
                        <Link href={"/content/media"} replace={true} title="Post data" className="btn-circle" aria-label="media-data">
                            <ListBulletIcon width={20} height={20} />
                        </Link>
                    }
                </div>
            </div>

            <Success data={success} hide={() => setSuccess({ title: "", description: "" })} />

            {spinner && <Spinner className="rounded-tl-3xl rounded-br-3xl" />}
            <div className="relative flex lg:flex-row flex-col lg:space-x-4 space-y-4 lg:space-y-0 space-x-0 items-stretch mb-4 ">

                <div className="relative bg-white shadow-lg w-full rounded-tl-3xl h-64 lg:h-[800px] rounded-br-3xl p-6 dark:bg-slate-700">
                    <Image
                        className="object-cover object-center w-full h-64 lg:h-[800px] rounded-tl-3xl rounded-br-3xl"
                        src={process.env.NEXT_PUBLIC_STATIC_FILES + data.path}
                        alt="Person"
                        fill
                        priority
                        sizes="(max-width: 768px) 100vw,(max-width: 1200px) 50vw,33vw"
                    />
                </div>
                <div className="relative w-full lg:w-1/3 text-gray-700 dark:text-slate-300 ">
                    <div className="sticky top-4 py-6 px-4 bg-white shadow-lg rounded-tl-3xl rounded-br-3xl group dark:bg-slate-700">
                        <div className="mb-2">
                            <label htmlFor="description" className="label mb-0 text-xs">Name</label>
                            {data?.name}
                        </div>
                        <div className="mb-2">
                            <label htmlFor="description" className="label mb-0 text-xs">Description</label>
                            {data?.description}
                        </div>
                        <div className="mb-2">
                            <label htmlFor="original-file-name" className="label mb-0 text-xs">Original File Name</label>
                            {data?.original_file_name}
                        </div>
                        <div className="mb-2">
                            <label htmlFor="type" className="label mb-0 text-xs">Type</label>
                            {data?.type}
                        </div>
                        <div className="mb-2">
                            <label htmlFor="extenstion" className="label mb-0 text-xs">Extension</label>
                            {data?.extension}
                        </div>
                        <div className="mb-2">
                            <label htmlFor="size" className="label mb-0 text-xs">File Size</label>
                            {parseFloat((data.size / 1000).toString()).toFixed(2)} KB
                        </div>
                        <div className="mb-2 text-sm">
                            <label htmlFor="path" className="label mb-0 text-xs">Path</label>
                            <Link href={process.env.NEXT_PUBLIC_STATIC_FILES + data.path} target="_blank" className="link">{process.env.NEXT_PUBLIC_STATIC_FILES + data.path}</Link>
                        </div>
                        <div className="mb-2">
                            <label htmlFor="tags" className="label mb-0 text-xs">Tags</label>
                            <div className="flex flex-wrap">
                                {data?.tags.map((tag: string) => <span key={Math.random()} className="inline-block bg-sky-600 py-1 px-2 text-xs mr-0.5 mt-0.5 text-slate-100 rounded-md">{tag}</span>)}
                            </div>
                        </div>

                        <div className="mb-2">
                            <label htmlFor="time" className="label text-xs">Time Info</label>
                            <div className="">
                                <div className="flex space-x-2"><ClockIcon width={16} className="mr-2" /> Uploaded</div>
                                <div className="ml-6">
                                    <div className="text-xs">{FullDateTime(data.uploaded.at)}</div>
                                    <div className="text-sm font-semibold">{data.uploaded.by.name}</div>
                                </div>
                            </div>
                            {data?.updated && data?.updated?.by &&
                                <div className="">
                                    <div className="flex space-x-2"><ClockIcon width={16} className="mr-2" /> Updated</div>
                                    <div className="ml-6">
                                        <div className="text-xs">{FullDateTime(data.updated.at)}</div>
                                        <div className="text-sm font-semibold">{data.updated.by.name}</div>
                                    </div>
                                </div>
                            }
                        </div>

                    </div>
                </div>

            </div >
        </main >
    )
}