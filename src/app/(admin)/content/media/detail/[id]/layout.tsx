import DashboardLayout from "@/components/ui/layouts/dashboard"
import clientPromise from "@/lib/connections/mongo";
import { ObjectId } from "mongodb";
import { getServerSession } from "next-auth";
import { notFound, redirect } from "next/navigation";

export const revalidate = 0
export const dynamic = 'force-dynamic'
export default async function RootLayout(props:any) {
    const session = await getServerSession();
    if (!session)
        redirect('/');
    if (!ObjectId.isValid(props.params.id))
        notFound()

    const db = (await clientPromise).db()
    const data = await db.collection("v_content_media").findOne({_id: new ObjectId(props.params.id)})

    if (!data)
        notFound()

    props.params.data=JSON.parse(JSON.stringify(data))
    return (
        <>{props.children}</>
    )
}

const title = 'Media Detail'
const description = 'Web File / Media Detail'
const keywords = ["web media", "media", "web content media", "media", "content media", "detail"]
const url = process.env.SITE_URL + '/content/media/detail/'
export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: false,
        follow: false,
        nocache: false,
        googleBot: {
            index: false,
            follow: false
        },
    },
}