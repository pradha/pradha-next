'use client'
import { useSession } from "next-auth/react"
import { redirect } from 'next/navigation';
import { FormEvent, useState } from 'react'
import Link from "next/link";
import { UserIcon, KeyIcon, ArrowPathIcon, ListBulletIcon, CloudArrowUpIcon } from "@heroicons/react/24/outline";
import { Success, Warning } from "@/components/ui/alert";
import { Spinner } from "@/components/ui/loader";
import CreatableSelect from "react-select/creatable";
import Image from "next/image";

export const revalidate = 5;
export const dynamic = 'force-dynamic';
export default function Page() {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })

    let errors = {
        image: "",
        name: "",
        description: "",
        tags: ""
    }
    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState({ errors: errors })
    const [success, setSuccess] = useState({ title: "", description: "" })
    const [tags, setTags] = useState<any[]>([])
    const [thumbnail, setThumbnail] = useState<string>("/img/no-image.png")

    const previewImage = (e: FormEvent) => {
        var reader = new FileReader();
        let target = e.target as HTMLInputElement
        if (target.files) {
            if (!target.files[0].type.match(/image\/(png|jpg|jpeg|webp)/i)) {
                alert("Image mime type is not valid");
                return;
            }
            reader.onloadend = (e) => {
                setThumbnail(reader.result as string)
            }
            const fileData = reader.readAsDataURL(target.files[0])
        }

    }
    const submit = async (e: FormEvent) => {
        e.preventDefault()
        setWarning({ errors: errors })
        setSpinner(true)
        setSuccess({ title: "", description: "" })

        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement

        const body = new FormData()
        const file = target.file.files[0]
        if (file) {
            body.append("name", (document.getElementById("name") as HTMLInputElement).value)
            body.append("description", target.description.value)
            body.append("tags", JSON.stringify(tags.map((v: any) => v.value)))
            body.append("file", file)
            const submit = await fetch('/api/content/media', {
                method: "POST",
                body
            });
            try {
                const res = await submit.json()
                if (submit.ok) {
                    (document.getElementById("name") as HTMLInputElement).value = ""
                    target.description.value = ""
                    setTags([])
                    setThumbnail("/img/no-image.png")
                    setSuccess({ title: "Success", description: "Media has been successfully uploaded" })
                } else {
                    if (await res.errors) errors = await res.errors
                    setWarning({ errors: errors })
                }
            } catch (err: any) {
                errors.image = await err.message
                setWarning({ errors: errors })
            }
        } else {
            errors.image = "no input element"
            setWarning({ errors: errors })
        }
        setSpinner(false)
    }
    return (
        <main>
            <div className="flex flex-col md:flex-row md:space-y-0 space-y-4 mb-4 justify-between">
                <div>
                    <h1 className="text-xl lg:text-2xl font-semibold">Upload File</h1>
                    <div className="text-sm text-slate-500">Upload new File or Media</div>
                </div>
                <div className="flex items-center space-x-2">
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a3401") &&
                        <Link href={"/content/media"} title="Media data" className="btn-circle" aria-label="media-data">
                            <ListBulletIcon width={20} height={20} />
                        </Link>
                    }
                </div>
            </div>

            <Success data={success} hide={() => setSuccess({ title: "", description: "" })} />

            <Warning className="" errors={Object.values(warning.errors).filter(a => a != "")} hide={() => setWarning({ errors: errors })} />
            <form onSubmit={submit} className="relative flex lg:flex-row flex-col lg:space-x-4 space-y-4 lg:space-y-0 space-x-0 items-stretch mb-4 ">

                <div className="relative flex-1 group bg-white shadow-lg w-full rounded-tl-3xl h-64 lg:h-[500px] rounded-br-3xl dark:bg-slate-700">
                    <img
                        className="object-contain object-center w-full h-64 lg:h-[500px] rounded-tl-3xl rounded-br-3xl"
                        src={thumbnail}
                        alt="File"
                    />
                    <button type="button" onClick={() => (document.getElementById("file") as HTMLInputElement).click()} className="hidden absolute group-hover:block mx-auto bottom-1/2 top-1/2 left-1/2 right-1/ h-16 transform -translate-x-1/2 text-center w-32 border border-gray-500 py-2 px-1 rounded-lg shadow bg-gray-200 text-sm dark:text-slate-600">Select File</button>
                    <input type="file" id="file" name="file" onChange={previewImage} className="hidden" accept="image/png, image/gif, image/jpeg, image/webp" />
                </div>
                <div className="relative w-full lg:w-1/2 text-gray-700 dark:text-slate-300 ">
                    <div className="sticky top-4 py-6 px-4 bg-white shadow-lg rounded-tl-3xl rounded-br-3xl group dark:bg-slate-700">
                        {spinner && <Spinner className="rounded-tl-3xl rounded-br-3xl" />}


                        <div className="mb-2">
                            <label htmlFor="description" className="label">Name <sup className="text-rose-500">*</sup></label>
                            <div className="relative mb-2">

                                <input type="name" id="name" name='name' className={warning?.errors.name ? "input-error" : "input"} placeholder="Media Name" />
                            </div>
                        </div>
                        <div className="mb-2">
                            <label htmlFor="description" className="label">Description <sup className="text-rose-500">*</sup></label>
                            <textarea name="description" id="description" placeholder="Media Description" className={warning?.errors.description ? "input-error" : "input"} rows={3}></textarea>
                        </div>
                        <div className="mb-4">
                            <label htmlFor="tags" className="label">Media Tags <sup className="text-rose-500">*</sup></label>
                            <CreatableSelect
                                name="tags"
                                instanceId={"tags"}
                                noOptionsMessage={() => null}
                                components={{ IndicatorSeparator: null }}
                                isSearchable
                                isClearable
                                isMulti
                                defaultValue={tags}
                                onChange={(choice: any) => setTags(choice)}
                                options={tags}
                                className="react-select" classNamePrefix="react-select"
                            />
                        </div>
                        <div className="">
                            <button type="submit" className="btn btn-primary flex items-center space-x-2">
                                <CloudArrowUpIcon width={20} height={20} className="mr-2" />
                                Upload Media
                            </button>
                        </div>



                    </div>
                </div>

            </form >
        </main>
    )
}