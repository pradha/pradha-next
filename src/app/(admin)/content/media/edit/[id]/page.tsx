'use client'
import { useSession } from "next-auth/react"
import { redirect, usePathname } from 'next/navigation';
import { FormEvent, Fragment, useEffect, useState } from 'react'
import Link from "next/link";
import { UserIcon, KeyIcon, ArrowPathIcon, PencilSquareIcon, ListBulletIcon, PlusIcon, PencilIcon, EyeIcon, EyeSlashIcon, LinkIcon, CheckIcon, XMarkIcon, ClockIcon, IdentificationIcon } from "@heroicons/react/24/outline";
import { Success, Warning } from "@/components/ui/alert";
import { Spinner } from "@/components/ui/loader";
import { ParseHTML } from "@/lib/parseHtml";
import { Dialog, Transition } from "@headlessui/react";
import path from "path";
import { FullDateTime } from "@/lib/time";
import Image from "next/image";
import CreatableSelect from "react-select/creatable";

export const revalidate = 0
export const dynamic = 'force-dynamic'
export default function Page(props: any) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })

    const pathName = usePathname()

    let errors = {
        name: "",
        description: "",
        tags: ""
    }
    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState({ errors: errors })
    const [success, setSuccess] = useState(props.searchParams?.status && props.searchParams?.status == "published" ? { title: "Success", description: "Post has been successfully published" } : { title: "", description: "" })
    const [data, setData] = useState(props.params.data)
    const [tags, setTags] = useState(data?.tags.map((tag: string) => ({ label: tag, value: tag })))

    useEffect(() => {
        fetch(path.posix.join('/api/', pathName as string, "../..", "?id=" + props.params.id))
            .then((data) => data.json())
            .then((res) => {
                props.params.data = res.data;
                setData(res.data);
                (document.getElementById('name') as HTMLInputElement).value = res.data.name;
                (document.getElementById('description') as HTMLInputElement).value = res.data.description;

                setTags(res.data.tags.map((tag: string) => ({ label: tag, value: tag })));
            })
    }, []);

    const submit = async (e: FormEvent) => {
        e.preventDefault()
        setWarning({ errors: errors })
        setSpinner(true)

        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        let target = e.target as HTMLFormElement
        const submit = await fetch(path.posix.join("/api/", pathName as string, '../..'), {
            method: 'PUT',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                id: data._id,
                name: (document.getElementById("name") as HTMLInputElement).value,
                description: target.description.value,
                tags: tags.map((v: any) => v.value)
            })
        })
        let res = await submit.json()
        if (submit.ok) {
            setData(res.data)
            props.params.data = res.data
            //window.location.replace(path.posix.join(pathName as string, '../..', 'detail', res.data._id + "?status=updated"))
            setSuccess({ title: "Success", description: "Media has been successfully updated" })
        } else {
            errors = await res.errors
            setWarning({ errors: errors })
            errors = {
                name: "",
                description: "",
                tags: ""
            }
        }
        setSpinner(false)
    }
    return (
        <main>
            <div className="flex flex-col md:flex-row md:space-y-0 space-y-4 mb-4 justify-between">
                <div>
                    <h1 className="text-xl lg:text-2xl font-semibold">Edit Media</h1>
                    <div className="text-sm text-slate-500">Edit File / Media Information</div>
                </div>

                <div className="flex items-center space-x-2">
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a3403") &&
                        <Link href={"/content/media/upload/"} replace={true} title="Create Media" className="btn-circle" aria-label="create-media">
                            <PlusIcon width={20} height={20} />
                        </Link>
                    }
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a3405") &&
                        <Link href={"/content/media/detail/" + props.params.id} replace={true} title="Media Detail" className="btn-circle" aria-label="media-detail">
                            <IdentificationIcon width={20} height={20} />
                        </Link>
                    }
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a3401") &&
                        <Link href={"/content/media"} replace={true} title="Media data" className="btn-circle" aria-label="media-data">
                            <ListBulletIcon width={20} height={20} />
                        </Link>
                    }
                </div>
            </div>

            <Success data={success} hide={() => setSuccess({ title: "", description: "" })} />

            <Warning className="" errors={Object.values(warning.errors).filter(a => a != "")} hide={() => setWarning({ errors: errors })} />
            <div className="relative flex lg:flex-row flex-col lg:space-x-4 space-y-4 lg:space-y-0 space-x-0 items-stretch mb-4 ">

                <div className="relative bg-white shadow-lg w-full rounded-tl-3xl h-64 lg:h-[800px] rounded-br-3xl p-6 dark:bg-slate-700">
                    <Image
                        className="object-cover object-center w-full h-64 lg:h-[800px] rounded-tl-3xl rounded-br-3xl"
                        src={process.env.NEXT_PUBLIC_STATIC_FILES + data.path}
                        alt="Person"
                        fill
                        priority
                        sizes="(max-width: 768px) 100vw,(max-width: 1200px) 50vw,33vw"
                    />
                </div>
                <div className="relative w-full lg:w-1/3 text-gray-700 dark:text-slate-300 ">
                    <div className="sticky top-4 py-6 px-4 bg-white shadow-lg rounded-tl-3xl rounded-br-3xl group dark:bg-slate-700">
                        {spinner && <Spinner className="rounded-tl-3xl rounded-br-3xl" />}


                        <form onSubmit={submit}>
                            <div className="mb-2">
                                <label htmlFor="description" className="label">Name <sup className="text-rose-500">*</sup></label>
                                <div className="relative mb-2">

                                    <input type="name" id="name" name='name' className={warning?.errors.name ? "input-error" : "input"} defaultValue={data.name} placeholder="Media Name" />
                                </div>
                            </div>
                            <div className="mb-2">
                                <label htmlFor="description" className="label">Description <sup className="text-rose-500">*</sup></label>
                                <textarea name="description" id="description" placeholder="Media Description" defaultValue={data?.description} className={warning?.errors.description ? "input-error" : "input"} rows={3}></textarea>
                            </div>
                            <div className="mb-4">
                                <label htmlFor="tags" className="label">Media Tags <sup className="text-rose-500">*</sup></label>
                                <CreatableSelect
                                    name="tags"
                                    instanceId={"tags"}
                                    noOptionsMessage={() => null}
                                    components={{ IndicatorSeparator: null }}
                                    isSearchable
                                    isClearable
                                    isMulti
                                    defaultValue={tags}
                                    onChange={(choice: any) => setTags(choice)}
                                    options={[]}
                                    className="react-select" classNamePrefix="react-select"
                                />
                            </div>
                            <div className="">
                                <button type="submit" className="btn btn-primary flex items-center space-x-2">
                                    <ArrowPathIcon width={20} height={20} className="mr-2" />
                                    Update Media
                                </button>
                            </div>

                        </form>

                    </div>
                </div>

            </div >
        </main >
    )
}