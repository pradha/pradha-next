import DashboardLayout from "@/components/ui/layouts/dashboard"
import clientPromise from "@/lib/connections/mongo";
import { getServerSession } from "next-auth";
import { redirect } from "next/navigation";

export const revalidate = 0
export const dynamic = 'force-dynamic'
export default async function RootLayout(props:any) {
    const session = await getServerSession();
    if (!session)
        redirect('/');

    const db = (await clientPromise).db()
    const parents = JSON.parse(JSON.stringify(await db.collection("v_content_categories").find({ $or: [{ parent: null }, { parent: { $size: 0 } }] }).toArray()));
    props.params.parents=parents
    return (
        <>{props.children}</>
        
    )
}

const title = 'New Category'
const description = 'Create New Content Categories'
const keywords = ["category", "content", "content categories", "category", "create"]
const url = process.env.SITE_URL + '/content/category'
export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: false,
        follow: false,
        nocache: false,
        googleBot: {
            index: false,
            follow: false
        },
    },
}