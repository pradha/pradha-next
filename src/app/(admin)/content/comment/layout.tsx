import DashboardLayout from "@/components/ui/layouts/dashboard"
import { getServerSession } from "next-auth";
import { redirect } from "next/navigation";

export default async function RootLayout(props:any) {
    const session = await getServerSession();
    if (!session)
        redirect('/');
        
    return (
        <DashboardLayout>{props.children}</DashboardLayout>
    )
}

const title = 'Comments'
const description = 'Content Comments'
const keywords = ["comment", "content", "content comment", "comments"]
const url = process.env.SITE_URL + '/content/comment'
export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: false,
        follow: false,
        nocache: false,
        googleBot: {
            index: false,
            follow: false
        },
    },
}