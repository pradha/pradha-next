
'use client'
import { useSession } from "next-auth/react"
import { redirect } from 'next/navigation';
import { FormEvent, useState } from 'react'
import Link from "next/link";
import { UserIcon, KeyIcon, ArrowPathIcon, PencilSquareIcon, ListBulletIcon } from "@heroicons/react/24/outline";
import { Warning } from "@/components/ui/alert";
import { Spinner } from "@/components/ui/loader";

export default function Page() {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })

    let errors: string[] = []
    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState(errors)
    
    const submit = async (e: FormEvent) => {
        e.preventDefault()
        setWarning(errors)
        setSpinner(true)

        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

       // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement
    }
    return (
        <main>
            <div className="flex flex-col md:flex-row md:space-y-0 space-y-4 mb-4 justify-between">
                <div>
                    <h1 className="text-xl lg:text-2xl font-semibold">Comment Details</h1>
                    <div className="text-sm text-slate-500">Post Content Details</div>
                </div>
                <div className="flex items-center space-x-2">
                    <Link href={"/content/comment/"} title="Comment Data" className="btn-circle" aria-label="comment-data">
                        <ListBulletIcon width={20} height={20} />
                    </Link>
                </div>
            </div>

            <div className="relative bg-white shadow-lg w-full rounded-tl-3xl rounded-br-3xl p-6 dark:bg-slate-700">
                {spinner && <Spinner className="rounded-tl-3xl rounded-br-3xl" />}
                <form onSubmit={submit} className="ng-untouched ng-pristine ng-valid">
                    <Warning className="" errors={warning} hide={() => setWarning(errors)} />
                    
                </form>
            </div>
        </main>
    )
}