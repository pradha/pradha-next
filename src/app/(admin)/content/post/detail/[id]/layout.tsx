import DashboardLayout from "@/components/ui/layouts/dashboard"
import clientPromise from "@/lib/connections/mongo";
import { ObjectId } from "mongodb";
import { getServerSession } from "next-auth";
import { notFound, redirect } from "next/navigation";

export const revalidate = 0
export const dynamic = 'force-dynamic'
export default async function RootLayout(props:any) {
    const session = await getServerSession();
    if (!session)
        redirect('/');
    if (!ObjectId.isValid(props.params.id))
        notFound()

    const db = (await clientPromise).db()
    const data = await db.collection("v_content_posts").findOne({_id: new ObjectId(props.params.id)})
    if (!data)
        notFound()

    const categories = JSON.parse(JSON.stringify(await db.collection("v_content_categories").find().toArray()));
    props.params.categories=categories
    props.params.data=JSON.parse(JSON.stringify(data))
    return (
        <>{props.children}</>
    )
}

const title = 'Post Detail'
const description = 'Web Content Post Detail'
const keywords = ["web content", "post", "web content post", "content", "create post", "detail"]
const url = process.env.SITE_URL + '/content/post/detail/'
export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: false,
        follow: false,
        nocache: false,
        googleBot: {
            index: false,
            follow: false
        },
    },
}