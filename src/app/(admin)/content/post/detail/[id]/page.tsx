'use client'
import { useSession } from "next-auth/react"
import { redirect, usePathname } from 'next/navigation';
import { FormEvent, Fragment, useState } from 'react'
import Link from "next/link";
import { UserIcon, KeyIcon, ArrowPathIcon, PencilSquareIcon, ListBulletIcon, PlusIcon, PencilIcon, EyeIcon, EyeSlashIcon, LinkIcon, CheckIcon, XMarkIcon, ClockIcon } from "@heroicons/react/24/outline";
import { Success, Warning } from "@/components/ui/alert";
import { Spinner } from "@/components/ui/loader";
import { ParseHTML } from "@/lib/parseHtml";
import { Dialog, Transition } from "@headlessui/react";
import path from "path";
import { FullDateTime } from "@/lib/time";
import Image from "next/image";

export default function Page(props: any) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })

    const pathName = usePathname()

    let errors = {
        title: ""
    }
    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState({ errors: errors })
    const [success, setSuccess] = useState(props.searchParams?.status && props.searchParams?.status == "published" ? { title: "Success", description: "Post has been successfully published" } : props.searchParams?.status == "updated" ? { title: "Success", description: "Post has been successfully updated" } : { title: "", description: "" })
    const [publish, setPublish] = useState(false)
    const [data, setData] = useState(props.params.data)
    const [thumbnail, setThumbnail] = useState(data?.thumbnail ? process.env.NEXT_PUBLIC_STATIC_FILES + data.thumbnail : "/img/no-image.png")

    const submit = async (e: FormEvent) => {
        e.preventDefault()
        setWarning({ errors: errors })
        setSpinner(true)
        setPublish(false)

        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        const submit = await fetch(path.posix.join("/api/", pathName as string, '../..'), {
            method: 'OPTIONS',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                id: data._id,
            })
        })
        let res = await submit.json()
        if (submit.ok) {
            setData(res.data)
            setSuccess({ title: "Success", description: "Post has been successfully published" })
        } else {
            errors = await res.errors
            setWarning({ errors: errors })
            errors = {
                title: ""
            }
        }
        setSpinner(false)
    }
    return (
        <main>
            <div className="flex flex-col md:flex-row md:space-y-0 space-y-4 mb-4 justify-between">
                <div>
                    <h1 className="text-xl lg:text-2xl font-semibold">Post Details</h1>
                    <div className="text-sm text-slate-500">Post Content Details</div>
                </div>

                <div className="flex items-center space-x-2">
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a3206") && data.status == "Draft" &&
                        <>
                            <button type="button" title="Create Post" className="btn-circle" onClick={() => setPublish(true)} aria-label="publish-post">
                                <EyeIcon width={20} height={20} />
                            </button>
                            <Transition appear show={publish} as={Fragment}>
                                <Dialog as="div" className="relative z-10" onClose={() => setPublish(false)}>
                                    <Transition.Child
                                        as={Fragment}
                                        enter="ease-out duration-300"
                                        enterFrom="opacity-0"
                                        enterTo="opacity-100"
                                        leave="ease-in duration-200"
                                        leaveFrom="opacity-100"
                                        leaveTo="opacity-0"
                                    >
                                        <div className="fixed inset-0 bg-white dark:bg-black dark:bg-opacity-75 blur-lg bg-opacity-75" />
                                    </Transition.Child>

                                    <div className="fixed inset-0 overflow-y-auto">
                                        <div className="flex min-h-full items-center justify-center p-4 text-center">
                                            <Transition.Child
                                                as={Fragment}
                                                enter="ease-out duration-300"
                                                enterFrom="opacity-0 scale-95"
                                                enterTo="opacity-100 scale-100"
                                                leave="ease-in duration-200"
                                                leaveFrom="opacity-100 scale-100"
                                                leaveTo="opacity-0 scale-95"
                                            >
                                                <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-2xl bg-white p-6 text-left align-middle shadow-xl transition-all">
                                                    <Dialog.Title
                                                        as="h3"
                                                        className="text-lg flex justify-between font-medium leading-6 text-gray-900"
                                                    >
                                                        Publish Post
                                                        <XMarkIcon title="close" width={16} className="cursor-pointer  text-slate-400" onClick={() => setPublish(false)} />
                                                    </Dialog.Title>
                                                    <div className="mt-2">
                                                        <p className="text-sm text-gray-500">
                                                            Are you sure you want to publish this article :
                                                        </p>
                                                        <blockquote className="italic font-semibold m-4">&quot;{data.title}&quot;</blockquote>
                                                    </div>

                                                    <form onSubmit={submit} className="flex space-x-2 justify-end mt-4">
                                                        <button className="btn btn-primary flex items-center space-x-2">
                                                            <CheckIcon width={20} height={20} className="mr-2" />
                                                            Confirm
                                                        </button>
                                                        <button
                                                            type="button"
                                                            className="inline-flex justify-center rounded-md border border-transparent bg-blue-100 px-4 py-2 text-sm font-medium text-blue-900 hover:bg-blue-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-blue-500 focus-visible:ring-offset-2"
                                                            onClick={() => setPublish(false)}
                                                        >
                                                            Close
                                                        </button>
                                                    </form>
                                                </Dialog.Panel>
                                            </Transition.Child>
                                        </div>
                                    </div>
                                </Dialog>
                            </Transition>
                        </>

                    }
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a3203") &&
                        <Link href={"/content/post/create/"} title="Create Post" className="btn-circle" aria-label="create-post">
                            <PlusIcon width={20} height={20} />
                        </Link>
                    }
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a3204") &&
                        <Link href={"/content/post/edit/" + props.params.id} title="Post Detail" className="btn-circle" aria-label="post-edit">
                            <PencilIcon width={20} height={20} />
                        </Link>
                    }
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a3201") &&
                        <Link href={"/content/post"} title="Post data" className="btn-circle" aria-label="post-data">
                            <ListBulletIcon width={20} height={20} />
                        </Link>
                    }
                </div>
            </div>

            <Success data={success} hide={() => setSuccess({ title: "", description: "" })} />
            <div className="relative">
                {spinner && <Spinner className="rounded-tl-3xl rounded-br-3xl" />}
                <div className="relative flex lg:flex-row flex-col lg:space-x-4 space-x-0 items-stretch mb-4 ">

                    <div className="relative bg-white shadow-lg w-full rounded-tl-3xl rounded-br-3xl p-6 dark:bg-slate-700">
                        <article className="w-full dark:text-slate-300">
                            <h1 className="text-3xl font-semibold">{data?.title}</h1>
                            <div className="mb-4">
                                {ParseHTML(data?.content)}
                            </div>
                        </article>
                        <div className="mb-4 flex flex-wrap">
                            {data?.tags.map((tag: string) => <span key={Math.random()} className="inline-block bg-sky-600 py-1 px-2 text-xs mr-0.5 mt-0.5 text-slate-100 rounded-md">{tag}</span>)}
                        </div>
                    </div>
                    <div className="relative w-full lg:w-1/3 text-gray-700 dark:text-slate-300 ">
                        <div className="sticky top-4 py-6 px-4 bg-white shadow-lg rounded-tl-3xl rounded-br-3xl group dark:bg-slate-700">
                            <div className="mb-4">
                                <label htmlFor="description" className="label">Description <sup className="text-rose-500">*</sup></label>
                                {data?.description}
                            </div>
                            <div className="mb-4 ">
                                <label htmlFor="category" className="label">Category <sup className="text-rose-500">*</sup></label>
                                <div className="flex flex-wrap border border-slate-300 rounded-md py-2  dark:border-slate-600">
                                    {props?.params?.categories.map((cat: any, i: number) => {
                                        if (!cat.parent)
                                            return <div key={cat._id} className="mx-2 flex-1 rounded-md">
                                                <div className="flex items-center mb-1">
                                                    <input type="checkbox" id={cat._id} name="category" value={cat._id} placeholder={cat.name} defaultChecked={data.categories.map((cat: any) => cat._id).includes(cat._id)} className="checkbox" />
                                                    <label htmlFor={cat._id} className="pl-2 text-xs font-medium text-gray-600 dark:text-gray-400 cursor-pointer">{cat.name}</label>
                                                </div>

                                                {props?.params?.categories.map((sub: any, i: number) => {
                                                    if (sub.parent?._id == cat._id)
                                                        return <div key={sub._id} className="flex items-center ml-6 mb-1">
                                                            <input type="checkbox" id={sub._id} name="category" value={sub._id} placeholder={sub.name} defaultChecked={data.categories.includes(cat._id)} className="checkbox" />
                                                            <label htmlFor={sub._id} className="pl-2 text-xs font-medium text-gray-600 dark:text-gray-400 cursor-pointer">{sub.name}</label>
                                                        </div>
                                                })}
                                            </div>
                                    })}
                                </div>
                            </div>
                            <div className="mb-4">
                                <label htmlFor="thumbnail" className="label">Thumbnail <sup className="text-rose-500">*</sup></label>
                                <div className="mb-2">
                                    <div className="relative group mx-auto mt-2 h-48 rounded-md border-dashed border-2 w-full border-gray-300 overflow-hidden">
                                        <Image
                                            className="object-cover object-center w-full h-48 rounded-tl-3xl rounded-br-3xl"
                                            src={thumbnail}
                                            alt="Person"
                                            fill
                                            priority
                                            sizes="(max-width: 768px) 100vw,(max-width: 1200px) 50vw,33vw"
                                        />
                                    </div>
                                    Caption : <span className="text-slate-500">{data?.thumbnail_caption}</span>
                                </div>
                            </div>
                            <div className="mb-4">
                                <label htmlFor="slug" className="label">Slug <sup className="text-rose-500">*</sup></label>
                                <div className="flex space-x-2 flex-row items-center content-center">
                                    <LinkIcon width={16} />
                                    <Link title="go to article" target="_blank" href={"/read/" + data?.slug}>/read/{data?.slug}</Link>
                                </div>
                            </div>
                            <div className="mb-4">
                                <label htmlFor="slug" className="label">Status</label>
                                <div className="flex space-x-2 flex-row items-center content-center">
                                    {data?.status == "Published" && <EyeIcon className="w-5 h-5  mr-2" />}
                                    {data?.status != "Published" && <EyeSlashIcon className="w-5 h-5  mr-2" />}
                                    {data?.status}
                                </div>
                                {data?.status == "Published" && <><div className="text-xs ml-7">{FullDateTime(data.published.at)}</div><div className="text-sm ml-7 font-semibold">{data.published.by.name}</div></>}
                            </div>
                            <div className="mb-4">
                                <label htmlFor="slug" className="label">Time Info</label>
                                <div className="">
                                    <div className="flex space-x-2"><ClockIcon width={16} className="mr-2" /> Created</div>
                                    <div className="ml-6">
                                        <div className="text-xs">{FullDateTime(data.created.at)}</div>
                                        <div className="text-sm font-semibold">{data.created.by.name}</div>
                                    </div>
                                </div>
                                {data?.updated && data?.updated?.by &&
                                    <div className="">
                                        <div className="flex space-x-2"><ClockIcon width={16} className="mr-2" /> Updated</div>
                                        <div className="ml-6">
                                            <div className="text-xs">{FullDateTime(data.updated.at)}</div>
                                            <div className="text-sm font-semibold">{data.updated.by.name}</div>
                                        </div>
                                    </div>
                                }
                            </div>

                            <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                                <div className="flex-1 mb-2">
                                    <label className="label" htmlFor="language">Language <sup className="text-rose-500">*</sup></label>
                                    <div className="relative inline-block w-full text-gray-700 dark:text-slate-300">
                                        {data?.language}
                                    </div>
                                </div>
                                <div className="flex-1 mb-2">
                                    <label className="label" htmlFor="name">Comment <sup className="text-rose-500">*</sup></label>
                                    {data?.comment ? <div className="flex"><CheckIcon className="text-green-500 font-bold mr-2" width={24} /> Yes</div> : <div className="flex"><XMarkIcon className="text-green-500 mr-2 font-bold" width={24} /></div>}
                                </div>
                            </div>

                        </div>
                    </div>

                </div >
            </div>
        </main >
    )
}