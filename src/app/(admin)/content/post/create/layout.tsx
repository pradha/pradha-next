import DashboardLayout from "@/components/ui/layouts/dashboard"
import clientPromise from "@/lib/connections/mongo";
import { getServerSession } from "next-auth";
import { redirect } from "next/navigation";

export const revalidate = 0
export const dynamic = 'force-dynamic'
export default async function RootLayout(props:any) {
    const session = await getServerSession();
    if (!session)
        redirect('/');

    const db = (await clientPromise).db()
    const categories = JSON.parse(JSON.stringify(await db.collection("v_content_categories").find().toArray()));
    props.params.categories=categories
    return (
        <>{props.children}</>
    )
}

const title = 'Create New Post'
const description = 'Create New Web Content Posts'
const keywords = ["web content", "post", "web content post", "content", "create post", "create"]
const url = process.env.SITE_URL + '/content/post/create'
export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: false,
        follow: false,
        nocache: false,
        googleBot: {
            index: false,
            follow: false
        },
    },
}