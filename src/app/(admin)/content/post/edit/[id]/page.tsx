'use client'
import { useSession } from "next-auth/react"
import { redirect, usePathname } from 'next/navigation';
import { FormEvent, useEffect, useState } from 'react'
import Link from "next/link";
import { ArrowPathIcon, IdentificationIcon, ListBulletIcon, PlusIcon, TagIcon } from "@heroicons/react/24/outline";
import { Success, Warning } from "@/components/ui/alert";
import { Spinner } from "@/components/ui/loader";
import CreatableSelect from "react-select/creatable";
import dynamic from "next/dynamic";
import { OutputData } from "@editorjs/editorjs";
import path from "path";
import Image from "next/image";

const Editor = dynamic(() => import("@/components/ui/editor"), {
    ssr: false,
});


export default function Page(props: any) {
    const { data: session, status } = useSession({
        required: true,
        onUnauthenticated() {
            redirect('/');
        },
    })

    const pathName = usePathname()

    let errors = {
        title: "",
        content: "",
        tags: "",
        description: "",
        category: "",
        thumbnail: "",
        thumbnail_caption: ""
    }

    const [data, setData] = useState(props.params.data)
    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState({ errors: errors })
    const [success, setSuccess] = useState(props.searchParams?.status && props.searchParams?.status == "created" ? { title: "Success", description: "Your new post has been successfully created" } : props.searchParams?.status && props.searchParams?.status == "updated" ? { title: "Success", description: "Post has been successfully updated" } : { title: "", description: "" })
    const [thumbnail, setThumbnail] = useState(data?.thumbnail ? process.env.NEXT_PUBLIC_STATIC_FILES + data.thumbnail : "/img/no-image.png")
    const [editor, setEditor] = useState<OutputData>(data.content);
    const [tags, setTags] = useState(data.tags.map((tag: string) => ({ label: tag, value: tag })))

    const changeThumbnail = async (e: FormEvent) => {
        setWarning({ errors: errors })
        setSpinner(true);

        const body = new FormData()
        const file = (e.target as HTMLInputElement)
        if (file && file.files) {
            setSpinner(true)
            body.append("file", file.files[0])
            const submit = await fetch('/api/content/image', {
                method: "POST",
                body
            });
            try {
                const res = await submit.json()
                if (submit.ok) {
                    (document.getElementById("thumbnail_text") as HTMLInputElement).value = res.file.url
                    setThumbnail(res.file.url)
                } else {
                    if (await res.errors) errors = await res.errors
                    else errors.thumbnail = await res.message.toString
                    setWarning({ errors: errors })
                }
            } catch (err: any) {
                errors.thumbnail = await err.message
                setWarning({ errors: errors })
            }
        } else {
            errors.thumbnail = "no input element"
            setWarning({ errors: errors })
        }
        setSpinner(false)
    }

    const submit = async (e: FormEvent) => {
        e.preventDefault()
        setWarning({ errors: errors })
        setSpinner(true)

        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement
        const submit = await fetch(path.posix.join("/api/", pathName as string, '../..'), {
            method: 'PUT',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                id: data._id,
                title: (document.getElementById("title") as HTMLInputElement).value,
                content: editor,
                tags: tags.map((v: any) => v.value),
                description: target.description.value,
                categories: Array.from(document.querySelectorAll('input[name="category"]:checked')).map(x => (x as HTMLInputElement).value),
                thumbnail: target.thumbnail_text.value,
                thumbnail_caption: target.thumbnail_caption.value,
                language: target.language.value,
                comment: parseInt(target.comment.value) ? true : false,
                status: data.status=="Published" ? target.status.value : data.status 
            })
        });
        //await new Promise(resolve => setTimeout(resolve, 3000));
        const res = await submit.json()
        if (submit.ok) {
            window.location.replace(path.posix.join(pathName as string, '../..', 'detail', data._id + "?status=updated"))
        } else {
            errors = await res.errors
            setWarning({ errors: errors })
            errors = {
                title: "",
                content: "",
                tags: "",
                description: "",
                category: "",
                thumbnail: "",
                thumbnail_caption: ""
            }
        }
        setSpinner(false)
    }
    return (
        <main>
            <div className="flex flex-col md:flex-row md:space-y-0 space-y-4 mb-4 justify-between">
                <div>
                    <h1 className="text-xl lg:text-2xl font-semibold">Edit Post</h1>
                    <div className="text-sm text-slate-500">Edit Content Posts</div>
                </div>
                <div className="flex items-center space-x-2">
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a3203") &&
                        <Link href={"/content/post/create/"} title="Create Post" className="btn-circle" aria-label="create-post">
                            <PlusIcon width={20} height={20} />
                        </Link>
                    }
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a3205") &&
                        <Link href={"/content/post/detail/" + props.params.id} title="Post Detail" className="btn-circle" aria-label="post-detail">
                            <IdentificationIcon width={20} height={20} />
                        </Link>
                    }
                    {session?.user.group.roles.includes("61e3de6ecc4029a9d11a3201") &&
                        <Link href={"/content/post"} title="Post data" className="btn-circle" aria-label="post-data">
                            <ListBulletIcon width={20} height={20} />
                        </Link>
                    }
                </div>
            </div>
            <form onSubmit={submit} className="relative" >

                <Success data={success} hide={() => setSuccess({ title: "", description: "" })} />

                <Warning className="" errors={Object.values(warning.errors).filter(a => a != "")} hide={() => setWarning({ errors: errors })} />
                <div className="flex flex-col items-stretch lg:flex-row lg:space-x-4 space-y-4 lg:space-y-0">
                    <div className="relative bg-white shadow-lg w-full rounded-tl-3xl rounded-br-3xl p-6 dark:bg-slate-700 mb-4">
                        {spinner && <Spinner className="rounded-tl-3xl rounded-br-3xl" />}
                        <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                            <div className="flex-1 mb-2">
                                <label className="label" htmlFor="title">Title <sup className="text-rose-500">*</sup></label>
                                <input type="title" id="title" name='title' defaultValue={data.title} className={"text-xl font-semibold border-b w-full rounded-md focus:outline-none p-2 dark:bg-gray-600 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"} placeholder="Post Title" />
                            </div>
                        </div>
                        <div className="mb-4">
                            <label htmlFor="content" className="label">Content <sup className="text-rose-500">*</sup></label>
                            <Editor data={editor} onChange={setEditor} holder={"editor"} />
                        </div>
                        <div className="mb-4">
                            <label htmlFor="tags" className="label">Post Tags <sup className="text-rose-500">*</sup></label>
                            <CreatableSelect
                                name="tags"
                                instanceId={"tags"}
                                noOptionsMessage={() => null}
                                components={{ IndicatorSeparator: null }}
                                isSearchable
                                isClearable
                                isMulti
                                defaultValue={tags}
                                onChange={(choice: any) => setTags(choice)}
                                options={[]}
                                className="react-select" classNamePrefix="react-select"
                            />
                        </div>
                    </div>
                    <div className="relative w-full lg:w-1/3 text-gray-700 dark:text-slate-300 mb-4">

                        <div className="sticky top-4 py-6 px-4 bg-white shadow-lg rounded-tl-3xl rounded-br-3xl dark:bg-slate-700">
                            {spinner && <Spinner className="rounded-tl-3xl rounded-br-3xl" />}
                            <div className="mb-4">
                                <label htmlFor="description" className="label">Description <sup className="text-rose-500">*</sup></label>
                                <textarea name="description" id="description" defaultValue={data.description} placeholder="Post Description" className={"input"} rows={3}></textarea>
                            </div>
                            <div className="mb-4 ">
                                <label htmlFor="category" className="label">Category <sup className="text-rose-500">*</sup></label>
                                <div className="flex flex-wrap border border-slate-300 dark:border-slate-500 dark:bg-slate-600 rounded-md py-2">
                                    {props?.params?.categories.map((cat: any, i: number) => {
                                        if (!cat.parent)
                                            return <div key={cat._id} className="mx-2 flex-1 rounded-md">
                                                <div className="flex items-center mb-1">
                                                    <input type="checkbox" id={cat._id} name="category" value={cat._id} placeholder={cat.name} defaultChecked={data.categories.map((cat: any) => cat._id).includes(cat._id)} className="checkbox" />
                                                    <label htmlFor={cat._id} className="pl-2 text-xs font-medium text-gray-600 dark:text-gray-400 cursor-pointer">{cat.name}</label>
                                                </div>

                                                {props?.params?.categories.map((sub: any, i: number) => {
                                                    if (sub.parent?._id == cat._id)
                                                        return <div key={sub._id} className="flex items-center ml-6 mb-1">
                                                            <input type="checkbox" id={sub._id} name="category" value={sub._id} placeholder={sub.name} defaultChecked={data.categories.includes(cat._id)} className="checkbox" />
                                                            <label htmlFor={sub._id} className="pl-2 text-xs font-medium text-gray-600 dark:text-gray-400 cursor-pointer">{sub.name}</label>
                                                        </div>
                                                })}
                                            </div>
                                    })}
                                </div>
                            </div>
                            <div className="mb-4">
                                <label htmlFor="thumbnail" className="label">Thumbnail <sup className="text-rose-500">*</sup></label>
                                <div className="mb-4">
                                    <div className="relative group mx-auto mt-2 h-48 rounded-md border-dashed border-2 w-full border-gray-300 overflow-hidden">
                                        <Image
                                            className="object-cover object-center w-full h-48"
                                            src={thumbnail}
                                            alt="Person"
                                            fill
                                            priority
                                            sizes="(max-width: 768px) 100vw,(max-width: 1200px) 50vw,33vw"
                                        />
                                        <button type="button" onClick={() => (document.getElementById("thumbnail") as HTMLInputElement).click()} className="hidden absolute group-hover:block mx-auto bottom-5 left-1/2 right-1/2 transform -translate-x-1/2 text-center w-32 border border-gray-500 py-2 px-1 rounded-lg shadow bg-gray-200 text-sm dark:text-slate-600">Change</button>
                                        <input onChange={(e) => { changeThumbnail(e) }} type="file" id="thumbnail" name="thumbnail" className="hidden" accept="image/png, image/gif, image/jpeg, image/webp" />
                                        <input type="hidden" name="thumnail_text" id="thumbnail_text" defaultValue={data.thumbnail} />
                                    </div>
                                </div>
                                <div className="relative mb-2">
                                    <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                        <TagIcon width={18} className="text-gray-400" />
                                    </div>
                                    <input type="thumbnail_caption" id="thumbnail_caption" name='thumbnail_caption' defaultValue={data.thumbnail_caption} className={"input-icon"} placeholder="Thumbnail Caption" />
                                </div>
                            </div>
                            <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                                <div className="flex-1 mb-2">
                                    <label className="label" htmlFor="language">Language <sup className="text-rose-500">*</sup></label>
                                    <div className="relative inline-block w-full text-gray-700">
                                        <select name="language" id="language" defaultValue={data.language} className="select">
                                            <option value="id_ID">Indonesia</option>
                                            <option value="en_US">English</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="flex-1 mb-2">
                                    <label className="label" htmlFor="name">Comment <sup className="text-rose-500">*</sup></label>
                                    <div className="relative inline-block w-full text-gray-700">
                                        <select name="comment" id="comment" defaultValue={data.comment ? "1" : "0"} className="select">
                                            <option value="1">Enable</option>
                                            <option value="0">Disable</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            {data.status == "Published" &&
                                <div className='flex flex-col lg:flex-row space-x-0 lg:space-x-4 lg:space-y-0 space-y-2  mb-2'>
                                    <div className="flex-1 mb-2">
                                        <label className="label" htmlFor="status">Status <sup className="text-rose-500">*</sup></label>
                                        <div className="relative inline-block w-full text-gray-700">
                                            <select name="status" id="status" defaultValue={data.status} className="select">
                                                <option value="Draft">Draft</option>
                                                <option value="Published">Published</option>
                                                <option value="Archived">Archived</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            }
                            <div className="">
                                <button type="submit" className="btn btn-primary flex items-center space-x-2">
                                    <ArrowPathIcon width={20} height={20} className="mr-2" />
                                    Update Post
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form >
        </main >
    )
}