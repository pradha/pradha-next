import './globals.css'
import Provider from '@/components/session/provider'
import Script from 'next/script';
import { getServerSession } from 'next-auth';
import { authOptions } from "@/lib/auth/options";

export default async function RootLayout(props: any) {
    const session = await getServerSession(authOptions)
    return (
        <html lang="en">
            <head>
                <meta content="width=device-width, initial-scale=1" name="viewport" />
                <link rel="apple-touch-icon" sizes="120x120" href="/img/favicon/apple-touch-icon.png" />
                <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png" />
                <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png" />
                <link rel="manifest" href="/img/favicon/site.webmanifest" />
                <link rel="mask-icon" href="/img/favicon/safari-pinned-tab.svg" color="#5bbad5" />
                <meta name="msapplication-TileColor" content="#5bbad5" />
                <meta name="theme-color" content="#ffffff" />
                {process.env?.NODE_ENV == "production" && process.env?.NEXT_PUBLIC_FB_APP_ID && <meta property="fb:app_id" content={process.env.NEXT_PUBLIC_FB_APP_ID} />}
            </head>
            {!session && process.env?.NODE_ENV == "production" && process.env?.NEXT_PUBLIC_GTM &&
                <Script async src={"https://www.googletagmanager.com/gtag/js?id=" + process.env.NEXT_PUBLIC_GTM}></Script>
            }
            {!session && process.env?.NODE_ENV == "production" && process.env?.NEXT_PUBLIC_GTM &&
                <Script id="ga">
                    {`
                        window.dataLayer = window.dataLayer || [];
                        function gtag(){window.dataLayer.push(arguments);}
                        gtag('js', new Date());
                
                        gtag('config', '${process.env.NEXT_PUBLIC_GTM}');
                        `}
                </Script>
            }
            <body>
                <Provider>{props.children}</Provider>
            </body>
        </html>
    )
}

export const metadata = {
    generator: 'Next.js',
    applicationName: process.env.NAME,
    title: process.env.NAME,
    description: process.env.DESCRIPTION,
    referrer: 'origin-when-cross-origin',
    keywords: (process.env?.KEYWORDS as string).split(","),
    authors: [{ name: process.env?.AUTHOR as string }],
    creator: process.env.DEFAULT_CREATOR,
    publisher: process.env.DEFAULT_PUBLISHER,
    icons: {
        icon: '/img/favicon/favicon.ico',
        shortcut: '/img/favicon/favicon.ico',
        apple: '/img/favicon/apple-touch-icon.png',
        other: {
            rel: 'apple-touch-icon-precomposed',
            url: '/apple-touch-icon.png',
        },
    },
    openGraph: {
        title: process.env.NAME,
        description: process.env.DESCRIPTION,
        url: process.env.SITE_URL,
        siteName: process.env.NAME,
        images: [
            {
                url: '/img/logo.webp',
                width: 800,
                height: 600,
            },
            {
                url: '/img/logo.webp',
                width: 1800,
                height: 1600,
                alt: process.env.NAME,
            },
        ],
        locale: 'en_US',
        type: 'website',
    },
    robots: {
        index: true,
        follow: true,
        nocache: true,
        googleBot: {
            index: true,
            follow: true,
            noimageindex: false,
            'max-video-preview': -1,
            'max-image-preview': 'large',
            'max-snippet': -1,
        },
    },
    twitter: {
        card: 'summary_large_image',
        title: process.env.NAME,
        description: process.env.DESCRIPTION,
        siteId: process.env.TWITTER_SITE_ID,
        creator: process.env.TWITTER_CREATOR,
        creatorId: process.env.TWITTER_CREATOR_ID,
        images: ['/img/logo.webp'],
    },
    verification: {
        google: 'google',
        yandex: "asas",
        yahoo: process.env.VERIFICATION_YAHOO,
    },
    alternates: {
        canonical: process.env.SITE_URL,
        languages: {},
        types: {
            'application/rss+xml': process.env.SITE_URL + '/rss',
        },
    },
};
