import PublicLayout from "@/components/ui/layouts/public"
import clientPromise from "@/lib/connections/mongo"
import { FindOptions, ObjectId } from "mongodb"

export default async function QuranLayout(props: any) {
    const db = (await clientPromise).db()
    const projection: FindOptions['projection'] = {verses:0}
    const surahs = await db.collection("quran").find({}, { projection } as FindOptions).toArray()
    props.params.surahs = JSON.parse(JSON.stringify(surahs))
    return (
        <PublicLayout>{props.children}</PublicLayout>
    )
}

const title = 'Quran Online'
const description = 'Quran Online'
const keywords = ["quran", "quran online"]
const url = process.env.SITE_URL + '/quran'

export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: true,
        follow: true,
        nocache: false,
        googleBot: {
            index: true,
            follow: true
        },
    },
}