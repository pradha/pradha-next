'use client'
import { OtherCategories, OtherPages, Pages } from "@/components/ui/layouts/pages";
import { ListOfContents, ParseHTML } from "@/lib/parseHtml";
import { DayDateFormat, FullShort, ISO8601DateTime, ISODate } from "@/lib/time";
import { BriefcaseIcon, CheckIcon, EyeSlashIcon, HomeIcon, PhoneIcon, QuestionMarkCircleIcon } from "@heroicons/react/24/outline";
import Link from "next/link";
import { notFound } from "next/navigation";
import Image from "next/image";
import { ArabicNumber, Truncate } from "@/lib/text";
import { SocialShare } from "@/components/social/share";
import { useEffect, useState } from "react";

export const revalidate = 0

export default function ReadPage(props: any) {
    const [surahs, setSurahs] = useState(props.params.surahs)
    const [scrollY, setScrollY] = useState(0);

    return <div className="relative max-w-6xl flex lg:flex-row flex-col lg:space-x-4 space-x-0 items-stretch mb-4 mx-auto">
        <div className="relative hidden lg:block lg:w-1/4 text-gray-700  dark:text-slate-300 ">
            <div className="sticky top-16">
                <div className="bg-white shadow-lg rounded-xl group mb-4 dark:bg-slate-700 p-4">
                    <div className="font-semibold my-2 dark:text-slate-200">Quran Online</div>
                    <div className="text-sm">Silahkan Pilih daftar Surat</div>
                </div>



            </div>
        </div>

        <div className="flex-1">
            <div className="relative bg-white shadow-lg w-full rounded-xl dark:bg-slate-700 mb-4">
                <div className="lg:py-4 py-2 px-6">
                    <h1 className="text-2xl mb-4">Quran Online</h1>
                    <div className="grid grid-cols-2 lg:grid-cols-4 gap-4">
                        {surahs && surahs.length >= 1 && surahs.map((surah: any) =>
                            <Link key={surah._id} title={surah.name.short + " - " + surah.name.transliteration.id + " (" + surah.name.translation.id + ")"} href={'/quran/' + surah.number} as={'/quran/' + surah.number} className={"block rounded overflow-hidden shadow-lg hover:shadow-xl transition-all ease-in-out cursor-pointer bg-gradient-to-br dark:from-slate-600 dark:to-slate-800 from-stone-100 to-stone-200 p-2 lg:p-4"}>
                                <h2 className="text-right text-lg" dir="rtl" lang="ar">({ArabicNumber(surah.number)}) {surah.name.short} </h2>
                                <div className="">({surah.number}). {surah.name.transliteration.id} </div>
                                <div className="italic text-xs">({surah.name.translation.id})</div>
                            </Link>
                        )}
                    </div>
                </div>
            </div>

        </div>
    </div>
}

