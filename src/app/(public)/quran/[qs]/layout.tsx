import PublicLayout from "@/components/ui/layouts/public"
import clientPromise from "@/lib/connections/mongo"
import { Truncate } from "@/lib/text"
import { ISO8601DateTime } from "@/lib/time"
import { FindOptions, ObjectId } from "mongodb"
import { notFound } from "next/navigation"


const getSurah = async (number: string) => {
    let surah: any, verse: any, surahNum = 0, verseNum = 0, page = 0
    const db = (await clientPromise).db()
    if (decodeURIComponent(number).includes(":")) {

        let split = decodeURIComponent(number).split(":", 2)
        if (!isNaN(Number(split[0])))
            surahNum = Number(split[0])
        if (!isNaN(Number(split[1])))
            verseNum = Number(split[1])
        surah = await db.collection("quran").findOne({ number: surahNum, 'verses.number.inSurah': verseNum }, { projection: { "verses.$": 1, number: 1, revelation: 1, tafsir: 1, preBismillah: 1, name: 1 } } as FindOptions)
        if (surah)
            page = surah.verses[0].meta.page
    } else if (!isNaN(Number(number))) {
        surahNum = Number(number)
        surah = await db.collection("quran").findOne({ number: surahNum })
    }
    page = surah.verses[0].meta.page
    let pageVerses = await db.collection("quran").aggregate([
        { $match: { 'verses.meta.page': page } },
        { $unwind: '$verses' },
        {
            $addFields: {
                verses: {
                    name: '$name',
                    surahNum: '$number',
                    preBismillah: '$preBismillah'
                }
            }
        },
        { $match: { 'verses.meta.page': page } },
        {
            $group: {
                _id: '$verses.meta.page',
                verses: { $push: '$verses' }
            }
        }
    ]).toArray()

    let previousPage = await db.collection("quran").aggregate([
        { $match: { 'verses.meta.page': (page-1) } },
        { $unwind: '$verses' },
        {
            $addFields: {
                verses: {
                    surahNum: '$number',
                    preBismillah: '$preBismillah'
                }
            }
        },
        { $match: { 'verses.meta.page': (page-1) } },
        {
            $group: {
                _id: '$verses.meta.page',
                verses: { $push: '$verses' }
            }
        }
    ]).toArray()

    let nextPage = await db.collection("quran").aggregate([
        { $match: { 'verses.meta.page': (page+1) } },
        { $unwind: '$verses' },
        {
            $addFields: {
                verses: {
                    surahNum: '$number',
                    preBismillah: '$preBismillah'
                }
            }
        },
        { $match: { 'verses.meta.page': (page+1) } },
        {
            $group: {
                _id: '$verses.meta.page',
                verses: { $push: '$verses' }
            }
        }
    ]).toArray()
    return { surah, verse, surahNum, verseNum, page, pageVerses, previousPage, nextPage }
}

export default async function QuranLayout(props: any) {
    let { surah, verse, surahNum, verseNum, page, pageVerses, previousPage, nextPage } = await getSurah(props.params.qs)
    if (!surah)
        notFound()

    props.params.surah = JSON.parse(JSON.stringify(surah))
    props.params.verse = verse
    props.params.surahNum = surahNum
    props.params.verseNum = verseNum
    props.params.page = page
    props.params.pageVerses = JSON.parse(JSON.stringify(pageVerses))[0]
    props.params.previousPage = JSON.parse(JSON.stringify(previousPage))[0]
    props.params.nextPage = JSON.parse(JSON.stringify(nextPage))[0]
    return (
        <>{props.children}</>
    )
}

export async function generateMetadata(props: any) {
    let { surah, verse, surahNum, verseNum } = await getSurah(props.params.qs)
    if (!surah)
        notFound()
    const title = surah.name.transliteration.id + " [" + surahNum + (verseNum != 0 ? ":" + verseNum : "") + "] - " + surah.name.translation.id
    const description = Truncate(verseNum != 0 ? surah.verses[0].text.transliteration.en : surah.tafsir.id, 157)
    const keywords = ["quran", "quran online", surah.name.transliteration.id,]
    const url = process.env.SITE_URL + '/quran/' + surahNum + (verseNum != 0 ? ":" + verseNum : "")
    const type = "article"
    const image = process.env.SITE_URL + '/api/images/quran?s='+surahNum+(verseNum != 0 ? "&v=" + verseNum : "")
    return {
        title: title,
        description: description,
        keywords: keywords,
        openGraph: {
            title: title,
            description: description,
            url: url,
            publishedTime: ISO8601DateTime(new Date().toDateString()),
            authors: "Allah SWT",
            images: [
                {
                    url: image,
                    width: 1200,
                    height: 600,
                    alt: title,
                }
            ]
        },
        twitter: {
            card: 'summary_large_image',
            title: title,
            description: description,
            images: [image],
        },
        alternates: {
            canonical: url,
            languages: {},
        },
        robots: {
            index: true,
            follow: true,
            nocache: false,
            googleBot: {
                index: true,
                follow: true
            },
        },
    }
}