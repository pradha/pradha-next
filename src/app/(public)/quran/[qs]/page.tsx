'use client'
import { ChevronLeftIcon, ChevronRightIcon } from "@heroicons/react/24/outline";
import Link from "next/link";
import { notFound } from "next/navigation";
import Image from "next/image";
import { ArabicNumber, Truncate } from "@/lib/text";
import { SocialShare } from "@/components/social/share";
import { useEffect, useState } from "react";
import { Noto_Naskh_Arabic } from 'next/font/google'
const arabic = Noto_Naskh_Arabic({ weight: ['400', '700'], subsets: ['latin'] })

export const revalidate = 0

export default function QuranPage(props: any) {
    const [surah, setSurah] = useState(props.params.surah)
    const [page, setPage] = useState(props.params.page);
    const [verses, setVerses] = useState(props.params.pageVerses.verses);
    const [previous, setPrevious] = useState(props.params.previousPage);
    const [next, setNext] = useState(props.params.nextPage);
    return <div className="relative max-w-6xl flex lg:flex-row flex-col lg:space-x-4 space-x-0 items-stretch mb-4 mx-auto">
        <div className="relative hidden lg:block lg:w-1/4 text-gray-700  dark:text-slate-300 ">
            <div className="sticky top-16">
                <div className="bg-white shadow-lg rounded-xl group mb-4 dark:bg-slate-700 p-4">
                    <h1 className="font-semibold my-2 dark:text-slate-200">{surah.name.transliteration.id} [{props.params.surahNum + (props.params.verseNum != 0 ? ":" + props.params.verseNum : "")}]</h1>
                    <div className="text-sm">{props.params.verseNum != 0 ? props.params.surah.verses[0].translation.id : surah.tafsir.id}</div>
                    <div>Juz {props.params.surah.verses[0].meta.juz}</div>
                </div>

                <div className="bg-white shadow-lg rounded-xl group mb-4 dark:bg-slate-700 p-4">
                    <div className="font-semibold my-2 dark:text-slate-200">Tafsir</div>
                    <div className="text-sm mb-4">{props.params.surah.verses[0].tafsir.id.long}</div>
                </div>
            </div>
        </div>

        <div className="flex-1">
            <div className="relative lg:hidden bg-white shadow-lg rounded-xl group mb-4 dark:bg-slate-700 p-4">
                <h1 className="font-semibold my-2 dark:text-slate-200 text-sm">{surah.name.transliteration.id} [{props.params.surahNum + (props.params.verseNum != 0 ? ":" + props.params.verseNum : "")}]</h1>
                <div className="text-xs mb-4 pb-4 border-b border-slate-300">{props.params.verseNum != 0 ? props.params.surah.verses[0].translation.id : surah.tafsir.id}</div>
                <SocialShare url={process.env.NEXT_PUBLIC_SITE_URL + "/quran/" + props.params.surahNum + (props.params.verseNum != 0 ? ":" + props.params.verseNum : "")} />
            </div>

            <div className="flex items-center justify-center space-x-4 text-lg">
                {next?.verses && <Link href={'/quran/' + next.verses[0].surahNum + ":" + next.verses[0].number.inSurah}><ChevronLeftIcon width={20} /></Link>}
                <span><small>{page}</small> | {ArabicNumber(page)}</span>
                {previous?.verses && <Link href={'/quran/' + previous.verses[previous.verses.length - 1].surahNum + ":" + previous.verses[previous.verses.length - 1].number.inSurah}><ChevronRightIcon width={20} /></Link>}
            </div>
            <div className="relative bg-white shadow-lg w-full rounded-xl dark:bg-slate-700 mb-4">

                <div className="lg:py-4 py-2 px-6">
                    <div className="text-justify text-xl lg:text-3xl lg:py-4 lg:px-6 p-2 leading-8 lg:leading-loose arabic.className" dir="rtl" lang="ar">
                        {verses.map((verse: any, i: number) => <span key={i + "-" + verse.number.inQuran + "-" + verse.number.inSurah}>
                            {verse.number.inSurah == 1 && <div className="text-center mt-4">
                                <div className="text-xl border-y border-gray-200 mb-4">{verse.name.long}</div>
                                {verse.preBismillah != null && <div title={verse.preBismillah.text.transliteration.en + "\n\n" + verse.preBismillah.translation.id} >{verse.preBismillah.text.arab}</div>}
                            </div>}
                            <Link title={verse.text.transliteration.en + "\n\n" + verse.translation.id} href={'/quran/' + verse.surahNum + ":" + verse.number.inSurah} className={"px-2 py-2 hover:bg-green-100 rounded-md " + (verse.surahNum == props.params.surahNum && verse.number.inSurah == props.params.verseNum ? "bg-green-200" : "")}>{verse.text.arab}  ({ArabicNumber(verse.number.inSurah)})</Link>
                        </span>)}
                    </div>
                </div>
            </div>
            <SocialShare className="justify-center mb-4" url={process.env.NEXT_PUBLIC_SITE_URL + "/quran/" + props.params.surahNum + (props.params.verseNum != 0 ? ":" + props.params.verseNum : "")} />
            <div className="flex items-center justify-center space-x-4 text-lg">
                {next?.verses && <Link href={'/quran/' + next.verses[0].surahNum + ":" + next.verses[0].number.inSurah}><ChevronLeftIcon width={20} /></Link>}
                <span><small>{page}</small> | {ArabicNumber(page)}</span>
                {previous?.verses && <Link href={'/quran/' + previous.verses[previous.verses.length - 1].surahNum + ":" + previous.verses[previous.verses.length - 1].number.inSurah}><ChevronRightIcon width={20} /></Link>}
            </div>
        </div>
    </div>
}

