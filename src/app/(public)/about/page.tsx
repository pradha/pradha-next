'use client'
import { OtherPages, Pages } from "@/components/ui/layouts/pages";
import { BriefcaseIcon, CheckIcon, EyeSlashIcon, HomeIcon, PhoneIcon, QuestionMarkCircleIcon } from "@heroicons/react/24/outline";
import Link from "next/link";
import { usePathname } from "next/navigation";

export default function AboutPage(props: any) {
    const pathName = usePathname()
    return <div className="relative max-w-6xl flex lg:flex-row flex-col lg:space-x-4 space-x-0 items-stretch mb-4 mx-auto">
        <div className="relative hidden lg:block lg:w-1/3 text-gray-700  dark:text-slate-300 ">
            <div className="sticky top-16">
                <Pages />
                <OtherPages />
            </div>
        </div>
        <div className="relative bg-white shadow-lg w-full lg:rounded-xl rounded-none lg:py-4 py-2 px-6 dark:bg-slate-700">
            <h1 className="text-xl font-semibold mb-2">About Us</h1>
            <p className="mb-4">Hey, Thanks for visiting my website. I’m Aditya. I’m a Software Developer living in Indonesia. I am a fan of technology, entrepreneurship, and travel. I’m also interested in gardening and programming.</p>
            <p className="mb-4">This is my personal website, which contains information that I am usually researching. I write about various things such as technology, biographies of famous people, history, personal opinion, finance, science, and of course programming.</p>
            <p className="mb-4">I’m not an expert in a specific programming language (specialist), I'm a generalist. I like to try and research new technology.</p>
            <p className="mb-4">I don't like crowds, annoying people are everywhere and it makes me tired easily.</p>
            <p className="mb-4">I hope you found all of the information on <b>{process.env.NEXT_PUBLIC_SITE_URL?.replaceAll("https://","")?.replaceAll("http://","")?.replaceAll("www.","")}</b> helpful, as i love to share them with you.</p>
            <p className="mb-4">If you require any more information or have any questions about our site, please feel free to contact us by email at <b>info@{process.env.NEXT_PUBLIC_SITE_URL?.replaceAll("https://","")?.replaceAll("http://","")?.replaceAll("www.","")}</b>.</p>
        </div>
    </div>
}