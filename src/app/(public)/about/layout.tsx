import PublicLayout from "@/components/ui/layouts/public"

export default async function RootLayout(props:any) {
    return (
        <PublicLayout>{props.children}</PublicLayout>
    )
}

const title = 'About'
const description = 'About'
const keywords = ["about"]
const url = process.env.SITE_URL + '/about'
export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: true,
        follow: true,
        nocache: false,
        googleBot: {
            index: true,
            follow: true
        },
    },
}