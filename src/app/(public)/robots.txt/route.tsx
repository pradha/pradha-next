import { cookies } from 'next/headers';

export async function GET(request: Request) {
  const cookieStore = cookies();
  const token = cookieStore.get('token');

  return new Response(`User-agent: *
Disallow:
Crawl-delay: 10
Disallow: /account/
Disallow: /dashboard/ 
Disallow: /system/ 
Disallow: /content/ 
  
Sitemap: ${process.env.SITE_URL}/sitemap.xml`, {
    status: 200,
    headers: { 'Set-Cookie': `token=${token}` },
  });
}