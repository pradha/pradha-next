import clientPromise from '@/lib/connections/mongo';
import { ISODate } from '@/lib/time';
import { NextApiRequest, NextApiResponse } from 'next';
import { headers } from 'next/headers';

export async function GET(request: Request) {
    return new Response(`<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<url>
<loc>${process.env.SITE_URL}/about</loc>
<lastmod>${ISODate(new Date().toDateString())}</lastmod>
</url>
<url>
<loc>${process.env.SITE_URL}/portofolio</loc>
<lastmod>${ISODate(new Date().toDateString())}</lastmod>
</url>
<url>
<loc>${process.env.SITE_URL}/contact</loc>
<lastmod>${ISODate(new Date().toDateString())}</lastmod>
</url>
<url>
<loc>${process.env.SITE_URL}/disclaimer</loc>
<lastmod>${ISODate(new Date().toDateString())}</lastmod>
</url>
<url>
<loc>${process.env.SITE_URL}/terms-conditions</loc>
<lastmod>${ISODate(new Date().toDateString())}</lastmod>
</url>
<url>
<loc>${process.env.SITE_URL}/privacy-policy</loc>
<lastmod>${ISODate(new Date().toDateString())}</lastmod>
</url>
</urlset>`, { status: 200, headers: { 'Content-Type': 'text/xml' } })
}