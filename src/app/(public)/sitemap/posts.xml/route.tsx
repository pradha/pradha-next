import clientPromise from '@/lib/connections/mongo';
import { ISODate } from '@/lib/time';
import { NextApiRequest, NextApiResponse } from 'next';
import { headers } from 'next/headers';

export async function GET(request: Request) {
    let url: string = ""
    const db = (await clientPromise).db()
    const posts = JSON.parse(JSON.stringify(await db.collection("content_posts").find({status: "Published"}).toArray()))
    posts.map((cat: any, i: number) => {
        url += `<url>
        <loc>${process.env.SITE_URL}/category/${cat.slug}</loc>
        <lastmod>${ISODate(new Date().toDateString())}</lastmod>
    </url>`
    });
    return new Response(`<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
       ${url}
</urlset>`, { status: 200, headers: { 'Content-Type': 'text/xml' } })
}