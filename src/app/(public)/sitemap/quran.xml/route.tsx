import clientPromise from '@/lib/connections/mongo';
import { ISODate } from '@/lib/time';
import { FindOptions } from 'mongodb';
import { NextApiRequest, NextApiResponse } from 'next';
import { headers } from 'next/headers';

export async function GET(request: Request) {
    let url: string = ""
    const db = (await clientPromise).db()
    const projection: FindOptions['projection'] = {number:1}
    let quran = JSON.parse(JSON.stringify(await db.collection("quran").find({}, { projection }).toArray()))
    let pages = JSON.parse(JSON.stringify(await db.collection("quran").aggregate([
        { $unwind: '$verses' },
        {
            $addFields: {
                verses: {
                    name: '$name',
                    surahNum: '$number',
                    preBismillah: '$preBismillah'
                }
            }
        },
        {
            $group: {
                _id: '$verses.meta.page',
                verses: { $push: '$verses' }
            }
        }
    ]).toArray()))
    quran.map((surah:any) => {
        url += `<url>
            <loc>${process.env.SITE_URL}/quran/${surah.number}</loc>
            <lastmod>${ISODate(new Date().toDateString())}</lastmod>
        </url>`
    })
    pages.map((page: any, i: number) => {
        page.verses.map((verse: any) => {
            url += `<url>
            <loc>${process.env.SITE_URL}/quran/${verse.surahNum}:${verse.number.inSurah}</loc>
            <lastmod>${ISODate(new Date().toDateString())}</lastmod>
        </url>`
        })
    });
    return new Response(`<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
       ${url}
</urlset>`, { status: 200, headers: { 'Content-Type': 'text/xml' } })
}