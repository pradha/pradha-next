
import clientPromise from "@/lib/connections/mongo";
import fs from 'fs'

export const revalidate = 0
export const dynamic = 'force-dynamic'
let number: string = "0"
export default async function RootLayout(props: any) {
    number = props.params.number;
    const db = (await clientPromise).db()
    const counters = await db.collection("v_queue_counters").find({ display: props.params.number, 'status.active': true }).toArray()
    const setting = await db.collection("queue_settings").findOne()

    props.params.counters = JSON.parse(JSON.stringify(counters))
    props.params.setting = JSON.parse(JSON.stringify(setting))

    let videos = new Array()
    fs.readdirSync('./public/videos/').forEach(file => {
        videos.push(file)
    });
    props.params.videos = videos
    return (
        <>{props.children}</>
    )
}

const title = 'Queue Display'
const description = 'Queue Display'
const keywords = ["queue", "display", "queue display", "displays", "show"]
const url = process.env.SITE_URL + '/queue/display/'
export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: false,
        follow: false,
        nocache: false,
        googleBot: {
            index: false,
            follow: false
        },
    },
}