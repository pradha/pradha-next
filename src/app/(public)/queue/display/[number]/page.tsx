'use client'
import Marquee from "react-fast-marquee"
import { ChangeEvent, FormEvent, Fragment, useEffect, useState } from 'react'
import { io } from "socket.io-client"
export default function Display(props: any) {
    const [socket, setSocket] = useState<any>(null)
    const [connected, setConnected] = useState<boolean>(false)
    const [tickers, setTickers] = useState<string[]>(new Array())
    //const [tickers, setTickers] = useState<string[]>(new Array())
    //const [playing, setPlaying] = useState<boolean>(false)
    var word = ["", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas"];
    var unit = ["puluh", "belas", "ratus", "ribu", "juta", "miliar", "triliun"];

    const createSentence = (num: number) => {
        let value = 0
        var temp = "";

        if (num < 12) {
            temp = " " + word[num];
        } else if (num < 20) {
            temp = createSentence(num - 10) + " Belas";
        } else if (num < 100) {
            value = Math.floor(num / 10);
            temp = createSentence(value) + " Puluh " + createSentence(num % 10);
        } else if (num < 200) {
            temp = " Seratus " + createSentence(num - 100);
        } else if (num < 1000) {
            value = Math.floor(num / 100);
            temp = createSentence(value) + " Ratus " + createSentence(num % 100);
        } else if (num < 2000) {
            temp = " Seribu " + createSentence(num - 1000);
        } else if (num < 1000000) {
            value = Math.floor(num / 1000);
            temp = createSentence(value) + " Ribu " + createSentence(num % 1000);
        } else if (num < 1000000000) {
            value = Math.floor(num / 1000000);
            temp = createSentence(value) + " Juta " + createSentence(num % 1000000);
        } else if (num < 1000000000000) {
            value = Math.floor(num / 1000000000);
            temp = createSentence(value) + " Miliar " + createSentence(num % 1000000000);
        } else if (num < 1000000000000000) {
            value = Math.floor(num / 1000000000000);
            temp = createSentence(num / 1000000000000) + " Triliun" + createSentence(num % 1000000000000);
        }
        return temp.trim();
    }

    const playAudio = async (audio: HTMLAudioElement) => {
        return new Promise(res => {
            audio.play()
            audio.onended = res
        })
    }

    const playVideo = (video: HTMLVideoElement) => {
        return new Promise(res => {
            video.play();
            video.onended = res
        })
    }
    const playVideoList = async (videoList: string[]) => {
        var video = document.getElementById('video') as HTMLVideoElement
        for (let i = 0; i < videoList.length; i++) {
            video.setAttribute('src', `/videos/${videoList[i]}`)
            video.setAttribute('type', 'video/mp4')
            video.load()
            await playVideo(video)
            if (i < videoList.length) i = 0
        }
    }



    useEffect(() => {
        var video = document.getElementById('video') as HTMLVideoElement
        if (props.params.setting && props.params.setting.video) {
            playVideoList(props.params.videos)
            video.volume = 0.1
        }
        let tick = new Array()
        props.params.setting?.information.split(/\r?\n/).map((info:string) => {
            const text = info.split("|")
            
            if (text[0] == props.params.number.toString()){
                tick.push(text[1])
            }
        })
        setTickers(tick)
        const socketIO = io(process.env.NEXT_PUBLIC_SOCKET_SERVER_URL as string, {
            transports: ["websocket"],
        });
        socketIO.on("connect", () => {
            console.log("SOCKET CONNECTED!", socketIO.id)
            setConnected(true)
        });
        socketIO.on("disconnect", () => {
            console.log("SOCKET DISCONNECTED!")
            setConnected(false)
        })

        setSocket(socketIO)

        let playing = false
        socketIO.on("call-queue", async (msg: any) => {
            if (!playing) {
                video.volume = 0.05
                console.log("Call Received", msg.symbol + msg.number);
                playing = true;
                let sentence = createSentence(msg.number);
                (document.getElementById("mainNum") as HTMLDivElement).innerHTML = msg.symbol + msg.number;
                (document.getElementById("numWords") as HTMLDivElement).innerHTML = `<span className="font-bold">${msg.symbol}</span>, <span className="italic">${sentence}</span>`;
                (document.getElementById("counterName") as HTMLDivElement).innerHTML = msg.called?.counter?.name;
                (document.getElementById("mainNum") as HTMLDivElement).classList.add('animate-bounce');
                (document.getElementById("mainNum") as HTMLDivElement).classList.add('text-red-500');

                let playList = new Array();
                var attention = (document.getElementById('attentionSound') as HTMLAudioElement)
                var antrian = (document.getElementById('nomorAntrianSound') as HTMLAudioElement)
                playList.push(attention)
                playList.push(antrian)
                var letter = (document.getElementById('sound-' + msg.symbol) as HTMLAudioElement)
                playList.push(letter)

                let words = sentence.split(" ")
                for (let i = 0; i < words.length; i++) {
                    if (words[i].toLowerCase() == "nol")
                        playList.push(document.getElementById('sound-nol') as HTMLAudioElement)
                    if (words[i].toLowerCase() == "satu")
                        playList.push(document.getElementById('sound-satu') as HTMLAudioElement)
                    if (words[i].toLowerCase() == "dua")
                        playList.push(document.getElementById('sound-dua') as HTMLAudioElement)
                    if (words[i].toLowerCase() == "tiga")
                        playList.push(document.getElementById('sound-tiga') as HTMLAudioElement)
                    if (words[i].toLowerCase() == "empat")
                        playList.push(document.getElementById('sound-empat') as HTMLAudioElement)
                    if (words[i].toLowerCase() == "lima")
                        playList.push(document.getElementById('sound-lima') as HTMLAudioElement)
                    if (words[i].toLowerCase() == "enam")
                        playList.push(document.getElementById('sound-enam') as HTMLAudioElement)
                    if (words[i].toLowerCase() == "tujuh")
                        playList.push(document.getElementById('sound-tujuh') as HTMLAudioElement)
                    if (words[i].toLowerCase() == "delapan")
                        playList.push(document.getElementById('sound-delapan') as HTMLAudioElement)
                    if (words[i].toLowerCase() == "sembilan")
                        playList.push(document.getElementById('sound-sembilan') as HTMLAudioElement)
                    if (words[i].toLowerCase() == "sepuluh")
                        playList.push(document.getElementById('sound-sepuluh') as HTMLAudioElement)
                    if (words[i].toLowerCase() == "sebelas")
                        playList.push(document.getElementById('sound-sebelas') as HTMLAudioElement)
                    if (words[i].toLowerCase() == "belas")
                        playList.push(document.getElementById('sound-belas') as HTMLAudioElement)
                    if (words[i].toLowerCase() == "puluh")
                        playList.push(document.getElementById('sound-puluh') as HTMLAudioElement)
                    if (words[i].toLowerCase() == "seratus")
                        playList.push(document.getElementById('sound-seratus') as HTMLAudioElement)
                    if (words[i].toLowerCase() == "ratus")
                        playList.push(document.getElementById('sound-ratus') as HTMLAudioElement)
                    if (words[i].toLowerCase() == "seribu")
                        playList.push(document.getElementById('sound-seribu') as HTMLAudioElement)
                    if (words[i].toLowerCase() == "ribu")
                        playList.push(document.getElementById('sound-ribu') as HTMLAudioElement)
                }
                playList.push(document.getElementById('diSound') as HTMLAudioElement);
                playList.push(document.getElementById('sound-' + msg.called.counter._id) as HTMLAudioElement);

                (document.getElementById(`counter-${msg.called.counter._id}-number`) as HTMLElement).innerHTML = msg.symbol + msg.number
                for (let i = 0; i < playList.length; i++) {
                    await playAudio(playList[i])
                }
                (document.getElementById("mainNum") as HTMLDivElement).classList.remove('animate-bounce');
                (document.getElementById("mainNum") as HTMLDivElement).classList.remove('text-red-500');
                playing = false
                video.volume = 0.1
            }
        })

        return () => {
            console.log("Disconnecting from WebSocket server...");
            socketIO.disconnect();
            setConnected(false)
        };
    }, [])

    return (
        <main className="grid gap-4 content-between min-h-screen min-w-full bg-gradient-to-tr from-blue-600 to-sky-400">
            <div className="flex flex-row space-x-4 items-stretch m-4">
                <div className="flex items-center flex-1 bg-white rounded-lg shadow-lg">
                    <div className="mx-auto text-center p-8">
                        <div className="text-4xl">Nomor Antrian</div>
                        <div id="mainNum" className="text-9xl font-bold mt-14 transition-all ease-in-out">---</div>
                        <div id="numWords" className="text-lg font-semibold "></div>
                        <div id="nickname" className="text-3xl font-semibold mt-6"></div>
                        <div id="counterName" className="text-4xl font-bold mt-16">----</div>
                    </div>
                </div>
                <div className="flex flex-1 items-center">
                    <div className="mx-auto w-full h-full">
                        <video id="video" autoPlay className="w-full bg-slate-500 rounded-lg h-full">
                            Your browser does not support the video tag.
                        </video>
                    </div>
                </div>
            </div>
            <div className="p-4">
                <div className="flex flex-wrap space-x-4 items-stretch">
                    {props.params.counters.map((counter: any) =>
                        <div key={counter._id} className="border border-blue-400 rounded-lg shadow-lg flex flex-col flex-1 items-center p-8 text-white bg-gradient-to-tr from-blue-600 to-sky-400">
                            <div className="mx-auto text-center ">
                                <div className="text-2xl">{counter.name}</div>
                                <div id={`counter-${counter._id}-number`} className="text-7xl font-semibold my-10">--</div>
                            </div>
                        </div>)}
                </div>
            </div>
            {props.params.setting && props.params.setting.ticker.display &&
                <div className="text-3xl font-semibold flex p-4 bg-white items-center">
                    <Marquee>
                        {
                            tickers.map((ticker: string) => <Fragment key={Math.random()}>{ticker}</Fragment>)
                        }
                    </Marquee>
                </div>
            }
            <audio id="attentionSound">
                <source src={process.env.NEXT_PUBLIC_STATIC_FILES+"/audio/attention.mp3"} type="audio/mpeg" />
            </audio>
            <audio id="nomorAntrianSound">
                <source src={process.env.NEXT_PUBLIC_STATIC_FILES+"/audio/nomor_antrian.mp3"} type="audio/mpeg" />
            </audio>
            <audio id="diSound">
                <source src={process.env.NEXT_PUBLIC_STATIC_FILES+"/audio/di.mp3"} type="audio/mpeg" />
            </audio>
            <Fragment>
                {Array.from(Array(26)).map((e, i) => i + 65).map((x) =>
                    <audio key={'sound-' + String.fromCharCode(x)} id={'sound-' + String.fromCharCode(x)}>
                        <source src={process.env.NEXT_PUBLIC_STATIC_FILES+"/audio/" + String.fromCharCode(x) + ".mp3"} type="audio/mpeg" />
                    </audio>
                )}
            </Fragment>
            {word.filter((num) => num != "").map((num: string) =>
                <audio key={'sound-' + num.toLowerCase()} id={'sound-' + num.toLowerCase()}>
                    <source src={process.env.NEXT_PUBLIC_STATIC_FILES+"/audio/" + num.toLowerCase() + ".mp3"} type="audio/mpeg" />
                </audio>
            )}

            {unit.filter((unit) => unit != "").map((unit: string) =>
                <audio key={'sound-' + unit.toLowerCase()} id={'sound-' + unit.toLowerCase()}>
                    <source src={process.env.NEXT_PUBLIC_STATIC_FILES+"/audio/" + unit.toLowerCase() + ".mp3"} type="audio/mpeg" />
                </audio>
            )}
            <audio key={'sound-seratus'} id={'sound-seratus'}>
                <source src={process.env.NEXT_PUBLIC_STATIC_FILES+"/audio/seratus.mp3"} type="audio/mpeg" />
            </audio>
            <audio key={'sound-seribu'} id={'sound-seribu'}>
                <source src={process.env.NEXT_PUBLIC_STATIC_FILES+"/audio/seribu.mp3"} type="audio/mpeg" />
            </audio>
            {props.params.counters.map((counter: any) =>
                <audio key={counter._id} id={'sound-' + counter._id}>
                    <source src={process.env.NEXT_PUBLIC_STATIC_FILES+counter.audio} type="audio/mpeg" />
                </audio>
            )}

        </main>
    )
}