
import clientPromise from "@/lib/connections/mongo";
export const revalidate = 0
export const dynamic = 'force-dynamic'
export default async function RootLayout(props: any) {
    const db = (await clientPromise).db()
    const services = await db.collection("v_queue_services").find({ 'status.active': true, 'status.print': true }).toArray()
    const setting = await db.collection("queue_settings").findOne()
    props.params.services = JSON.parse(JSON.stringify(services))
    props.params.setting = JSON.parse(JSON.stringify(setting))
    return (
        <>{props.children}</>
    )
}

const title = 'Queue Touch'
const description = 'Queue Print Touch'
const keywords = ["queue", "touch", "queue print", "touch print", "show"]
const url = process.env.SITE_URL + '/queue/ticket/touch'
export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: false,
        follow: false,
        nocache: false,
        googleBot: {
            index: false,
            follow: false
        },
    },
}