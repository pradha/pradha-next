'use client'
import Marquee from "react-fast-marquee"
import { ChangeEvent, FormEvent, Fragment, useEffect, useState } from 'react'
import { io } from "socket.io-client"
export default function Touch(props: any) {
    const [tickers, setTickers] = useState<string[]>(new Array())
    const [printing, setPrinting] = useState<boolean>(false)
    useEffect(() => {
        let tick = new Array()
        props.params.setting?.information.split(/\r?\n/).map((info: string) => {
            const text = info.split("|")
            if (text[0] == "touch") {
                tick.push(text[1])
            }
        })
        setTickers(tick)

        var timer: any,          // timer required to reset
            timeout = 200;  // timer reset in ms

        let main = document.getElementById('main') as HTMLElement
        main.addEventListener("dblclick", function (evt) {
            timer = setTimeout(function () {
                timer = null;
            }, timeout);
        });
        main.addEventListener("click", function (evt) {
            if (timer) {
                clearTimeout(timer);
                timer = null;
                if (confirm("Are you sure you want to close this window")) {
                    window.close()
                } else {
                    return false;
                }
            }
        });
    }, [])
    const pushPrint = async (service: string) => {
        setPrinting(true)
        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }
        if (!printing) {
            const submit = await fetch("/api/queue/ticket", {
                method: 'POST',
                credentials: 'include',
                cache: 'no-cache',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    date: new Date(),
                    service: service,
                    print: true
                })
            });
            //await new Promise(resolve => setTimeout(resolve, 3000));
            const res = await submit.json()
            if (submit.ok) {
                console.log(res)
            } else {
                alert("Oops!! Print Touch Error" + submit)
            }
        }
        setPrinting(false)
    }


    return (
        <main id="main" className="grid content-between gap-4 min-h-screen min-w-full touch-bg">
            <div></div>
            <div className="flex flex-row space-x-4 items-stretch m-4 w-5/6 mx-auto">
                <div className="flex items-center flex-1 rounded-lg">

                </div>
                <div className="flex flex-1 items-center">
                    <div className="flex flex-col w-1/2 space-y-4 items-stretch mx-auto">
                        {props.params.services.map((service: any) =>
                            <div onClick={() => pushPrint(service._id)} key={service._id} className="transition-all ease-in-out active:scale-95 duration-300 border border-blue-400 flex-1 rounded-lg shadow-lg flex flex-col items-center p-8 text-white bg-gradient-to-tr from-blue-600 to-sky-400 hover:border-blue-500 hover:transition-all hover:from-blue-700 hover:to-sky-400 active:from-blue-800 active:to-sky-500 cursor-pointer ">
                                <div className="mx-auto text-center ">
                                    <div className="text-3xl font-bold">{service.name}</div>
                                    <div className="text-xl font-semibold">{service.description}</div>
                                </div>
                            </div>)}
                    </div>
                </div>

            </div>
            {props.params.setting && props.params.setting.ticker.touch &&
                <div className="text-3xl font-semibold flex p-4 bg-white items-center shadow-lg">
                    <Marquee>
                        {
                            tickers.map((ticker: string) => <Fragment key={Math.random()}>{ticker}</Fragment>)
                        }
                    </Marquee>
                </div>
            }

            {((props.params.setting && !props.params.setting.ticker.touch) || !props.params.setting) && <div></div>}

        </main>
    )
}