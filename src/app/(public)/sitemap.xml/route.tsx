import { NextApiRequest, NextApiResponse } from 'next';
import { headers } from 'next/headers';

export async function GET(request: Request) {
    return new Response(`<?xml version="1.0" encoding="UTF-8"?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <sitemap>
        <loc>${process.env.SITE_URL}/sitemap/pages.xml</loc>
    </sitemap>
    <sitemap>
        <loc>${process.env.SITE_URL}/sitemap/categories.xml</loc>
    </sitemap>
    <sitemap>
        <loc>${process.env.SITE_URL}/sitemap/posts.xml</loc>
    </sitemap>
    <sitemap>
        <loc>${process.env.SITE_URL}/sitemap/quran.xml</loc>
    </sitemap>
</sitemapindex>`, {status: 200 , headers: {'Content-Type': 'text/xml'} })
}