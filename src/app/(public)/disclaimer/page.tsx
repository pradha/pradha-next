'use client'
import { OtherPages, Pages } from "@/components/ui/layouts/pages";
import { BriefcaseIcon, CheckIcon, EyeSlashIcon, HomeIcon, PhoneIcon, QuestionMarkCircleIcon } from "@heroicons/react/24/outline";
import Link from "next/link";
import { usePathname } from "next/navigation";

export default function PortofolioPage(props: any) {
    const pathName = usePathname()
    return <div className="relative max-w-6xl flex lg:flex-row flex-col lg:space-x-4 space-x-0 items-stretch mb-4 mx-auto">
        <div className="relative hidden lg:block lg:w-1/3 text-gray-700  dark:text-slate-300 ">
            <div className="sticky top-16">
                <Pages />
                <OtherPages />
            </div>
        </div>
        <div className="relative bg-white shadow-lg w-full lg:rounded-xl rounded-none lg:py-4 py-2 px-6 dark:bg-slate-700">
            <h1 className="text-xl font-semibold mb-2">Disclaimer for {process.env.NEXT_PUBLIC_APP_NAME}</h1>

            <p className="mb-4">If you require any more information or have any questions about our site's disclaimer, please feel free to contact us by email at info@{process.env.NEXT_PUBLIC_SITE_URL?.replaceAll("https://","")?.replaceAll("http://","")?.replaceAll("www.","")}.</p>

            <h2 className="text-lg font-semibold mb-2">Disclaimers for {process.env.NEXT_PUBLIC_SITE_URL?.replaceAll("https://","")?.replaceAll("http://","")}</h2>

            <p className="mb-4">All the information on this website - {process.env.NEXT_PUBLIC_SITE_URL} - is published in good faith and for general information purpose only. {process.env.NEXT_PUBLIC_SITE_URL?.replaceAll("https://","")?.replaceAll("http://","")?.replaceAll("www.","")} does not make any warranties about the completeness, reliability and accuracy of this information. Any action you take upon the information you find on this website ({process.env.NEXT_PUBLIC_SITE_URL?.replaceAll("https://","")?.replaceAll("http://","")?.replaceAll("www.","")}), is strictly at your own risk. {process.env.NEXT_PUBLIC_SITE_URL?.replaceAll("https://","")?.replaceAll("http://","")?.replaceAll("www.","")} will not be liable for any losses and/or damages in connection with the use of our website.</p>

            <p className="mb-4">From our website, you can visit other websites by following hyperlinks to such external sites. While we strive to provide only quality links to useful and ethical websites, we have no control over the content and nature of these sites. These links to other websites do not imply a recommendation for all the content found on these sites. Site owners and content may change without notice and may occur before we have the opportunity to remove a link which may have gone 'bad'.</p>

            <p className="mb-4">Please be also aware that when you leave our website, other sites may have different privacy policies and terms which are beyond our control. Please be sure to check the Privacy Policies of these sites as well as their "Terms of Service" before engaging in any business or uploading any information.</p>

            <h2 className="text-lg font-semibold mb-2">Consent</h2>

            <p className="mb-4">By using our website, you hereby consent to our disclaimer and agree to its terms.</p>

            <h2 className="text-lg font-semibold mb-2">Update</h2>

            <p className="mb-4">Should we update, amend or make any changes to this document, those changes will be prominently posted here.</p>
        </div>
    </div>
}