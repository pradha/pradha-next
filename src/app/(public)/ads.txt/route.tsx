import { cookies } from 'next/headers';
import { notFound } from 'next/navigation';

export async function GET(request: Request) {
    const cookieStore = cookies();
    const token = cookieStore.get('token');

    if (process.env?.GOOGLE_PUBLISHER && process.env?.GOOGLE_PUBLISHER != "")
        return new Response(`google.com, pub-5989727903620612, DIRECT, f08c47fec0942fa0`, {
            status: 200,
            headers: { 'Set-Cookie': `token=${token}` },
        });
    else
        notFound()
}