'use client'
import { OtherPages, Pages } from "@/components/ui/layouts/pages";
import { BriefcaseIcon, CheckIcon, EyeSlashIcon, HomeIcon, PhoneIcon, QuestionMarkCircleIcon } from "@heroicons/react/24/outline";
import Link from "next/link";
import { usePathname } from "next/navigation";

export default function AboutPage(props: any) {
    const pathName = usePathname()
    return <div className="relative max-w-6xl flex lg:flex-row flex-col lg:space-x-4 space-x-0 items-stretch mb-4 mx-auto">
        <div className="relative hidden lg:block lg:w-1/3 text-gray-700  dark:text-slate-300 ">
            <div className="sticky top-16">
                <Pages />
                <OtherPages />
            </div>
        </div>
        <div className="relative bg-white shadow-lg w-full lg:rounded-xl rounded-none lg:py-4 py-2 px-6 dark:bg-slate-700">
            <h1 className="text-2xl font-semibold mb-4" >Privacy Policy</h1>

            <p className="my-4">At {process.env.NEXT_PUBLIC_APP_NAME}, accessible from {process.env.NEXT_PUBLIC_SITE_URL}, one of our main priorities is the privacy of our visitors. This Privacy Policy document contains types of information that is collected and recorded by {process.env.NEXT_PUBLIC_APP_NAME} and how we use it.</p>

            <p className="my-4">If you have additional questions or require more information about our Privacy Policy, do not hesitate to <Link href="/contact">contact</Link> us.</p>

            <h2 className="text-lg font-semibold mb-4">Log Files</h2>

            <p className="my-4">{process.env.NEXT_PUBLIC_APP_NAME} follows a standard procedure of using log files. These files log visitors when they visit websites. All hosting companies do this and a part of hosting services' analytics. The information collected by log files include internet protocol (IP) addresses, browser type, Internet Service Provider (ISP), date and time stamp, referring/exit pages, and possibly the number of clicks. These are not linked to any information that is personally identifiable. The purpose of the information is for analyzing trends, administering the site, tracking users' movement on the website, and gathering demographic information.</p>

            <h2 className="text-lg font-semibold mb-4">Cookies and Web Beacons</h2>

            <p className="my-4">Like any other website, {process.env.NEXT_PUBLIC_APP_NAME} uses 'cookies'. These cookies are used to store information including visitors' preferences, and the pages on the website that the visitor accessed or visited. The information is used to optimize the users' experience by customizing our web page content based on visitors' browser type and/or other information.</p>

            <h2 className="text-lg font-semibold mb-4">Google DoubleClick DART Cookie</h2>

            <p className="my-4">Google is one of a third-party vendor on our site. It also uses cookies, known as DART cookies, to serve ads to our site visitors based upon their visit to www.website.com and other sites on the internet. However, visitors may choose to decline the use of DART cookies by visiting the Google ad and content network Privacy Policy at the following URL – <Link href="https://policies.google.com/technologies/ads">https://policies.google.com/technologies/ads</Link></p>

            <h2 className="text-lg font-semibold mb-4">Our Advertising Partners</h2>

            <p className="my-4">Some of advertisers on our site may use cookies and web beacons. Our advertising partners are listed below. Each of our advertising partners has their own Privacy Policy for their policies on user data. For easier access, we hyperlinked to their Privacy Policies below.</p>

            <ul className="mb-4">
                <li>
                    <p>Google</p>
                    <p><Link href="https://policies.google.com/technologies/ads">https://policies.google.com/technologies/ads</Link></p>
                </li>
            </ul>

            <h2 className="text-lg font-semibold mb-4">Privacy Policies</h2>

            <p className="my-4">You may consult this list to find the Privacy Policy for each of the advertising partners of {process.env.NEXT_PUBLIC_APP_NAME}.</p>

            <p className="my-4">Third-party ad servers or ad networks uses technologies like cookies, JavaScript, or Web Beacons that are used in their respective advertisements and links that appear on {process.env.NEXT_PUBLIC_APP_NAME}, which are sent directly to users' browser. They automatically receive your IP address when this occurs. These technologies are used to measure the effectiveness of their advertising campaigns and/or to personalize the advertising content that you see on websites that you visit.</p>

            <p className="my-4">Note that {process.env.NEXT_PUBLIC_APP_NAME} has no access to or control over these cookies that are used by third-party advertisers.</p>

            <h2 className="text-lg font-semibold mb-4">Third Party Privacy Policies</h2>

            <p className="my-4">{process.env.NEXT_PUBLIC_APP_NAME}'s Privacy Policy does not apply to other advertisers or websites. Thus, we are advising you to consult the respective Privacy Policies of these third-party ad servers for more detailed information. It may include their practices and instructions about how to opt-out of certain options. </p>

            <p className="my-4">You can choose to disable cookies through your individual browser options. To know more detailed information about cookie management with specific web browsers, it can be found at the browsers' respective websites. What Are Cookies?</p>

            <h2 className="text-lg font-semibold mb-4">Children's Information</h2>

            <p className="my-4">Another part of our priority is adding protection for children while using the internet. We encourage parents and guardians to observe, participate in, and/or monitor and guide their online activity.</p>

            <p className="my-4">{process.env.NEXT_PUBLIC_APP_NAME} does not knowingly collect any Personal Identifiable Information from children under the age of 13. If you think that your child provided this kind of information on our website, we strongly encourage you to contact us immediately and we will do our best efforts to promptly remove such information from our records.</p>

            <h2 className="text-lg font-semibold mb-4">Online Privacy Policy Only</h2>

            <p className="my-4">This Privacy Policy applies only to our online activities and is valid for visitors to our website with regards to the information that they shared and/or collect in {process.env.NEXT_PUBLIC_APP_NAME}. This policy is not applicable to any information collected offline or via channels other than this website.</p>

            <h2 className="text-lg font-semibold mb-4">Consent</h2>

            <p className="my-4">By using our website, you hereby consent to our Privacy Policy and agree to its Terms and Conditions.</p>
        </div>
    </div>
}