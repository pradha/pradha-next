import PublicLayout from "@/components/ui/layouts/public"

export default async function RootLayout(props:any) {
    return (
        <PublicLayout>{props.children}</PublicLayout>
    )
}

const title = 'Privacy Policy'
const description = 'This Privacy Policy document contains types of information that is collected and recorded by '+process.env.NAME+' and how we use it.'
const keywords = ["privacy policy"]
const url = process.env.SITE_URL + '/privacy-policy'
export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: true,
        follow: false,
        nocache: false,
        googleBot: {
            index: true,
            follow: false
        },
    },
}