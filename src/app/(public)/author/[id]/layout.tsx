import PublicLayout from "@/components/ui/layouts/public"
import clientPromise from "@/lib/connections/mongo"
import { ObjectId } from "mongodb"

export default async function RootLayout(props: any) {
    const db = (await clientPromise).db()
    const author = JSON.parse(JSON.stringify(await db.collection("v_system_users").findOne({ _id: new ObjectId(props.params.id) })))
    return (
        <PublicLayout>{props.children}</PublicLayout>
    )
}