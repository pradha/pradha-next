import { OtherCategories } from "@/components/ui/layouts/pages"
import clientPromise from "@/lib/connections/mongo"
import { Truncate } from "@/lib/text"
import { FullShort } from "@/lib/time"
import { ObjectId } from "mongodb"
import Link from "next/link"
import Image from "next/image"

export const revalidate = 0
export async function generateMetadata(props: any) {
    const db = (await clientPromise).db()
    const author = JSON.parse(JSON.stringify(await db.collection("v_system_users").findOne({ _id: new ObjectId(props.params.id) })))
    const title = "Author : " + (author?.name ? author.name : "Not Found")
    const description = author?.biography ? author.biography : "Not Found"
    const keywords = author?.name
    const url = process.env.SITE_URL + '/author/' + author?._id
    return {
        title: title,
        description: description,
        keywords: keywords,
        openGraph: {
            title: title,
            description: description,
            url: url,
        },
        twitter: {
            title: title,
            description: description,
        },
        alternates: {
            canonical: url,
            languages: {},
        },
        robots: {
            index: true,
            follow: true,
            nocache: false,
            googleBot: {
                index: true,
                follow: true
            },
        },
    }
}

export default async function AuthorPage(props: any) {
    const db = (await clientPromise).db()
    const author = JSON.parse(JSON.stringify(await db.collection("v_system_users").findOne({ _id: new ObjectId(props.params.id) })))
    let categories = JSON.parse(JSON.stringify(await db.collection("v_content_categories").find().toArray()))
    let dataPerPage = process.env.DATA_PER_PAGE ? parseInt(process.env.DATA_PER_PAGE) : 15
    const posts = await db.collection("v_content_posts").find({ 'created.by._id': new ObjectId(author?._id), status: 'Published' }).sort('published.at', -1).limit(dataPerPage).toArray()
    return <div className="relative max-w-6xl flex lg:flex-row flex-col lg:space-x-4 space-x-0 items-stretch mb-4 mx-auto">
        <div className="relative hidden lg:block lg:w-1/3 text-gray-700  dark:text-slate-300 ">
            <div className="sticky top-16">
                <div className="bg-white shadow-lg rounded-xl group mb-4 dark:bg-slate-700  p-4 gap-3 ">
                    <div className="font-semibold mb-2">Ditulis Oleh</div>
                    <div className="flex mb-2 space-x-4">
                        <div className="relative h-10 w-10">
                            <img
                                className="h-full w-full rounded-full object-cover object-center"
                                src={author?.photo ? author.photo : "/img/person.jpg"}
                                alt="Person"
                            />
                            <span className={"absolute right-0 bottom-0 h-2 w-2 rounded-full ring ring-white " + (author?.status?.active ? "bg-green-400" : "bg-red-400")}></span>
                        </div>
                        <div className="text-sm">
                            <div className="font-medium text-slate-700 dark:text-slate-200"><Link href={"/author/" + author?._id} title={author?.name}>{author?.name}</Link></div>
                            <div className="text-gray-400 dark:text-slate-300 text-sm">{FullShort(author?.created?.at)}</div>
                        </div>
                    </div>
                    <p className="text-sm">{author?.biography}</p>
                </div>
                <OtherCategories categories={categories} />

            </div>
        </div>
        <div className="relative bg-white shadow-lg w-full rounded-xl lg:py-4 py-2 px-6 dark:bg-slate-700">
            <h1 className="text-2xl  mb-4">Author : <span className="font-semibold">{author?.name}</span></h1>
            <div className="block lg:hidden text-sm mb-4">{author?.biography}</div>
            {posts?.map((post: any, i: number) => <Link key={post._id} href={"/read/" + post.slug} className="flex lg:flex-row flex-col bg-white shadow-lg rounded-lg overflow-hidden mb-4 dark:bg-slate-600 transition-all transform ease-in-out hover:shadow-2xl">
                <div className="relative lg:w-1/3 w-full bg-cover h-48" >
                    <Image src={process.env.NEXT_PUBLIC_STATIC_FILES + post.thumbnail} alt={post.thumbnail_caption} fill className="object-cover w-96 h-52" sizes="(max-width: 768px) 100vw,
              (max-width: 1200px) 50vw,
              33vw" blurDataURL="data:image/webp;base64,UklGRvwDAABXRUJQVlA4WAoAAAAgAAAAwQAAwQAASUNDUMgBAAAAAAHIAAAAAAQwAABtbnRyUkdCIFhZWiAAAAAAAAAAAAAAAABhY3NwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAA9tYAAQAAAADTLQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAlkZXNjAAAA8AAAACRyWFlaAAABFAAAABRnWFlaAAABKAAAABRiWFlaAAABPAAAABR3dHB0AAABUAAAABRyVFJDAAABZAAAAChnVFJDAAABZAAAAChiVFJDAAABZAAAAChjcHJ0AAABjAAAADxtbHVjAAAAAAAAAAEAAAAMZW5VUwAAAAgAAAAcAHMAUgBHAEJYWVogAAAAAAAAb6IAADj1AAADkFhZWiAAAAAAAABimQAAt4UAABjaWFlaIAAAAAAAACSgAAAPhAAAts9YWVogAAAAAAAA9tYAAQAAAADTLXBhcmEAAAAAAAQAAAACZmYAAPKnAAANWQAAE9AAAApbAAAAAAAAAABtbHVjAAAAAAAAAAEAAAAMZW5VUwAAACAAAAAcAEcAbwBvAGcAbABlACAASQBuAGMALgAgADIAMAAxADZWUDggDgIAALAUAJ0BKsIAwgA+7XCwVK0mJCMjU8mxoB2JZ27gCA2P7UuDUbB9eMfoBcDe/GvCX4kS3JrR3qvu2othCTck3cluZuNnY3y0tE22kAmbB73x82OQSh4UkhKC+QKH47sehZdaeqxb/WWZavQji1p+3sXO5DOEESU4biG50oYJrqHMShSuJaRjQZ3Z/jCZXxB7H73RAq3E3btzzznJLOx2dfJHE12dnLWX5VAzePBuAAD+7RoxlXq9Mh73LRXj2GcwTcJrX1SZbsNzyMQpOHkE3jX3tMWspBdfzLgUnOBdhHc97O9PxD63gCUyTYQQYFSly343ihzwQ6xbwDMDF9ExSjVJd8PrKvyEWv0ymJYO6HNpiInsKib0cGpX5dmeoafkqvmoLNl1MjhztGd2SArxEon17Gced9ixPyRvxp2R7NNY8n2ViYn3X3TD8zXXCeuR+GwCkQDavPgVxhICYYeFiYLmlfYH5tMgBjjTmDR4RLiKwUTU5ku5/JIh+z7w5uue0zBtmcyLTNpt9kv4pt4HNccE9VWI/IpPi7/yxrga84cO7N57Zaicm+vpHrewzE42g0kyaC4NixL+b64U3s20IALku160Tsq+n9/jdQXUq4pvbM1IlixSb76prSwkU0uakKB2UZ5prX7jEPHRlbshuBvLVaPEaoHTuv0z09ZE984+C3IHYU7XWjflaErgAAA="
                        placeholder="blur" loading="lazy" />
                </div>
                <div className="lg:w-2/3 w-full p-4">
                    <h3 title={post?.title} className="text-gray-900 font-bold text-lg dark:text-slate-200">{Truncate(post?.title, 60)}</h3>
                    <p className="my-2 text-gray-600 text-sm dark:text-slate-300">{post.description.substring(0, 150)}</p>
                    <div className="text-xs text-slate-600 dark:text-slate-400">{post.published?.at ? FullShort(post.published?.at) : FullShort(post.created?.at)} <span className="hidden lg:inline-block">&#8594; by {post.created?.by?.name}</span></div>
                </div>
            </Link>)}
        </div>
    </div>
}

