import clientPromise from "@/lib/connections/mongo"
import { notFound } from "next/navigation"
import Image from "next/image"
import Link from "next/link"
import { ObjectId } from "mongodb"

export const revalidate = 0;
export default async function SubscribePage(props: any) {
    const db = (await clientPromise).db()
    let check = await db.collection("system_subscribers").findOne({ email: decodeURIComponent(props.params.email) })
    if (check) {
        await db.collection("system_subscribers").updateOne({ _id: new ObjectId(check._id) }, { $set: { status: "Deleted" } })
        return <section className='grid content-center min-h-screen bg-slate-300 dark:bg-slate-600'>
            <div className="container mx-auto transition ease-in-out delay-150">
                <div className='flex justify-center px-3 my-6 lg:px-6 lg:my-12'>
                    <div className='flex w-full shadow-lg xl:w-1/2 lg:w-10/12 rounded-xl'>
                        <div className='relative w-full p-4 bg-white lg:p-4 dark:bg-slate-700 rounded-xl'>
                            <Image className="block mx-auto mt-4 lg:hidden" src="/img/logo.webp" width={50} height={50} alt="logo" loading='lazy' />
                            <h3 className="pt-4 mb-2 text-2xl text-center dark:text-gray-200">Unsubscribed</h3>
                            <div className="mb-8 text-center dark:text-gray-200">Your email {check?.email} has been successfully UnSubscribed from our updates</div>
                            <div className="text-center flex space-x-4 justify-center mb-4">
                                <Link href={"/"} title={"Home Page"} className="link p-0">Home</Link>
                                <Link href={"/about"} title={"Abou"} className="link p-0">About Us</Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    }
    else notFound()

}
const title = 'Unsubscribe '
const description = 'Unsubscribe'
const keywords = ["Unsubscribe", "Unsubscribed"]
const url = process.env.SITE_URL + '/subscribe/delete'
export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: false,
        follow: false,
        nocache: false,
        googleBot: {
            index: false,
            follow: false
        },
    },
}