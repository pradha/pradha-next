
import clientPromise from "@/lib/connections/mongo";
import fs from 'fs'
import { ObjectId } from "mongodb";
import { notFound } from "next/navigation";

export const revalidate = 0
export const dynamic = 'force-dynamic'
let number: string = "0"
export default async function RootLayout(props: any) {
    number = props.params.number;
    const db = (await clientPromise).db()
    const data = JSON.parse(JSON.stringify(await db.collection("v_accounting_transactions").findOne({_id: new ObjectId(props.params.id)})))
    
    if (!data)
        notFound() 
    props.params.data = data

    return (
        <>{props.children}</>
    )
}

const title = 'Print'
const description = 'Print Transaction'
const keywords = ["print", "transaction", "print transaction"]
const url = process.env.SITE_URL + '/accounting/transaction/print/'
export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: false,
        follow: false,
        nocache: false,
        googleBot: {
            index: false,
            follow: false
        },
    },
}