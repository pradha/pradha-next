'use client'

import { DayDateFormat, FullShort } from "@/lib/time"
import { Fragment, useState } from "react"
import Image from "next/image"
import { FormatMoney } from "@/lib/text"


export default function Touch(props: any) {
    const [data, setData] = useState(props.params.data)
    let debit = {
        number: "",
        name: "",
        description: "",
    }

    if (data?.type == "IV")
        data?.journals.map((journal: any) => {
            if (journal.credit.$numberDecimal == 0.00)
                debit = {
                    number: journal.account.number,
                    name: journal.account.name,
                    description: journal.account.description,
                }
        })
    let tdebit: number = 0, tcredit: number = 0
    let amount: number = 0.00
    return (
        <main id="print" className={"mx-auto " + (data?.type == "IV" ? "w-64" : "max-w-3xl")}>

            <div className="flex flex-row space-x-4 items-stretch mt-2 mb-6 mx-auto content-center">
                <div className="flex flex-col flex-1 items-center rounded-lg content-center">
                    <Image className="block my-2" src="/img/logo.webp" width={120} height={120} alt="logo" priority />
                    <div className="font-bold">{process.env.NEXT_PUBLIC_APP_NAME}</div>
                    <div className="font-bold">#{data.code}</div>
                    <div className="font-semibold text-sm">{FullShort(data.confirmed.at.toString())}</div>
                    <div className="text-sm">{debit?.number} - {debit?.name}</div>
                </div>

            </div>
            {data?.type == "IV" && data.journals.filter((journal: any) => { if (data?.type == "IV" && journal.credit.$numberDecimal > 0) return true }).map((journal: any) => {
                tdebit = tdebit + parseFloat(journal.debit.$numberDecimal);
                tcredit = tcredit + parseFloat(journal.credit.$numberDecimal);
                return <div key={"journal-" + journal.id} className={"flex flex-row content-stretch"}>
                    <div className="flex flex-col flex-1 mb-2 text-sm">
                        <div className="font-semibold">{journal.account.name}</div>
                        <div className="text-xs">{FormatMoney(journal.price.$numberDecimal)} x {journal.quantity}</div>
                    </div>
                    <div className="flex flex-1 text-right justify-end align-bottom text-sm">
                        {FormatMoney(journal.credit.$numberDecimal)}
                    </div>
                </div>
            })}
            <div className={"flex flex-row content-stretch my-2"}>
                <div className="flex flex-col flex-1 mb-2 text-sm">
                    <div className="font-semibold">Total</div>
                </div>
                <div className="flex flex-1 text-right justify-end align-bottom">
                    {FormatMoney(tcredit)}
                </div>
            </div>
            <div className={"flex flex-row content-stretch my-2"}>
                <div className="flex flex-col flex-1 mb-2 text-sm">
                   Setor
                </div>
                <div className="flex flex-1 justify-end align-bottom">
                    : .....................
                </div>
            </div>
            <div className={"flex flex-row content-stretch my-2"}>
                <div className="flex flex-col flex-1 mb-2 text-sm">
                    Sisa
                </div>
                <div className="flex flex-1 justify-end align-bottom">
                    : .....................
                </div>
            </div>
            <div className={"flex flex-row content-stretch my-2"}>
                <div className="flex flex-col flex-1 mb-2 text-sm">
                    <div className="font-semibold">Jumlah Total</div>
                </div>
                <div className="flex flex-1  justify-end align-bottom">
                    : .....................
                </div>
            </div>
            <div className={"flex flex-row content-stretch my-2"}>
                <div className="flex flex-col flex-1 mb-2 text-sm">
                    Keterangan
                </div>
                <div className="flex-1">
                    :
                </div>
            </div>
            <br/>
            <br/>
            <br/>
            <div className="text-lg text-center">Terima Kasih</div>
        </main>
    )
}