import PublicLayout from "@/components/ui/layouts/public"

export default async function RootLayout(props:any) {
    return (
        <PublicLayout>{props.children}</PublicLayout>
    )
}

const title = 'Terms & Conditions'
const description = 'These terms and conditions outline the rules and regulations for the use of '+process.env.NAME
const keywords = ["privacy policy"]
const url = process.env.SITE_URL + '/terms-conditions'
export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: true,
        follow: false,
        nocache: false,
        googleBot: {
            index: true,
            follow: false
        },
    },
}