
import clientPromise from "@/lib/connections/mongo";
import fs from 'fs'
import { ObjectId } from "mongodb";
import { notFound } from "next/navigation";

export const revalidate = 0
export const dynamic = 'force-dynamic'
let number: string = "0"
export default async function RootLayout(props: any) {
    number = props.params.number;
    const db = (await clientPromise).db()
    const billing = await db.collection("v_system_billing").findOne({ _id: new ObjectId(props.params.id) })
    
    if (!billing)
        notFound()

    props.params.billing = JSON.parse(JSON.stringify(billing))

    return (
        <>{props.children}</>
    )
}

const title = 'Billing'
const description = 'Print Billing'
const keywords = ["print", "billing", "print billing"]
const url = process.env.SITE_URL + '/system/billing/print/'
export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: false,
        follow: false,
        nocache: false,
        googleBot: {
            index: false,
            follow: false
        },
    },
}