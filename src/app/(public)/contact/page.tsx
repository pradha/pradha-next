'use client'
import { Transition } from "@headlessui/react";
import Link from "next/link";
import Image from "next/image";
import { FormEvent, useCallback, useState } from "react";
import { useGoogleReCaptcha } from "react-google-recaptcha-v3";
import path from "path";
import { usePathname } from "next/navigation";
import { Spinner } from "@/components/ui/loader";
import { Success, Warning } from "@/components/ui/alert";
import { EnvelopeIcon, UserIcon } from "@heroicons/react/24/outline";

export default function ContactPage(props: any) {
    const { executeRecaptcha } = useGoogleReCaptcha();
    const pathName = usePathname()
    let errors = {
        recaptcha: "",
        name: "",
        email: "",
        phone: "",
        message: ""
    }

    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState({ errors: errors })
    const [success, setSuccess] = useState({ title: "", description: "" })

    const handleSumitForm = useCallback((e: FormEvent) => {
        e.preventDefault();
        setWarning({ errors: errors })
        setSpinner(true)
        if (!executeRecaptcha) {
            //console.log("Execute recaptcha not yet available");
            return;
        }
        executeRecaptcha("enquiryFormSubmit").then((gReCaptchaToken) => {
            //console.log(gReCaptchaToken, "response Google reCaptcha server");
            submit(gReCaptchaToken, e);
        });
    }, [executeRecaptcha]);

    const submit = async (gReCaptchaToken: string, e: FormEvent) => {
        setWarning({ errors: errors })
        setSpinner(true)

        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement
        const submit = await fetch(path.posix.join("/api/", "contact"), {
            method: 'POST',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                "Set-Cookie": "SameSite=Lax; Secure"
            },
            body: JSON.stringify({
                name: (document.getElementById("name") as HTMLInputElement).value,
                email: target.email.value,
                phone: target.phone.value,
                message: target.message.value,
                gRecaptchaToken: gReCaptchaToken,
            })
        });
        //await new Promise(resolve => setTimeout(resolve, 3000));
        const res = await submit.json()
        if (submit.ok) {
            (document.getElementById("name") as HTMLInputElement).value = ""
            target.email.value = ""
            target.phone.value = ""
            target.message.value = ""

            setSuccess({ title: "Success", description: "Thank you for contacting us. Your message has been received, We will follow up your message in about 1 day" })
        } else {
            errors = await res.errors
            setWarning({ errors: errors })
            console.log(warning, errors)
            errors = {
                recaptcha: "",
                name: "",
                email: "",
                phone: "",
                message: ""
            }

        }
        setSpinner(false)
    }
    return <div className='flex lg:space-x-8 flex-col lg:flex-row space-y-8 lg:space-y-0 items-center lg:mt-8'>
        <div className='flex-1'>
            <div className='hidden lg:flex items-center flex-wrap lg:space-x-4 space-x-2 justify-center order-last lg:order-first lg:text-base text-xs mb-8'>
                <Image
                    className='hover:scale-105 transition-transform duration-300 mx-auto'
                    src="/img/logo.webp"
                    alt="Logo"
                    width={180}
                    height={180}
                    placeholder='blur'
                    loading='lazy'
                    blurDataURL="/img/logo.webp"
                />
            </div>
            <h2 className='text-center mb-4 text-2xl'>Follow me on social media</h2>
            <div className='flex items-center flex-wrap lg:space-x-4 space-x-2 justify-center order-last lg:order-first lg:text-base text-xs mb-8'>
                <Link href="https://www.linkedin.com/in/adityudhna" title='My LinkedIn Page' className='hover:scale-105 transition-all duration-300 flex flex-col items-center justify-center' target="_blank">
                    <svg fill="#0077b5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" width="32px" height="32px"><path d="M48,8H16c-4.418,0-8,3.582-8,8v32c0,4.418,3.582,8,8,8h32c4.418,0,8-3.582,8-8V16C56,11.582,52.418,8,48,8z M24,47h-5V27h5 V47z M24.029,23.009C23.382,23.669,22.538,24,21.5,24c-1.026,0-1.865-0.341-2.519-1.023S18,21.458,18,20.468 c0-1.02,0.327-1.852,0.981-2.498C19.635,17.323,20.474,17,21.5,17c1.038,0,1.882,0.323,2.529,0.969 C24.676,18.615,25,19.448,25,20.468C25,21.502,24.676,22.349,24.029,23.009z M47,47h-5V35.887C42,32.788,40.214,31,38,31 c-1.067,0-2.274,0.648-2.965,1.469S34,34.331,34,35.594V47h-5V27h5v3.164h0.078c1.472-2.435,3.613-3.644,6.426-3.652 C44.5,26.5,47,29.5,47,34.754V47z" /></svg>
                    LinkedIn
                </Link>
                <Link href="https://www.facebook.com/dyata" className='hover:scale-105 transition-all duration-300 flex flex-col items-center justify-center' target="_blank">
                    <svg fill="#4267B2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" width="32px" height="32px"><path d="M48,7H16c-4.418,0-8,3.582-8,8v32c0,4.418,3.582,8,8,8h17V38h-6v-7h6v-5c0-7,4-11,10-11c3.133,0,5,1,5,1v6h-4 c-2.86,0-4,2.093-4,4v5h7l-1,7h-6v17h8c4.418,0,8-3.582,8-8V15C56,10.582,52.418,7,48,7z" /></svg>
                    Facebook
                </Link>
                <Link href="https://twitter.com/Pradha_ID" title='My Twitter Page' className='hover:scale-105 transition-all duration-300 flex flex-col items-center justify-center' target="_blank">
                    <svg fill="#00acee" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" width="32px" height="32px"><path d="M61.932,15.439c-2.099,0.93-4.356,1.55-6.737,1.843c2.421-1.437,4.283-3.729,5.157-6.437	c-2.265,1.328-4.774,2.303-7.444,2.817C50.776,11.402,47.735,10,44.366,10c-6.472,0-11.717,5.2-11.717,11.611	c0,0.907,0.106,1.791,0.306,2.649c-9.736-0.489-18.371-5.117-24.148-12.141c-1.015,1.716-1.586,3.726-1.586,5.847	c0,4.031,2.064,7.579,5.211,9.67c-1.921-0.059-3.729-0.593-5.312-1.45c0,0.035,0,0.087,0,0.136c0,5.633,4.04,10.323,9.395,11.391	c-0.979,0.268-2.013,0.417-3.079,0.417c-0.757,0-1.494-0.086-2.208-0.214c1.491,4.603,5.817,7.968,10.942,8.067	c-4.01,3.109-9.06,4.971-14.552,4.971c-0.949,0-1.876-0.054-2.793-0.165C10.012,54.074,16.173,56,22.786,56	c21.549,0,33.337-17.696,33.337-33.047c0-0.503-0.016-1.004-0.04-1.499C58.384,19.83,60.366,17.78,61.932,15.439" /></svg>
                    Twitter
                </Link>
                <Link href="https://www.instagram.com/id.pradha" title='My Instagram Page' className='hover:scale-105 transition-all duration-300 flex flex-col items-center justify-center' target="_blank">
                    <svg fill="#962fbf" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" width="32px" height="32px"><path d="M 21.580078 7 C 13.541078 7 7 13.544938 7 21.585938 L 7 42.417969 C 7 50.457969 13.544938 57 21.585938 57 L 42.417969 57 C 50.457969 57 57 50.455062 57 42.414062 L 57 21.580078 C 57 13.541078 50.455062 7 42.414062 7 L 21.580078 7 z M 47 15 C 48.104 15 49 15.896 49 17 C 49 18.104 48.104 19 47 19 C 45.896 19 45 18.104 45 17 C 45 15.896 45.896 15 47 15 z M 32 19 C 39.17 19 45 24.83 45 32 C 45 39.17 39.169 45 32 45 C 24.83 45 19 39.169 19 32 C 19 24.831 24.83 19 32 19 z M 32 23 C 27.029 23 23 27.029 23 32 C 23 36.971 27.029 41 32 41 C 36.971 41 41 36.971 41 32 C 41 27.029 36.971 23 32 23 z" /></svg>
                    Instagram
                </Link>
            </div>
            <h2 className='text-center mb-4 text-2xl'>Direct Contact</h2>
            <div className='flex items-center flex-wrap lg:space-x-4 space-x-2 justify-center order-last lg:order-first lg:text-base text-xs mb-4'>
                <Link href="mailto:adit@pradha.id" title='My Email' className='hover:scale-105 transition-all duration-300 flex flex-col items-center justify-center' target="_blank">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" width="32px" height="32px" fill="currentColor">
                        <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z" />
                        <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z" />
                    </svg>
                    Mail
                </Link>
                <Link href="https://t.me/pradha" title='My Telegram Chat' className='hover:scale-105 transition-all duration-300 flex flex-col items-center justify-center' target="_blank">
                    <svg fill="#0088cc" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" width="32px" height="32px"><path d="M46.137,6.552c-0.75-0.636-1.928-0.727-3.146-0.238l-0.002,0C41.708,6.828,6.728,21.832,5.304,22.445	c-0.259,0.09-2.521,0.934-2.288,2.814c0.208,1.695,2.026,2.397,2.248,2.478l8.893,3.045c0.59,1.964,2.765,9.21,3.246,10.758	c0.3,0.965,0.789,2.233,1.646,2.494c0.752,0.29,1.5,0.025,1.984-0.355l5.437-5.043l8.777,6.845l0.209,0.125	c0.596,0.264,1.167,0.396,1.712,0.396c0.421,0,0.825-0.079,1.211-0.237c1.315-0.54,1.841-1.793,1.896-1.935l6.556-34.077	C47.231,7.933,46.675,7.007,46.137,6.552z M22,32l-3,8l-3-10l23-17L22,32z" /></svg>
                    Telegram
                </Link>

            </div>
        </div>
        <div className='flex-1 px-4 lg:px-0 order-first lg:order-last'>
            <h1 className='text-4xl font-bold mb-2'>Let's Talk</h1>
            <p className='mb-4 text-gray-500'>Contact me directly via social media or fill out the form and i will get back to you soon</p>
            {spinner && <Spinner className="rounded-tl-3xl rounded-br-3xl" />}
            <Success data={success} hide={() => setSuccess({ title: "", description: "" })} />
            {success.title == "" &&
                <form onSubmit={handleSumitForm} method='post' className='mb-8'>
                    <Warning className="" errors={Object.values(warning.errors).filter(a => a != "")} hide={() => setWarning({ errors: errors })} />
                    <div className='mb-4'>
                        <label htmlFor="name" className="block mb-2 text-sm">Name<sup className='text-red-600'>*</sup></label>
                        <div className="relative mb-2">
                            <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                <UserIcon className="text-gray-400 w-5 h-5" />
                            </div>
                            <input type="text" name="name" id="name" placeholder="Your Name" className={warning?.errors.name ? "input-icon-error" : "input-icon"} />
                        </div>
                    </div>
                    <div className='mb-4'>
                        <div className='flex lg:flex-row flex-col space-y-4 lg:space-y-0 space-x-0 lg:space-x-4'>
                            <div className='flex-1'>
                                <label htmlFor="email" className="block mb-2 text-sm">E-Mail<sup className='text-red-600'>*</sup></label>
                                <div className="relative mb-2">
                            <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                <EnvelopeIcon className="text-gray-400 w-5 h-5" />
                            </div>
                            <input type="email" name="email" id="email" placeholder="Your Email" className={warning?.errors.email ? "input-icon-error" : "input-icon"} />
                        </div>
                                
                            </div>
                            <div className='flex-1'>
                                <label htmlFor="phone" className="block mb-2 text-sm">Phone</label>
                                <input type="phone" name="phone" id="phone" placeholder="Your Phone" className="input" />
                            </div>
                        </div>
                    </div>
                    <div className='mb-4'>
                        <label htmlFor="message" className="block mb-2 text-sm">Message<sup className='text-red-600'>*</sup></label>
                        <textarea name="message" id="message" placeholder="Your Message" rows={5} className={warning?.errors.message ? "input-error" : "input"}></textarea>
                    </div>
                    <div>
                        <button type="submit" className="btn btn-primary w-full">Send</button>
                    </div>
                </form>
            }
        </div>
    </div>

}