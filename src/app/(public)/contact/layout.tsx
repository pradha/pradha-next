import ReCaptcha from "@/components/captcha/reCaptcha"
import PublicLayout from "@/components/ui/layouts/public"

export default async function RootLayout(props: any) {
    return (
        <ReCaptcha>
            <PublicLayout>{props.children}</PublicLayout>
        </ReCaptcha>
    )
}

const title = 'Contact'
const description = 'Contact me directly via my social media or fill out the form and i will get back to you soon'
const keywords = ["contact"]
const url = process.env.SITE_URL + '/contact'
export const metadata = {
    title: title,
    description: description,
    keywords: keywords,
    openGraph: {
        title: title,
        description: description,
        url: url,
    },
    twitter: {
        title: title,
        description: description,
    },
    alternates: {
        canonical: url,
        languages: {},
    },
    robots: {
        index: true,
        follow: true,
        nocache: false,
        googleBot: {
            index: true,
            follow: true
        },
    },
}