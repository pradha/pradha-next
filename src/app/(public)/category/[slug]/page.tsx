import { OtherCategories, OtherPages, Pages } from "@/components/ui/layouts/pages";
import { ParseHTML } from "@/lib/parseHtml";
import { Truncate } from "@/lib/text";
import { FullShort } from "@/lib/time";
import { BriefcaseIcon, CheckIcon, EyeSlashIcon, HomeIcon, PhoneIcon, QuestionMarkCircleIcon } from "@heroicons/react/24/outline";
import Link from "next/link";
import { notFound, usePathname } from "next/navigation";
import clientPromise from "@/lib/connections/mongo";
import { ObjectId } from "mongodb";
import Image from "next/image";

export const revalidate = 0
export async function generateMetadata(props: any) {
    const db = (await clientPromise).db()
    const category = await db.collection("v_content_categories").findOne({ slug: props.params.slug })
    const title = "Category : " + (category?.name ? category.name : "Not Found")
    const description = category?.description ? category.description : "Not Found"
    const keywords = category?.tags
    const url = process.env.SITE_URL + '/category/' + category?.slug
    return {
        title: title,
        description: description,
        keywords: keywords,
        openGraph: {
            title: title,
            description: description,
            url: url,
        },
        twitter: {
            title: title,
            description: description,
        },
        alternates: {
            canonical: url,
            languages: {},
        },
        robots: {
            index: true,
            follow: true,
            nocache: false,
            googleBot: {
                index: true,
                follow: true
            },
        },
    }
}

export default async function CategoryPage(props: any) {
    const db = (await clientPromise).db()
    const data = await db.collection("v_content_categories").findOne({ slug: props.params.slug })

    if (!data)
        notFound()

    let categories = JSON.parse(JSON.stringify(await db.collection("v_content_categories").find().toArray()))
    let dataPerPage = process.env.DATA_PER_PAGE ? parseInt(process.env.DATA_PER_PAGE) : 15
    const posts = await db.collection("v_content_posts").find({ 'categories._id': new ObjectId(data?._id), status: 'Published' }).sort('published.at', -1).limit(dataPerPage).toArray()
    return <div className="relative max-w-6xl flex lg:flex-row flex-col lg:space-x-4 space-x-0 items-stretch mb-4 mx-auto">
        <div className="relative hidden lg:block lg:w-1/3 text-gray-700  dark:text-slate-300 ">
            <div className="sticky top-16">
                <div className="bg-white shadow-lg rounded-xl group mb-4 dark:bg-slate-700 p-4">
                    <div className="font-semibold my-2 dark:text-slate-200">{data.name}</div>
                    <div className="text-sm">{data?.description}</div>
                </div>
                <OtherCategories categories={categories} />

            </div>
        </div>
        <div className="relative bg-white shadow-lg w-full rounded-xl lg:py-4 py-2 px-6 dark:bg-slate-700">
            <h1 className="text-2xl  mb-4">Category : <span className="font-semibold">{data.name}</span></h1>
            <div className="block lg:hidden text-sm mb-4">{data?.description}</div>
            {posts?.map((post: any, i: number) => <Link key={post._id} href={"/read/" + post.slug} className="flex lg:flex-row flex-col bg-white shadow-lg rounded-lg overflow-hidden mb-4 dark:bg-slate-600 transition-all transform ease-in-out hover:shadow-2xl">
                <div className="relative lg:w-1/3 w-full bg-cover h-48" >
                <Image src={process.env.NEXT_PUBLIC_STATIC_FILES + post.thumbnail} alt={post.thumbnail_caption} fill className="object-cover w-96 h-52" sizes="(max-width: 768px) 100vw,
              (max-width: 1200px) 50vw,
              33vw" blurDataURL="data:image/webp;base64,UklGRvwDAABXRUJQVlA4WAoAAAAgAAAAwQAAwQAASUNDUMgBAAAAAAHIAAAAAAQwAABtbnRyUkdCIFhZWiAAAAAAAAAAAAAAAABhY3NwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAA9tYAAQAAAADTLQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAlkZXNjAAAA8AAAACRyWFlaAAABFAAAABRnWFlaAAABKAAAABRiWFlaAAABPAAAABR3dHB0AAABUAAAABRyVFJDAAABZAAAAChnVFJDAAABZAAAAChiVFJDAAABZAAAAChjcHJ0AAABjAAAADxtbHVjAAAAAAAAAAEAAAAMZW5VUwAAAAgAAAAcAHMAUgBHAEJYWVogAAAAAAAAb6IAADj1AAADkFhZWiAAAAAAAABimQAAt4UAABjaWFlaIAAAAAAAACSgAAAPhAAAts9YWVogAAAAAAAA9tYAAQAAAADTLXBhcmEAAAAAAAQAAAACZmYAAPKnAAANWQAAE9AAAApbAAAAAAAAAABtbHVjAAAAAAAAAAEAAAAMZW5VUwAAACAAAAAcAEcAbwBvAGcAbABlACAASQBuAGMALgAgADIAMAAxADZWUDggDgIAALAUAJ0BKsIAwgA+7XCwVK0mJCMjU8mxoB2JZ27gCA2P7UuDUbB9eMfoBcDe/GvCX4kS3JrR3qvu2othCTck3cluZuNnY3y0tE22kAmbB73x82OQSh4UkhKC+QKH47sehZdaeqxb/WWZavQji1p+3sXO5DOEESU4biG50oYJrqHMShSuJaRjQZ3Z/jCZXxB7H73RAq3E3btzzznJLOx2dfJHE12dnLWX5VAzePBuAAD+7RoxlXq9Mh73LRXj2GcwTcJrX1SZbsNzyMQpOHkE3jX3tMWspBdfzLgUnOBdhHc97O9PxD63gCUyTYQQYFSly343ihzwQ6xbwDMDF9ExSjVJd8PrKvyEWv0ymJYO6HNpiInsKib0cGpX5dmeoafkqvmoLNl1MjhztGd2SArxEon17Gced9ixPyRvxp2R7NNY8n2ViYn3X3TD8zXXCeuR+GwCkQDavPgVxhICYYeFiYLmlfYH5tMgBjjTmDR4RLiKwUTU5ku5/JIh+z7w5uue0zBtmcyLTNpt9kv4pt4HNccE9VWI/IpPi7/yxrga84cO7N57Zaicm+vpHrewzE42g0kyaC4NixL+b64U3s20IALku160Tsq+n9/jdQXUq4pvbM1IlixSb76prSwkU0uakKB2UZ5prX7jEPHRlbshuBvLVaPEaoHTuv0z09ZE984+C3IHYU7XWjflaErgAAA="
                        placeholder="blur" loading="lazy" />
                </div>
                <div className="lg:w-2/3 w-full p-4">
                    <h3 title={post?.title} className="text-gray-900 font-bold text-lg dark:text-slate-200">{Truncate(post?.title, 60)}</h3>
                    <p className="my-2 text-gray-600 text-sm dark:text-slate-300">{Truncate(post.description, 160)}</p>
                    <div className="text-xs text-slate-600 dark:text-slate-400">{post.published?.at ? FullShort(post.published?.at) : FullShort(post.created?.at)} <span className="hidden lg:inline-block">&#8594; by {post.created?.by?.name}</span></div>
                </div>
            </Link>)}
        </div>
    </div>
}

