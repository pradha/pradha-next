import PublicLayout from "@/components/ui/layouts/public"

export default async function RootLayout(props: any) {
    return (
        <PublicLayout>{props.children}</PublicLayout>
    )
}


