import PublicLayout from "@/components/ui/layouts/public"
import clientPromise from "@/lib/connections/mongo"
import { ObjectId } from "mongodb"
import { notFound } from "next/navigation"

export default async function RootLayout(props: any) {
    const db = (await clientPromise).db()
    const account = await db.collection("v_system_users").findOne({ _id: new ObjectId(props.params.id), valid_thru: { $gte: new Date() }  })
    
    if (!account)
        notFound()
    props.params.account = JSON.parse(JSON.stringify(account))
    return (
        <>{props.children}</>
    )
}