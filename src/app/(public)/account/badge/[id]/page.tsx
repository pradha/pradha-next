'use client'

import { DayDateFormat } from "@/lib/time"
import { useState } from "react"
import Image from "next/image"
import { FormatMoney } from "@/lib/text"
import QRCode from "react-qr-code"


export default function Badge(props: any) {
    const [data, setData] = useState(props.params.account)
    return (
            <div className="mx-auto h-[1011px] w-[638px] py-16 leading-3 text-center dark:text-black" style={{
                backgroundImage: "url(/img/badge-bg.jpg)", backgroundSize: "cover", backgroundRepeat: "no-repeat", backgroundPosition: "center center"
            }}>
                <img
                    className="h-64 w-64 rounded-full ring-2 ring-white object-cover object-center mx-auto mb-16"
                    src={data?.photo ? data.photo : '/img/person.jpg'}
                    alt="Person"
                    loading="lazy"
                />
                <div className="font-semibold text-4xl p-0 m-0">{data?.name}</div>
                <div className="m-0 mb-16 p-0 text-2xl">Anggota SKMN</div>
                <div className="font-semibold text-2xl">No ID:<br /><span className="underline font-bold">{data?._id?.toString()}</span></div>
                <div className="font-semibold text-lg mb-16">VALID THRU : {data?.valid_thru ? DayDateFormat(data.valid_thru) : "N/A"}</div>
                <QRCode size={200} className={"my-4 mx-auto"} value={data._id} />
            </div>
    )
}