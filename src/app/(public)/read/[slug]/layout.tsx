import PublicLayout from "@/components/ui/layouts/public"
import clientPromise from "@/lib/connections/mongo";
import { Truncate } from "@/lib/text";
import { ISO8601DateTime } from "@/lib/time";
import { notFound } from "next/navigation";

export async function generateMetadata(props: any) {
    const db = (await clientPromise).db()
    const post = await db.collection("v_content_posts").findOne({ slug: props.params.slug, status: "Published" })
    const title = post?.title ? post.title : "Not Found"
    const description = post?.description ? Truncate(post.description, 155) : "Not Found"
    const keywords = post?.tags
    const type = "article"
    const url = process.env.SITE_URL + '/read/' + post?.slug
    const image = process.env.NEXT_PUBLIC_STATIC_FILES + post?.thumbnail
    return {
        title: title,
        description: description,
        keywords: keywords,
        openGraph: {
            type: type,
            title: title,
            description: description,
            url: url,
            publishedTime: ISO8601DateTime(post?.published.at),
            authors: [post?.created.by.name],
            images: [
                {
                    url: image,
                    width: 400,
                    height: 300,
                    alt: title,
                },
                {
                    url: image,
                    width: 800,
                    height: 600,
                    alt: title,
                },
                {
                    url: image,
                    width: 1200,
                    height: 630,
                    alt: title,
                },
                {
                    url: image,
                    width: 1800,
                    height: 1600,
                    alt: title,
                },
            ],
        },
        alternates: {
            canonical: url,
            languages: {},
        },
        twitter: {
            card: 'summary_large_image',
            title: title,
            description: description,
            images: [image],
        },
        robots: {
            index: true,
            follow: true,
            nocache: false,
            googleBot: {
                index: true,
                follow: true
            },
        },
    }
}
export default async function RootLayout(props: any) {
    const db = (await clientPromise).db()
    const post = JSON.parse(JSON.stringify(await db.collection("v_content_posts").findOne({ slug: props.params.slug, status: "Published" })))

    if (!post)
        notFound()
    let categories = JSON.parse(JSON.stringify(await db.collection("v_content_categories").find().toArray()))

    props.params.data = post
    props.params.categories = categories
    return (
        <PublicLayout>{props.children}</PublicLayout>
    )
}


