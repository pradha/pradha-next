'use client'
import { OtherCategories, OtherPages, Pages } from "@/components/ui/layouts/pages";
import { ListOfContents, ParseHTML } from "@/lib/parseHtml";
import { DayDateFormat, FullShort, ISO8601DateTime, ISODate } from "@/lib/time";
import Link from "next/link";
import { notFound, usePathname } from "next/navigation";
import Image from "next/image";
import { Truncate } from "@/lib/text";
import { SocialShare } from "@/components/social/share";
import { FormEvent, Fragment, useEffect, useState } from "react";
import { useSession } from "next-auth/react";
import path from "path";
import { Spinner } from "@/components/ui/loader";
import { Success, Warning } from "@/components/ui/alert";
import { MouseEvent } from 'react';

export const revalidate = 0

export default function ReadPage(props: any) {
    const { data: session, status } = useSession({
        required: false
    })

    let errors: string[] = []
    const [data, setData] = useState(props.params.data)
    const [categories, setCategories] = useState(props.params.categories)
    const [related, setRelated] = useState<any[]>([])
    const [scrollY, setScrollY] = useState(0);
    const [spinner, setSpinner] = useState(false)
    const [warning, setWarning] = useState(errors)
    const [success, setSuccess] = useState({ title: "", description: "" })
    const [reply, setReply] = useState<null | string>(null)
    const [replies, setReplies] = useState<any[]>([])
    const [comments, setComments] = useState({
        count: 0,
        data: [],
        dataPerPage: 0,
        page: 1,
        status: "Empty",
        total: 1,

    })

    if (!data)
        notFound()

    const jsonLd = {
        '@context': 'https://schema.org',
        '@type': 'Article',
        author: data?.created?.by?.name,
        name: data?.title,
        image: data?.thumbnail,
        description: data?.description,
        datePublished: ISODate(data?.published?.at),
        dateCreated: ISODate(data?.created?.at),
    };
    useEffect(() => {
        var scrollMaxY = (document.documentElement.scrollHeight - document.documentElement.clientHeight)
        function watchScroll() {
            window.addEventListener("scroll", () => setScrollY(window.pageYOffset));
            if (scrollY > 1 / 2 * scrollMaxY) {
                fetch('/api/post/related?slug=' + data.slug)
                    .then((data) => data.json())
                    .then((res) => {
                        setRelated(res.related)
                    })

            }
        }
        getComments()
        if (related.length <= 0)
            watchScroll();
        return () => {
            window.removeEventListener("scroll", () => setScrollY(window.pageYOffset));
        };
    }, []);

    const getComments = async () => {
        fetch('/api/post/comment?content=' + data._id)
            .then((data) => data.json())
            .then(async (res) => {
                setComments(res)
                let comms: string[] = []
                await Promise.all(
                    res.data.map((com: any) => {
                        comms.push(com._id)
                    })
                )
                getReplies(comms)

            })
    }
    const getReplies = async (comments: string[]) => {
        fetch('/api/post/comment/reply?comments=' + comments)
            .then((data) => data.json())
            .then((res) => {
                setReplies(res.data)
            })
    }
    const comment = async (e: FormEvent) => {
        e.preventDefault()
        setSuccess({ title: "", description: "" })
        setWarning(errors)
        setSpinner(true)

        if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
        }

        // await new Promise(resolve => setTimeout(resolve, 1000));
        let target = e.target as HTMLFormElement
        const submit = await fetch(path.posix.join("/api/post/comment"), {
            method: 'POST',
            credentials: 'include',
            cache: 'no-cache',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                content: data._id,
                comment: target.comment.value,
                reply: target?.reply ? target.reply.value : null
            })
        });
        //await new Promise(resolve => setTimeout(resolve, 3000));
        const res = await submit.json()
        if (submit.ok) {
            target.comment.value = ""
            if (!target?.reply) {
                getComments();
                setSuccess({ title: "Success", description: "Comment has been submitted" })
            } else {
                let comms: string[] = []
                await Promise.all(
                    comments.data.map((com: any) => {
                        comms.push(com._id)
                    })
                )
                getReplies(comms)
            }
            setReply(null)

        } else {
            errors = await res.errors
            if (!target?.reply)
                setWarning(Object.values(errors).filter(a => a != ""))
            else alert(Object.values(errors).filter(a => a != ""))
            errors = []
        }
        setSpinner(false)
        
    }

    const Reply = ({ id }: { id: string }) => {
        return (
            <div id={id} className="">
                <div className="w-full relative bg-white shadow-md rounded-lg group dark:bg-slate-700 p-4">
                    {spinner && <Spinner className="rounded-tl-3xl rounded-br-3xl" />}
                    <form onSubmit={comment} className="w-full">
                        <div className="flex flex-row lg:space-x-4 space-x-2 items-center">
                            <div className="flex flex-col content-center flex-shrink-0 w-8 h-8 bg-opacity-50 rounded-full bg-slate-200 dark:bg-slate-600">
                                <img src={session?.user.photo ? process.env.NEXT_PUBLIC_STATIC_FILES + session.user.photo : "/img/person.jpg"}
                                    className="object-cover w-12 h-12 rounded-full shadow hover:shadow-xl my-1"
                                    alt={session?.user.name} />
                            </div>
                            <div className="mb-2 flex-1">
                                <textarea
                                    className="input mt-2"
                                    name="comment"
                                    placeholder="Add a reply"></textarea>
                                <input type={"hidden"} name="reply" value={id} />
                            </div>
                        </div>
                        <div className="flex flex-row space-x-4 justify-end content-center items-center">
                            <button type="button" className="btn btn-yellow text-xs" onClick={() => setReply(null)}>
                                Close
                            </button>
                            <button type="submit" className="btn btn-primary text-xs">
                                Send Reply
                            </button>

                        </div>
                    </form>
                </div>
            </div>
        )
    }

    const appendReply = async (e: MouseEvent<HTMLButtonElement>) => {
        setReply(e.currentTarget.getAttribute('data-id'));
        await new Promise(resolve => setTimeout(resolve, 300));
        document.getElementsByTagName("textarea")[1].focus()
    }
    return <div className="relative max-w-6xl flex lg:flex-row flex-col lg:space-x-4 space-x-0 items-stretch mb-4 mx-auto">
        <div className="relative hidden lg:block lg:w-1/4 text-gray-700  dark:text-slate-300 ">
            <div className="sticky top-16">
                <div className="bg-white shadow-lg rounded-xl group mb-4 dark:bg-slate-700 p-4">
                    <div className="font-semibold my-2 dark:text-slate-200">{data.title}</div>
                    <div className="text-sm">{data?.description}</div>
                </div>
                <div className="bg-white shadow-lg rounded-xl group mb-4 dark:bg-slate-700 p-4">
                    <div className="font-semibold my-2 dark:text-slate-200">List of Contents (Daftar Isi)</div>
                    <div className="text-sm">
                        <ul className="list-disc ml-4">
                            {ListOfContents(data?.content)}
                        </ul>
                    </div>
                </div>
                <div className="bg-white shadow-lg rounded-xl group mb-4 dark:bg-slate-700  p-4 gap-3 ">
                    <div className="font-semibold mb-2">Ditulis Oleh</div>
                    <div className="flex mb-2 space-x-4">
                        <div className="relative h-10 w-10">
                            <img
                                className="h-full w-full rounded-full object-cover object-center"
                                src={data.created?.by?.photo ? data?.created?.by?.photo : "/img/person.jpg"}
                                alt="Person"
                            />
                            <span className={"absolute right-0 bottom-0 h-2 w-2 rounded-full ring ring-white " + (data.status?.active ? "bg-green-400" : "bg-red-400")}></span>
                        </div>
                        <div className="text-sm">
                            <div className="font-medium text-slate-700 dark:text-slate-200"><Link href={"/author/" + data.created?.by?._id} title={data.created?.by?.name}>{data.created?.by?.name}</Link></div>
                            <div className="text-gray-400 dark:text-slate-300 text-sm">{data.published?.at ? FullShort(data.published?.at) : FullShort(data.created?.at)}</div>
                        </div>
                    </div>
                    <p className="text-sm">{data.created?.by?.biography}</p>
                </div>
                <OtherCategories categories={categories} />

            </div>
        </div>

        <div className="flex-1">
            <div className="relative bg-white shadow-lg w-full rounded-xl dark:bg-slate-700 mb-4">
                <div>
                    <figure className="relative">
                        <div className="relative">
                            <Image
                                className={"w-full object-cover mb-2 rounded-t-xl"}
                                src={process.env.NEXT_PUBLIC_STATIC_FILES + data?.thumbnail}
                                alt={data?.thumbnail_caption}
                                width={800}
                                height={600}
                                priority
                                blurDataURL="data:image/webp;base64,UklGRvwDAABXRUJQVlA4WAoAAAAgAAAAwQAAwQAASUNDUMgBAAAAAAHIAAAAAAQwAABtbnRyUkdCIFhZWiAAAAAAAAAAAAAAAABhY3NwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAA9tYAAQAAAADTLQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAlkZXNjAAAA8AAAACRyWFlaAAABFAAAABRnWFlaAAABKAAAABRiWFlaAAABPAAAABR3dHB0AAABUAAAABRyVFJDAAABZAAAAChnVFJDAAABZAAAAChiVFJDAAABZAAAAChjcHJ0AAABjAAAADxtbHVjAAAAAAAAAAEAAAAMZW5VUwAAAAgAAAAcAHMAUgBHAEJYWVogAAAAAAAAb6IAADj1AAADkFhZWiAAAAAAAABimQAAt4UAABjaWFlaIAAAAAAAACSgAAAPhAAAts9YWVogAAAAAAAA9tYAAQAAAADTLXBhcmEAAAAAAAQAAAACZmYAAPKnAAANWQAAE9AAAApbAAAAAAAAAABtbHVjAAAAAAAAAAEAAAAMZW5VUwAAACAAAAAcAEcAbwBvAGcAbABlACAASQBuAGMALgAgADIAMAAxADZWUDggDgIAALAUAJ0BKsIAwgA+7XCwVK0mJCMjU8mxoB2JZ27gCA2P7UuDUbB9eMfoBcDe/GvCX4kS3JrR3qvu2othCTck3cluZuNnY3y0tE22kAmbB73x82OQSh4UkhKC+QKH47sehZdaeqxb/WWZavQji1p+3sXO5DOEESU4biG50oYJrqHMShSuJaRjQZ3Z/jCZXxB7H73RAq3E3btzzznJLOx2dfJHE12dnLWX5VAzePBuAAD+7RoxlXq9Mh73LRXj2GcwTcJrX1SZbsNzyMQpOHkE3jX3tMWspBdfzLgUnOBdhHc97O9PxD63gCUyTYQQYFSly343ihzwQ6xbwDMDF9ExSjVJd8PrKvyEWv0ymJYO6HNpiInsKib0cGpX5dmeoafkqvmoLNl1MjhztGd2SArxEon17Gced9ixPyRvxp2R7NNY8n2ViYn3X3TD8zXXCeuR+GwCkQDavPgVxhICYYeFiYLmlfYH5tMgBjjTmDR4RLiKwUTU5ku5/JIh+z7w5uue0zBtmcyLTNpt9kv4pt4HNccE9VWI/IpPi7/yxrga84cO7N57Zaicm+vpHrewzE42g0kyaC4NixL+b64U3s20IALku160Tsq+n9/jdQXUq4pvbM1IlixSb76prSwkU0uakKB2UZ5prX7jEPHRlbshuBvLVaPEaoHTuv0z09ZE984+C3IHYU7XWjflaErgAAA="
                                placeholder="blur"
                                sizes="(max-width: 768px) 100vw,
              (max-width: 1200px) 50vw,
              33vw" />
                        </div>
                        <figcaption className="text-slate-500 text-xs lg:text-sm mx-4 dark:text-slate-300">{data?.thumbnail_caption}</figcaption>
                    </figure>
                </div>

                <div className="lg:py-4 py-2 px-6">
                    <script
                        type="application/ld+json"
                        dangerouslySetInnerHTML={{ __html: JSON.stringify(jsonLd) }}
                    />
                    <article className="w-full dark:text-slate-300 my-4">
                        <h1 className="text-2xl font-semibold" >{data.title}</h1>
                        <div className="flex justify-between space-x-2  mb-4">
                            <div className="text-sm text-slate-600 dark:text-slate-300">
                                <span>{data.post?.published?.at ? FullShort(data.post?.published?.at) : FullShort(data.created?.at)}</span>
                                <span className="hidden lg:inline-block ml-2"> by {data.created?.by?.name}</span>
                            </div>
                        </div>
                        <SocialShare url={process.env.NEXT_PUBLIC_SITE_URL + "/read/" + data?.slug} />
                        <div className="lg:hidden bg-white shadow-lg rounded-xl group mb-4 dark:bg-slate-700 p-4">
                            <div className="font-semibold my-2 dark:text-slate-200">List of Contents (Daftar Isi)</div>
                            <div className="text-sm">
                                <ul className="list-disc ml-4">
                                    {ListOfContents(data?.content)}
                                </ul>
                            </div>
                        </div>
                        <div className="mb-4">
                            {ParseHTML(data?.content)}
                        </div>
                    </article>
                    <div className="mb-4 flex flex-wrap">
                        {data.tags.map((tag: string, i: number) => <span key={tag + "-" + i} className="inline-block dark:bg-sky-800 bg-sky-500 py-1 px-2 text-xs mr-0.5 mt-0.5 text-slate-100 rounded-md">{tag}</span>)}
                    </div>
                    <div className="bg-white shadow-lg rounded-xl group mb-4 dark:bg-slate-600 py-2 px-4 gap-3 lg:hidden">
                        <div className="flex mb-2 space-x-4">
                            <div className="relative h-10 w-10">
                                <img
                                    className="h-full w-full rounded-full object-cover object-center"
                                    src={data.created?.by?.photo ? data.created?.by?.photo : "/img/person.jpg"}
                                    alt="Person"
                                    loading="lazy"
                                />
                                <span className={"absolute right-0 bottom-0 h-2 w-2 rounded-full ring ring-white" + (data.status?.active ? "bg-green-400" : "bg-red-400")}></span>
                            </div>
                            <div className="text-sm">
                                <div className="font-medium text-gray-700 dark:text-slate-200"><Link href={"/author/" + data.created?.by?._id} title={data.created?.by?.name}>{data.created?.by?.name}</Link></div>
                                <div className="text-gray-400 text-sm">{data.published?.at ? FullShort(data.published?.at) : FullShort(data.created?.at)}</div>
                            </div>
                        </div>
                        <p className="text-sm">{data.created?.by?.biography}</p>
                    </div>
                </div>
            </div>
            {related.length >= 1 &&
                <div className="text-semibold my-2 text-lg mx-2">Related Post</div>
            }
            <div className="grid grid-cols-2 lg:grid-cols-4 gap-4">
                {related.length >= 1 && related.map((relate: any) => <Link key={data._id} title={relate.title} href={'/read/' + relate.slug} as={'/read/' + relate.slug} className={"block rounded overflow-hidden shadow-lg hover:shadow-xl transition-all ease-in-out cursor-pointer dark:bg-slate-700 bg-white"}>
                    <Image src={process.env.NEXT_PUBLIC_STATIC_FILES + relate.thumbnail} alt={relate.thumbnail_caption} width={300}
                        height={300}
                        priority
                        blurDataURL="data:image/webp;base64,UklGRvwDAABXRUJQVlA4WAoAAAAgAAAAwQAAwQAASUNDUMgBAAAAAAHIAAAAAAQwAABtbnRyUkdCIFhZWiAAAAAAAAAAAAAAAABhY3NwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAA9tYAAQAAAADTLQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAlkZXNjAAAA8AAAACRyWFlaAAABFAAAABRnWFlaAAABKAAAABRiWFlaAAABPAAAABR3dHB0AAABUAAAABRyVFJDAAABZAAAAChnVFJDAAABZAAAAChiVFJDAAABZAAAAChjcHJ0AAABjAAAADxtbHVjAAAAAAAAAAEAAAAMZW5VUwAAAAgAAAAcAHMAUgBHAEJYWVogAAAAAAAAb6IAADj1AAADkFhZWiAAAAAAAABimQAAt4UAABjaWFlaIAAAAAAAACSgAAAPhAAAts9YWVogAAAAAAAA9tYAAQAAAADTLXBhcmEAAAAAAAQAAAACZmYAAPKnAAANWQAAE9AAAApbAAAAAAAAAABtbHVjAAAAAAAAAAEAAAAMZW5VUwAAACAAAAAcAEcAbwBvAGcAbABlACAASQBuAGMALgAgADIAMAAxADZWUDggDgIAALAUAJ0BKsIAwgA+7XCwVK0mJCMjU8mxoB2JZ27gCA2P7UuDUbB9eMfoBcDe/GvCX4kS3JrR3qvu2othCTck3cluZuNnY3y0tE22kAmbB73x82OQSh4UkhKC+QKH47sehZdaeqxb/WWZavQji1p+3sXO5DOEESU4biG50oYJrqHMShSuJaRjQZ3Z/jCZXxB7H73RAq3E3btzzznJLOx2dfJHE12dnLWX5VAzePBuAAD+7RoxlXq9Mh73LRXj2GcwTcJrX1SZbsNzyMQpOHkE3jX3tMWspBdfzLgUnOBdhHc97O9PxD63gCUyTYQQYFSly343ihzwQ6xbwDMDF9ExSjVJd8PrKvyEWv0ymJYO6HNpiInsKib0cGpX5dmeoafkqvmoLNl1MjhztGd2SArxEon17Gced9ixPyRvxp2R7NNY8n2ViYn3X3TD8zXXCeuR+GwCkQDavPgVxhICYYeFiYLmlfYH5tMgBjjTmDR4RLiKwUTU5ku5/JIh+z7w5uue0zBtmcyLTNpt9kv4pt4HNccE9VWI/IpPi7/yxrga84cO7N57Zaicm+vpHrewzE42g0kyaC4NixL+b64U3s20IALku160Tsq+n9/jdQXUq4pvbM1IlixSb76prSwkU0uakKB2UZ5prX7jEPHRlbshuBvLVaPEaoHTuv0z09ZE984+C3IHYU7XWjflaErgAAA="
                        placeholder="blur"
                        sizes="(max-width: 768px) 100vw,
              (max-width: 1200px) 50vw,
              33vw" />
                    <div className="font-semibold text-sm p-2">
                        {Truncate(relate.title, 50)}
                        <span className="block text-xs font-semibold text-gray-600 dark:text-gray-400 my-2">{DayDateFormat(relate.published?.at)}</span>
                    </div>
                </Link>)}
            </div>
            {data?.comment &&
                <div className="w-full relative bg-white shadow-lg rounded-tl-3xl rounded-br-3xl group mb-4 dark:bg-slate-700 p-4">
                    {spinner && <Spinner className="rounded-tl-3xl rounded-br-3xl" />}
                    <h4 className="font-semibold">Comments ({comments?.count})</h4>
                    <Success data={success} hide={() => setSuccess({ title: "", description: "" })} className={"mt-2"} />
                    <form onSubmit={comment} className="w-full">
                        <Warning className="mt-2" errors={warning} hide={() => setWarning(errors)} />
                        <div className="mb-2">
                            <textarea
                                className="input mt-2"
                                name="comment"
                                placeholder="Add a comment"></textarea>
                        </div>
                        <div className="flex flex-row space-x-4 justify-end content-center items-center">
                            <div className="flex flex-col content-center flex-shrink-0 w-8 h-8 bg-opacity-50 rounded-full bg-slate-200 dark:bg-slate-600">
                                <img src={session?.user.photo ? process.env.NEXT_PUBLIC_STATIC_FILES + session.user.photo : "/img/person.jpg"}
                                    className="object-cover w-12 h-12 rounded-full shadow hover:shadow-xl"
                                    alt={session?.user.name} />
                            </div>
                            <div className="mr-2 text-sm">{session?.user.name}</div>
                            <button type="submit" className="btn btn-primary">
                                Comment
                            </button>
                        </div>
                    </form>
                </div>
            }
            {comments?.data.length > 0 &&
                <div className="relative bg-white shadow-lg rounded-tl-3xl rounded-br-3xl group mb-4 dark:bg-slate-700 p-2">
                    {comments?.data?.map((comment: any) => <div key={comment._id}>
                        <div className="flex flex-row space-x-4 w-full mb-2 hover:bg-slate-50 dark:hover:bg-slate-800 rounded-xl p-2">
                            <div className="flex flex-col content-center flex-shrink-0 w-8 h-8 bg-opacity-50 rounded-full bg-slate-200 dark:bg-slate-600">
                                <img src={comment?.created?.by?.photo ? process.env.NEXT_PUBLIC_STATIC_FILES + comment?.created?.by?.photo : "/img/person.jpg"}
                                    className="object-cover w-12 h-12 rounded-full shadow hover:shadow-xl my-1"
                                    alt={comment?.created?.by?.name} />
                            </div>
                            <div className="flex flex-col mb-4 w-full">
                                <div className="mb-1 flex items-center">
                                    <div className="flex-1">
                                        <h5 className="mr-2 text-sm font-semibold">{comment.created.by.name}</h5>
                                        <h6 className="text-slate-500 text-xs">{FullShort(comment.created.at)}</h6>
                                    </div>
                                    {session?.user?.id &&
                                        <button type="button" className="btn btn-primary text-xs py-1 px-2" data-id={comment._id} onClick={(e) => appendReply(e)}>REPLY</button>
                                    }
                                </div>
                                <div className="mb-2 font-normal text-base">{comment?.comment.split("\n").map((line: any) => <Fragment key={Math.random()}>{line}<br /></Fragment>)}</div>

                                {replies?.map((reply: any) => {
                                    if (reply.reply._id == comment._id)
                                        return <div className="flex flex-row space-x-4 w-full hover:bg-slate-50 dark:hover:bg-slate-800 p-2">
                                            <div className="flex flex-col content-center flex-shrink-0 w-8 h-8 bg-opacity-50 rounded-full bg-slate-200 dark:bg-slate-600">
                                                <img src={reply?.created?.by?.photo ? process.env.NEXT_PUBLIC_STATIC_FILES + reply?.created?.by?.photo : "/img/person.jpg"}
                                                    className="object-cover w-12 h-12 rounded-full shadow hover:shadow-xl my-1"
                                                    alt={reply?.created?.by?.name} />
                                            </div>
                                            <div className="flex flex-col mb-2 w-full">
                                                <div className="mb-1 flex items-center">
                                                    <div className="flex-1">
                                                        <h5 className="mr-2 text-sm font-semibold">{reply.created.by.name}</h5>
                                                        <h6 className="text-slate-500 text-xs">{FullShort(reply.created.at)}</h6>
                                                    </div>
                                                </div>
                                                <div className="font-normal text-base">{reply?.comment.split("\n").map((line: any) => <Fragment key={Math.random()}>{line}<br /></Fragment>)}</div>
                                            </div>
                                        </div>
                                })}

                                {reply == comment._id && <Reply id={comment._id} />}
                            </div>

                        </div>
                    </div>
                    )}
                </div>
            }
        </div>
    </div>
}

