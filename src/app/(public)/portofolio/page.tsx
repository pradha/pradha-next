'use client'
import { OtherPages, Pages } from "@/components/ui/layouts/pages";
import { BriefcaseIcon, CheckIcon, EyeSlashIcon, HomeIcon, PhoneIcon, QuestionMarkCircleIcon } from "@heroicons/react/24/outline";
import Link from "next/link";
import { usePathname } from "next/navigation";

export default function PortofolioPage(props: any) {
    const pathName = usePathname()
    return <div className="relative max-w-6xl flex lg:flex-row flex-col lg:space-x-4 space-x-0 items-stretch mb-4 mx-auto">
        <div className="relative hidden lg:block lg:w-1/3 text-gray-700  dark:text-slate-300 ">
            <div className="sticky top-16">
                <Pages />
                <OtherPages />
            </div>
        </div>
        <div className="relative bg-white shadow-lg w-full lg:rounded-xl rounded-none lg:py-4 py-2 px-6 dark:bg-slate-700">
            <h1 className="text-xl font semibold">Portofolio</h1>
        </div>
    </div>
}