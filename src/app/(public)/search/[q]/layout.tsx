import PublicLayout from "@/components/ui/layouts/public"
import clientPromise from "@/lib/connections/mongo";
import { Truncate } from "@/lib/text";
import { ISO8601DateTime } from "@/lib/time";

export async function generateMetadata(props: any) {
    const db = (await clientPromise).db()
    
    const title = "Search Result: "+decodeURIComponent(props.params.q).split("+").join(" ")
    const description = "Search result for: "+decodeURIComponent(props.params.q).split("+").join(" ")
    const keywords = decodeURIComponent(props.params.q).split("+")
    const type = "article"
    const url = process.env.SITE_URL + '/search/' + props.params.q
    return {
        title: title,
        description: description,
        keywords: keywords,
        openGraph: {
            type: type,
            title: title,
            description: description,
            url: url,
            publishedTime: ISO8601DateTime(new Date().toString()),
        },
        alternates: {
            canonical: url,
            languages: {},
        },
        twitter: {
            card: 'summary_large_image',
            title: title,
            description: description,
        },
        robots: {
            index: true,
            follow: true,
            nocache: false,
            googleBot: {
                index: true,
                follow: true
            },
        },
    }
}

export default async function RootLayout(props: any) {
    const db = (await clientPromise).db()
    
    let search = []
    for (var src of decodeURIComponent(props.params.q).split("+")){
        //console.log(src)
        search.push({tags: new RegExp(src, "i")})
        search.push({title: new RegExp(src, "i")})
        search.push({description: new RegExp(src, "i")})
    }
    const post = await db.collection("v_content_posts").find({ $or: search, status: "Published" }).toArray()
    let categories = JSON.parse(JSON.stringify(await db.collection("v_content_categories").find().toArray()))

    props.params.data = JSON.parse(JSON.stringify(post))
    props.params.categories = categories
    props.params.search = decodeURIComponent(props.params.q).split("+").join(" ")
    return (
        <PublicLayout>{props.children}</PublicLayout>
    )
}


