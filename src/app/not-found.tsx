export default function NotFound() {
    return (
        <section className='grid content-center min-h-screen bg-slate-300 dark:bg-slate-600'>
            <div className="container mx-auto transition ease-in-out delay-150">
                <div className='flex justify-center px-3 my-6 lg:px-6 lg:my-12'>
                    <div className='w-full p-6 text-center bg-white shadow-lg xl:w-1/4 lg:w-1/2 rounded-xl dark:bg-slate-800 dark:text-slate-300'>
                        <h1 className="text-2xl font-semibold">404</h1>
                        <h2>Not Found</h2>
                    </div>
                </div>
            </div>
        </section>
    );
}