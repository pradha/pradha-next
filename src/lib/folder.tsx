import path from "path";
import fs from "fs";
import util from "util"

export const createFolder = async (folderPath: string) => {
    const directoryPath = path.resolve(folderPath);
    const mkdir = util.promisify(fs.mkdirSync);
    fs.readdir(directoryPath, async function (err, files) {
        if (err) {
            return new Promise(res => {
                mkdir(directoryPath, { recursive: true })
            })
        }
        if (files)
            files.forEach(function (file) {
                //console.log(file);
            });
    })
}