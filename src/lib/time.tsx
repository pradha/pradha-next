const DaysID = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
const Days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
const DaysShort = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
const Months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
const MonthsShort = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]

export const FullDayDate = (time:string) => {
    const date = new Date(time)
    return `${Days[date.getDay()]}, ${date.getDate()} ${Months[date.getMonth()]} ${date.getFullYear()}`
} // Thursday, 23 February 2023

export const FullDateTime = (date: string) => {
    let data = new Date(date)
    return Days[data.getDay()]+", "+data.getDate()+" "+(Months[data.getMonth()])+" "+data.getFullYear()+" "+data.getHours().toString().padStart(2, '0')+":"+data.getMinutes().toString().padStart(2, '0')+":"+data.getSeconds().toString().padStart(2, '0')
} // Thursday, 10 April 2022 09:31:07

export const DateTime = (date: string) => {
    let data = new Date(date)
    return data.getDate()+" "+(Months[data.getMonth()])+" "+data.getFullYear()+" "+data.getHours().toString().padStart(2, '0')+":"+data.getMinutes().toString().padStart(2, '0')+":"+data.getSeconds().toString().padStart(2, '0')
} // 10 April 2022 09:31:07

export const DateOnly= (date: string) => {
    let data = new Date(date)
    return data.getDate()+" "+(Months[data.getMonth()])+" "+data.getFullYear()+" "+data.getHours().toString().padStart(2, '0')
} // 

export const DateTimeShort = (date: string) => {
    let data = new Date(date)
    return data.getDate()+" "+MonthsShort[data.getMonth()]+" "+data.getFullYear()+" "+data.getHours().toString().padStart(2, '0')+":"+data.getMinutes().toString().padStart(2, '0')+":"+data.getSeconds().toString().padStart(2, '0')
} // 10 Apr 2022 09:31:07

export const FullShortDay = (date: string) => {
    let data = new Date(date)
    return DaysShort[data.getDay()]+", "+data.getDate()+" "+Months[data.getMonth()]+" "+data.getFullYear()+" "+data.getHours().toString().padStart(2, '0')+":"+data.getMinutes().toString().padStart(2, '0')+":"+data.getSeconds().toString().padStart(2, '0')
} // Thu, 10 April 2022 09:31:07

export const FullShortMonth = (date: string) => {
    let data = new Date(date)
    return Days[data.getDay()]+", "+data.getDate()+" "+MonthsShort[data.getMonth()]+" "+data.getFullYear()+" "+data.getHours().toString().padStart(2, '0')+":"+data.getMinutes().toString().padStart(2, '0')+":"+data.getSeconds().toString().padStart(2, '0')
} // Thursday, 10 Apr 2022 09:31:07

export const FullShort = (date: string) => {
    let data = new Date(date)
    return DaysShort[data.getDay()]+", "+data.getDate()+" "+MonthsShort[data.getMonth()]+" "+data.getFullYear()+" "+data.getHours().toString().padStart(2, '0')+":"+data.getMinutes().toString().padStart(2, '0')+":"+data.getSeconds().toString().padStart(2, '0')
} // Thu, 10 Apr 2022 09:31:07

export const DayName = (date: string) => {
    let data = new Date(date)
    return Days[data.getDay()]
} // Thu
export const DayNameShort = (date: string) => {
    let data = new Date(date)
    return DaysShort[data.getDay()]
} // Thu

export const MonthYearShort = (date: string) => {
    let data = new Date(date)
    return MonthsShort[data.getMonth()]+" "+data.getFullYear()
} // Thu

export const ISODate = (date: string) => {
    let data = new Date(date)
    return data.getFullYear()+"-"+(data.getMonth()+1).toString().padStart(2,"0")+"-"+data.getDate().toString().padStart(2,"0")
} // 2022-03-10

export const ISODateTime = (date: string) => {
    let data = new Date(date)
    return data.getFullYear()+"-"+(data.getMonth()).toString().padStart(2,"0")+"-"+data.getDate().toString().padStart(2,"0")+" "+data.getHours().toString().padStart(2,"0")+":"+data.getMinutes().toString().padStart(2,"0")+":"+data.getSeconds().toString().padStart(2,"0")
} // 2022-03-10 04:02:00

export const ISO8601DateTime = (date: string) => {
    let data = new Date(date)
    return data.getFullYear()+"-"+(data.getMonth()).toString().padStart(2,"0")+"-"+data.getDate().toString().padStart(2,"0")+"T"+data.getHours().toString().padStart(2,"0")+":"+data.getMinutes().toString().padStart(2,"0")+":"+data.getSeconds().toString().padStart(2,"0")+"+07:00"
} // 2022-03-10T04:02:00+07:00

export const DateFormat = (date: string) => {
    let data = new Date(date)
    return data.getDate().toString().padStart(2,"0")+" "+(data.getMonth()).toString().padStart(2,"0")+"-"+data.getFullYear()
}
export const DayDateFormat = (date: string) => {
    let data = new Date(date)
    return DaysShort[data.getDay()]+", "+data.getDate().toString().padStart(2,"0")+" "+MonthsShort[data.getMonth()]+" "+data.getFullYear()
} // Thu, 10 Apr 2022

export const DayDateFormatID = (date: string) => {
    let data = new Date(date)
    return DaysID[data.getDay()]+", "+data.getDate().toString().padStart(2,"0")+" "+MonthsShort[data.getMonth()]+" "+data.getFullYear()
} // Thu, 10 Apr 2022