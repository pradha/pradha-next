export const Nl2br = (text: string) => {
    var breakTag = '<br ' + '/>'
    return (text + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}

export const Slug = (text: string) => {
    return text.toString().toLowerCase()
        .replace(/^-+/, '')
        .replace(/-+$/, '')
        .replace(/\s+/g, '-')
        .replace(/\-\-+/g, '-')
        .replace(/[^\w\-]+/g, '');
}

export const Truncate = (text: string, length: number) => {
    if (text?.length > length) {
        return text.slice(0, length) + "...";
    } else {
        return text;
    }
}

export const FormatNumber = (n: string) => {
    // format number 1000000 to 1,234,567
    return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export const FormatMoney = (amount: string | number) => {
    if (amount.toString().indexOf(".") >= 0) {
        var decimal_pos = amount.toString().indexOf(".");
        var left_side = FormatNumber(amount.toString().substring(0, decimal_pos));
        var right_side = amount.toString().substring(decimal_pos).replace(/\D/g, "");
        right_side = parseFloat("." + right_side).toFixed(2).substring(1, 5);

        return left_side + right_side;
    } else {
        return FormatNumber(amount.toString()) + ".00"
    }
}
export const FormatCurrency = (input: HTMLInputElement, currency: string, blur: string) => {
    // appends $ to value, validates decimal side
    // and puts cursor back in right position.
    // get input value
    var input_val = input.value;
    // don't validate empty input
    if (input_val === "") {
        return;
    }

    // original length
    var original_len = input_val.length;

    // initial caret position
    var caret_pos = input.selectionStart;

    // check for decimal
    if (input_val.indexOf(".") >= 0) {
        // get position of first decimal
        // this prevents multiple decimals from
        // being entered
        var decimal_pos = input_val.indexOf(".");

        // split number by decimal point
        var left_side = input_val.substring(0, decimal_pos);
        var right_side = input_val.substring(decimal_pos);

        // add commas to left side of number
        left_side = FormatNumber(left_side);

        // validate right side
        right_side = FormatNumber(right_side);

        // On blur make sure 2 numbers after decimal
        if (blur === "blur") {
            right_side += "00";
        }

        // Limit decimal to only 2 digits
        right_side = right_side.substring(0, 2);

        // join number by .
        input_val = currency + left_side + "." + right_side;
    } else {
        // no decimal entered
        // add commas to number
        // remove all non-digits
        input_val = FormatNumber(input_val);
        input_val = currency + input_val;

        // final formatting
        if (blur === "blur") {
            input_val += ".00";
        }
    }

    // send updated string to input
    input.value = input_val;

    // put caret back in the right position
    var updated_len = input_val.length;
    caret_pos = updated_len - original_len + (caret_pos ? caret_pos : 0);
    input.setSelectionRange(caret_pos, caret_pos);
}

export const CheckDigit = (number: string) => {
    "use strict"
    /* Check that input string conveys number of digits that correspond to a given GS1 key */
    if (/(^\d{7}$)|(^\d{11}$)|(^\d{12}$)|(^\d{13}$)|(^\d{16}$)|(^\d{17}$)/.test(number) === false) {
        return null;
    } else {
        /* Reverse string */
        number = [...number.toString()].reverse().join('');
        /* Alternatively fetch digits, multiply them by 3 or 1, and sum them up */
        let sum = 0;
        for (let i = number.length - 1; i >= 0; i--) {
            if (parseInt(number[i]) === 0) {
                continue;
            } else {
                if (i % 2 !== 0) {
                    sum += parseInt(number[i]) * 1;
                }
                else {
                    sum += parseInt(number[i]) * 3;
                }
            }
        }
        /* Subtract sum from nearest equal or higher multiple of ten */
        let checkDigit = Math.ceil(sum / 10) * 10 - sum;
        return checkDigit;
    }
}

export const ArabicNumber = (num: number | string) => {
    var number =
        ["\u0660", "\u0661", "\u0662", "\u0663", "\u0664", "\u0665", "\u0666", "\u0667", "\u0668", "\u0669"];

    var strNum = ""
    for (let i = 0; i < num.toString().length; i++) {
        strNum += number[parseInt(num.toString().charAt(i))];
    }
    return strNum;

}