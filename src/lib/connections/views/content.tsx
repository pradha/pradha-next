import clientPromise from "@/lib/connections/mongo";

export const VContentCategories = async () => {
    let db = (await clientPromise).db()
    if (!await db.listCollections({ name: 'v_content_categories' }).hasNext()){
        await db.createCollection("v_content_categories", { "viewOn": "content_categories", "pipeline": [{"$lookup":{"from":"content_categories","localField":"parent","foreignField":"_id","as":"parent"}},{"$lookup":{"from":"system_users","localField":"created.by","foreignField":"_id","as":"created.by"}},{"$lookup":{"foreignField":"_id","as":"updated.by","from":"system_users","localField":"updated.by"}},{"$set":{"parent":{"$arrayElemAt":["$parent",0]},"created.by":{"$arrayElemAt":["$created.by",0]},"updated.by":{"$arrayElemAt":["$updated.by",0]}}}] })
    }
}

export const VContentPosts = async () => {
    let db = (await clientPromise).db()
    if (!await db.listCollections({ name: 'v_content_posts' }).hasNext()){
        await db.createCollection("v_content_posts", { "viewOn": "content_posts", "pipeline": [{"$lookup":{"localField":"categories","foreignField":"_id","as":"categories","from":"content_categories"}},{"$lookup":{"from":"system_users","localField":"created.by","foreignField":"_id","as":"created.by"}},{"$lookup":{"from":"system_users","localField":"updated.by","foreignField":"_id","as":"updated.by"}},{"$set":{"created.by":{"$arrayElemAt":["$created.by",0]},"updated.by":{"$arrayElemAt":["$updated.by",0]}}}] })
    }
}

export const VContentMedia = async () => {
    let db = (await clientPromise).db()
    if (!await db.listCollections({ name: 'v_content_media' }).hasNext()){
        await db.createCollection("v_content_media", { "viewOn": "content_media", "pipeline": [{"$lookup":{"from":"system_users","localField":"uploaded.by","foreignField":"_id","as":"uploaded.by"}},{"$lookup":{"as":"updated.by","from":"system_users","localField":"updated.by","foreignField":"_id"}},{"$set":{"uploaded.by":{"$arrayElemAt":["$uploaded.by",0]},"updated.by":{"$arrayElemAt":["$updated.by",0]}}}] })
    }
}

export const VContentComments = async () => {
    let db = (await clientPromise).db()
    if (!await db.listCollections({ name: 'v_content_comments' }).hasNext()){
        await db.createCollection("v_content_comments", { "viewOn": "content_comments", "pipeline": [{"$lookup":{"from":"content_comments","localField":"reply","foreignField":"_id","as":"reply"}},{"$lookup":{"from":"system_users","localField":"created.by","foreignField":"_id","as":"created.by"}},{"$lookup":{"foreignField":"_id","as":"updated.by","from":"system_users","localField":"updated.by"}},{"$set":{"reply":{"$arrayElemAt":["$reply",0]},"created.by":{"$arrayElemAt":["$created.by",0]},"updated.by":{"$arrayElemAt":["$updated.by",0]}}}] })
    }
}

