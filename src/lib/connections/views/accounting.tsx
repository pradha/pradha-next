import clientPromise from "@/lib/connections/mongo";

export const VAccountingCoa = async () => {
    let db = (await clientPromise).db()
    if (!await db.listCollections({ name: 'v_accounting_coa' }).hasNext()){
        await db.createCollection("v_accounting_coa", { "viewOn": "accounting_coa", "pipeline": [{"$lookup":{"from":"accounting_coa","localField":"parent","foreignField":"_id","as":"parent"}},{"$lookup":{"from":"system_users","localField":"created.by","foreignField":"_id","as":"created.by"}},{"$lookup":{"from":"system_users","localField":"updated.by","foreignField":"_id","as":"updated.by"}},{"$set":{"updated.by":{"$arrayElemAt":["$updated.by",0]},"parent":{"$arrayElemAt":["$parent",0]},"created.by":{"$arrayElemAt":["$created.by",0]}}}] })
    }
}

export const VAccountingAccounts = async () => {
    let db = (await clientPromise).db()
    if (!await db.listCollections({ name: 'v_accounting_accounts' }).hasNext()){
        await db.createCollection("v_accounting_accounts", { "viewOn": "accounting_accounts", "pipeline": [{"$lookup":{"from":"accounting_coa","localField":"coa","foreignField":"_id","as":"coa"}},{"$set":{"coa":{"$arrayElemAt":["$coa",0]}}},{"$addFields":{"number_str":{"$toString":{"$toLong":"$number"}}}},{"$lookup":{"from":"accounting_coa","localField":"coa.parent","foreignField":"_id","as":"coa.parent"}},{"$lookup":{"from":"system_users","localField":"created.by","foreignField":"_id","as":"created.by"}},{"$lookup":{"from":"system_users","localField":"updated.by","foreignField":"_id","as":"updated.by"}},{"$lookup":{"from":"system_users","localField":"confirmed.by","foreignField":"_id","as":"confirmed.by"}},{"$lookup":{"from":"system_users","localField":"closed.by","foreignField":"_id","as":"closed.by"}},{"$set":{"updated.by":{"$arrayElemAt":["$updated.by",0]},"confirmed.by":{"$arrayElemAt":["$confirmed.by",0]},"closed.by":{"$arrayElemAt":["$closed.by",0]},"coa.parent":{"$arrayElemAt":["$coa.parent",0]},"created.by":{"$arrayElemAt":["$created.by",0]}}}] })
    }
}

export const VAccountingTransactions = async () => {
    let db = (await clientPromise).db()
    if (!await db.listCollections({ name: 'v_accounting_transactions' }).hasNext()){
        await db.createCollection("v_accounting_transactions", { "viewOn": "accounting_transactions", "pipeline": [{"$unwind":"$journals"},{"$lookup":{"foreignField":"_id","as":"journals.account","from":"accounting_accounts","localField":"journals.account"}},{"$set":{"journals.account":{"$arrayElemAt":["$journals.account",0]}}},{"$lookup":{"from":"accounting_coa","localField":"journals.account.coa","foreignField":"_id","as":"journals.account.coa"}},{"$set":{"journals.account.coa":{"$arrayElemAt":["$journals.account.coa",0]}}},{"$lookup":{"as":"journals.account.coa.parent","from":"accounting_coa","localField":"journals.account.coa.parent","foreignField":"_id"}},{"$set":{"journals.account.coa.parent":{"$arrayElemAt":["$journals.account.coa.parent",0]}}},{"$group":{"root":{"$mergeObjects":"$$ROOT"},"journals":{"$push":"$journals"},"_id":"$_id"}},{"$replaceRoot":{"newRoot":{"$mergeObjects":["$root","$$ROOT"]}}},{"$unset":["root"]},{"$lookup":{"localField":"created.by","foreignField":"_id","as":"created.by","from":"system_users"}},{"$lookup":{"foreignField":"_id","as":"updated.by","from":"system_users","localField":"updated.by"}},{"$lookup":{"from":"system_users","localField":"confirmed.by","foreignField":"_id","as":"confirmed.by"}},{"$lookup":{"localField":"aborted.by","foreignField":"_id","as":"aborted.by","from":"system_users"}},{"$lookup":{"localField":"reversed.by","foreignField":"_id","as":"reversed.by","from":"system_users"}},{"$lookup":{"localField":"reversed.transaction","foreignField":"_id","as":"reversed.transaction","from":"accounting_transactions"}},{"$set":{"created.by":{"$arrayElemAt":["$created.by",0]},"updated.by":{"$arrayElemAt":["$updated.by",0]},"confirmed.by":{"$arrayElemAt":["$confirmed.by",0]},"aborted.by":{"$arrayElemAt":["$aborted.by",0]},"reversed.by":{"$arrayElemAt":["$reversed.by",0]},"reversed.transaction":{"$arrayElemAt":["$reversed.transaction",0]}}}] })
    }
}

export const VAccountingTransactionDetails = async () => {
    let db = (await clientPromise).db()
    if (!await db.listCollections({ name: 'v_accounting_transaction_details' }).hasNext()){
        await db.createCollection("v_accounting_transaction_details", { "viewOn": "v_accounting_transactions", "pipeline": [{"$unwind":"$journals"}] })
    }
}