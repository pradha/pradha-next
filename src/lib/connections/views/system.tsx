import clientPromise from "@/lib/connections/mongo";

export const VSystemGroups = async () => {
    let db = (await clientPromise).db()
    if (!await db.listCollections({ name: 'v_system_groups' }).hasNext()){
        await db.createCollection("v_system_groups", { "viewOn": "system_groups", "pipeline": [{ "$lookup": { "localField": "roles", "foreignField": "_id", "as": "roles", "from": "system_roles" } }, { "$lookup": { "from": "system_users", "localField": "created.by", "foreignField": "_id", "as": "created.by" } }, { "$lookup": { "from": "system_users", "localField": "updated.by", "foreignField": "_id", "as": "updated.by" } }, { "$set": { "created.by": { "$arrayElemAt": ["$created.by", 0] }, "updated.by": { "$arrayElemAt": ["$updated.by", 0] } } }] })
    }
}

export const VSystemRoles = async () => {
    let db = (await clientPromise).db()
    if (!await db.listCollections({ name: 'v_system_roles' }).hasNext()){
        await db.createCollection("v_system_roles", { "viewOn": "system_roles", "pipeline": [{ "$lookup": { "from": "system_roles", "localField": "parent", "foreignField": "_id", "as": "parent" } }] })
    }
}

export const VSystemUsers = async () => {
    let db = (await clientPromise).db()
    if (!await db.listCollections({ name: 'v_system_users' }).hasNext()){
        await db.createCollection("v_system_users", { "viewOn": "system_users", "pipeline": [{ "$lookup": { "from": "system_groups", "localField": "group", "foreignField": "_id", "as": "group" } }, { "$lookup": { "localField": "created.by", "foreignField": "_id", "as": "created.by", "from": "system_users" } }, { "$lookup": { "as": "updated.by", "from": "system_users", "localField": "updated.by", "foreignField": "_id" } }, { "$set": { "created.by": { "$arrayElemAt": ["$created.by", 0] }, "updated.by": { "$arrayElemAt": ["$updated.by", 0] }, "group": { "$arrayElemAt": ["$group", 0] } } }, { "$unwind": { "preserveNullAndEmptyArrays": true, "path": "$products" } }, { "$lookup": { "as": "address.province", "from": "provinces", "localField": "address.province", "foreignField": "_id" } }, { "$unwind": { "path": "$address.province", "preserveNullAndEmptyArrays": true } }, { "$lookup": { "localField": "address.district", "foreignField": "_id", "as": "address.district", "from": "v_districts" } }, { "$unwind": { "path": "$address.district", "preserveNullAndEmptyArrays": true } }, { "$group": { "_id": "$_id", "root": { "$mergeObjects": "$$ROOT" }, "address": { "$push": "$address" } } }, { "$replaceRoot": { "newRoot": { "$mergeObjects": ["$root", "$$ROOT"] } } }, { "$unset": ["root"] }] })
    }
}

export const VSystemBilling = async () => {
    let db = (await clientPromise).db()
    if (!await db.listCollections({ name: 'v_system_billing' }).hasNext()) {
        await db.createCollection("v_system_billing", {
            "viewOn": "system_billing", "pipeline": [{ "$lookup": { "from": "system_users", "localField": "created.by", "foreignField": "_id", "as": "created.by" } }, { "$lookup": { "foreignField": "_id", "as": "updated.by", "from": "system_users", "localField": "updated.by" } }, { "$lookup": { "foreignField": "_id", "as": "confirmed.by", "from": "system_users", "localField": "confirmed.by" } }, { "$set": { "created.by": { "$arrayElemAt": ["$created.by", 0] }, "updated.by": { "$arrayElemAt": ["$updated.by", 0] }, "confirmed.by": { "$arrayElemAt": ["$confirmed.by", 0] } } }]
        })
    }
}