import clientPromise from "@/lib/connections/mongo";



export const VQueueServices = async () => {
    let db = (await clientPromise).db()
    if (!await db.listCollections({ name: 'v_queue_services' }).hasNext()) {
        await db.createCollection("v_queue_services", { "viewOn": "queue_services", "pipeline": [{ "$lookup": { "from": "system_users", "localField": "created.by", "foreignField": "_id", "as": "created.by" } }, { "$lookup": { "foreignField": "_id", "as": "updated.by", "from": "system_users", "localField": "updated.by" } }, { "$set": { "created.by": { "$arrayElemAt": ["$created.by", 0] }, "updated.by": { "$arrayElemAt": ["$updated.by", 0] } } }] })
    }
}

export const VQueueCounters = async () => {
    let db = (await clientPromise).db()
    if (!await db.listCollections({ name: 'v_queue_counters' }).hasNext()) {
        await db.createCollection("v_queue_counters", {
            "viewOn": "queue_counters", "pipeline": [{
                "$lookup": {
                    "localField": "services", "foreignField": "_id", "as": "services", "from": "queue_services", "pipeline": [
                        {
                            "$project": {
                                "_id": 1,
                                "name": 1,
                                "description": 1,
                                "symbol": 1,
                            },
                        },
                    ],
                }
            }, { "$lookup": { "from": "system_users", "localField": "created.by", "foreignField": "_id", "as": "created.by" } }, { "$lookup": { "foreignField": "_id", "as": "updated.by", "from": "system_users", "localField": "updated.by" } }, { "$set": { "created.by": { "$arrayElemAt": ["$created.by", 0] }, "updated.by": { "$arrayElemAt": ["$updated.by", 0] } } }]
        })
    }
}

export const VQueueTickets = async () => {
    let db = (await clientPromise).db()
    if (!await db.listCollections({ name: 'v_queue_tickets' }).hasNext()) {
        await db.createCollection("v_queue_tickets", {
            "viewOn": "queue_tickets", "pipeline": [{
                "$lookup": {
                    "localField": "service", "foreignField": "_id", "as": "service", "from": "queue_services", "pipeline": [
                        {
                            "$project": {
                                "_id": 1,
                                "name": 1,
                                "description": 1,
                                "symbol": 1,
                            },
                        },
                    ],
                }
            }, {
                "$lookup": {
                    "localField": "transfered", "foreignField": "_id", "as": "transfered", "from": "queue_tickets", "pipeline": [
                        {
                            "$project": {
                                "_id": 1,
                                "service": 1,
                                "symbol": 1,
                                "number": 1,
                                "created": 1,
                                "called": 1
                            },
                        },
                    ],
                }
            }, { "$lookup": { "from": "system_users", "localField": "created.by", "foreignField": "_id", "as": "created.by", "pipeline": [
                {
                    "$project": {
                        "_id": 1,
                        "username": 1,
                        "name": 1,
                        "photo": 1,
                        "biography": 1,
                    },
                },
            ] } }, { "$lookup": { "foreignField": "_id", "as": "called.counter", "from": "queue_counters", "localField": "called.counter", "pipeline": [
                {
                    "$project": {
                        "_id": 1,
                        "name": 1,
                        "description": 1,
                        "display": 1,
                        "services": 1,
                        "audio": 1,
                    },
                },
            ] } }, { "$lookup": { "foreignField": "_id", "as": "called.by", "from": "system_users", "localField": "called.by" } }, { "$set": { "service": { "$arrayElemAt": ["$service", 0] }, "transfered": { "$arrayElemAt": ["$transfered", 0] }, "created.by": { "$arrayElemAt": ["$created.by", 0] }, "called.counter": { "$arrayElemAt": ["$called.counter", 0] }, "called.by": { "$arrayElemAt": ["$called.by", 0] } } }]
        })
    }
}

