import clientPromise from "@/lib/connections/mongo";

export const VCompanyRegions = async () => {
    let db = (await clientPromise).db()
    if (!await db.listCollections({ name: 'v_company_regions' }).hasNext()){
        await db.createCollection("v_company_regions", { "viewOn": "company_regions", "pipeline": [{"$lookup":{"from":"company_regions","localField":"parent","foreignField":"_id","as":"parent"}},{"$lookup":{"from":"system_users","localField":"created.by","foreignField":"_id","as":"created.by"}},{"$lookup":{"foreignField":"_id","as":"updated.by","from":"system_users","localField":"updated.by"}},{"$set":{"parent":{"$arrayElemAt":["$parent",0]},"created.by":{"$arrayElemAt":["$created.by",0]},"updated.by":{"$arrayElemAt":["$updated.by",0]}}}] })
    }
}

export const VCompanyWarehouses = async () => {
    let db = (await clientPromise).db()
    if (!await db.listCollections({ name: 'v_company_warehouses' }).hasNext()){
        await db.createCollection("v_company_warehouses", { "viewOn": "company_warehouses", "pipeline": [{"$lookup":{"from":"system_users","localField":"created.by","foreignField":"_id","as":"created.by"}},{"$lookup":{"foreignField":"_id","as":"updated.by","from":"system_users","localField":"updated.by"}},{"$set":{"created.by":{"$arrayElemAt":["$created.by",0]},"updated.by":{"$arrayElemAt":["$updated.by",0]}}}] })
    }
}

export const VCompanyProducts = async () => {
    let db = (await clientPromise).db()
    if (!await db.listCollections({ name: 'v_company_products' }).hasNext()){
        await db.createCollection("v_company_products", { "viewOn": "company_products", "pipeline": [{"$lookup":{"from":"system_users","localField":"created.by","foreignField":"_id","as":"created.by"}},{"$lookup":{"foreignField":"_id","as":"updated.by","from":"system_users","localField":"updated.by"}},{"$set":{"created.by":{"$arrayElemAt":["$created.by",0]},"updated.by":{"$arrayElemAt":["$updated.by",0]}}}] })
    }
}

