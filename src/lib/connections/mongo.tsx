import { MongoClient } from "mongodb";

if (!process.env.MONGODB_URI) throw new Error("Please add your mongodb URI to your .env file")

const URI = process.env.MONGODB_URI
const options = {}

let client: MongoClient = new MongoClient(URI, options)
let clientPromise: Promise<MongoClient>

if (process.env.NODE_ENV != "production") {
    let globalWithMongoClient = global as typeof globalThis & { _mongoClientPromise: Promise<MongoClient> }
    if (!globalWithMongoClient._mongoClientPromise)
        globalWithMongoClient._mongoClientPromise = client.connect()

    clientPromise = globalWithMongoClient._mongoClientPromise
} else clientPromise = client.connect()

export default clientPromise