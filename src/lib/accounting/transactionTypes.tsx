export const TransactionTypes = [{
    code: "JR",
    name: "Journal Transaction",
    description: "Free Journal Transaction"
}, {
    code: "IN",
    name: "Incoming Transaction",
    description: "Incoming Transaction"
}, {
    code: "TF",
    name: "Transfer Fund Transaction",
    description: "Transfer Fund Transaction"
}, {
    code: "OT",
    name: "Outgoing Transaction",
    description: "Outgoing Transaction"
}, {
    code: "IV",
    name: "Invoice Transaction",
    description: "Invoice Transaction"
}, {
    code: "RV",
    name: "Reverse Transaction",
    description: "Reverse Transaction"
}]