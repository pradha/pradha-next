export const MasterCoA = [{
    code: 1,
    name: "ASSET (AKTIVA)",
    description: "ASSET (AKTIVA) ACCOUNT",
    position: "Db"
}, {
    code: 2,
    name: "LIABLITY (KEWAJIBAN/HUTANG)",
    description: "LIABILITY (KEWAJIBAN/HUTANG) ACCOUNT",
    position: "Cr"
}, {
    code: 3,
    name: "EQUITY (MODAL)",
    description: "EQUITY (MODAL) ACCOUNT",
    position: "Cr"
}, {
    code: 4,
    name: "REVENUE (PENDAPATAN)",
    description: "REVENUE (PENDAPATAN) ACCOUNT",
    position: "Cr"
}, {
    code: 5,
    name: "HARGA POKOK PENJUALAN (HPP)",
    description: "HARGA POKOK PENJUALAN (HPP) COGS ACCOUNT",
    position: "Db"
}, {
    code: 6,
    name: "EXPENSE (BIAYA/BEBAN)",
    description: "EXPENSE (BIAYA/BEBAN) ACCOUNT",
    position: "Db"
}, {
    code: 7,
    name: "PENDAPATAN LAIN-LAIN",
    description: "PENDAPATAN LAIN-LAIN (OTHER INCOME ACCOUNT)",
    position: "Cr"
}]