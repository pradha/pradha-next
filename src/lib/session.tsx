import { authOptions } from "@/lib/auth/options";
import { getServerSession } from "next-auth";

export const Session = async() => {
    const session = await getServerSession(authOptions)
    if (session) {
        return session
    } 
    return null
}