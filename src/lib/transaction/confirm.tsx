import { Decimal128, ObjectId } from "mongodb";
import { getServerSession } from "next-auth";
import { authOptions } from "@/lib/auth/options";
import clientPromise from "../connections/mongo";

export const Confirm = async (id: string | ObjectId) => {
    const session = await getServerSession(authOptions)
    if (session) {
        const db = (await clientPromise).db()
        const tx = await db.collection('v_accounting_transactions').findOne({ _id: new ObjectId(id), status: "Pending" })
        let clearAccount = true
        if (tx) {
            for (var account of tx.journals) {
                if (account.account.status != "Active") {
                    clearAccount = false
                    break
                }
            }
            if (!clearAccount) return { status: false, message: "Some account has not active" }

            let journals: object[] = []
            for (var journal of tx.journals) {
                let final_balance: number = 0.00
                if (journal.account.coa.position == "Db")
                    final_balance = Number(journal.account.balance) + Number(journal.debit) - Number(journal.credit)
                else
                    final_balance = Number(journal.account.balance) + Number(journal.credit) - Number(journal.debit)

                await db.collection('accounting_accounts').updateOne({ _id: new ObjectId(journal.account._id) }, { $set: { balance: new Decimal128(final_balance.toString()) } })
                if (journal?.price && journal.quantity)
                    journals.push({
                        id: journal.id,
                        account: journal.account._id,
                        type: journal.type,
                        previous_balance: new Decimal128(journal.account.balance.toString()),
                        price: new Decimal128(journal.price.toString()),
                        quantity: Number(journal.quantity.toString()),
                        debit: new Decimal128(journal.debit.toString()),
                        credit: new Decimal128(journal.credit.toString()),
                        final_balance: new Decimal128(final_balance.toString())
                    })
                else
                    journals.push({
                        id: journal.id,
                        account: journal.account._id,
                        type: journal.type,
                        previous_balance: new Decimal128(journal.account.balance.toString()),
                        debit: new Decimal128(journal.debit.toString()),
                        credit: new Decimal128(journal.credit.toString()),
                        final_balance: new Decimal128(final_balance.toString())
                    })
            }
            let data = { journals: journals, status: "Confirmed", confirmed: { at: new Date(), by: new ObjectId(session.user.id) } }
            let confirm = await db.collection('accounting_transactions').updateOne({ _id: new ObjectId(tx._id) }, { $set: data })
            return { status: true, message: "OK", confirm }
        } else return { status: false, message: "No Transaction" }
    } else {
        return { status: false, message: "Invalid session" }
    }
}