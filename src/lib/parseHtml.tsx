export const ParseHTML = (content: any) => {
    let html: any[] = [];
    content?.blocks.map((block: any, i: BigInt) => {
        switch (block.type) {
            case "header":
                switch (block.data.level) {
                    case 1: html.push(<h1 key={block.id} className="text-2xl font-semibold"><span id={block.id}>{block.data.text}</span></h1>)
                        break
                    case 2: html.push(<h2 key={block.id} className="text-xl font-semibold"><span id={block.id}>{block.data.text}</span></h2>)
                        break
                    case 3: html.push(<h3 key={block.id} className="text-lg font-semibold"><span id={block.id}>{block.data.text}</span></h3>)
                        break
                    case 4: html.push(<h4 id={block.id} key={block.id} className="text-base font-semibold">{block.data.text}</h4>)
                        break
                    case 5: html.push(<h5 id={block.id} key={block.id} className="text-sm font-semibold">{block.data.text}</h5>)
                        break
                    default: html.push(<h6 id={block.id} key={block.id} className="text-xs font-semibold">{block.data.text}</h6>)
                        break
                }
                break
            case "paragraph": html.push(<p id={block.id} key={block.id} className="my-4" dangerouslySetInnerHTML={{ __html: block.data.text }}></p>)
                break
            case "image": html.push(<figure id={block.id} key={block.id} className="my-2"><img className={"w-full min-h-32 object-cover my-1"} src={block.data.file?.url} alt={block.data.caption} loading="lazy" /><figcaption className="text-slate-500 mx-4">{block.data.caption}</figcaption></figure>)
                break
            case "list": html.push(<ul id={block.id} key={block.id} className="my-4 list-disc ml-6">{block.data.items.map((item: any) => <li key={Math.random()} dangerouslySetInnerHTML={{ __html: item }}></li>)}</ul>)
                break
            default: html.push(<div id={block.id} key={block.id}>{block.data.text}</div>)
                break
        }
    })
    return html
}

export const ListOfContents = (content: any) => {
    let html: any[] = [];
    content?.blocks.map((block: any, i: BigInt) => {
        switch (block.type) {
            case "header":
            case 2: html.push(<li key={block.id}><a href={"#" + block.id} id={block.id}>{block.data.text}</a></li>)
                break
            case 3: html.push(<li key={block.id} className={"ml-2"}><a href={"#" + block.id} id={block.id}>{block.data.text}</a></li>)
                break
            default: break
        }

    })
    return html
}