import * as nodemailer from 'nodemailer'

export const sendMail = async (from: string, to: string, subject: string, text: string, html: string) => {
    let transporter = nodemailer.createTransport({
        debug: true,
        logger: true,
        host: process.env.MAIL_HOST,
        port: process.env.MAIL_PORT,
        secure: process.env.MAIL_SECURE == "true" ? true : false,

        auth: {
            user: process.env.MAIL_USER,
            pass: process.env.MAIL_PASSWORD,
        },
        tls: {
            rejectUnauthorized: false
        }
    } as nodemailer.TransportOptions);

    let send = await transporter.sendMail({
        from: from + "<" + process.env.MAIL_FROM_DEFAULT + ">",
        to: to,
        subject: subject,
        text: text,
        html: html
    });
    return send
}

const htmlTemplate = (title: string, subtitle: string, content: string) => {
    return `<div bgcolor="#334155" style="background-color:#334155;font-size:1em;padding:2em;"><div style="margin:5em auto;max-width:800px;background-color:#e2e8f0;border-radius:10px;padding:20px"><h1 style="margin:0">${title}</h1><h2 style="margin:0;">${subtitle}</h2><div style="margin:10px 0;filter: drop-shadow(0 10px 8px rgb(0 0 0 / 0.04)) drop-shadow(0 4px 3px rgb(0 0 0 / 0.1));padding:10px 20px;border-radius: 10px;">${content}</div><p style="font-size:0.9em;font-style:italic">*) Email ini dikirim secara otomatis oleh sistem, anda tidak perlu membalas email ini</p></div></div>`
}

export const registrationMail = (name:string, activationLink:string) => {
    let text = "Hi... "+name+"\nTerima kasih telah mendaftar di aplikasi "+process.env.NAME+"\n klik tautan dibawah ini untuk mengaktivasi akun anda\n"+activationLink+"\n\nTerima Kasih"
    let html = ""
    if (process.env.NAME)
        html = htmlTemplate(process.env.NAME, "Pendaftaran Akun", "<p>Hi... <strong>"+name+"</strong></p><p>Terima kasih telah mendaftar di aplikasi <strong>"+process.env.NAME+"</strong></p><p>Klik tautan dibawah ini untuk mengaktivasi akun anda</p><blockquote><a href='"+activationLink+"'>"+activationLink+"</a></blockquote><p>Terima Kasih</p>")
    return {text, html}
}

export const resetPasswordMail = (name:string, resetLink:string) => {
    let text = "Hi... "+name+"\nAnda melakukan permintaan untuk mereset password akun anda di aplikasi "+process.env.NAME+"\n klik tautan dibawah ini untuk melakukan reset password akun anda\n"+resetLink+"\n\nTerima Kasih"
    let html = ""
    if (process.env.NAME)
        html = htmlTemplate(process.env.NAME, "Reset Password", "<p>Hi... <strong>"+name+"</strong></p><p>Anda melakukan permintaan untuk mereset password akun anda di aplikasi <strong>"+process.env.NAME+"</strong></p><p>Klik tautan dibawah ini untuk melakukan reset password akun anda</p><blockquote><a href='"+resetLink+"'>"+resetLink+"</a></blockquote><p>Terima Kasih</p>")
    return {text, html}
}

export const passwordChangedMail = (name:string) => {
    let text = "Hi... "+name+"\npassword akun anda di aplikasi "+process.env.NAME+"\n telah berhasil diubah\n\nTerima Kasih"
    let html = ""
    if (process.env.NAME)
        html = htmlTemplate(process.env.NAME, "Password Changed", "<p>Hi... <strong>"+name+"</strong></p><p>password akun anda di aplikasi <strong>"+process.env.NAME+"</strong> telah berhasil diubah</p><p>Terima Kasih</p>")
    return {text, html}
}

export const subscribeMail = (email:string, confirmLink:string) => {
    let text = "Anda mendaftarkan email ("+email+") anda untuk berlangganan update (subscribe) dari aplikasi "+process.env.NAME+"\n klik tautan dibawah ini untuk melakukan konfirmasi email anda\n"+confirmLink+"\n\nTerima Kasih"
    let html = ""
    if (process.env.NAME)
        html = htmlTemplate(process.env.NAME, "Subscribe Confirmation", "<p>Anda mendaftarkan email ("+email+") anda untuk berlangganan update (subscribe) dari aplikasi  <strong>"+process.env.NAME+"</strong></p><p> klik tautan dibawah ini untuk melakukan konfirmasi email anda</p><blockquote><a href='"+confirmLink+"'>"+confirmLink+"</a></blockquote><p>Terima Kasih</p>")
    return {text, html}
}

export const contactMail = (name:string, email:string, phone:string, message:string, url:string) => {
    let text = "Hi.. "+name+" Thank you for contacting us "+process.env.NAME+", we will get back to you in a day or two\nThe following is the message information that you have sent\n\nName : "+name+"\nEmail : "+email+"\nPhone : "+phone+"\nMessage : "+message
    let html = ""
    if (process.env.NAME)
        html = htmlTemplate(process.env.NAME, "Message Received", "<p>Hi.. "+name+" Thank you for contacting us "+process.env.NAME+", we will get back to you in a day or two</p><p> The following is the message information that you have sent</p><blockquote><div>Name : "+name+"</div><div>Email : "+email+"</div><div>Phone : "+phone+"</div><div>Message : "+message+"</div></blockquote><p>Regards</p>")
    return {text, html}
}

export const reportContactMail = (name:string, email:string, phone:string, message:string, url:string) => {
    let text = "Hi.. someone has messaged you from "+process.env.NAME+"\nThe following is the message information sent\n\nName : "+name+"\nEmail : "+email+"\nPhone : "+phone+"\nMessage : "+message
    let html = ""
    if (process.env.NAME)
        html = htmlTemplate(process.env.NAME, "Subscribe Confirmation", "<p>Hi.. someone has messaged you "+process.env.NAME+"</p><p> The following is the message information sent</p><blockquote><div>Name : "+name+"</div><div>Email : "+email+"</div><div>Phone : "+phone+"</div><div>Message : "+message+"</div></blockquote><p>Regards</p>")
    return {text, html}
}