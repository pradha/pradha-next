import { Decimal128, ObjectId } from "mongodb";
import { getServerSession } from "next-auth";
import clientPromise from "../connections/mongo";
import jwt from 'jsonwebtoken'

export const verify = async (token:string) => {
    const db = (await clientPromise).db();
    if (jwt.verify(token, process.env.JWT_KEY as string)){
        return true
    } else return Promise.reject(new Error('invalid token'))
} 