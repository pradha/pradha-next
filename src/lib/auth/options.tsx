import NextAuth from 'next-auth'
import type { NextAuthOptions } from 'next-auth'
import CredentialsProvider from 'next-auth/providers/credentials';
import clientPromise from '@/lib/connections/mongo';
import bcrypt from 'bcrypt'
import { ObjectId } from 'mongodb';

export const authOptions: NextAuthOptions = {
    providers: [
        CredentialsProvider({
            name: 'Credentials',
            credentials: {
                usermail: { label: 'Username or Email', type: 'text' },
                password: { label: 'Password', type: 'password' },
                gRecaptchaToken: { label: 'reCaptcha', type: 'text' },
                remember: { label: 'Remember', type: 'checkbox' }
            },
            authorize: async (credentials, _req) => {
                try {
                    const reCaptcha = await fetch("https://www.google.com/recaptcha/api/siteverify", {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/x-www-form-urlencoded",
                        },
                        body: `secret=${process.env.RECAPTCHA_SECRET_KEY}&response=${credentials!.gRecaptchaToken}`,
                    })
                    const verify = await reCaptcha.json()
                    if (verify.score > 0.5) {
                        const db = (await clientPromise).db();
                        const user = await db.collection('v_system_users').findOne({ 'status.active': true, $or: [{ username: credentials!.usermail }, { email: credentials!.usermail }] });
                        
                        if (user && bcrypt.compareSync(credentials!.password, user.password)) {
                            console.log("user and pssword match", verify, credentials)
                            // add user loggins in logs here
                            return Promise.resolve({ id: user._id.toString(), name: user.name, username: user.username, email: user.email, photo: user?.photo, group: user.group });
                        } else {
                            return Promise.reject(new Error('invalid credentials username / password'))
                        }
                    } else {
                        console.log(verify, credentials)
                        return Promise.reject(new Error('invalid credentials'))
                    }

                } catch (err) {
                    return Promise.reject(new Error('error: ' + err))
                }
            },
        })
    ],
    secret: process.env.NEXTAUTH_SECRET,
    session: {
        maxAge: 60 * 60, // an hour
        updateAge: 60, // an minute
    },
    callbacks: {
        signIn: async ({ user, account, profile, email, credentials }) => {
            try {
                if (user && "id" in user === false)
                    return Promise.reject(new Error("Invalid username or password"))
                else return true
            } catch (err) {
                return Promise.reject(new Error("Error has been occured"))
            }
        },
        redirect: async ({ url, baseUrl }) => {
            return url.startsWith(baseUrl) ? url : baseUrl
        },
        session: async ({ session, token, user }) => {
            if (token) {
                const db = (await clientPromise).db();
                const user = await db.collection('v_system_users').findOne({ _id: new ObjectId(token.id as string) })
                session.user.id = token.id as string
                session.user.username = token.username as string
                session.user.name = user?.name as string
                session.user.email = user?.email as string
                session.user.photo = user?.photo ? user.photo as string : ""
                session.user.group = user?.group as object
            }
            return Promise.resolve(session)
        },
        jwt: async ({ token, user, account, profile, isNewUser }) => {
            if (user) {
                token.id = user.id,
                    token.username = "username" in user ? user.username : null,
                    token.name = user.name,
                    token.email = user.email,
                    token.photo = "photo" in user ? user.photo : null,
                    token.group = "group" in user ? user.group : null
            }
            return Promise.resolve(token)
        }
    }
}
