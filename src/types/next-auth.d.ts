import NextAuth, { DefaultSession } from "next-auth"

declare module "next-auth" {
    /**
     * Returned by `useSession`, `getSession` and received as a prop on the `SessionProvider` React Context
     */
    interface Session extends DefaultSession {
        user: {
            id: string,
            username: string,
            name: name,
            email: string,
            photo: string,
            group: any
        }
    }
}

declare module 'next-auth/client' {
    export * from 'next-auth/client'

    export interface Session extends DefaultSession {
        user: {
            id: string,
            username: string,
            name: name,
            email: string,
            photo: string,
            group: any
        }
    }
}
declare module "next-auth/jwt" {
    /** Returned by the `jwt` callback and `getToken`, when using JWT sessions */
    interface JWT {
        idToken?: string,
        user: {
            id: string,
            username: string,
            name: name,
            email: string,
            photo: string,
            group: any
        }
    }
}