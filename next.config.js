/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: true,
    async headers() {
        return [
            {
                source: '/contact',
                headers: [
                    {
                        key: 'Set-Cookie',
                        value: 'SameSite=None; Secure',
                    }
                ],
            },
            {
                source: '/account/signin',
                headers: [
                    {
                        key: 'Set-Cookie',
                        value: 'SameSite=None; Secure',
                    }
                ],
            },
            {
                source: '/account/signup',
                headers: [
                    {
                        key: 'Set-Cookie',
                        value: 'SameSite=None; Secure',
                    }
                ],
            },
        ]
    },
    images: {
        // limit of 25 deviceSizes values
        deviceSizes: [320, 480, 640, 750, 828, 960, 1080, 1200, 1440, 1920, 2048, 2560, 3840],
        // limit of 25 imageSizes values
        imageSizes: [16, 32, 48, 64, 96, 128, 256, 384],
        // limit of 50 domains values
        domains: ["127.0.0.1", "localhost", "pradha.id", "www.pradha.id", "static.pradha.id"],
        // path prefix for Image Optimization API, useful with `loader`
        path: '/_next/image',
        // loader can be 'default', 'imgix', 'cloudinary', 'akamai', or 'custom'
        loader: 'default',
        // file with `export default function loader({src, width, quality})`
        //loaderFile: '',
        // disable static imports for image files
        disableStaticImages: false,
        // minimumCacheTTL is in seconds, must be integer 0 or more
        minimumCacheTTL: 60,
        // ordered list of acceptable optimized image formats (mime types)
        formats: ['image/webp'],
        // enable dangerous use of SVG images
        dangerouslyAllowSVG: false,
        // set the Content-Security-Policy header
        contentSecurityPolicy: "default-src 'self'; script-src 'none'; sandbox;",
        // limit of 50 objects
        remotePatterns: [],
        // when true, every image will be unoptimized
        unoptimized: false,
    },
}

module.exports = nextConfig
